/* GNOME-DB
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <locale.h>

#include "gnome-db.h"
#include <libgda/libgda.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-window.h>
#include <libgnomeui/gnome-window-icon.h>
#include <bonobo/bonobo-i18n.h>

static GtkWidget *
create_progress_dialog (void)
{
	GtkWidget *dialog;
	GtkWidget *label;

	/* create dialog */
	dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (dialog), _("Loading..."));
	gnome_window_icon_set_from_file (GTK_WINDOW (dialog),
					 GNOME_DB_PIXMAPDIR "/gnome-db.png");

	label = gnome_db_new_label_widget (
		_("The GNOME-DB front-end is now loading. Please wait."));
	gtk_container_add (GTK_CONTAINER (dialog), label);

	/* show dialog */
	gtk_widget_show (dialog);

	return dialog;
}

static void
create_window (gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *dialog;

	/* create the progress dialog */
	dialog = create_progress_dialog ();

	/* program initialization */
	frontend_config_init ();
	frontend_session_init ();

	/* create main window */
	window = frontend_window_new ();

	gtk_widget_destroy (dialog);
}

int
main (int argc, char *argv[])
{
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GNOMEDB_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	/* library initialization */
	gnome_db_init ("gnome-db", VERSION, argc, argv);

	/* run program */
	gnome_db_main_run ((GdaInitFunc) create_window, NULL);

	return 0;
}
