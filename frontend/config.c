/* GNOME-DB
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <bonobo/bonobo-i18n.h>
#include <libgda/gda-config.h>
#include <gnome-db-defs.h>
#include "gnome-db.h"

void
frontend_config_init (void)
{
	GList *list;

	/* generate default configuration if first time */
	if (!gnome_db_config_has_section (GNOME_DB_SHELL_CONFIG_SECTION_LOAD)) {
		gnome_db_config_set_string (GNOME_DB_SHELL_CONFIG_SECTION_LOAD "/"
				       GNOME_DB_ID_DATABASE, GNOME_DB_ID_DATABASE);
		gnome_db_config_set_string (GNOME_DB_SHELL_CONFIG_SECTION_LOAD "/"
				       GNOME_DB_ID_MANAGER, GNOME_DB_ID_MANAGER);
	}

	if (!gnome_db_config_has_key (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT))
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT, TRUE);
	if (!gnome_db_config_has_key (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY))
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY, TRUE);

	/* generate a default data source if there are none */
	if ((list = gnome_db_config_get_data_source_list ()))
		gnome_db_config_free_data_source_list (list);
	else {
		gchar *str;

		str = g_strdup_printf ("FILENAME=%s/gnome-db-test.db",
				       g_get_home_dir ());
		gnome_db_config_save_data_source (_("Default"),
					     "OAFIID:GNOME_Database_Default_Provider",
					     str,
					     _("Default GNOME-DB data source"),
					     NULL, NULL);
		g_free (str);
	}
}
