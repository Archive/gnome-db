/* GNOME-DB
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnome-db.h"
#include <gnome-db-shell.h>
#include <libgnomedb/gnome-db-window.h>

static GList *open_windows = NULL;

/*
 * Callbacks
 */

static void
window_closed (GnomeDbWindow *window, gpointer user_data)
{
	GList *l;

	g_return_if_fail (GNOME_DB_IS_WINDOW (window));
	g_return_if_fail (GNOME_DB_IS_SHELL (user_data));

	for (l = g_list_first (open_windows); l != NULL; l = l->next) {
		GnomeDbShell *shell = GNOME_DB_SHELL (l->data);

		if (GNOME_DB_WINDOW (gnome_db_shell_get_window (shell)) == window) {
			open_windows = g_list_remove (open_windows, shell);
			if (open_windows == NULL) {
				gnome_db_main_quit ();
			}
			break;
		}
	}
}

/*
 * Public functions
 */

GtkWidget *
frontend_window_new (void)
{
	GnomeDbShell *shell;
	GtkWidget *window;

	shell = gnome_db_shell_new ();
	open_windows = g_list_append (open_windows, shell);

	window = gnome_db_shell_get_window (shell);
	g_signal_connect (G_OBJECT (window), "close", G_CALLBACK (window_closed), shell);

	return window;
}
