/* GNOME-DB
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <bonobo/bonobo-i18n.h>
#include <libgda/gda-log.h>
#include <libgnomeui/gnome-client.h>
#include "gnome-db.h"

static GnomeClient *session_client = NULL;

/*
 * Public functions
 */

void
frontend_session_init (void)
{
	session_client = gnome_master_client ();
	if (!GNOME_CLIENT (session_client)) {
		gda_log_error (_("Could not connect to session manager"));
		return;
	}
}
