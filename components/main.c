/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <locale.h>

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-i18n.h>
#include "components-connection.h"
#include "components-database.h"
#include "components-database-config.h"

static void initialize_component_factory (gpointer user_data);

static gint objects_loaded = 0;

int
main (int argc, char *argv[])
{
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GNOMEDB_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	/* initialize application */
	gnome_db_init ("gnome-db-components", VERSION, argc, argv);

	/* run application */
	gnome_db_main_run ((GdaInitFunc) initialize_component_factory, NULL);

	/* free all resources */
	components_connection_close_all ();

	return 0;
}

/*
 * Callbacks
 */

static void
object_destroyed_cb (BonoboObject *object, gpointer user_data)
{
	objects_loaded--;
	if (objects_loaded == 0)
		gnome_db_main_quit ();
}

static BonoboObject *
factory_callback (BonoboGenericFactory *factory, const gchar *id, gpointer closure)
{
	BonoboObject *object = NULL;

	if (!strcmp (id, GNOME_DB_ID_DATABASE))
		object = g_object_new (COMPONENTS_TYPE_DATABASE, NULL);
	else if (!strcmp (id, GNOME_DB_ID_DATABASE_CONFIG))
		object = g_object_new (COMPONENTS_TYPE_DATABASE_CONFIG, NULL);

	if (BONOBO_IS_OBJECT (object)) {
		objects_loaded++;
		g_signal_connect (G_OBJECT (object), "destroy",
				  G_CALLBACK (object_destroyed_cb), NULL);
	}

	return object;
}

/*
 * Private functions
 */

static void
initialize_component_factory (gpointer user_data)
{
	BonoboGenericFactory *component_factory;

	component_factory = bonobo_generic_factory_new (
		"OAFIID:GNOME_Database_UIComponentFactory",
		factory_callback,
		NULL);
	if (!component_factory) {
		gnome_db_main_quit ();
		return;
	}
}
