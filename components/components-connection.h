/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if !defined(__components_connection_h__)
#  define __components_connection_h__

#include <libgda/gda-connection.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

GdaConnection     *components_connection_open (const gchar *name,
					       const gchar *user,
					       const gchar *password,
					       gboolean only_if_exists,
					       gboolean dont_ref);
GdaConnection     *components_connection_open_dialog (GtkWidget *parent,
						      gchar **selected_name);
void               components_connection_close (GdaConnection *cnc);
void               components_connection_close_all (void);

G_END_DECLS

#endif
