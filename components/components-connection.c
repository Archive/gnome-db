/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkstock.h>
#include <libgda/gda-client.h>
#include <libgnomedb/gnome-db-error-dialog.h>
#include <libgnomedb/gnome-db-login.h>
#include <libgnomedb/gnome-db-login-dialog.h>
#include <libgnomedb/gnome-db-util.h>
#include <bonobo/bonobo-i18n.h>

#include "components-connection.h"

static GdaClient *client = NULL;

/*
 * Callbacks
 */

static void
cnc_error_cb (GdaClient *client, GdaConnection *cnc, GList *error_list, gpointer user_data)
{
	GtkWidget *error_dialog;

	g_return_if_fail (GDA_IS_CLIENT (client));
	g_return_if_fail (GDA_IS_CONNECTION (cnc));
	g_return_if_fail (error_list != NULL);

	if (error_list != NULL) {
		error_dialog = gnome_db_error_dialog_new (_("Error viewer"));
		gnome_db_error_dialog_show_errors (GNOME_DB_ERROR_DIALOG (error_dialog),
						   error_list);

		/* FIXME: add to an internal list, to be able to get all the errors occured
		   for this connection at a later time */
	}
}

/*
 * Private functions
 */

static GdaClient *
get_gda_client (void)
{
	if (!client) {
		client = gda_client_new ();
		g_signal_connect (G_OBJECT (client), "error", G_CALLBACK (cnc_error_cb), NULL);
	}

	return client;
}

/*
 * Public functions
 */

GdaConnection *
components_connection_open (const gchar *name,
			    const gchar *user,
			    const gchar *password,
			    gboolean only_if_exists,
			    gboolean dont_ref)
{
	GdaConnection *cnc;

	g_return_val_if_fail (name != NULL, NULL);

	if (only_if_exists) {
		cnc = gda_client_find_connection (get_gda_client (), name, user, password);
		if (GDA_IS_CONNECTION (cnc) && !dont_ref) {
			cnc = gda_client_open_connection (get_gda_client (), name,
							  user, password);
		}
	}
	else
		cnc = gda_client_open_connection (get_gda_client (), name, user, password);

	if (!cnc)
		gnome_db_show_error (_("Could not open connection to %s"), name);

	return cnc;
}

GdaConnection *
components_connection_open_dialog (GtkWidget *parent, gchar **selected_name)
{
	GtkWidget *dialog;
	GdaConnection *cnc = NULL;

	/* create the dialog */
	dialog = gnome_db_login_dialog_new (_("Open Connection"));
	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog),
					      GTK_WINDOW (parent));

	/* run the dialog */
	if (gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (dialog))) {
		const gchar *name;
		const gchar *user;
		const gchar *password;

		name = gnome_db_login_dialog_get_dsn (GNOME_DB_LOGIN_DIALOG (dialog));
		user = gnome_db_login_dialog_get_username (GNOME_DB_LOGIN_DIALOG (dialog));
		password = gnome_db_login_dialog_get_password (GNOME_DB_LOGIN_DIALOG (dialog));

		cnc = components_connection_open (name, user, password, FALSE, FALSE);
		if (GDA_IS_CONNECTION (cnc) && selected_name)
			*selected_name = g_strdup (name);
	}

	gtk_widget_destroy (dialog);

	return cnc;
}

void
components_connection_close (GdaConnection *cnc)
{
	g_return_if_fail (GDA_IS_CONNECTION (cnc));
	gda_connection_close (cnc);
}

void
components_connection_close_all (void)
{
	if (GDA_IS_CLIENT (client))
		gda_client_close_all_connections (client);
}
