/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if !defined(__components_database_config_h__)
#  define __components_database_config_h__

#include <libgnomedb/gnome-db-control.h>
#include <gnome-db-defs.h>

G_BEGIN_DECLS

#define COMPONENTS_TYPE_DATABASE_CONFIG            (components_database_config_get_type ())
#define COMPONENTS_DATABASE_CONFIG(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, COMPONENTS_TYPE_DATABASE_CONFIG, ComponentsDatabaseConfig))
#define COMPONENTS_DATABASE_CONFIG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, COMPONENTS_TYPE_DATABASE_CONFIG, ComponentsDatabaseConfigClass))
#define COMPONENTS_IS_DATABASE_CONFIG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, COMPONENTS_TYPE_DATABASE_CONFIG))
#define COMPONENTS_IS_DATABASE_CONFIG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), COMPONENTS_TYPE_DATABASE_CONFIG))

typedef struct {
	GnomeDbControl control;
} ComponentsDatabaseConfig;

typedef struct {
	GnomeDbControlClass parent_class;
} ComponentsDatabaseConfigClass;

GType                     components_database_config_get_type (void);
ComponentsDatabaseConfig *components_database_config_new (void);

G_END_DECLS

#endif
