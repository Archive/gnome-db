/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if !defined(__components_sql_view_h__)
#  define __components_sql_view_h__

#include <gtk/gtkvbox.h>
#include <libgda/gda-connection.h>

G_BEGIN_DECLS

#define COMPONENTS_TYPE_SQL_VIEW    (components_sql_view_get_type ())
#define COMPONENTS_SQL_VIEW(obj)    (GTK_CHECK_CAST (obj, COMPONENTS_TYPE_SQL_VIEW, ComponentsSqlView))
#define COMPONENTS_IS_SQL_VIEW(obj) (GTK_CHECK_TYPE (obj, COMPONENTS_TYPE_SQL_VIEW))

typedef struct _ComponentsSqlView        ComponentsSqlView;
typedef struct _ComponentsSqlViewClass   ComponentsSqlViewClass;
typedef struct _ComponentsSqlViewPrivate ComponentsSqlViewPrivate;

struct _ComponentsSqlView {
	GtkVBox box;
	ComponentsSqlViewPrivate *priv;
};

struct _ComponentsSqlViewClass {
	GtkVBoxClass parent_class;
};

GType          components_sql_view_get_type (void);

GtkWidget     *components_sql_view_new (GdaConnection *cnc);
GtkWidget     *components_sql_view_new_from (const gchar *name,
					     const gchar *user,
					     const gchar *password);

void           components_sql_view_cut_clipboard (ComponentsSqlView *sql_view);
void           components_sql_view_copy_clipboard (ComponentsSqlView *sql_view);
void           components_sql_view_paste_clipboard (ComponentsSqlView *sql_view);

GdaConnection *components_sql_view_get_connection (ComponentsSqlView *sql_view);
void           components_sql_view_set_connection (ComponentsSqlView *sql_view,
						   GdaConnection *cnc);

void           components_sql_view_open_command (ComponentsSqlView *sql_view);
void           components_sql_view_save_command (ComponentsSqlView *sql_view);
void           components_sql_view_run_current (ComponentsSqlView *sql_view);
gint           components_sql_view_get_row_count (ComponentsSqlView *sql_view);

G_END_DECLS

#endif
