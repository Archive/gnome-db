/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <bonobo/bonobo-i18n.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkstock.h>
#include <libgnomeui/gnome-window-icon.h>
#include <libgnomedb/gnome-db-browser.h>
#include <libgnomedb/gnome-db-connection-properties.h>
#include <libgnomedb/gnome-db-util.h>
#include "components-database.h"
#include "components-database-config.h"
#include "components-connection.h"
#include "components-sql-view.h"

#define PARENT_TYPE GNOME_DB_TYPE_SHELL_COMPONENT

static void components_database_class_init (ComponentsDatabaseClass *klass);
static void components_database_init       (ComponentsDatabase *comp,
					    ComponentsDatabaseClass *klass);
static void components_database_finalize   (GObject *object);

static GnomeDbControl *components_database_get_config_control (GnomeDbShellComponent *comp);

static void verb_DatabaseBeginTrans (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseCommit (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseConnect (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseDisconnect (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseOpenCommand (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseProperties (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseRollback (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseRun (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_DatabaseSaveCommand (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_EditCopy (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_EditCut (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_EditPaste (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_ViewBrowserTab (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_ViewRefresh (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_ViewSqlTab (BonoboUIComponent *uic, void *user_data, const char *path);

static BonoboUIVerb database_verbs[] = {
	BONOBO_UI_VERB ("EditCopy", verb_EditCopy),
        BONOBO_UI_VERB ("EditCut", verb_EditCut),
        BONOBO_UI_VERB ("EditPaste", verb_EditPaste),
        BONOBO_UI_VERB ("DatabaseBeginTrans", verb_DatabaseBeginTrans),
        BONOBO_UI_VERB ("DatabaseCommit", verb_DatabaseCommit),
        BONOBO_UI_VERB ("DatabaseConnect", verb_DatabaseConnect),
        BONOBO_UI_VERB ("DatabaseDisconnect", verb_DatabaseDisconnect),
        BONOBO_UI_VERB ("DatabaseOpenCommand", verb_DatabaseOpenCommand),
        BONOBO_UI_VERB ("DatabaseProperties", verb_DatabaseProperties),
        BONOBO_UI_VERB ("DatabaseRollback", verb_DatabaseRollback),
        BONOBO_UI_VERB ("DatabaseRun", verb_DatabaseRun),
	BONOBO_UI_VERB ("DatabaseSaveCommand", verb_DatabaseSaveCommand),
	BONOBO_UI_VERB ("ViewBrowserTab", verb_ViewBrowserTab),
	BONOBO_UI_VERB ("ViewRefresh", verb_ViewRefresh),
	BONOBO_UI_VERB ("ViewSqlTab", verb_ViewSqlTab),
	BONOBO_UI_VERB_END
};
static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */

static void
sensitize_database_commands (ComponentsDatabase *comp, BonoboUIComponent *uic)
{
	const gchar *sensitive;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	sensitive = comp->current_connection ? "1" : "0";

	bonobo_ui_component_set_prop (uic, "/commands/DatabaseDisconnect",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseOpenCommand",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseSaveCommand",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseProperties",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseRun",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseBeginTrans",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseCommit",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/DatabaseRollback",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/EditCopy",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/EditCut",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/EditPaste",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/ViewBrowserTab",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/ViewRefresh",
				      "sensitive", sensitive, NULL);
	bonobo_ui_component_set_prop (uic, "/commands/ViewSqlTab",
				      "sensitive", sensitive, NULL);
}

/*
 * Callbacks
 */

static void
browser_progress_message_cb (GnomeDbBrowser *browser, const gchar *msg, gpointer user_data)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));
	gnome_db_control_set_status (control, msg);
}

static void
control_activated_cb (GnomeDbControl *control, gpointer user_data)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	sensitize_database_commands (
		COMPONENTS_DATABASE (comp),
		bonobo_control_get_ui_component (BONOBO_CONTROL (control)));
}

/*
 * Bonobo UI verbs
 */

static void
verb_DatabaseBeginTrans (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	if (gda_connection_begin_transaction (comp->current_connection, NULL))
		gnome_db_control_set_status (control, _("Transaction started"));
	else
		gnome_db_control_set_status (control, _("Could not start transaction"));
}

static void
verb_DatabaseCommit (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	if (gda_connection_commit_transaction (comp->current_connection, NULL))
		gnome_db_control_set_status (control, _("Transaction finished successfully"));
	else
		gnome_db_control_set_status (control, _("Could not save transaction"));
}

static void
verb_DatabaseConnect (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GdaConnection *cnc;
	gchar *str;
	gchar *dsn_name;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	cnc = components_connection_open_dialog (
		gtk_widget_get_toplevel (comp->notebook), &dsn_name);
	if (!GDA_IS_CONNECTION (cnc))
		return;

	comp->current_connection = cnc;

	str = g_strdup_printf ("%s@%s", dsn_name, gda_connection_get_username (cnc));
	comp->open_connections = g_list_append (comp->open_connections, cnc);

	gnome_db_browser_set_connection (GNOME_DB_BROWSER (comp->browser), cnc);
	components_sql_view_set_connection (COMPONENTS_SQL_VIEW (comp->sql), cnc);

	/* free memory */
	g_free (str);
	g_free (dsn_name);

	sensitize_database_commands (comp, uic);
}

static void
verb_DatabaseDisconnect (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	g_return_if_fail (comp->current_connection);

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	gnome_db_control_set_status (control, _("Closing connection"));
	components_connection_close (comp->current_connection);
	comp->open_connections = g_list_remove (comp->open_connections,
						comp->current_connection);
	if (!comp->open_connections)
		comp->current_connection = NULL;
	else
		comp->current_connection = GDA_CONNECTION (comp->open_connections->data);

	gnome_db_browser_set_connection (GNOME_DB_BROWSER (comp->browser),
					 comp->current_connection);
	components_sql_view_set_connection (COMPONENTS_SQL_VIEW (comp->sql),
					    comp->current_connection);

	sensitize_database_commands (comp, uic);
	gnome_db_control_set_status (control, NULL);
}

static void
verb_DatabaseOpenCommand (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	components_sql_view_open_command (COMPONENTS_SQL_VIEW (comp->sql));
}

static void
verb_DatabaseProperties (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;
	GtkWidget *dialog;
	GtkWidget *props;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	dialog = gtk_dialog_new_with_buttons (
		_("Connection properties"),
		GTK_WINDOW (gtk_widget_get_toplevel (comp->notebook)), 0,
		GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL,
		NULL);
	gnome_window_icon_set_from_file (GTK_WINDOW (dialog),
					 GNOME_DB_PIXMAPDIR "/gnome-db.png");
	
	props = gnome_db_connection_properties_new (comp->current_connection);
	gtk_widget_show (props);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), props, TRUE, TRUE, 0);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static void
verb_DatabaseRollback (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	if (gda_connection_rollback_transaction (comp->current_connection, NULL))
		gnome_db_control_set_status (control, _("Transaction aborted"));
	else
		gnome_db_control_set_status (control, _("Could not abort transaction"));
}

static void
verb_DatabaseRun (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	gint rows;
	gchar *msg;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	gnome_db_control_set_status (control, _("Running command..."));
	components_sql_view_run_current (COMPONENTS_SQL_VIEW (comp->sql));

	rows = components_sql_view_get_row_count (COMPONENTS_SQL_VIEW (comp->sql));
	if (rows != 1)
		msg = g_strdup_printf (_("%d rows returned"), rows);
	else
		msg = g_strdup (_("1 row returned"));
	gnome_db_control_set_status (control, msg);
	g_free (msg);
}

static void
verb_DatabaseSaveCommand (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	components_sql_view_save_command (COMPONENTS_SQL_VIEW (comp->sql));
}

static void
verb_EditCopy (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	components_sql_view_copy_clipboard (COMPONENTS_SQL_VIEW (comp->sql));
}

static void
verb_EditCut (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	components_sql_view_cut_clipboard (COMPONENTS_SQL_VIEW (comp->sql));
}

static void
verb_EditPaste (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	components_sql_view_paste_clipboard (COMPONENTS_SQL_VIEW (comp->sql));
}

static void
verb_ViewBrowserTab (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	gtk_notebook_set_current_page (
		GTK_NOTEBOOK (comp->notebook),
		gtk_notebook_page_num (GTK_NOTEBOOK (comp->notebook), comp->browser));
}

static void
verb_ViewRefresh (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbControl *control;
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));

	control = gnome_db_shell_component_get_control (GNOME_DB_SHELL_COMPONENT (comp));

	gnome_db_control_set_status (control, _("Requerying database..."));
	gnome_db_browser_refresh (GNOME_DB_BROWSER (comp->browser));
	gnome_db_control_set_status (control, NULL);
}

static void
verb_ViewSqlTab (BonoboUIComponent *uic, void *user_data, const char *path)
{
	ComponentsDatabase *comp = (ComponentsDatabase *) user_data;

	g_return_if_fail (COMPONENTS_IS_DATABASE (comp));
	gtk_notebook_set_current_page (
		GTK_NOTEBOOK (comp->notebook),
		gtk_notebook_page_num (GTK_NOTEBOOK (comp->notebook), comp->sql));
}

/*
 * ComponentsDatabase class implementation
 */

static void
components_database_class_init (ComponentsDatabaseClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GnomeDbShellComponentClass *component_class = GNOME_DB_SHELL_COMPONENT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = components_database_finalize;
	component_class->get_config_control = components_database_get_config_control;
}

static void
components_database_init (ComponentsDatabase *comp, ComponentsDatabaseClass *klass)
{
	GnomeDbShellComponent *shell_comp;
	GnomeDbControl *control;

	comp->current_connection = NULL;
	comp->open_connections = NULL;

	/* create the control's widget */
	comp->notebook = gnome_db_new_notebook_widget ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (comp->notebook), FALSE);
	gtk_notebook_popup_disable (GTK_NOTEBOOK (comp->notebook));

	comp->browser = gnome_db_browser_new ();
	g_signal_connect (G_OBJECT (comp->browser), "progress_message",
			  G_CALLBACK (browser_progress_message_cb), comp);
	gtk_widget_show (comp->browser);
	gtk_notebook_append_page (GTK_NOTEBOOK (comp->notebook),
				  comp->browser,
				  gtk_label_new (_("Browser")));

	comp->sql = components_sql_view_new (NULL);
	gtk_widget_show (comp->sql);
	gtk_notebook_append_page (GTK_NOTEBOOK (comp->notebook),
				  comp->sql,
				  gtk_label_new (_("SQL")));

	/* sets the Bonobo control */
	shell_comp = gnome_db_shell_component_construct (
		GNOME_DB_SHELL_COMPONENT (comp), comp->notebook);
	if (!GNOME_DB_IS_SHELL_COMPONENT (shell_comp)) {
		g_object_unref (G_OBJECT (comp));
		return;
	}

	control = gnome_db_shell_component_get_control (shell_comp);
	g_signal_connect (G_OBJECT (control), "activated",
                          G_CALLBACK (control_activated_cb), shell_comp);
	gnome_db_control_set_ui (control,
				 GNOMEDB_DATADIR,
				 "gnome-db-database.xml",
				 database_verbs,
				 shell_comp);
}

static void
components_database_finalize (GObject *object)
{
	ComponentsDatabase *database = (ComponentsDatabase *) object;

	g_return_if_fail (COMPONENTS_IS_DATABASE (database));

	parent_class->finalize (object);
}

GType
components_database_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ComponentsDatabaseClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) components_database_class_init,
			NULL,
			NULL,
			sizeof (ComponentsDatabase),
			0,
			(GInstanceInitFunc) components_database_init
		};
		type = g_type_register_static (PARENT_TYPE, "ComponentsDatabase", &info, 0);
	}
	return type;
}

GnomeDbShellComponent *
components_database_new (void)
{
	GnomeDbShellComponent *comp;

	comp = g_object_new (COMPONENTS_TYPE_DATABASE, NULL);
	return comp;
}

static GnomeDbControl *
components_database_get_config_control (GnomeDbShellComponent *comp)
{
	ComponentsDatabaseConfig *config;

	g_return_val_if_fail (COMPONENTS_IS_DATABASE (comp), NULL);

	config = components_database_config_new ();
	g_return_val_if_fail (COMPONENTS_IS_DATABASE_CONFIG (config), NULL);

	return GNOME_DB_CONTROL (config);
}
