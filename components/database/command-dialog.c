/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <bonobo/bonobo-i18n.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktable.h>
#include <libgnomedb/gnome-db-gray-bar.h>
#include <libgnomedb/gnome-db-grid.h>
#include <libgnomedb/gnome-db-list.h>
#include <libgnomedb/gnome-db-util.h>
#include "command-dialog.h"

/*
 * Private functions
 */

static GtkWidget *
create_queries_list (void)
{
	GtkWidget *list;

	list = gnome_db_grid_new ();
	gtk_widget_show (list);

	return list;
}

static GtkWidget *
create_sql_command_list (void)
{
	GtkWidget *list;

	list = gnome_db_list_new ();
	gtk_widget_show (list);

	return list;
}

/*
 * Public functions
 */

gchar *
command_dialog_open (void)
{
	GtkWidget *dialog;
	GtkWidget *table;
	GtkWidget *title;
	GtkWidget *list;
	GtkWidget *scroll;
	GtkWidget *text;
	gchar *cmd = NULL;

	/* create the dialog box */
	dialog = gtk_dialog_new ();
	gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (dialog), _("Ok"), GTK_RESPONSE_OK);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	/* create the dialog contents */
	table = gnome_db_new_table_widget (3, 2, FALSE);

	title = gnome_db_gray_bar_new (_("SQL commands"));
	gtk_widget_show (title);
	gtk_table_attach (GTK_TABLE (table), title, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 2, 2);
	list = create_sql_command_list ();
	gtk_table_attach (GTK_TABLE (table), list, 0, 1, 1, 2,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 2, 2);

	title = gnome_db_gray_bar_new (_("Saved queries"));
	gtk_widget_show (title);
	gtk_table_attach (GTK_TABLE (table), title, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 2, 2);
	list = create_queries_list ();
	gtk_table_attach (GTK_TABLE (table), list, 1, 2, 1, 2,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 2, 2);

	scroll = gnome_db_new_scrolled_window_widget ();
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	text = gnome_db_new_text_widget (NULL);
	gtk_container_add (GTK_CONTAINER (scroll), text);
	gtk_table_attach (GTK_TABLE (table), scroll, 0, 2, 2, 3,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 2, 2);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), table, TRUE, TRUE, 0);

	/* run the dialog */
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
	}

	gtk_widget_destroy (dialog);

	return cmd;
}
