/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib/gstrfuncs.h>
#include <bonobo/bonobo-i18n.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkvpaned.h>
#include <libgnomedb/gnome-db-grid.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-sql-editor.h>
#include <string.h>
#include "command-dialog.h"
#include "components-connection.h"
#include "components-sql-view.h"

struct _ComponentsSqlViewPrivate {
	/* the connections we're using */
	GdaConnection *cnc;

	GList *current_recsets;

	/* internal widgets */
	GtkWidget *paned;
	GtkWidget *text;
	GtkWidget *grid;
};

static void components_sql_view_class_init (ComponentsSqlViewClass *klass);
static void components_sql_view_init       (ComponentsSqlView *sql_view,
					    ComponentsSqlViewClass *klass);
static void components_sql_view_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * Callbacks
 */

/*
 * ComponentsSqlView class implementation
 */

static void
components_sql_view_class_init (ComponentsSqlViewClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = components_sql_view_finalize;
}

static void
components_sql_view_init (ComponentsSqlView *sql_view, ComponentsSqlViewClass *klass)
{
	GtkWidget *frame;

	/* allocate our private structure */
	sql_view->priv = g_new0 (ComponentsSqlViewPrivate, 1);
	sql_view->priv->cnc = NULL;

	sql_view->priv->paned = gnome_db_new_vpaned_widget ();
	gtk_widget_show (sql_view->priv->paned);
	gtk_box_pack_start (GTK_BOX (sql_view), sql_view->priv->paned, 1, 1, 0);

	/* create the SQL buffer */
	frame = gnome_db_new_frame_widget (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	sql_view->priv->text = gnome_db_sql_editor_new ();
	gtk_widget_show (sql_view->priv->text);
	gtk_container_add (GTK_CONTAINER (frame), sql_view->priv->text);
	gtk_paned_add1 (GTK_PANED (sql_view->priv->paned), frame);

	/* create the grid widget */
	sql_view->priv->grid = gnome_db_grid_new ();
	gtk_widget_show (sql_view->priv->grid);

	gtk_paned_add2 (GTK_PANED (sql_view->priv->paned), sql_view->priv->grid);

	//gtk_widget_show_all (sql_view->priv->paned);

	/* initialize private data */
	sql_view->priv->current_recsets = NULL;
}

static void
components_sql_view_finalize (GObject *object)
{
	ComponentsSqlView *sql_view = (ComponentsSqlView *) object;

	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));

	/* free memory */
	g_list_foreach (sql_view->priv->current_recsets, (GFunc) g_object_unref, NULL);
	g_list_free (sql_view->priv->current_recsets);
	sql_view->priv->current_recsets = NULL;

	if (GDA_IS_CONNECTION (sql_view->priv->cnc)) {
		components_connection_close (sql_view->priv->cnc);
		sql_view->priv->cnc = NULL;
	}

	g_free (sql_view->priv);
	sql_view->priv = NULL;

	parent_class->finalize (object);
}

GType
components_sql_view_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ComponentsSqlViewClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) components_sql_view_class_init,
			NULL,
			NULL,
			sizeof (ComponentsSqlView),
			0,
			(GInstanceInitFunc) components_sql_view_init
		};
		type = g_type_register_static (GTK_TYPE_VBOX, "ComponentsSqlView", &info, 0);
	}

	return type;
}

GtkWidget *
components_sql_view_new (GdaConnection *cnc)
{
	ComponentsSqlView *sql_view;

	sql_view = g_object_new (COMPONENTS_TYPE_SQL_VIEW, NULL);
	if (GDA_IS_CONNECTION (cnc))
		components_sql_view_set_connection (sql_view, cnc);

	return GTK_WIDGET (sql_view);
}

GtkWidget *
components_sql_view_new_from (const gchar *name,
			      const gchar *user,
			      const gchar *password)
{
	GtkWidget *sql_view;
	GdaConnection *cnc;

	/* first try to open the connection */
	cnc = components_connection_open (name, user, password, FALSE, FALSE);
	if (!GDA_IS_CONNECTION (cnc))
		return NULL;

	sql_view = components_sql_view_new (cnc);

	/* we unref the connection, since we internally ref it in
	   components_sql_view_set_connection */
	g_object_unref (G_OBJECT (cnc));

	return sql_view;
}

void
components_sql_view_cut_clipboard (ComponentsSqlView *sql_view)
{
	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));
	gnome_db_text_cut_clipboard (gnome_db_sql_editor_get_text_view(
		GNOME_DB_SQL_EDITOR(sql_view->priv->text)));
}

void
components_sql_view_copy_clipboard (ComponentsSqlView *sql_view)
{
	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));
	gnome_db_text_copy_clipboard (gnome_db_sql_editor_get_text_view(
		GNOME_DB_SQL_EDITOR(sql_view->priv->text)));
}

void
components_sql_view_paste_clipboard (ComponentsSqlView *sql_view)
{
	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));
	gnome_db_text_paste_clipboard (gnome_db_sql_editor_get_text_view(
		GNOME_DB_SQL_EDITOR(sql_view->priv->text)));
}

GdaConnection *
components_sql_view_get_connection (ComponentsSqlView *sql_view)
{
	g_return_val_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view), NULL);
	return sql_view->priv->cnc;
}

void
components_sql_view_set_connection (ComponentsSqlView *sql_view, GdaConnection *cnc)
{
	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));

	if (GDA_IS_CONNECTION (cnc))
		g_object_ref (G_OBJECT (cnc));
	if (GDA_IS_CONNECTION (sql_view->priv->cnc))
		g_object_unref (G_OBJECT (sql_view->priv->cnc));

	sql_view->priv->cnc = cnc;

	gnome_db_text_clear (gnome_db_sql_editor_get_text_view (
				     GNOME_DB_SQL_EDITOR (sql_view->priv->text)));
	gnome_db_grid_set_model (GNOME_DB_GRID (sql_view->priv->grid), NULL);
}

void
components_sql_view_open_command (ComponentsSqlView *sql_view)
{
	gchar *uri;

	uri = gnome_db_select_file_dialog (gtk_widget_get_toplevel (GTK_WIDGET (sql_view)),
					   _("Select SQL file"));
	if (uri) {
		if (!gnome_db_sql_editor_load_from_file (
			    GNOME_DB_SQL_EDITOR (sql_view->priv->text), uri)) {
			gnome_db_show_error (_("Could not load file at %s"), uri);
		}

		g_free (uri);
	}
}

void
components_sql_view_save_command (ComponentsSqlView *sql_view)
{
	gchar *uri;

	uri = gnome_db_select_file_dialog (gtk_widget_get_toplevel (GTK_WIDGET (sql_view)),
					   _("Select SQL file"));
	if (uri) {
		if (!gnome_db_sql_editor_save_to_file (
			    GNOME_DB_SQL_EDITOR (sql_view->priv->text), uri)) {
			gnome_db_show_error (_("Could not save to %s"), uri);
		}

		g_free (uri);
	}
}

void
components_sql_view_run_current (ComponentsSqlView *sql_view)
{
	gchar *sql_txt;
	GdaCommand *cmd;
	GList *recset_list;

	g_return_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view));

	if (!GDA_IS_CONNECTION (sql_view->priv->cnc))
		return;

	sql_txt = gnome_db_sql_editor_get_all_text (
		GNOME_DB_SQL_EDITOR(sql_view->priv->text));
	if (!sql_txt || strlen (sql_txt) <= 0)
		return;

	/* execute the command */
	cmd = gda_command_new (sql_txt, GDA_COMMAND_TYPE_SQL, 0);
	recset_list = gda_connection_execute_command (sql_view->priv->cnc, cmd, NULL);
	if (recset_list != NULL) {
		/* free previous list of recordsets */
		g_list_foreach (sql_view->priv->current_recsets, (GFunc) g_object_unref, NULL);
		g_list_free (sql_view->priv->current_recsets);

		sql_view->priv->current_recsets = recset_list;

		/* show the first recordset */
		gnome_db_grid_set_model (
			GNOME_DB_GRID (sql_view->priv->grid),
			GDA_DATA_MODEL (sql_view->priv->current_recsets->data));
	}
}

gint
components_sql_view_get_row_count (ComponentsSqlView *sql_view)
{
	GdaDataModel *model;

	g_return_val_if_fail (COMPONENTS_IS_SQL_VIEW (sql_view), 0);

	model = gnome_db_grid_get_model (GNOME_DB_GRID (sql_view->priv->grid));
	return (GDA_IS_DATA_MODEL (model) ? gda_data_model_get_n_rows (model) : 0);
}
