/* GNOME-DB Components
 * Copyright (C) 2000-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <libgnomedb/gnome-db-util.h>
#include "components-database-config.h"

#define PARENT_TYPE GNOME_DB_TYPE_CONTROL

static void components_database_config_class_init (ComponentsDatabaseConfigClass *klass);
static void components_database_config_init       (ComponentsDatabaseConfig *config,
						   ComponentsDatabaseConfigClass *klass);
static void components_database_config_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * ComponentsDatabaseConfig class implementation
 */

static void
components_database_config_class_init (ComponentsDatabaseConfigClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = components_database_config_finalize;
}

static void
components_database_config_init (ComponentsDatabaseConfig *config,
				 ComponentsDatabaseConfigClass *klass)
{
	GtkWidget *table;

	/* create widgets */
	table = gnome_db_new_table_widget (6, 3, FALSE);

	/* create the Bonobo control */
	gnome_db_control_construct (GNOME_DB_CONTROL (config), table);
}

static void
components_database_config_finalize (GObject *object)
{
	ComponentsDatabaseConfig *config = (ComponentsDatabaseConfig *) object;

	g_return_if_fail (COMPONENTS_IS_DATABASE_CONFIG (config));

	parent_class->finalize (object);
}

GType
components_database_config_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ComponentsDatabaseConfigClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) components_database_config_class_init,
			NULL,
			NULL,
			sizeof (ComponentsDatabaseConfig),
			0,
			(GInstanceInitFunc) components_database_config_init
		};
		type = g_type_register_static (PARENT_TYPE,
					       "ComponentsDatabaseConfig",
					       &info, 0);
	}
	return type;
}

ComponentsDatabaseConfig *
components_database_config_new (void)
{
	ComponentsDatabaseConfig *config;

	config = g_object_new (COMPONENTS_TYPE_DATABASE_CONFIG, NULL);
	return config;
}
