/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include "gnome-db-shortcut.h"
#include <libgnomedb/gnome-db-util.h>
#include <bonobo/bonobo-i18n.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkimage.h>

#define PARENT_TYPE GTK_TYPE_EVENT_BOX

static void gnome_db_shortcut_class_init (GnomeDbShortcutClass *klass);
static void gnome_db_shortcut_init       (GnomeDbShortcut *shortcut,
					  GnomeDbShortcutClass *klass);
static void gnome_db_shortcut_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */

/*
 * GnomeDbShortcut class implementation
 */

static void
gnome_db_shortcut_class_init (GnomeDbShortcutClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_shortcut_finalize;
}

static void
gnome_db_shortcut_init (GnomeDbShortcut *shortcut, GnomeDbShortcutClass *klass)
{
	GtkWidget* frame;

	/* create the toolbar widget */
	frame = gnome_db_new_frame_widget (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (shortcut), frame);
	shortcut->tb = gnome_db_new_toolbar_widget (GTK_ORIENTATION_VERTICAL,
						    GTK_TOOLBAR_BOTH);
	gnome_db_set_widget_bg_color (shortcut->tb, "darkgray");
	gtk_toolbar_set_tooltips (GTK_TOOLBAR (shortcut->tb), FALSE);
	gtk_container_add (GTK_CONTAINER (frame), shortcut->tb);
}

static void
gnome_db_shortcut_finalize (GObject *object)
{
	GnomeDbShortcut *shortcut = (GnomeDbShortcut *) object;

	g_return_if_fail (GNOME_DB_IS_SHORTCUT (shortcut));

	parent_class->finalize (object);
}

GType
gnome_db_shortcut_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomeDbShortcutClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_shortcut_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbShortcut),
			0,
			(GInstanceInitFunc) gnome_db_shortcut_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbShortcut", &info, 0);
	}
	return type;
}

GtkWidget *
gnome_db_shortcut_new (void)
{
	GnomeDbShortcut* shortcut;

	shortcut = g_object_new (GNOME_DB_TYPE_SHORTCUT, NULL);
	return GTK_WIDGET (shortcut);
}

typedef struct {
	GnomeDbShortcut *shortcut;
	GnomeDbShortcutSelectFunc select_func;
	gpointer user_data;
	gchar *str;
	gchar *tooltip;
} ToolbarSelectionData;

static void
internal_select_func (GtkButton *button, gpointer user_data)
{
	ToolbarSelectionData *cb_data = user_data;

	g_return_if_fail (cb_data != NULL);

	if (cb_data->select_func != NULL) {
		cb_data->select_func (cb_data->shortcut,
				      cb_data->str,
				      cb_data->tooltip,
				      cb_data->user_data);
	}
}

static void
item_destroyed_cb (GtkObject *object, gpointer user_data)
{
	ToolbarSelectionData *cb_data = user_data;

	g_return_if_fail (cb_data != NULL);

	g_free (cb_data->str);
	g_free (cb_data->tooltip);
	g_free (cb_data);
}

void
gnome_db_shortcut_append (GnomeDbShortcut *shortcut,
                          const gchar *str,
                          const gchar *tooltip,
                          const gchar *stock_id,
                          GnomeDbShortcutSelectFunc select_func,
                          gpointer user_data)
{
	GtkWidget* pixmap;
	ToolbarSelectionData *cb_data;
	GtkWidget *w;

	g_return_if_fail (GNOME_DB_IS_SHORTCUT(shortcut));
	g_return_if_fail (str != NULL);

	pixmap = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_MENU);
	gtk_widget_show (pixmap);
	//gtk_toolbar_append_space (GTK_TOOLBAR(shortcut->tb));

	cb_data = g_new0 (ToolbarSelectionData, 1);
	cb_data->shortcut = shortcut;
	cb_data->select_func = select_func;
	cb_data->user_data = user_data;
	cb_data->str = g_strdup (str);
	cb_data->tooltip = g_strdup (tooltip);

	w = gtk_toolbar_append_item (GTK_TOOLBAR(shortcut->tb), _(str),
				     _(tooltip), NULL, pixmap,
				     GTK_SIGNAL_FUNC (internal_select_func), cb_data);

	g_signal_connect (G_OBJECT (w), "destroy",
			  G_CALLBACK (item_destroyed_cb), cb_data);
}

void
gnome_db_shortcut_remove (GnomeDbShortcut *shortcut, const gchar *str)
{
}

