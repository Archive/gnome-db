/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-widget.h>
#include <libgnomedb/gnome-db-window.h>
#include "gnome-db-shell-component-client.h"

#define PARENT_TYPE GNOME_DB_TYPE_CONTROL_WIDGET

struct _GnomeDbShellComponentClientPrivate {
	GnomeDbShell *shell;
	GNOME_Database_UIShellComponent corba_shell_comp;
};

static void gnome_db_shell_component_client_class_init (GnomeDbShellComponentClientClass *klass);
static void gnome_db_shell_component_client_init       (GnomeDbShellComponentClient *client,
							GnomeDbShellComponentClientClass *klass);
static void gnome_db_shell_component_client_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * GnomeDbShellComponentClient class implementation
 */

static void
gnome_db_shell_component_client_class_init (GnomeDbShellComponentClientClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_shell_component_client_finalize;
}

static void
gnome_db_shell_component_client_init (GnomeDbShellComponentClient *client,
				      GnomeDbShellComponentClientClass *klass)
{
	client->priv = g_new0 (GnomeDbShellComponentClientPrivate, 1);
	client->priv->shell = NULL;
	client->priv->corba_shell_comp = CORBA_OBJECT_NIL;
}

static void
gnome_db_shell_component_client_finalize (GObject *object)
{
	GnomeDbShellComponentClient *client = (GnomeDbShellComponentClient *) object;

	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (client));

	/* free memory */
	g_free (client->priv);
	client->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_shell_component_client_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomeDbShellComponentClientClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_shell_component_client_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbShellComponentClient),
			0,
			(GInstanceInitFunc) gnome_db_shell_component_client_init
		};
		type = g_type_register_static (PARENT_TYPE,
					       "GnomeDbShellComponentClient",
					       &info, 0);
	}
	return type;
}

/**
 * gnome_db_shell_component_client_new
 */
GtkWidget *
gnome_db_shell_component_client_new (GnomeDbShell *shell, const gchar *iid)
{
	GnomeDbShellComponentClient *client;
	GnomeDbShellComponentClient *new_client;
	CORBA_Environment ev;

	g_return_val_if_fail (GNOME_DB_IS_SHELL (shell), NULL);
	g_return_val_if_fail (iid != NULL, NULL);

	client = g_object_new (GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT, NULL);

	client->priv->shell = shell;

	new_client = GNOME_DB_SHELL_COMPONENT_CLIENT (gnome_db_control_widget_construct (
		GNOME_DB_CONTROL_WIDGET (client),
		iid,
		gnome_db_window_get_ui_container (
			GNOME_DB_WINDOW (gnome_db_shell_get_window (shell)))));
	if (!GNOME_DB_IS_SHELL_COMPONENT_CLIENT (new_client)) {
		g_object_unref (G_OBJECT (client));
		return NULL;
	}

	/* get the UIShellComponent interface */
	new_client->priv->corba_shell_comp =
		gnome_db_control_widget_get_objref (GNOME_DB_CONTROL_WIDGET (new_client));

	CORBA_exception_init (&ev);

	GNOME_Database_UIShellComponent_addClient (new_client->priv->corba_shell_comp,
						   BONOBO_OBJREF (shell), &ev);
	if (BONOBO_EX (&ev)) {
		//g_object_unref (new_client);
		//new_client = NULL;
	}

	CORBA_exception_free (&ev);

	return GTK_WIDGET (new_client);
}

/**
 * gnome_db_shell_component_client_get_version
 * @wid: control widget
 *
 * Return the version supported by the underlying control.
 *
 * Returns: a newly allocated string that contains the version number.
 */
gchar *
gnome_db_shell_component_client_get_version (GnomeDbShellComponentClient *client)
{
	CORBA_Environment ev;
	CORBA_char *corba_version;
	gchar *version;

	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (client), NULL);

	CORBA_exception_init (&ev);
	corba_version = GNOME_Database_UIShellComponent_getVersion (
		client->priv->corba_shell_comp, &ev);
	if (BONOBO_EX (&ev)) {
		CORBA_exception_free (&ev);
		return NULL;
	}

	CORBA_exception_free (&ev);

	version = g_strdup (corba_version);
	CORBA_free (corba_version);

	return version;
}

/**
 * gnome_db_shell_component_client_get_config_control
 */
GnomeDbControlWidget *
gnome_db_shell_component_client_get_config_control (GnomeDbShellComponentClient *client)
{
	Bonobo_Control corba_control;
	CORBA_Environment ev;

	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (client), NULL);

	CORBA_exception_init (&ev);
	corba_control = GNOME_Database_UIShellComponent_getConfigControl (
		client->priv->corba_shell_comp, &ev);
	if (BONOBO_EX (&ev) || !corba_control) {
		CORBA_exception_free (&ev);
		return NULL;
	}

	return GNOME_DB_CONTROL_WIDGET (
		gnome_db_control_widget_new_from_corba (
			corba_control,
			gnome_db_control_widget_get_ui_container (GNOME_DB_CONTROL_WIDGET (client))));
}
