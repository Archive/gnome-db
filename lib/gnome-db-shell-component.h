/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_shell_component_h__)
#  define __gnome_db_shell_component_h__

#include <bonobo/bonobo-object.h>
#include <libgnomedb/gnome-db-control.h>
#include <GNOME_Database_UI.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SHELL_COMPONENT            (gnome_db_shell_component_get_type())
#define GNOME_DB_SHELL_COMPONENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SHELL_COMPONENT, GnomeDbShellComponent))
#define GNOME_DB_SHELL_COMPONENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SHELL_COMPONENT, GnomeDbShellComponentClass))
#define GNOME_DB_IS_SHELL_COMPONENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SHELL_COMPONENT))
#define GNOME_DB_IS_SHELL_COMPONENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SHELL_COMPONENT))

typedef struct _GnomeDbShellComponent        GnomeDbShellComponent;
typedef struct _GnomeDbShellComponentClass   GnomeDbShellComponentClass;
typedef struct _GnomeDbShellComponentPrivate GnomeDbShellComponentPrivate;

struct _GnomeDbShellComponent {
	BonoboObject object;
	GnomeDbShellComponentPrivate* priv;
};

struct _GnomeDbShellComponentClass {
	BonoboObjectClass parent_class; 

	POA_GNOME_Database_UIShellComponent__epv epv;

	/* virtual methods */
	gboolean (* select_connection) (GnomeDbShellComponent *comp,
					const gchar *name,
					const gchar *username,
					const gchar *password);
	GnomeDbControl * (* get_config_control) (GnomeDbShellComponent *comp);
};

GType                  gnome_db_shell_component_get_type (void);
GnomeDbShellComponent *gnome_db_shell_component_construct (GnomeDbShellComponent *comp,
							   GtkWidget *widget);
GnomeDbShellComponent *gnome_db_shell_component_new (GtkWidget *widget);
GnomeDbControl        *gnome_db_shell_component_get_control (GnomeDbShellComponent *comp);
gboolean               gnome_db_shell_component_select_connection (GnomeDbShellComponent *comp,
								   const gchar *name,
								   const gchar *username,
								   const gchar *password);
GnomeDbControl        *gnome_db_shell_component_get_config_control (GnomeDbShellComponent *comp);

/* notifications to clients */
void gnome_db_shell_component_notify_connection_opened (GnomeDbShellComponent *comp,
							const gchar *name,
							const gchar *username,
							const gchar *password);
void gnome_db_shell_component_notify_connection_closed (GnomeDbShellComponent *comp,
							const gchar *name,
							const gchar *username,
							const gchar *password);

G_END_DECLS

#endif
