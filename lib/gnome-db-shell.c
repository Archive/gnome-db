/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <bonobo/bonobo-i18n.h>
#include <libgda/gda-config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkbbox.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkvpaned.h>
#include <libgnomedb/gnome-db-config.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-window.h>
#include <libgnomeui/gnome-about.h>
#include <libgnomeui/gnome-window-icon.h>
#include "gnome-db-defs.h"
#include "gnome-db-shell.h"
#include "gnome-db-shell-settings.h"
#include "gnome-db-shortcut.h"
#include "gnome-db-summary.h"

#define PARENT_TYPE BONOBO_OBJECT_TYPE

struct _GnomeDbShellPrivate {
	GtkWidget *window;
	GtkWidget *workarea;
	GtkWidget *summary;
	GtkWidget *shortcut;
	GtkWidget *notebook;

	GList *activated_components;
	glong config_listener_id;

	gboolean show_summary;
	gboolean show_shortcut;
};

static void gnome_db_shell_class_init (GnomeDbShellClass *klass);
static void gnome_db_shell_init       (GnomeDbShell *shell, GnomeDbShellClass *klass);
static void gnome_db_shell_finalize   (GObject *object);


static void shortcut_button_clicked_cb (GnomeDbShortcut *shortcut,
					const gchar *label,
					const gchar *tooltip,
					gpointer user_data);

static void verb_FileNewDatabase (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_FileNewWindow (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_FileExit (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_HelpAbout (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_HelpSubmitBug (BonoboUIComponent *uic, void *user_data, const char *path);
static void verb_ToolsSettings (BonoboUIComponent *uic, void *user_data, const char *path);
static void toggle_ViewShortcutBar (BonoboUIComponent *uic,
				    const char *path,
				    Bonobo_UIComponent_EventType type,
				    const char *state,
				    gpointer user_data);
static void toggle_ViewSummary (BonoboUIComponent *uic,
				const char *path,
				Bonobo_UIComponent_EventType type,
				const char *state,
				gpointer user_data);

static BonoboUIVerb shell_verbs [] = {
	BONOBO_UI_VERB ("FileNewDatabase", verb_FileNewDatabase),
	BONOBO_UI_VERB ("FileNewWindow", verb_FileNewWindow),
	BONOBO_UI_VERB ("FileExit", verb_FileExit),
	BONOBO_UI_VERB ("ToolsSettings", verb_ToolsSettings),
	BONOBO_UI_VERB ("HelpAbout", verb_HelpAbout),
	BONOBO_UI_VERB ("HelpSubmitBug", verb_HelpSubmitBug),
	BONOBO_UI_VERB_END
};
static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */
static void
add_components_to_shortcut (GnomeDbShell *shell)
{
	GList *components;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	components = gnome_db_config_get_component_list (
		"repo_ids.has('IDL:GNOME/Database/UIShellComponent:1.0')");
	if (components) {
		GList *node;
		GnomeDbComponentInfo *comp_info;
          
		for (node = g_list_first (components);
		     node != NULL;
		     node = g_list_next (node)) {
			GdaParameter *param;
			gchar *shell_icon;

			comp_info = (GnomeDbComponentInfo *) node->data;
			param = gda_parameter_list_find (comp_info->properties,
							 "shell_icon");
			shell_icon = gda_value_stringify (gda_parameter_get_value (param));
			gnome_db_shortcut_append (
				GNOME_DB_SHORTCUT (shell->priv->shortcut),
				comp_info->description,
				comp_info->id,
				shell_icon,
				shortcut_button_clicked_cb,
				(gpointer) shell);

			g_free (shell_icon);
		}

		gnome_db_config_free_component_list (components);
	}

	/* load the default component */
	if (gnome_db_config_has_key (GNOME_DB_SHELL_CONFIG_DEFAULT_COMPONENT)) {
		/* FIXME */
	}
	else {
		/* start database component by default */
		shortcut_button_clicked_cb (GNOME_DB_SHORTCUT (shell->priv->shortcut),
					    _("Database"), GNOME_DB_ID_DATABASE, shell);
	}
}

static void
setup_window (GnomeDbShell *shell)
{
	GtkWidget *box;
	gchar *title;
	BonoboUIComponent *uic;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	/* create the window for this shell */
	shell->priv->window = gnome_db_window_new ("gnome-db",
						   GNOMEDB_DATADIR,
						   "gnome-db.xml",
						   shell_verbs,
						   shell);
	gtk_widget_set_usize (shell->priv->window, 500, 350);
	title = g_strdup_printf (_("GNOME-DB %s"), VERSION);
	gtk_window_set_title (GTK_WINDOW (shell->priv->window), title);
	g_free (title);
	gnome_window_icon_set_from_file (
		GTK_WINDOW (shell->priv->window),
		GNOMEDB_PIXMAPDIR "/gnome-db.png");

	shell->priv->workarea = gtk_hpaned_new ();
	shell->priv->shortcut = gnome_db_shortcut_new ();

	gtk_paned_add1 (GTK_PANED (shell->priv->workarea), shell->priv->shortcut);

	box = gnome_db_new_vbox_widget (FALSE, 0);
	shell->priv->summary = gnome_db_summary_new ();
	gtk_box_pack_start (GTK_BOX (box), shell->priv->summary, FALSE, TRUE, 0);

	shell->priv->notebook = gnome_db_new_notebook_widget ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (shell->priv->notebook), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (shell->priv->notebook), FALSE);
	gtk_notebook_popup_disable (GTK_NOTEBOOK (shell->priv->notebook));
	gtk_box_pack_start (GTK_BOX (box), shell->priv->notebook, TRUE, TRUE, 0);
	gtk_paned_add2 (GTK_PANED (shell->priv->workarea), box);

	gtk_widget_show (shell->priv->workarea);
	gnome_db_window_set_contents (GNOME_DB_WINDOW (shell->priv->window),
				      shell->priv->workarea);
	gnome_db_window_show (GNOME_DB_WINDOW (shell->priv->window));

	add_components_to_shortcut (shell);

	/* load state of window from configuration */
	uic = gnome_db_window_get_ui_component (GNOME_DB_WINDOW (shell->priv->window));

	if (gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT) ||
	    !gnome_db_config_has_key (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT)) {
		gtk_widget_show (shell->priv->shortcut);
		shell->priv->show_shortcut = TRUE;
		bonobo_ui_component_set_prop (uic, "/commands/ViewShortcutBar",
					      "state", "1", NULL);
	}
	else {
		gtk_widget_hide (shell->priv->shortcut);
		shell->priv->show_shortcut = FALSE;
		bonobo_ui_component_set_prop (uic, "/commands/ViewShortcutBar",
					      "state", "0", NULL);
	}

	if (gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY) ||
	    !gnome_db_config_has_key (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY)) {
		gtk_widget_show (shell->priv->summary);
		shell->priv->show_summary = TRUE;
		bonobo_ui_component_set_prop (uic, "/commands/ViewSummary",
					      "state", "1", NULL);
	}
	else {
		gtk_widget_hide (shell->priv->summary);
		shell->priv->show_summary = FALSE;
		bonobo_ui_component_set_prop (uic, "/commands/ViewSummary",
					      "state", "0", NULL);
	}

	bonobo_ui_component_add_listener (uic, "ViewShortcutBar",
					  toggle_ViewShortcutBar, shell);
	bonobo_ui_component_add_listener (uic, "ViewSummary",
					  toggle_ViewSummary, shell);
}

/*
 * Callbacks
 */

static void
config_changed_cb (GConfClient *client,
		   guint cnxm_id,
		   GConfEntry *entry,
		   gpointer user_data)
		   
{
	BonoboUIComponent *uic;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;
	const gchar *path;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	path = entry->key;

	uic = gnome_db_window_get_ui_component (GNOME_DB_WINDOW (shell->priv->window));

	if (!strcmp (path, GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT)) {
		/* hide/show shortcut bar */
		if (gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT)) {
			gtk_widget_show (shell->priv->shortcut);
			shell->priv->show_shortcut = TRUE;
			bonobo_ui_component_set_prop (uic, "/commands/ViewShortcutBar",
						      "state", "1", NULL);
		}
		else {
			gtk_widget_hide (shell->priv->shortcut);
			shell->priv->show_shortcut = FALSE;
			bonobo_ui_component_set_prop (uic, "/commands/ViewShortcutBar",
						      "state", "0", NULL);
		}
	}
	else if (!strcmp (path, GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY)) {
		/* hide/show summary */
		if (gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY)) {
			gtk_widget_show (shell->priv->summary);
			shell->priv->show_summary = TRUE;
			bonobo_ui_component_set_prop (uic, "/commands/ViewSummary",
						      "state", "1", NULL);
		}
		else {
			gtk_widget_hide (shell->priv->summary);
			shell->priv->show_summary = FALSE;
			bonobo_ui_component_set_prop (uic, "/commands/ViewSummary",
						      "state", "0", NULL);
		}
	}
}

static void
shortcut_button_clicked_cb (GnomeDbShortcut *shortcut,
			    const gchar *label,
			    const gchar *tooltip,
			    gpointer user_data)
{
	GList *node;
	GtkWidget *wid;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	/* see if the component is already loaded */
	for (node = g_list_first (shell->priv->activated_components);
	     node != NULL;
	     node = g_list_next (node)) {
		wid = GTK_WIDGET (node->data);
		if (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (wid)) {
			if (!g_strcasecmp (
				    gnome_db_control_widget_get_id (GNOME_DB_CONTROL_WIDGET (wid)),
				    tooltip)) {
				gnome_db_shell_activate_tab (
					shell,
					GNOME_DB_SHELL_COMPONENT_CLIENT (wid));
				return;
			}
		}
	}

	/* load it */
	wid = gnome_db_shell_component_client_new (shell, tooltip);
	if (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (wid)) {
		const gchar *title;

		g_object_set_data_full (G_OBJECT (wid),
			"GNOME_DB_Manager_ComponentDescription",
			g_strdup (label),
			(GDestroyNotify) g_free);
		title = gnome_db_control_widget_get_id (GNOME_DB_CONTROL_WIDGET (wid));
		gnome_db_shell_add_tab (shell, GNOME_DB_SHELL_COMPONENT_CLIENT (wid), title);
		gnome_db_shell_activate_tab (shell, GNOME_DB_SHELL_COMPONENT_CLIENT (wid));

		shell->priv->activated_components = g_list_append (
			shell->priv->activated_components, wid);
	}
}

static void
verb_FileNewDatabase (BonoboUIComponent *uic, void *user_data, const char *path)
{
}

static void
verb_FileNewWindow (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
}

static void
verb_FileExit (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	bonobo_object_unref (BONOBO_OBJECT (shell));
}

static void
verb_HelpAbout (BonoboUIComponent *uic, void *user_data, const char *path)
{
	static const gchar *authors[] = {
                "Michael Lausch <michael@lausch.at>",
                "Rodrigo Moya <rodrigo@gnome-db.org>",
		"Stephan Heinze <stephan.heinze@xcom.de>",
		"Vivien Malerba <malerba@gnome-db.org>",
		"Nick Gorham <nick@lurcher.org>",
		"Chris Wiegand <cwiegand@urgentmail.com>",
		"Akira TAGOH <tagoh@gnome-db.org>",
		/* If your encoding allows it, use oacute (U00F3) for the 'o'
		 * of 'Perello' and use iacute (U00ED) for the 'i' of 'Marin' */
		N_("Carlos Perello Marin <carlos@gnome-db.org>"),
		"Holger Thon <holger.thon@gnome-db.org>",
		/* If your encoding allows it, use udiaeresis for the 'u'
		 * of 'Muller' */
		N_("Reinhard Muller <reinhard@gnue.org>"),
		"Gerhard Dieringer <gdieringer@compuserve.com>",
		"Gonzalo Paniagua Javier <gonzalo@gnome-db.org>",
                NULL
        };
        GtkWidget *dialog;
	GdkPixbuf *logo;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	logo = gdk_pixbuf_new_from_file (GNOMEDB_PIXMAPDIR "/gnome-db.png", NULL);

        dialog = gnome_about_new (_("GNOME-DB"), VERSION,
                                  _("Copyright 2001, The GNOME Foundation"),
                                  _("GNOME Database front-end"),
                                  authors, NULL, NULL, logo);
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (shell->priv->window));
	g_signal_connect (G_OBJECT (dialog), "destroy",
			  G_CALLBACK (gtk_widget_destroyed), &dialog);

	gtk_widget_show (dialog);

	gdk_pixbuf_unref (logo);
}

static void
verb_HelpSubmitBug (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
}

static void
verb_ToolsSettings (BonoboUIComponent *uic, void *user_data, const char *path)
{
	GtkWidget *dialog;
	GtkWidget *settings;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	/* create the dialog */
	dialog = gtk_dialog_new_with_buttons (
		_("Settings"),
		GTK_WINDOW (shell->priv->window), 0,
		GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL,
		NULL);
	gnome_window_icon_set_from_file (GTK_WINDOW (dialog),
					 GNOMEDB_PIXMAPDIR "/gnome-db.png");

	settings = gnome_db_shell_settings_new (shell);
	gtk_widget_show (settings);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), settings, TRUE, TRUE, 0);

	/* show the dialog */
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gtk_widget_destroy), dialog);
	gtk_widget_show (dialog);
}

static void
toggle_ViewShortcutBar (BonoboUIComponent *uic,
			const char *path,
			Bonobo_UIComponent_EventType type,
			const char *state,
			gpointer user_data)
{
	gboolean show;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	if (type != Bonobo_UIComponent_STATE_CHANGED)
		return;

	show = atoi (state);
	if (show)
		gnome_db_shell_show_shortcut (shell);
	else
		gnome_db_shell_hide_shortcut (shell);
}

static void
toggle_ViewSummary (BonoboUIComponent *uic,
		    const char *path,
		    Bonobo_UIComponent_EventType type,
		    const char *state,
		    gpointer user_data)
{
	gboolean show;
	GnomeDbShell *shell = (GnomeDbShell *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	if (type != Bonobo_UIComponent_STATE_CHANGED)
		return;

	show = atoi (state);
	if (show)
		gnome_db_shell_show_summary (shell);
	else
		gnome_db_shell_hide_summary (shell);
}

/*
 * GNOME::Database::UIShell CORBA implementation
 */

static void
impl_UIShell_notifyConnectionOpened (PortableServer_Servant servant,
				     const CORBA_char *name,
				     const CORBA_char *username,
				     const CORBA_char *password,
				     CORBA_Environment *ev)
{
	GnomeDbShell *shell = (GnomeDbShell *) bonobo_object (servant);

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
}

static void
impl_UIShell_notifyConnectionClosed (PortableServer_Servant servant,
				     const CORBA_char *name,
				     const CORBA_char *username,
				     const CORBA_char *password,
				     CORBA_Environment *ev)
{
	GnomeDbShell *shell = (GnomeDbShell *) bonobo_object (servant);

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
}

/*
 * GnomeDbShell class implementation
 */

static void
gnome_db_shell_class_init (GnomeDbShellClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	POA_GNOME_Database_UIShell__epv *epv = &klass->epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_shell_finalize;

	/* set the epv */
	epv->notifyConnectionOpened = impl_UIShell_notifyConnectionOpened;
	epv->notifyConnectionClosed = impl_UIShell_notifyConnectionClosed;
}

static void
gnome_db_shell_init (GnomeDbShell *shell, GnomeDbShellClass *klass)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	shell->priv = g_new0 (GnomeDbShellPrivate, 1);
	shell->priv->window = NULL;
	shell->priv->activated_components = NULL;

	/* add a listener for configuration changes */
	shell->priv->config_listener_id = gnome_db_config_add_listener (
		GNOME_DB_SHELL_CONFIG_SECTION_SHELL,
		config_changed_cb,
		shell);
}

static void
gnome_db_shell_finalize (GObject *object)
{
	GnomeDbShell *shell = (GnomeDbShell *) object;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));

	/* free memory */
	gnome_db_window_close (GNOME_DB_WINDOW (shell->priv->window));
	shell->priv->window = NULL;

	g_list_free (shell->priv->activated_components);
	shell->priv->activated_components = NULL;

	gnome_db_config_remove_listener (shell->priv->config_listener_id);

	g_free (shell->priv);
	shell->priv = NULL;

	parent_class->finalize (object);
}

BONOBO_TYPE_FUNC_FULL (GnomeDbShell,
		       GNOME_Database_UIShell,
		       PARENT_TYPE,
		       gnome_db_shell)

/**
 * gnome_db_shell_new
 *
 * Create a new shell component, which is the main window of the GNOME-DB
 * front-end program.
 *
 * Returns: a newly created #GnomeDbShell object
 */
GnomeDbShell *
gnome_db_shell_new (void)
{
	GnomeDbShell *shell;

	shell = g_object_new (GNOME_DB_TYPE_SHELL, NULL);

	/* create the GnomeDbWindow associated with this shell component */
	setup_window (shell);

	return shell;
}

/**
 * gnome_db_shell_get_window
 */
GtkWidget *
gnome_db_shell_get_window (GnomeDbShell *shell)
{
	g_return_val_if_fail (GNOME_DB_IS_SHELL (shell), NULL);
	return shell->priv->window;
}

void
gnome_db_shell_add_tab (GnomeDbShell *shell,
			GnomeDbShellComponentClient *wid,
			const gchar *label)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (wid));

	gtk_notebook_append_page (GTK_NOTEBOOK (shell->priv->notebook),
				  GTK_WIDGET (wid),
				  gtk_label_new (label));
	gtk_widget_show (GTK_WIDGET (wid));
}

/**
 * gnome_db_shell_activate_tab
 */
void
gnome_db_shell_activate_tab (GnomeDbShell *shell, GnomeDbShellComponentClient *wid)
{
	GtkWidget *w;
	gint current;
	const gchar *description;

	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (wid));

	current = gtk_notebook_get_current_page (GTK_NOTEBOOK (shell->priv->notebook));

	/* first, deactivate the current component */
	w = gtk_notebook_get_nth_page (GTK_NOTEBOOK (shell->priv->notebook), current);
	if (GNOME_DB_IS_SHELL_COMPONENT_CLIENT (w)) {
		if (w == GTK_WIDGET (wid))
			return;
		gnome_db_control_widget_deactivate (GNOME_DB_CONTROL_WIDGET (w));
	}

	/* activate the new one */
	gtk_notebook_set_current_page (
		GTK_NOTEBOOK (shell->priv->notebook),
		gtk_notebook_page_num (GTK_NOTEBOOK (shell->priv->notebook),
				       GTK_WIDGET (wid)));
	gnome_db_control_widget_activate (GNOME_DB_CONTROL_WIDGET (wid));

	/* set the title on the summary */
	description = g_object_get_data (G_OBJECT (wid), "GNOME_DB_Manager_ComponentDescription");
	if (description)
		gnome_db_summary_set_title (GNOME_DB_SUMMARY (shell->priv->summary), description);
}

GnomeDbShellComponentClient *
gnome_db_shell_get_current_tab (GnomeDbShell *shell)
{
	gint tab;

	g_return_val_if_fail (GNOME_DB_IS_SHELL (shell), NULL);

	tab = gtk_notebook_get_current_page (GTK_NOTEBOOK (shell->priv->notebook));
	return GNOME_DB_SHELL_COMPONENT_CLIENT (
		gtk_notebook_get_nth_page (GTK_NOTEBOOK (shell->priv->notebook), tab));
}

void
gnome_db_shell_show_shortcut (GnomeDbShell *shell)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT, TRUE);
}

void
gnome_db_shell_hide_shortcut (GnomeDbShell *shell)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT, FALSE);
}

void
gnome_db_shell_show_summary (GnomeDbShell *shell)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY, TRUE);
}

void
gnome_db_shell_hide_summary (GnomeDbShell *shell)
{
	g_return_if_fail (GNOME_DB_IS_SHELL (shell));
	gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY, FALSE);
}
