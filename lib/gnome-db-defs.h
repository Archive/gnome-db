/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_defs_h__)
#  define __gnome_db_defs_h__

/*
 * Bonobo components IDs
 */
#define GNOME_DB_ID_DATABASE        "OAFIID:GNOME_Database_UIDatabaseComponent"
#define GNOME_DB_ID_DATABASE_CONFIG "OAFIID:GNOME_Database_UIDatabaseConfigComponent"
#define GNOME_DB_ID_MANAGER         "OAFIID:GNOME_Database_UIManagerComponent"

/*
 * Definitions for configuration entries
 */
#define GNOME_DB_SHELL_CONFIG_SECTION_SHELL "/apps/gnome-db/Shell"
#define GNOME_DB_SHELL_CONFIG_SECTION_LOAD  "/apps/gnome-db/Shell/Load"

#define GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT    "/apps/gnome-db/Shell/ShowShortcut"
#define GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY     "/apps/gnome-db/Shell/ShowSummary"
#define GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS  "/apps/gnome-db/Shell/SaveWindowPos"
#define GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS "/apps/gnome-db/Shell/FixedWindowPos"
#define GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X1   "/apps/gnome-db/Shell/MainWindowX1"
#define GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X2   "/apps/gnome-db/Shell/MainWindowX2"
#define GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y1   "/apps/gnome-db/Shell/MainWindowY1"
#define GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y2   "/apps/gnome-db/Shell/MainWindowY2"

#define GNOME_DB_SHELL_CONFIG_DEFAULT_COMPONENT "/apps/gnome-db/Shell/DefaultComponent"

#endif
