/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_shell_component_client_h__)
#  define __gnome_db_shell_component_client_h__

#include <libgnomedb/gnome-db-control-widget.h>

typedef struct _GnomeDbShellComponentClient        GnomeDbShellComponentClient;

#include <gnome-db-shell.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT            (gnome_db_shell_component_client_get_type())
#define GNOME_DB_SHELL_COMPONENT_CLIENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT, GnomeDbShellComponentClient))
#define GNOME_DB_SHELL_COMPONENT_CLIENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT, GnomeDbShellComponentClientClass))
#define GNOME_DB_IS_SHELL_COMPONENT_CLIENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT))
#define GNOME_DB_IS_SHELL_COMPONENT_CLIENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SHELL_COMPONENT_CLIENT))

typedef struct _GnomeDbShellComponentClientClass   GnomeDbShellComponentClientClass;
typedef struct _GnomeDbShellComponentClientPrivate GnomeDbShellComponentClientPrivate;

struct _GnomeDbShellComponentClient {
	GnomeDbControlWidget control;
	GnomeDbShellComponentClientPrivate *priv;
};

struct _GnomeDbShellComponentClientClass {
	GnomeDbControlWidgetClass parent_class;
};

GType      gnome_db_shell_component_client_get_type (void);
GtkWidget *gnome_db_shell_component_client_new (GnomeDbShell *shell, const gchar *iid);

gchar     *gnome_db_shell_component_client_get_version (GnomeDbShellComponentClient *client);
GnomeDbControlWidget *gnome_db_shell_component_client_get_config_control (GnomeDbShellComponentClient *client);

G_END_DECLS

#endif
