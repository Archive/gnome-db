/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_shell_settings_h__)
#  define __gnome_db_shell_settings_h__

#include <gtk/gtkvbox.h>
#include "gnome-db-shell.h"

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SHELL_SETTINGS            (gnome_db_shell_settings_get_type())
#define GNOME_DB_SHELL_SETTINGS(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SHELL_SETTINGS, GnomeDbShellSettings))
#define GNOME_DB_SHELL_SETTINGS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SHELL_SETTINGS, GnomeDbShellSettingsClass))
#define GNOME_DB_IS_SHELL_SETTINGS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SHELL_SETTINGS))
#define GNOME_DB_IS_SHELL_SETTINGS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SHELL_SETTINGS))

typedef struct _GnomeDbShellSettings        GnomeDbShellSettings;
typedef struct _GnomeDbShellSettingsClass   GnomeDbShellSettingsClass;
typedef struct _GnomeDbShellSettingsPrivate GnomeDbShellSettingsPrivate;

struct _GnomeDbShellSettings {
	GtkVBox box;
	GnomeDbShellSettingsPrivate *priv;
};

struct _GnomeDbShellSettingsClass {
	GtkVBoxClass parent_class;
};

GType      gnome_db_shell_settings_get_type (void);
GtkWidget *gnome_db_shell_settings_new (GnomeDbShell *shell);

G_END_DECLS

#endif
