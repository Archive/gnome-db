/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_shortcut_h__)
#  define __gnome_db_shortcut_h__

#include <gtk/gtkeventbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SHORTCUT            (gnome_db_shortcut_get_type())
#define GNOME_DB_SHORTCUT(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SHORTCUT, GnomeDbShortcut))
#define GNOME_DB_SHORTCUT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SHORTCUT, GnomeDbShortcutClass))
#define GNOME_DB_IS_SHORTCUT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SHORTCUT))
#define GNOME_DB_IS_SHORTCUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SHORTCUT))

typedef struct _GnomeDbShortcut      GnomeDbShortcut;
typedef struct _GnomeDbShortcutClass GnomeDbShortcutClass;

struct _GnomeDbShortcut {
	GtkEventBox box;

	/* private */
	GtkWidget* tb;
};

struct _GnomeDbShortcutClass {
	GtkEventBoxClass parent_class;
};

GType      gnome_db_shortcut_get_type (void);
GtkWidget* gnome_db_shortcut_new (void);

typedef void (* GnomeDbShortcutSelectFunc) (GnomeDbShortcut *shortcut,
					    const gchar *label,
					    const gchar *tooltip,
					    gpointer user_data);

void       gnome_db_shortcut_append (GnomeDbShortcut *shortcut,
                                     const gchar *label,
                                     const gchar *tooltip,
                                     const gchar *icon_file,
                                     GnomeDbShortcutSelectFunc select_func,
                                     gpointer user_data);
void       gnome_db_shortcut_remove (GnomeDbShortcut *shortcut, const gchar *str);

G_END_DECLS

#endif
