/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <libgda/gda-log.h>
#include <gtk/gtksignal.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-i18n.h>
#include "gnome-db-shell-component.h"

#define PARENT_TYPE BONOBO_OBJECT_TYPE
#define CLASS(comp) (GNOME_DB_SHELL_COMPONENT_CLASS (G_OBJECT_GET_CLASS (comp)))

static void gnome_db_shell_component_class_init (GnomeDbShellComponentClass *klass);
static void gnome_db_shell_component_init       (GnomeDbShellComponent *comp,
						 GnomeDbShellComponentClass *klass);
static void gnome_db_shell_component_finalize   (GObject *object);

struct _GnomeDbShellComponentPrivate {
	GnomeDbControl *control;
	GList *shell_clients;
};

enum {
	LAST_SIGNAL
};

static guint shell_component_signals[LAST_SIGNAL];
static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */

static GList *
clean_shell_list (GnomeDbShellComponent *comp)
{
	/* FIXME check which clients are still alive, and remove dead ones */
	return comp->priv->shell_clients;
}

/*
 * CORBA methods implementation
 */

static CORBA_string
impl_UIShellComponent_getVersion (PortableServer_Servant servant, CORBA_Environment *ev)
{
	return CORBA_string_dup (VERSION);
}

static void
impl_UIShellComponent_addClient (PortableServer_Servant servant,
				 GNOME_Database_UIShell corba_shell,
				 CORBA_Environment *ev)
{
	GNOME_Database_UIShell shell_copy;
	GnomeDbShellComponent *comp = (GnomeDbShellComponent *) bonobo_object (servant);

	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp));

	shell_copy = CORBA_Object_duplicate (corba_shell, ev);
	if (BONOBO_EX (ev)) {
		gda_log_error (_("Could not duplicate UIShell object"));
		return;
	}

	comp->priv->shell_clients = g_list_append (comp->priv->shell_clients, shell_copy);
}

static CORBA_boolean
impl_UIShellComponent_selectConnection (PortableServer_Servant servant,
					const CORBA_char *name,
					const CORBA_char *username,
					const CORBA_char *password,
					CORBA_Environment *ev)
{
	GnomeDbShellComponent *comp = (GnomeDbShellComponent *) bonobo_object (servant);

	bonobo_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), FALSE, ev);
	return gnome_db_shell_component_select_connection (comp, name, username, password);
}

static Bonobo_Control
impl_UIShellComponent_getConfigControl (PortableServer_Servant servant,
					CORBA_Environment *ev)
{
	GnomeDbControl *control;
	GnomeDbShellComponent *comp = (GnomeDbShellComponent *) bonobo_object (servant);

	bonobo_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), CORBA_OBJECT_NIL, ev);
	
	control = gnome_db_shell_component_get_config_control (comp);
	if (!control) {
		bonobo_exception_general_error_set (ev, NULL, _("Couldn't load config control"));
		return CORBA_OBJECT_NIL;
	}

	return bonobo_object_corba_objref (BONOBO_OBJECT (control));
}

/*
 * GnomeDbShellComponent class implementation
 */

static void
gnome_db_shell_component_class_init (GnomeDbShellComponentClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	POA_GNOME_Database_UIShellComponent__epv *epv = &klass->epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_shell_component_finalize;

	/* set the epv */
	epv->getVersion = impl_UIShellComponent_getVersion;
	epv->addClient = impl_UIShellComponent_addClient;
	epv->selectConnection = impl_UIShellComponent_selectConnection;
	epv->getConfigControl = impl_UIShellComponent_getConfigControl;
}

static void
gnome_db_shell_component_init (GnomeDbShellComponent *comp,
			       GnomeDbShellComponentClass *klass)
{
	comp->priv = g_new0 (GnomeDbShellComponentPrivate, 1);
	comp->priv->control = NULL;
	comp->priv->shell_clients = NULL;
}

static void
gnome_db_shell_component_finalize (GObject *object)
{
	CORBA_Environment ev;
	GnomeDbShellComponent *comp = (GnomeDbShellComponent *) object;

	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp));

	/* free memory */
	CORBA_exception_init (&ev);
	g_list_foreach (comp->priv->shell_clients, (GFunc) CORBA_Object_release, &ev);
	g_list_free (comp->priv->shell_clients);
	comp->priv->shell_clients = NULL;
	CORBA_exception_free (&ev);

	g_free (comp->priv);
	comp->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

BONOBO_TYPE_FUNC_FULL (GnomeDbShellComponent,
		       GNOME_Database_UIShellComponent,
		       PARENT_TYPE,
		       gnome_db_shell_component)

/**
 * gnome_db_shell_component_construct
 */
GnomeDbShellComponent *
gnome_db_shell_component_construct (GnomeDbShellComponent *comp, GtkWidget *widget)
{
	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), comp);
	g_return_val_if_fail (GTK_IS_WIDGET (widget), comp);

	comp->priv->control = gnome_db_control_new (widget);
	bonobo_object_add_interface (BONOBO_OBJECT (comp),
				     BONOBO_OBJECT (comp->priv->control));

	return comp;
}

/**
 * gnome_db_shell_component_new
 * @w: the widget to be wrapped by the component
 *
 * Creates a new #GnomeDbShellComponent object, which is an instance of the
 * Bonobo::Control and GNOME::Database::UIShellComponent CORBA interfaces.
 * The GNOME::Database::UIShellComponent interface allows you to write new
 * components that integrate perfectly well in the GNOME-DB front end
 * application.
 *
 * When calling this function, you must specify an already created
 * #GtkWidget, which is the one that will be used for creating the
 * Bonobo control.
 *
 * Returns: the newly created #GnomeDbShellComponent object.
 */
GnomeDbShellComponent *
gnome_db_shell_component_new (GtkWidget *widget)
{
	GnomeDbShellComponent* comp;

	g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);
	
	comp = g_object_new (GNOME_DB_TYPE_SHELL_COMPONENT, NULL);
	return gnome_db_shell_component_construct (comp, widget);
}

/**
 * gnome_db_shell_component_get_control
 */
GnomeDbControl *
gnome_db_shell_component_get_control (GnomeDbShellComponent *comp)
{
	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), NULL);
	return comp->priv->control;
}

/**
 * gnome_db_shell_component_select_connection
 */
gboolean
gnome_db_shell_component_select_connection (GnomeDbShellComponent *comp,
					    const gchar *name,
					    const gchar *username,
					    const gchar *password)
{
	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), FALSE);
	g_return_val_if_fail (CLASS (comp)->select_connection != NULL, FALSE);

	return CLASS (comp)->select_connection (comp, name, username, password);
}

/**
 * gnome_db_shell_component_get_config_control
 */
GnomeDbControl *
gnome_db_shell_component_get_config_control (GnomeDbShellComponent *comp)
{
	g_return_val_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp), NULL);
	g_return_val_if_fail (CLASS (comp)->get_config_control != NULL, NULL);

	return CLASS (comp)->get_config_control (comp);
}

/**
 * gnome_db_shell_component_notify_connection_opened
 */
void
gnome_db_shell_component_notify_connection_opened (GnomeDbShellComponent *comp,
						   const gchar *name,
						   const gchar *username,
						   const gchar *password)
{
	GList *l;
	CORBA_Environment ev;

	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp));

	for (l = clean_shell_list (comp); l; l = l->next) {
		GNOME_Database_UIShell corba_shell = (GNOME_Database_UIShell) l->data;

		g_assert (corba_shell != CORBA_OBJECT_NIL);

		CORBA_exception_init (&ev);
		GNOME_Database_UIShell_notifyConnectionOpened (corba_shell, name, username,
							       password, &ev);
		CORBA_exception_free (&ev);
	}
}

/**
 * gnome_db_shell_component_notify_connection_closed
 */
void
gnome_db_shell_component_notify_connection_closed (GnomeDbShellComponent *comp,
						   const gchar *name,
						   const gchar *username,
						   const gchar *password)
{
	GList *l;
	CORBA_Environment ev;

	g_return_if_fail (GNOME_DB_IS_SHELL_COMPONENT (comp));

	for (l = clean_shell_list (comp); l; l = l->next) {
		GNOME_Database_UIShell corba_shell = (GNOME_Database_UIShell) l->data;

		g_assert (corba_shell != CORBA_OBJECT_NIL);

		CORBA_exception_init (&ev);
		GNOME_Database_UIShell_notifyConnectionClosed (corba_shell, name, username,
							       password, &ev);
		CORBA_exception_free (&ev);
	}
}
