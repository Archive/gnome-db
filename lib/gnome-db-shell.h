/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_shell_h__)
#  define __gnome_db_shell_h__

#include <bonobo/bonobo-object.h>
#include <gtk/gtkwidget.h>
#include <GNOME_Database_UI.h>

typedef struct _GnomeDbShell        GnomeDbShell;

#include <gnome-db-shell-component-client.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SHELL            (gnome_db_shell_get_type())
#define GNOME_DB_SHELL(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SHELL, GnomeDbShell))
#define GNOME_DB_SHELL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SHELL, GnomeDbShellClass))
#define GNOME_DB_IS_SHELL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SHELL))
#define GNOME_DB_IS_SHELL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SHELL))

typedef struct _GnomeDbShellClass   GnomeDbShellClass;
typedef struct _GnomeDbShellPrivate GnomeDbShellPrivate;

struct _GnomeDbShell {
	BonoboObject object;
	GnomeDbShellPrivate *priv;
};

struct _GnomeDbShellClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Database_UIShell__epv epv;
};

GtkType                      gnome_db_shell_get_type (void);
GnomeDbShell                *gnome_db_shell_new (void);

GtkWidget                   *gnome_db_shell_get_window (GnomeDbShell *shell);
void                         gnome_db_shell_add_tab (GnomeDbShell *shell,
						     GnomeDbShellComponentClient *wid,
						     const gchar *label);
void                         gnome_db_shell_activate_tab (GnomeDbShell *shell,
							  GnomeDbShellComponentClient *wid);
GnomeDbShellComponentClient *gnome_db_shell_get_current_tab (GnomeDbShell *shell);

void                         gnome_db_shell_show_shortcut (GnomeDbShell *shell);
void                         gnome_db_shell_hide_shortcut (GnomeDbShell *shell);
void                         gnome_db_shell_show_summary (GnomeDbShell *shell);
void                         gnome_db_shell_hide_summary (GnomeDbShell *shell);

G_END_DECLS

#endif
