/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gtk/gtktable.h>
#include <libgnomedb/gnome-db-connection-selector.h>
#include <libgnomedb/gnome-db-gray-bar.h>
#include <libgnomedb/gnome-db-util.h>
#include <bonobo/bonobo-i18n.h>
#include "gnome-db-summary.h"

#define PARENT_TYPE GTK_TYPE_VBOX

struct _GnomeDbSummaryPrivate {
	GtkWidget *table;
	GtkWidget *title_label;
	GtkWidget *connection_list;
};

static void gnome_db_summary_class_init (GnomeDbSummaryClass *klass);
static void gnome_db_summary_init       (GnomeDbSummary *summary, GnomeDbSummaryClass *klass);
static void gnome_db_summary_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * GnomeDbSummary class implementation
 */

static void
gnome_db_summary_class_init (GnomeDbSummaryClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_summary_finalize;
}

static void
gnome_db_summary_init (GnomeDbSummary *summary, GnomeDbSummaryClass *klass)
{
	g_return_if_fail (GNOME_DB_IS_SUMMARY (summary));

	summary->priv = g_new0 (GnomeDbSummaryPrivate, 1);

	/* set up widgets */
	summary->priv->table = gnome_db_new_table_widget (4, 6, FALSE);
	gtk_box_pack_start (GTK_BOX (summary), summary->priv->table, TRUE, TRUE, 0);

	summary->priv->title_label = gnome_db_gray_bar_new (_("Summary"));
	gtk_widget_show (summary->priv->title_label);
	gtk_table_attach (GTK_TABLE (summary->priv->table), summary->priv->title_label,
			  0, 6, 0, 1, GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 2, 2);

	/* add connection selector */
	summary->priv->connection_list = gnome_db_connection_selector_new ();
	gtk_widget_show (summary->priv->connection_list);
	gtk_table_attach (GTK_TABLE (summary->priv->table), summary->priv->connection_list,
				     5, 6, 3, 4, GTK_FILL, GTK_FILL, 2, 2);
}

static void
gnome_db_summary_finalize (GObject *object)
{
	GnomeDbSummary *summary = (GnomeDbSummary *) object;

	g_return_if_fail (GNOME_DB_IS_SUMMARY (summary));

	/* free memory */
	g_free (summary->priv);
	summary->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_summary_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomeDbSummaryClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_summary_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbSummary),
			0,
			(GInstanceInitFunc) gnome_db_summary_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbSummary", &info, 0);
	}
	return type;
}

/**
 * gnome_db_summary_new
 */
GtkWidget *
gnome_db_summary_new (void)
{
	GnomeDbSummary *summary;

	summary = g_object_new (GNOME_DB_TYPE_SUMMARY, NULL);
	return GTK_WIDGET (summary);
}

/**
 * gnome_db_summary_set_title
 */
void
gnome_db_summary_set_title (GnomeDbSummary *summary, const gchar *title)
{
	g_return_if_fail (GNOME_DB_IS_SUMMARY (summary));
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (summary->priv->title_label), title);
}
