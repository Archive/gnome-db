/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <bonobo/bonobo-i18n.h>
#include <libgda/gda-config.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkpaned.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktogglebutton.h>
#include <libgnomedb/gnome-db-config.h>
#include <libgnomedb/gnome-db-gray-bar.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-window.h>
#include "gnome-db-defs.h"
#include "gnome-db-shell-settings.h"
#include "gnome-db-shortcut.h"

#define PARENT_TYPE GTK_TYPE_VBOX

struct _GnomeDbShellSettingsPrivate {
	GHashTable *loaded_widgets;
	GnomeDbShell *shell;
	GtkWidget *toolbar;
	GtkWidget *title;
	GtkWidget *notebook;

	/* widgets */
	GtkWidget *mw_left;
	GtkWidget *mw_top;
	GtkWidget *mw_width;
	GtkWidget *mw_height;
};

static void gnome_db_shell_settings_class_init (GnomeDbShellSettingsClass *klass);
static void gnome_db_shell_settings_init       (GnomeDbShellSettings *settings,
						GnomeDbShellSettingsClass *klass);
static void gnome_db_shell_settings_finalize   (GObject *object);

static void leave_main_window_pos_cb (GtkToggleButton *toggle, gpointer user_data);
static void save_main_window_pos_cb (GtkToggleButton *toggle, gpointer user_data);
static void shortcut_button_clicked_cb (GnomeDbShortcut *shortcut,
					const gchar *label,
					const gchar *tooltip,
					gpointer user_data);
static void show_shortcut_bar_cb (GtkToggleButton *toggle, gpointer user_data);
static void show_summary_cb (GtkToggleButton *toggle, gpointer user_data);
static void use_fixed_position_cb (GtkToggleButton *toggle, gpointer user_data);

static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */

static void
add_components_to_shortcut (GnomeDbShellSettings *settings)
{
	GList *components;
	GList *node;
	GnomeDbComponentInfo *comp_info;
	gchar *str;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	/* add the 'General' tab */
	gnome_db_shortcut_append (GNOME_DB_SHORTCUT (settings->priv->toolbar),
				  _("General"), _("General"),
				  GTK_STOCK_PREFERENCES,
				  (GnomeDbShortcutSelectFunc) shortcut_button_clicked_cb,
				  settings);

	/* add the other available components */
	components = gnome_db_config_get_component_list (
		"repo_ids.has('IDL:GNOME/Database/UIShellComponent:1.0')");

	for (node = g_list_first (components);
	     node != NULL;
	     node = g_list_next (node)) {
		comp_info = (GnomeDbComponentInfo *) node->data;
		str = g_strdup_printf ("%s/%s",
				       GNOME_DB_SHELL_CONFIG_SECTION_LOAD,
				       comp_info->id);
		if (gnome_db_config_has_key (str)) {
			GdaParameter *param;
			gchar *shell_icon;
			gchar *config_iid;
			GtkWidget *config = NULL;

			/* get parameters */
			param = gda_parameter_list_find (comp_info->properties,
							 "shell_icon");
			shell_icon = gda_value_stringify (gda_parameter_get_value (param));

			param = gda_parameter_list_find (comp_info->properties,
							 "config_control");
			config_iid = gda_value_stringify (gda_parameter_get_value (param));

			/* load the config control */
			config = config_iid ?
				gnome_db_control_widget_new (config_iid, CORBA_OBJECT_NIL) :
				NULL;
			if (GNOME_DB_IS_CONTROL_WIDGET (config)) {
				gtk_widget_show (config);

				/* add the control to the main widget */
				gnome_db_shortcut_append (
					GNOME_DB_SHORTCUT (settings->priv->toolbar),
					comp_info->description,
					comp_info->id,
					shell_icon,
					(GnomeDbShortcutSelectFunc) shortcut_button_clicked_cb,
					(gpointer) settings);
				gtk_notebook_append_page (
					GTK_NOTEBOOK (settings->priv->notebook),
					config, gtk_label_new (comp_info->description));

				g_hash_table_insert (settings->priv->loaded_widgets,
						     g_strdup (comp_info->description),
						     config);
			}

			g_free (shell_icon);
			g_free (config_iid);
		}
		g_free (str);
	}

	gnome_db_config_free_component_list (components);
}

static void
create_main_window_frame (GnomeDbShellSettings *settings, GtkTable *main_table)
{
	GtkWidget *frame;
	GtkWidget *table;
	GtkWidget *button;
	GtkWidget *label;
	GtkWidget *separator;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));
	g_return_if_fail (GTK_IS_TABLE (main_table));

	/* create frame */
	frame = gnome_db_new_frame_widget (_("Main window"));
	gtk_table_attach (GTK_TABLE (main_table), frame, 0, 2, 0, 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 2, 2);
	table = gnome_db_new_table_widget (8, 5, FALSE);
	gtk_container_add (GTK_CONTAINER (frame), table);

	/* basic settings */
	button = gnome_db_new_check_button_widget (_("Show shortcut bar"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (button),
		gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (show_shortcut_bar_cb), settings);
	gtk_table_attach (GTK_TABLE (table), button, 0, 5, 0, 1, GTK_FILL, GTK_FILL, 2, 2);

	button = gnome_db_new_check_button_widget (_("Show summary"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (button),
		gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (show_summary_cb), settings);
	gtk_table_attach (GTK_TABLE (table), button, 0, 5, 1, 2, GTK_FILL, GTK_FILL, 2, 2);

	separator = gnome_db_new_hseparator_widget ();
	gtk_table_attach (GTK_TABLE (table), separator, 0, 5, 2, 3, GTK_FILL, GTK_FILL, 2, 2);

	/* 'Save window position' radio button */
	button = gnome_db_new_radio_button_widget (NULL, _("Save window position"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (button),
		gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (save_main_window_pos_cb), settings);
	gtk_table_attach (GTK_TABLE (table), button, 0, 5, 3, 4,
			  GTK_FILL, GTK_FILL, 0, 0);

	/* fixed position group */
	button = gnome_db_new_radio_button_widget (
		gtk_radio_button_get_group (GTK_RADIO_BUTTON (button)),
		_("Use fixed position/size"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (button),
		gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (use_fixed_position_cb), settings);
	gtk_table_attach (GTK_TABLE (table), button, 0, 5, 4, 5,
			  GTK_FILL, GTK_FILL, 0, 0);

	label = gnome_db_new_label_widget (_("Left"));
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 5, 6, GTK_FILL, GTK_FILL, 0, 0);
	settings->priv->mw_left = gnome_db_new_entry_widget (5, TRUE);
	gtk_table_attach (GTK_TABLE (table), settings->priv->mw_left, 2, 3, 5, 6, 0, 0, 2, 2);

	label = gnome_db_new_label_widget (_("Top"));
	gtk_table_attach (GTK_TABLE (table), label, 3, 4, 5, 6, GTK_FILL, GTK_FILL, 0, 0);
	settings->priv->mw_top = gnome_db_new_entry_widget (5, TRUE);
	gtk_table_attach (GTK_TABLE (table), settings->priv->mw_top, 4, 5, 5, 6, 0, 0, 2, 2);

	label = gnome_db_new_label_widget (_("Width"));
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 6, 7, GTK_FILL, GTK_FILL, 0, 0);
	settings->priv->mw_width = gnome_db_new_entry_widget (5, TRUE);
	gtk_table_attach (GTK_TABLE (table), settings->priv->mw_width, 2, 3, 6, 7, 0, 0, 2, 2);

	label = gnome_db_new_label_widget (_("Height"));
	gtk_table_attach (GTK_TABLE (table), label, 3, 4, 6, 7, GTK_FILL, GTK_FILL, 0, 0);
	settings->priv->mw_height = gnome_db_new_entry_widget (5, TRUE);
	gtk_table_attach (GTK_TABLE (table), settings->priv->mw_height, 4, 5, 6, 7, 0, 0, 2, 2);

	/* disable radio button */
	button = gnome_db_new_radio_button_widget (
		gtk_radio_button_get_group (GTK_RADIO_BUTTON (button)),
		_("Don't set size/position"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (button),
		!gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS) &&
		!gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (leave_main_window_pos_cb), settings);
	gtk_table_attach (GTK_TABLE (table), button, 0, 5, 7, 8,
			  GTK_FILL, GTK_FILL, 0, 0);

	/* set initial values for position entries */
	if (gnome_db_config_get_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS)) {
		gchar bfr[32];

		sprintf (bfr, "%d",
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X1));
		gtk_entry_set_text (GTK_ENTRY (settings->priv->mw_left), bfr);

		sprintf (bfr, "%d",
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y1));
		gtk_entry_set_text (GTK_ENTRY (settings->priv->mw_top), bfr);

		sprintf (bfr, "%d",
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X2) -
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X1));
		gtk_entry_set_text (GTK_ENTRY (settings->priv->mw_width), bfr);

		sprintf (bfr, "%d",
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y2) -
			 gnome_db_config_get_int (GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y1));
		gtk_entry_set_text (GTK_ENTRY (settings->priv->mw_height), bfr);
	}
	else {
		gtk_widget_set_sensitive (settings->priv->mw_left, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_top, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_width, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_height, FALSE);
	}
}

/*
 * Callbacks
 */
static void
leave_main_window_pos_cb (GtkToggleButton *toggle, gpointer user_data)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	if (gtk_toggle_button_get_active (toggle)) {
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS, FALSE);
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS, FALSE);

		gtk_widget_set_sensitive (settings->priv->mw_left, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_top, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_width, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_height, FALSE);
	}
}

static void
save_main_window_pos_cb (GtkToggleButton *toggle, gpointer user_data)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	if (gtk_toggle_button_get_active (toggle)) {
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS, TRUE);
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS, FALSE);

		gtk_widget_set_sensitive (settings->priv->mw_left, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_top, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_width, FALSE);
		gtk_widget_set_sensitive (settings->priv->mw_height, FALSE);
	}
}

static void
shortcut_button_clicked_cb (GnomeDbShortcut *shortcut,
			    const gchar *label,
			    const gchar *tooltip,
			    gpointer user_data)
{
	GtkWidget *widget;
	gint page_num;
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	widget = g_hash_table_lookup (settings->priv->loaded_widgets, label);
	if (!GTK_IS_WIDGET (widget))
		return;

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (settings->priv->notebook), widget);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (settings->priv->notebook), page_num);

	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (settings->priv->title), label);
}

static void
show_shortcut_bar_cb (GtkToggleButton *toggle, gpointer user_data)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	if (gtk_toggle_button_get_active (toggle))
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT, TRUE);
	else
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SHORTCUT, FALSE);
}

static void
show_summary_cb (GtkToggleButton *toggle, gpointer user_data)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	if (gtk_toggle_button_get_active (toggle))
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY, TRUE);
	else
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SHOW_SUMMARY, FALSE);
}

static void
use_fixed_position_cb (GtkToggleButton *toggle, gpointer user_data)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) user_data;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	if (gtk_toggle_button_get_active (toggle)) {
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_SAVE_WINDOW_POS, FALSE);
		gnome_db_config_set_boolean (GNOME_DB_SHELL_CONFIG_FIXED_WINDOW_POS, TRUE);
		gnome_db_config_set_int (
			GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X1,
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_left))));
		gnome_db_config_set_int (
			GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y1,
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_top))));
		gnome_db_config_set_int (
			GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_X2,
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_left))) +
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_width))));
		gnome_db_config_set_int (
			GNOME_DB_SHELL_CONFIG_MAIN_WINDOW_Y2,
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_top))) +
			atoi (gtk_entry_get_text (GTK_ENTRY (settings->priv->mw_height))));

		gtk_widget_set_sensitive (settings->priv->mw_left, TRUE);
		gtk_widget_set_sensitive (settings->priv->mw_top, TRUE);
		gtk_widget_set_sensitive (settings->priv->mw_width, TRUE);
		gtk_widget_set_sensitive (settings->priv->mw_height, TRUE);
	}
}

/*
 * GnomeDbShellSettings class implementation
 */

static void
gnome_db_shell_settings_class_init (GnomeDbShellSettingsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_shell_settings_finalize;
}

static void
gnome_db_shell_settings_init (GnomeDbShellSettings *settings,
			      GnomeDbShellSettingsClass *klass)
{
	GtkWidget *paned;
	GtkWidget *table;
	GtkWidget *box;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	/* allocate internal structure */
	settings->priv = g_new0 (GnomeDbShellSettingsPrivate, 1);
	settings->priv->shell = NULL;
	settings->priv->loaded_widgets = g_hash_table_new (g_str_hash, g_str_equal);

	/* set up widgets */
	paned = gnome_db_new_hpaned_widget ();
	gtk_box_pack_start (GTK_BOX (settings), paned, TRUE, TRUE, 0);

	settings->priv->toolbar = gnome_db_shortcut_new ();
	gtk_paned_add1 (GTK_PANED (paned), settings->priv->toolbar);
	gtk_widget_show (settings->priv->toolbar);

	box = gnome_db_new_vbox_widget (FALSE, 0);

	settings->priv->title = gnome_db_gray_bar_new (_("General"));
	gtk_widget_show (settings->priv->title);
	gtk_box_pack_start(GTK_BOX (box), settings->priv->title, FALSE, FALSE, 0);

	settings->priv->notebook = gnome_db_new_notebook_widget ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (settings->priv->notebook), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (settings->priv->notebook), FALSE);
	gtk_box_pack_start (GTK_BOX (box), settings->priv->notebook, TRUE, TRUE, 0);

	gtk_paned_add2 (GTK_PANED (paned), box);

	/* create the default configuration tab */
	table = gnome_db_new_table_widget (3, 2, FALSE);

	create_main_window_frame (settings, GTK_TABLE (table));

	gtk_notebook_append_page (GTK_NOTEBOOK (settings->priv->notebook),
				  table, gtk_label_new (_("General")));
	g_hash_table_insert (settings->priv->loaded_widgets, g_strdup (_("General")), table);
}

static void
gnome_db_shell_settings_finalize (GObject *object)
{
	GnomeDbShellSettings *settings = (GnomeDbShellSettings *) object;

	g_return_if_fail (GNOME_DB_IS_SHELL_SETTINGS (settings));

	/* free memory */
	g_hash_table_destroy (settings->priv->loaded_widgets);
	settings->priv->loaded_widgets = NULL;

	if (GNOME_DB_IS_SHELL (settings->priv->shell)) {
		bonobo_object_unref (BONOBO_OBJECT (settings->priv->shell));
		settings->priv->shell = NULL;
	}

	g_free (settings->priv);
	settings->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_shell_settings_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomeDbShellSettingsClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_shell_settings_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbShellSettings),
			0,
			(GInstanceInitFunc) gnome_db_shell_settings_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbShellSettings", &info, 0);
	}
	return type;
}

/**
 * gnome_db_shell_settings_new
 */
GtkWidget *
gnome_db_shell_settings_new (GnomeDbShell *shell)
{
	GnomeDbShellSettings *settings;

	settings = g_object_new (GNOME_DB_TYPE_SHELL_SETTINGS, NULL);
	if (GNOME_DB_IS_SHELL (shell)) {
		bonobo_object_ref (BONOBO_OBJECT (shell));
		settings->priv->shell = shell;
	}

	add_components_to_shortcut (settings);

	return GTK_WIDGET (settings);
}
