CREATE SEQUENCE "salesrep_id_seq" start 3 increment 1 maxvalue 2147483647 minvalue 1  cache 1 ;
SELECT nextval ('"salesrep_id_seq"');
CREATE SEQUENCE "customers_id_seq" start 3 increment 1 maxvalue 2147483647 minvalue 1  cache 1 ;
SELECT nextval ('"customers_id_seq"');
CREATE SEQUENCE "orders_id_seq" start 1 increment 1 maxvalue 2147483647 minvalue 1  cache 1 ;
SELECT nextval ('"orders_id_seq"');
CREATE SEQUENCE "warehouses_id_seq" start 1 increment 1 maxvalue 2147483647 minvalue 1  cache 1 ;
SELECT nextval ('"warehouses_id_seq"');
CREATE TABLE "salesrep" (
	"id" int4 DEFAULT nextval('salesrep_id_seq'::text) NOT NULL,
	"name" character varying(30) NOT NULL,
	"year_salary" int4 NOT NULL,
	"date_empl" date
);
CREATE TABLE "customers" (
	"id" int4 DEFAULT nextval('customers_id_seq'::text) NOT NULL,
	"name" character varying(30) NOT NULL,
	"city" character varying(30),
	"served_by" int4
);
CREATE TABLE "orders" (
	"id" int4 DEFAULT nextval('orders_id_seq'::text) NOT NULL,
	"customer_id" int4,
	"creation_date" date NOT NULL,
	"delivery_before" date,
	"delivery_at" date
);
CREATE TABLE "products" (
	"ref" character varying(15) NOT NULL,
	"name" character varying(20) NOT NULL,
	"price" float8,
	"wh_stored" int4,
	PRIMARY KEY ("ref")
);
CREATE TABLE "order_contents" (
	"order_id" int4 NOT NULL,
	"product_ref" character varying(15) NOT NULL,
	"qty" int4,
	"discount" float8
);
CREATE TABLE "warehouses" (
	"id" int4 DEFAULT nextval('warehouses_id_seq'::text) NOT NULL,
	"name" character varying(30) NOT NULL,
	"location" character varying(30)
);
INSERT INTO "salesrep" VALUES (1,'John Doe',300,'2001-06-01');
INSERT INTO "salesrep" VALUES (2,'Allan Parkson',320,'2001-06-15');
INSERT INTO "salesrep" VALUES (3,'Ben Callaway',250,'2001-06-15');
INSERT INTO "customers" VALUES (2,'Mad Max','Dallas',2);
INSERT INTO "customers" VALUES (3,'Brian Dawson','Chicago',1);
INSERT INTO "customers" VALUES (1,'Franck Pallo','New York',1);
INSERT INTO "orders" VALUES (1,2,'2001-06-07',NULL,NULL);
INSERT INTO "products" VALUES ('CHA001','Small red Chair',123.56,1);
INSERT INTO "products" VALUES ('CHA002','Big red armchair',256.75,1);
INSERT INTO "products" VALUES ('TAB001','Large Wood table',230,1);
INSERT INTO "products" VALUES ('TV001','Small color TV',120.2,1);
INSERT INTO "order_contents" VALUES (1,'CHA001',4,0.05);
INSERT INTO "order_contents" VALUES (1,'CHA002',2,0.15);
INSERT INTO "order_contents" VALUES (1,'TAB001',1,0.05);
INSERT INTO "order_contents" VALUES (1,'TV001',1,0);
INSERT INTO "warehouses" VALUES (1,'Main Storage building','NYC');
CREATE UNIQUE INDEX "salesrep_id_key" on "salesrep" using btree ( "id" "int4_ops" );
CREATE UNIQUE INDEX "customers_id_key" on "customers" using btree ( "id" "int4_ops" );
CREATE UNIQUE INDEX "orders_id_key" on "orders" using btree ( "id" "int4_ops" );
CREATE UNIQUE INDEX "warehouses_id_key" on "warehouses" using btree ( "id" "int4_ops" );
