/* datadisplay-common.h
 *
 * Copyright (C) 2000 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __DATA_DISPLAY_COMMON__
#define __DATA_DISPLAY_COMMON__

#include <gnome.h>
#include <libgda/libgda.h>
#include <gmodule.h>
#include "data-entry.h"

G_BEGIN_DECLS

typedef struct _DataDisplayFns DataDisplayFns;
struct _DataDisplayFns
{
	gchar          *descr;
	gchar          *detailled_descr;
	gchar          *plugin_name;	        /* NULL if builtin Data display */
	gchar          *plugin_file;	        /* NULL if builtin Data display */
	GModule        *lib_handle;	        /* NULL if builtin Data display */
	gchar          *version;	        /* NULL if builtin Data display */
	gchar        *(*get_unique_key)     ();	/* NULL if builtin Data display */
	guint           nb_gda_type;
	GdaValueType   *valid_gda_types;
	gboolean        expand_widget;

	/* sql_from_value() and str_from_value() may return NULL */
	DataEntry   *(*widget_from_value)   (const GdaValue * value);
	GdaValue    *(*value_from_widget)   (DataEntry * wid);
	void         (*widget_update)       (DataEntry * wid, const GdaValue * value);
	gchar       *(*sql_from_value)      (const GdaValue * value);
	gchar       *(*str_from_value)      (const GdaValue * value);
};

G_END_DECLS


#endif
