/* query-env.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2001 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query-env.h"
#include "choicecombo.h"
#include "gladefeditor.h"
#include "marshal.h"

static void query_env_class_init (QueryEnvClass * class);
static void query_env_init (QueryEnv * qe);
static void query_env_finalize (GObject   * object);

/* TODO FM: check usage of JOIN_CROSS; related to not used links in query */

/*
 *
 * Main functions for the object
 *
 */

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass   *parent_class = NULL;

enum
{
	MODIF_TABLE_CHANGED,
	NAME_CHANGED,
	TYPE_CHANGED,
	MODIFIED,
	ACTIONS_CHANGED,
	LAST_SIGNAL
};

static gint query_env_signals[LAST_SIGNAL] = { 0, 0, 0, 0 };

guint
query_env_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryEnvClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_env_class_init,
			NULL,
			NULL,
			sizeof (QueryEnv),
			0,
			(GInstanceInitFunc) query_env_init
		};		

		type = g_type_register_static (G_TYPE_OBJECT, "QueryEnv", &info, 0);
	}
	return type;
}

static void m_modified (QueryEnv *qev);

static void
query_env_class_init (QueryEnvClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	query_env_signals[MODIF_TABLE_CHANGED] =
		g_signal_new ("modif_table_changed",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (QueryEnvClass, modif_table_changed),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE, 0);

	query_env_signals[NAME_CHANGED] =
		g_signal_new ("name_changed",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (QueryEnvClass, name_changed),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE, 0);

	query_env_signals[TYPE_CHANGED] =
		g_signal_new ("type_changed",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (QueryEnvClass, type_changed),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE, 0);

	query_env_signals[MODIFIED] =
		g_signal_new ("modified",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (QueryEnvClass, modified),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE, 0);

	query_env_signals[ACTIONS_CHANGED] =
		g_signal_new ("actions_changed",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (QueryEnvClass, actions_changed),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE, 0);

	class->modif_table_changed = m_modified;
	class->name_changed = m_modified;
	class->type_changed = m_modified;
	class->actions_changed = m_modified;
	class->modified = NULL;

	object_class->finalize = query_env_finalize;
}

static void 
m_modified (QueryEnv *qev)
{
#ifdef debug_signal
	g_print (">> 'MODIFIED' from m_modified\n");
#endif
	g_signal_emit (G_OBJECT (qev), query_env_signals[MODIFIED], 0);
#ifdef debug_signal
	g_print ("<< 'MODIFIED' from m_modified\n");
#endif
}

static void
query_env_init (QueryEnv * env)
{
	env->q = NULL;
	env->modif_table = NULL;
	env->name = NULL;
	env->descr = NULL;
	env->actions =
		QUERY_ACTION_FIRST | QUERY_ACTION_LAST | QUERY_ACTION_PREV |
		QUERY_ACTION_NEXT | QUERY_ACTION_REFRESH | QUERY_ACTION_EDIT |
		QUERY_ACTION_VIEWALL;
	env->form = NULL;
	env->form_is_default = FALSE;
	env->execs_list = NULL;
}


static void query_view_removed_cb (Query * q, QueryView * qv, QueryEnv * qe);
GObject   *
query_env_new (Query * q)
{
	GObject   *obj;
	QueryEnv *qe;

	g_return_val_if_fail (q != NULL, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	obj = g_object_new (QUERY_ENV_TYPE, NULL);
	qe = QUERY_ENV (obj);
	qe->q = q;

	query_add_env (q, obj);

	/* connection to signals from Query */
	g_signal_connect (G_OBJECT (q), "query_view_removed",
			  G_CALLBACK (query_view_removed_cb), qe);
	return obj;
}

static void 
query_view_removed_cb (Query * q, QueryView * qv, QueryEnv * qe)
{
	if (IS_DB_TABLE (qv->obj) && (qe->modif_table == DB_TABLE (qv->obj))) {
		/* look to see if there is an alias of that table among the
		   other QueryViews of the Query */
		gboolean found = FALSE;
		GSList *list = q->views;
		while (list && !found) {
			if (IS_DB_TABLE (QUERY_VIEW (list->data)->obj) &&
			    (DB_TABLE (QUERY_VIEW (list->data)->obj) == qe->modif_table))
				found = TRUE; /* alias found! */
			list = g_slist_next (list);
		}
		if (!found)
			query_env_set_modif_table (qe, NULL);
	}
}

void
query_env_set_modif_table (QueryEnv * env, DbTable * table)
{
	env->modif_table = table;
#ifdef debug_signal
	g_print (">> 'MODIF_TABLE_CHANGED' from query_env_set_modif_table\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[MODIF_TABLE_CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'MODIF_TABLE_CHANGED' from query_env_set_modif_table\n");
#endif
}

static void obj_destroyed_cb (QueryEnv * env, GObject   * obj); /* GWeakNotify */
static void
query_env_finalize (GObject   * object)
{
	QueryEnv *qe;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_ENV (object));

	qe = QUERY_ENV (object);

	/* we destroy all the QueryExec objects */
	if (qe->execs_list) {
		list = qe->execs_list;
		while (list) {
			g_object_weak_unref (G_OBJECT (list->data), (GWeakNotify) (obj_destroyed_cb), qe);
			g_object_unref (G_OBJECT (qe->execs_list->data));
			list = g_slist_next (list);
		}
		g_slist_free (qe->execs_list);
		qe->execs_list = NULL;
	}
	

	g_signal_handlers_disconnect_by_func (G_OBJECT (qe->q),
					      G_CALLBACK (query_view_removed_cb), qe);

	query_del_env (qe->q, G_OBJECT (qe));

	if (qe->name)
		g_free (qe->name);

	if (qe->descr)
		g_free (qe->descr);


	/* for the parent class */
	parent_class->finalize (object);
}

void
query_env_register_exec_obj (QueryEnv * env, GObject   * obj)
{
	env->execs_list = g_slist_append (env->execs_list, obj);
	g_object_weak_ref (obj, (GWeakNotify) (obj_destroyed_cb), env);
}

static void
obj_destroyed_cb (QueryEnv * env, GObject   * obj)
{
	env->execs_list = g_slist_remove (env->execs_list, obj);
}


void
query_env_set_name (QueryEnv * env, const gchar * name)
{
	g_return_if_fail (env != NULL);
	if (env->name)
		g_free (env->name);

	if (name)
		env->name = g_strdup (name);
	else
		env->name = NULL;
#ifdef debug_signal
	g_print (">> 'NAME_CHANGED' from query_env_set_name\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[NAME_CHANGED], 0);
#ifdef debug_signal
	g_print (">> 'NAME_CHANGED' from query_env_set_name\n");
#endif
}


void
query_env_set_descr (QueryEnv * env, const gchar * descr)
{
	g_return_if_fail (env != NULL);
	if (env->descr)
		g_free (env->descr);

	if (descr)
		env->descr = g_strdup (descr);
	else
		env->descr = NULL;

#ifdef debug_signal
	g_print (">> 'NAME_CHANGED' from query_env_set_descr\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[NAME_CHANGED], 0);
#ifdef debug_signal
	g_print (">> 'NAME_CHANGED' from query_env_set_descr\n");
#endif
}

void
query_env_set_type (QueryEnv * env, gboolean form_is_default)
{
	g_return_if_fail (IS_QUERY_ENV (env));
	env->form_is_default = form_is_default;
#ifdef debug_signal
	g_print (">> 'TYPE_CHANGED' from query_env_set_type\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[TYPE_CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'TYPE_CHANGED' from query_env_set_type\n");
#endif
}

void 
query_env_set_actions (QueryEnv * env, guint actions)
{
	env->actions = actions;
#ifdef debug_signal
	g_print (">> 'ACTIONS_CHANGED' from query_env_set_actions\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[ACTIONS_CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'ACTIONS_CHANGED' from query_env_set_actions\n");
#endif	
}

void 
query_env_add_action (QueryEnv * env, QueryActions action)
{
	if (! (env->actions & action)) {
		env->actions = env->actions | action;
#ifdef debug_signal
		g_print (">> 'ACTIONS_CHANGED' from query_env_add_action\n");
#endif
		g_signal_emit (G_OBJECT (env), query_env_signals[ACTIONS_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'ACTIONS_CHANGED' from query_env_add_action\n");
#endif	
	}
}

void
query_env_del_action (QueryEnv * env, QueryActions action)
{
	if (env->actions & action) {
		env->actions = env->actions & (~action);
#ifdef debug_signal
	g_print (">> 'ACTIONS_CHANGED' from query_env_del_action\n");
#endif
	g_signal_emit (G_OBJECT (env), query_env_signals[ACTIONS_CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'ACTIONS_CHANGED' from query_env_del_action\n");
#endif	
	}
}

QueryEnv  *
query_env_build_from_xml_tree (ConfManager * conf, xmlNodePtr toptree)
{
	Query *q;
	QueryEnv *env;
	gchar *str;
	
	str = xmlGetProp (toptree, "query_id");
	q = query_find_from_xml_name (conf, NULL, str);
	g_free (str);

	/* q SHOULD NOT be NULL */
	g_assert (q);
	
	env = QUERY_ENV (query_env_new (q));
	
	str = xmlGetProp (toptree, "modif_table");
	if (str) {
		DbTable *table = database_find_table_from_xml_name (conf->db, str);
		query_env_set_modif_table (env, table);
		g_free (str);
	}

	str = xmlGetProp (toptree, "name");
	if (str) {
		query_env_set_name (env, str);
		g_free (str);
	}

	str = xmlGetProp (toptree, "descr");
	if (str) {
		query_env_set_descr (env, str);
		g_free (str);
	}

	str = xmlGetProp (toptree, "actions");
	if (str) {
		query_env_set_actions (env, atoi (str));
		g_free (str);
	}
	
	str = xmlGetProp (toptree, "form_default");
	if (str) {
		query_env_set_type (env, (*str == 't') ? TRUE : FALSE);
		g_free (str);
	}

	return env;
} 

void 
query_env_build_xml_tree (QueryEnv * env, xmlNodePtr toptree)
{
	xmlNodePtr node;
	gchar *str;

	node = xmlNewChild (toptree, NULL, "QueryEnv", NULL);

	str = query_env_get_xml_id (env);
	xmlSetProp (node, "id", str);
	g_free (str);

	str = query_get_xml_id (env->q);
	xmlSetProp (node, "query_id", str);
	g_free (str);

	if (env->modif_table) {
		str = db_table_get_xml_id (env->modif_table);
		xmlSetProp (node, "modif_table", str);
		g_free (str);
	}

	if (env->name)
		xmlSetProp (node, "name", env->name);

	if (env->descr)
		xmlSetProp (node, "descr", env->descr);

	str = g_strdup_printf ("%d", env->actions);
	xmlSetProp (node, "actions", str);
	g_free (str);

	if (env->form_is_default)
		xmlSetProp (node, "form_default", "t");
	else
		xmlSetProp (node, "form_default", "f");

	if (env->form) {
		/* FIXME: the Glade part */
	}
}

gchar *
query_env_get_xml_id (QueryEnv * env)
{
	gchar *str;

	g_return_val_if_fail (env, NULL);
	g_return_val_if_fail (IS_QUERY_ENV (env), NULL);

	str = g_strdup_printf ("QU%d:QE%d", env->q->id,
			       g_slist_index (env->q->envs, env));
	return str;
}
