/* relship-view.c
 *
 * Copyright (C) 2002 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "relship.h"
#include "relship-view.h"
#include "canvas-query-view.h"
#include "canvas-query-join.h"
#include "canvas-field.h"
#include "marshal.h"

/*
 * 
 * RelShipView object
 * 
 */

enum
{
	ITEM_MOVED,
	LAST_SIGNAL
};

static gint relship_signals[LAST_SIGNAL] = { 0 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

static void relship_view_class_init (RelShipViewClass * class);
static void relship_view_init       (RelShipView * rs);
static void relship_view_dispose    (GObject   * object);
static void relship_view_post_init  (RelShipView * rs);

guint
relship_view_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (RelShipViewClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) relship_view_class_init,
			NULL,
			NULL,
			sizeof (RelShipView),
			0,
			(GInstanceInitFunc) relship_view_init
		};		

		type = g_type_register_static (GNOME_TYPE_CANVAS, "RelShipView", &info, 0);
	}
	return type;
}

static void
relship_view_class_init (RelShipViewClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	relship_signals[ITEM_MOVED] =
		g_signal_new ("item_moved",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (RelShipViewClass, item_moved),
				NULL, NULL,
				marshal_VOID__POINTER, G_TYPE_NONE, 1,
				G_TYPE_POINTER);

	class->item_moved = NULL;

	object_class->dispose = relship_view_dispose;
}


static void
relship_view_init (RelShipView * rs)
{
	rs->query = NULL;
	rs->items = NULL;
	rs->background = NULL;

	rs->xmouse = 50.;
	rs->ymouse = 50.;
}

static void widget_size_changed_cb(GtkWidget *wid, GtkAllocation *alloc, RelShipView *rs);
static int canvas_event(GnomeCanvasItem *item, GdkEvent *event, RelShipView * rs);
GtkWidget *
relship_view_new (Query *q) 
{
	GObject   *obj;
	RelShipView *rsv;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	obj = g_object_new (RELSHIP_VIEW_TYPE, NULL);
	/* obj = g_object_new (RELSHIP_VIEW_TYPE, "aa", TRUE, NULL); */
	rsv = RELSHIP_VIEW (obj);

	rsv->query = q;
	relship_view_post_init (rsv);

	g_signal_connect (G_OBJECT (rsv),"size_allocate",
			  G_CALLBACK(widget_size_changed_cb), rsv);

	g_signal_connect (G_OBJECT (rsv->background),"event",
			  G_CALLBACK(canvas_event), rsv);

	return GTK_WIDGET (obj);
}

/* sets the background canvas item to occupy at least the whole window so the
   user can't click outside of it. */ 
static void widget_size_changed_cb(GtkWidget *wid, GtkAllocation *alloc, RelShipView *rsv)
{
	double wx1, wy1, wx2, wy2;
	double sx1, sy1, sx2, sy2;

	gnome_canvas_get_scroll_region (GNOME_CANVAS (rsv), &sx1, &sy1, &sx2, &sy2);
	gnome_canvas_window_to_world (GNOME_CANVAS (rsv), 0., 0., &wx1, &wy1);
	gnome_canvas_window_to_world (GNOME_CANVAS (rsv), alloc->width, alloc->height, &wx2, &wy2);

	if (wx1 > sx1) wx1 = sx1;
	if (wy1 > sy1) wy1 = sy1;
	if (wx2 < sx2) wx2 = sx2;
	if (wy2 < sy2) wy2 = sy2;
	
	gnome_canvas_item_set (rsv->background, 
			       "x1", wx1,
			       "y1", wy1,
			       "x2", wx2,
			       "y2", wy2,
			       NULL);
}


static GtkWidget *build_context_menu (RelShipView * rsv);
static int 
canvas_event(GnomeCanvasItem *item, GdkEvent *event, RelShipView * rsv)
{
	gboolean done = TRUE;

	if (g_object_get_data (G_OBJECT (gnome_canvas_root (GNOME_CANVAS (rsv))), "dragged_from")) {
		/* Dragging cancelled */
		g_object_set_data (G_OBJECT (gnome_canvas_root (GNOME_CANVAS (rsv))), "dragged_from", NULL);
	}

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (((GdkEventButton *) event)->button == 3) {
			GtkWidget *menu;

			rsv->xmouse = ((GdkEventButton *) event)->x;
			rsv->ymouse = ((GdkEventButton *) event)->y;
			menu = build_context_menu (rsv);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
					NULL, NULL, ((GdkEventButton *)event)->button,
					((GdkEventButton *)event)->time);
			
			done = FALSE;
			break;
		}
	default:
		done = FALSE;
		break;
	}

	return done;	
}


static void add_query_view_menu_cb (GtkMenuItem *mitem, RelShipView * rsv);
static GtkWidget *
build_context_menu (RelShipView * rsv)
{
	GtkWidget *menu, *entry, *menu2;
	GSList *list;

	menu = gtk_menu_new ();

	/* Tables */
	entry = gtk_menu_item_new_with_label (_("Add a table"));
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);

	menu2 = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (entry), menu2);

	list = rsv->query->conf->db->tables;
	while (list) {
		entry = gtk_menu_item_new_with_label (DB_TABLE (list->data)->name);
		gtk_menu_append (GTK_MENU (menu2), entry);
		gtk_widget_show (entry);
		g_object_set_data (G_OBJECT (entry), "qv", list->data);
		g_signal_connect (G_OBJECT (entry), "activate", 
				    G_CALLBACK (add_query_view_menu_cb), rsv); 
		list = g_slist_next (list);
	}

	/* Queries */
	if (rsv->query != QUERY (rsv->query->conf->top_query)) {
		entry = gtk_menu_item_new_with_label (_("Add a query"));
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		
		menu2 = gtk_menu_new ();
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (entry), menu2);

		/* Adding the sub queries */
		list = rsv->query->sub_queries;
		while (list) {
			entry = gtk_menu_item_new_with_label (QUERY (list->data)->name);
			gtk_menu_append (GTK_MENU (menu2), entry);
			gtk_widget_show (entry);
			g_object_set_data (G_OBJECT (entry), "qv", list->data);
			g_signal_connect (G_OBJECT (entry), "activate",
					    G_CALLBACK (add_query_view_menu_cb), rsv);
			list = g_slist_next (list);
		}
		
		/* Adding the parent query if possible */
		if (rsv->query->parent != QUERY (rsv->query->conf->top_query)) {
			entry = gtk_menu_item_new_with_label (rsv->query->parent->name);
			gtk_menu_append (GTK_MENU (menu2), entry);
			gtk_widget_show (entry);
			g_object_set_data (G_OBJECT (entry), "qv", rsv->query->parent);
			g_signal_connect (G_OBJECT (entry), "activate",
					    G_CALLBACK (add_query_view_menu_cb), rsv);
		}
	}

	/* A "Print" entry */
	entry = gtk_menu_item_new_with_label (_("Print"));
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);
	gtk_widget_set_sensitive (entry, FALSE);

	return menu;
}

static void 
add_query_view_menu_cb (GtkMenuItem *mitem, RelShipView * rsv)
{
	GObject   *obj;
	QueryView *qv;
	
	obj = G_OBJECT (g_object_get_data (G_OBJECT (mitem), "qv"));

	qv = query_add_view_with_obj (rsv->query, obj);	

	/* set the item at the position the mouse was when the context menu was opened */
	relship_item_set_position (RELSHIP (relship_find (rsv->query)), G_OBJECT (qv), 
				   rsv->xmouse, rsv->ymouse);
}

static void query_view_added_cb (Query *q, QueryView *view, RelShipView *rsv);
static void query_view_removed_notify (RelShipView *rsv, GnomeCanvasItem *item); /* GWeakNotify */
static void query_join_added_cb (Query *q, QueryJoin *join, RelShipView *rsv);
static void query_join_removed_notify (RelShipView *rsv, GnomeCanvasItem *item); /* GWeakNotify */
static void query_destroy_cb (RelShipView *rsv, Query *q); /* GWeakNotify */
static void item_moved_cb (CanvasBase *item, RelShipView *rsv);
static void drag_action_cb (CanvasBase *repport, CanvasBase *drag_to, CanvasBase * drag_from, RelShipView *rsv);
static void
relship_view_dispose (GObject   * object)
{
	RelShipView *rsv;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_RELSHIP_VIEW (object));

	rsv = RELSHIP_VIEW (object);

	if (rsv->query) {
		/* Weak ref on the query */
		g_object_weak_unref (G_OBJECT (rsv->query), (GWeakNotify) query_destroy_cb, rsv);
		/* connect for the QueryViews management */
		g_signal_handlers_disconnect_by_func (G_OBJECT (rsv->query), 
						      G_CALLBACK (query_view_added_cb), rsv);
		

		/* connect for the QueryJoins management */
		g_signal_handlers_disconnect_by_func (G_OBJECT (rsv->query), 
						      G_CALLBACK (query_join_added_cb), rsv);
		
		rsv->query = NULL;
	}
	

	/* get rid of the GnomeCanvasItems */
	list = rsv->items;
	while (list) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
						      G_CALLBACK (drag_action_cb), rsv);
		if (IS_CANVAS_QUERY_VIEW (list->data))
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
							      G_CALLBACK (item_moved_cb), rsv);

		list = g_slist_next (list);
	}
	if (rsv->items) {
		g_slist_free (rsv->items);
		rsv->items = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}



static void 
relship_view_post_init  (RelShipView * rsv)
{
	GSList *list;
	gdouble x1, y1, x2, y2;

	gnome_canvas_get_scroll_region (GNOME_CANVAS (rsv), &x1, &y1, &x2, &y2);
	rsv->background = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rsv)),
						GNOME_TYPE_CANVAS_RECT,
						"x1", 0.,
						"y1", 0.,
						"x2", 10.,
						"y2", 10.,
						"fill_color", "gray90",
						"outline_color", "black",
						"width_units", 1.0,
						NULL);
	gnome_canvas_item_lower_to_bottom (rsv->background);

	/* display all the QueryViews which are present */
	list = rsv->query->views;
	while (list) {
		query_view_added_cb (rsv->query, QUERY_VIEW (list->data), rsv);
		
		list = g_slist_next (list);
	}

	/* display all the QueryJoins which are present */
	list = rsv->query->joins;
	while (list) {
		GSList *list2;
		list2 = (GSList *) (list->data);
		while (list2) {
			query_join_added_cb (rsv->query, QUERY_JOIN (list2->data), rsv);
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}

	/* Weak ref on the query */
	g_object_weak_ref (G_OBJECT (rsv->query), (GWeakNotify) query_destroy_cb, rsv);

	/* connect to add an item when a QueryView is added */
	g_signal_connect (G_OBJECT (rsv->query), "query_view_added",
			  G_CALLBACK (query_view_added_cb), rsv);

	/* connect to add an item when a QueryJoin is added */
	g_signal_connect (G_OBJECT (rsv->query), "join_created",
			  G_CALLBACK (query_join_added_cb), rsv);
}

static void 
query_view_added_cb (Query *q, QueryView *view, RelShipView *rsv)
{
	gdouble x = 50., y = 50.;
	GnomeCanvasItem *item;
	RelShip *rs;
	RelShipItemData *id;

	rs = RELSHIP (relship_find (q));
	id = relship_find_item (rs, G_OBJECT (view));
	x = id->x;
	y = id->y;

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rsv)),
				      CANVAS_QUERY_VIEW_TYPE,
				      "query", rsv->query,
				      "query_view", view,
				      "x", x,
				      "y", y,
				      NULL);

	rsv->items = g_slist_append (rsv->items, item);
				 
	g_signal_connect (G_OBJECT (item), "moved",
			  G_CALLBACK (item_moved_cb), rsv);

	g_signal_connect (G_OBJECT (item), "drag_action",
			  G_CALLBACK (drag_action_cb), rsv);

	g_object_weak_ref (G_OBJECT (item), (GWeakNotify) query_view_removed_notify, rsv);
}

static void 
query_view_removed_notify (RelShipView *rs, GnomeCanvasItem *item)
{
	rs->items = g_slist_remove (rs->items, item);
}


static void 
item_moved_cb (CanvasBase *item, RelShipView *rs)
{
	double ix1, iy1, ix2, iy2;
	double sx1, sy1, sx2, sy2;
	gboolean changed = FALSE;

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (item), &ix1, &iy1, &ix2, &iy2);
	gnome_canvas_get_scroll_region (GNOME_CANVAS (rs), &sx1, &sy1, &sx2, &sy2);

	if (ix1 < sx1) {
		sx1 = ix1 - 2.;
		changed = TRUE;
	}

	if (iy1 < sy1) {
		sy1 = iy1 - 2.;
		changed = TRUE;
	}

	if (ix2 > sx2) {
		sx2 = ix2 + 2.;
		changed = TRUE;
	}

	if (iy2 > sy2) {
		sy2 = iy2 + 2.;
		changed = TRUE;
	}

	if (changed) {
		/* FIXME: there is a bug in the canvas because this function displays trash */
		gnome_canvas_set_scroll_region (GNOME_CANVAS (rs), sx1, sy1, sx2, sy2);
		
		/* keep the background layer at a good dimension */
		gnome_canvas_item_set (rs->background, 
				       "x1", sx1,
				       "y1", sy1,
				       "x2", sx2,
				       "y2", sy2,
				       NULL);
	}

#ifdef debug_signal
	g_print (">> 'ITEM_MOVED' from %s::item_moved_cb\n", __FILE__);
#endif
	g_signal_emit (G_OBJECT (rs), relship_signals[ITEM_MOVED], 0, item);
#ifdef debug_signal
	g_print ("<< 'ITEM_MOVED' from %s::item_moved_cb\n", __FILE__);
#endif
}

static void 
drag_action_cb (CanvasBase *repport, CanvasBase *drag_from, CanvasBase * drag_to, RelShipView *rs)
{
	g_return_if_fail (IS_RELSHIP_VIEW (rs));

	/* Dragging of field to field */
	if (IS_CANVAS_FIELD (drag_from) && IS_CANVAS_FIELD (drag_to)) {
		if (CANVAS_FIELD (drag_from)->cqv == CANVAS_FIELD (drag_to)->cqv) {
			gchar *str;
			GtkWidget *dlg;

			if (IS_DB_FIELD (CANVAS_FIELD (drag_from)->field))
				str = _("To create a self join, create an alias of this table (or view)\n"
					"and then join the two.");
			else
				str = _("To create a self join, create an alias of this query\n"
					"and then join the two.");
			dlg = gnome_app_message (GNOME_APP (rs->query->conf->app), str);
			gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		}
		else {
			query_add_join (rs->query,
					CANVAS_FIELD (drag_from)->cqv->view,
					CANVAS_FIELD (drag_from)->field,
					CANVAS_FIELD (drag_to)->cqv->view,
					CANVAS_FIELD (drag_to)->field);
		}
	}
}

static void 
query_join_added_cb (Query *q, QueryJoin *join, RelShipView *rs)
{
	GnomeCanvasItem *item;
	
	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rs)),
				      CANVAS_QUERY_JOIN_TYPE,
				      "query", rs->query,
				      "query_join", join,
				      "x", 0.,
				      "y", 0.,
				      NULL);

	rs->items = g_slist_append (rs->items, item);

	g_signal_connect (G_OBJECT (item), "drag_action",
			  G_CALLBACK (drag_action_cb), rs);

	g_object_weak_ref (G_OBJECT (item), (GWeakNotify) query_join_removed_notify, rs);
}

static void 
query_join_removed_notify (RelShipView *rs, GnomeCanvasItem *item)
{
	rs->items = g_slist_remove (rs->items, item);
}


static void 
query_destroy_cb (RelShipView *rs, Query *q)
{
	gtk_widget_destroy (GTK_WIDGET (rs));
}

void 
relship_view_refresh_items (RelShipView * rsv)
{
	GSList *list = rsv->items;
	RelShip *rs;
	gdouble x1, y1, x2, y2;

	rs = RELSHIP (relship_find (rsv->query));

	while (list) {
		GObject   *obj = NULL;
		
		if (IS_CANVAS_QUERY_VIEW (list->data))
			obj = G_OBJECT (CANVAS_QUERY_VIEW (list->data)->view);
		if (obj) {
			RelShipItemData *id;
			id = relship_find_item (rs, obj);

			gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (list->data),
						      &x1, &y1, &x2, &y2);
			if ((x1 != id->x) || (y1 != id->y)) {
				gnome_canvas_item_move (GNOME_CANVAS_ITEM (list->data),
							id->x - x1, id->y - y1);
#ifdef debug_signal
				g_print (">> 'MOVED' from %s::relship_view_refresh_items()\n", __FILE__);
#endif
				g_signal_emit_by_name (G_OBJECT (list->data), "moved");
#ifdef debug_signal
				g_print ("<< 'MOVED' from %s::relship_view_refresh_items()\n", __FILE__);
#endif
			}
		}
		list = g_slist_next (list);
	}
}

GnomeCanvasItem *
relship_view_find_query_view (RelShipView * rs, QueryView *qv)
{
	GnomeCanvasItem *cqv = NULL;
	GSList *list;

	g_return_val_if_fail (IS_RELSHIP_VIEW (rs), NULL);

	list = rs->items;
	while (list && !cqv) {
		if (IS_CANVAS_QUERY_VIEW (list->data) && 
		    (CANVAS_QUERY_VIEW (list->data)->view == qv))
			cqv = GNOME_CANVAS_ITEM (list->data);
		list = g_slist_next (list);
	}

	return cqv;
}
