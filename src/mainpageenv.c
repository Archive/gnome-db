/* mainpageenv.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mainpageenv.h"
#include "query-env.h"
#include "query-env-editor.h"

static void main_page_env_class_init (MainPageEnvClass * class);
static void main_page_env_init (MainPageEnv * wid);
static void main_page_env_finalize (GObject   * object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

typedef struct
{
	ConfManager *conf;
	QueryEnv    *env;
}
Row_Data;

/*
 * static functions 
 */
static void selection_made (GtkWidget * wid, gint row, gint column,
			    GdkEventButton * event, MainPageEnv *mpe);
static void selection_unmade (GtkWidget * wid, gint row, gint column,
			      GdkEventButton * event, MainPageEnv *mpe);
static void remove_env_cb (GObject   * obj, MainPageEnv *mpe);
static void create_env_cb (GObject   * obj, MainPageEnv *mpe);
static void properties_env_cb (GObject   * obj, MainPageEnv *mpe);

static void main_page_env_add_cb (GObject   * obj, QueryEnv * env, MainPageEnv *mpe);
static void main_page_env_drop_cb (GObject   * obj, QueryEnv * env, MainPageEnv *mpe);

guint
main_page_env_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MainPageEnvClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) main_page_env_class_init,
			NULL,
			NULL,
			sizeof (MainPageEnv),
			0,
			(GInstanceInitFunc) main_page_env_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "MainPageEnv", &info, 0);
	}

	return type;
}

static void
main_page_env_class_init (MainPageEnvClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = main_page_env_finalize;
}



static void
main_page_env_init (MainPageEnv * wid)
{
	GtkWidget *sw, *bb;
	gint i;

	/* setting spaces,... */
	gtk_container_set_border_width (GTK_CONTAINER (wid), GNOME_PAD / 2);

	/* Scrolled Window fro CList */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);

	/* CList */
	wid->clist = gtk_clist_new (3);
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 0, _("Form/Grid Name"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 1, _("Form/Grid Description"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 2, _("Associated query"));
	gtk_clist_set_selection_mode (GTK_CLIST (wid->clist), GTK_SELECTION_SINGLE);
	for (i = 0; i < 3; i++)
		gtk_clist_set_column_auto_resize (GTK_CLIST (wid->clist), i, TRUE);
	gtk_clist_column_titles_show (GTK_CLIST (wid->clist));
	gtk_clist_column_titles_passive (GTK_CLIST (wid->clist));
	gtk_container_add (GTK_CONTAINER (sw), wid->clist);
	gtk_widget_show (wid->clist);
	g_signal_connect (G_OBJECT (wid->clist), "select_row",
			    G_CALLBACK (selection_made), wid);
	g_signal_connect (G_OBJECT (wid->clist), "unselect_row",
			    G_CALLBACK (selection_unmade), wid);

	/* Button Box */
	bb = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_end (GTK_BOX (wid), bb, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (bb);

	/* New Env Button */
	wid->new_env = gtk_button_new_with_label (_("Create"));
	gtk_container_add (GTK_CONTAINER (bb), wid->new_env);
	gtk_widget_show (wid->new_env);
	g_signal_connect(G_OBJECT(wid->new_env), "clicked",
			   G_CALLBACK(create_env_cb), wid); 

	/* Properties Button */
	wid->properties = gtk_button_new_with_label (_("Properties"));
	gtk_container_add (GTK_CONTAINER (bb), wid->properties);
	gtk_widget_show (wid->properties);
	g_signal_connect(G_OBJECT(wid->properties), "clicked",
			   G_CALLBACK(properties_env_cb), wid); 


	/* Remove Env Button */
	wid->remove_env = gtk_button_new_with_label (_("Delete"));
	gtk_container_add (GTK_CONTAINER (bb), wid->remove_env);
	gtk_widget_show (wid->remove_env);
	g_signal_connect (G_OBJECT (wid->remove_env), "clicked",
			    G_CALLBACK (remove_env_cb), wid);

	wid->sel_row = -1;
}


GtkWidget *
main_page_env_new (ConfManager * conf)
{
	GObject   *obj;
	MainPageEnv *wid;

	obj = g_object_new (MAIN_PAGE_ENV_TYPE, NULL);
	wid = MAIN_PAGE_ENV (obj);
	wid->conf = conf;
	wid->queries = NULL;

	g_signal_connect (G_OBJECT (conf), "env_added",
			  G_CALLBACK (main_page_env_add_cb), wid);

	g_signal_connect (G_OBJECT (conf), "env_removed",
			  G_CALLBACK (main_page_env_drop_cb), wid);


	gtk_widget_set_sensitive (wid->remove_env, FALSE);
	gtk_widget_set_sensitive (wid->new_env, FALSE);
	gtk_widget_set_sensitive (wid->properties, FALSE);
	conf_manager_register_sensitive_on_connect (wid->conf,
						    GTK_WIDGET (wid->new_env));

	return GTK_WIDGET (obj);
}

static void main_page_env_finalize (GObject   * object)
{
	parent_class->finalize (object);
}

static void
selection_made (GtkWidget * wid,
		gint row, gint column, GdkEventButton * event, MainPageEnv *mpe)
{
	mpe->sel_row = row;
	gtk_widget_set_sensitive (mpe->remove_env, TRUE);
	gtk_widget_set_sensitive (mpe->properties, TRUE);
}


static void
selection_unmade (GtkWidget * wid,
		  gint row, gint column, GdkEventButton * event, MainPageEnv *mpe)
{
	mpe->sel_row = -1;
	gtk_widget_set_sensitive (mpe->remove_env, FALSE);
	gtk_widget_set_sensitive (mpe->properties, FALSE);
}

static void remove_env_answer_cb (gint reply, GObject   *obj);
static void
remove_env_cb (GObject   * obj, MainPageEnv *mpe)
{
	Row_Data *rdata = NULL;
	gchar *txt;

	if (mpe->sel_row >= 0)
		rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpe->clist),
							     mpe->sel_row);
	if (rdata) {
		if (rdata->env->name)
			txt = g_strdup_printf (_("Do you really want to remove\n"
						 "the grid/form '%s' for query '%s'?"),
					       rdata->env->name, rdata->env->q->name);
		else
			txt = g_strdup_printf (_("Do you really want to remove\n"
						 "the grid/form (unnamed) for query '%s'?"),
					       rdata->env->q->name);
		g_object_set_data (obj, "env", rdata->env);
		gnome_question_dialog (txt, (GnomeReplyCallback)
				       remove_env_answer_cb, obj);
		g_free (txt);
	}
}

static void
remove_env_answer_cb (gint reply, GObject   * obj)
{
	QueryEnv *env;

	env = QUERY_ENV (g_object_get_data (obj, "env"));

	if (reply == 0) 
		query_del_env (env->q, G_OBJECT (env));
}


static void 
create_env_cb (GObject   * obj, MainPageEnv *mpe)
{
	g_print("\n\nCreation NOT YET DONE\n\n");
}

static void properties_env_cb (GObject   * obj, MainPageEnv *mpe)
{
	Row_Data *rdata = NULL;

	if (mpe->sel_row >= 0)
		rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpe->clist),
							     mpe->sel_row);
	if (rdata) {
		/* FIXME */
		GtkWidget *dlg;	
		dlg = query_env_editor_get_in_dialog (rdata->env);
		gtk_widget_show (dlg);
	}
}



static void names_changed_cb (GObject   *obj, MainPageEnv *mpe);
static void
main_page_env_add_cb (GObject   * obj, QueryEnv * env, MainPageEnv *mpe)
{
	gchar *col[3] = {"", "", ""};
	gint i = 0;
	Row_Data *rdata;


	i = gtk_clist_append (GTK_CLIST (mpe->clist), col);

	rdata = g_new0 (Row_Data, 1);
	rdata->conf = mpe->conf;
	rdata->env = env;
	gtk_clist_set_row_data (GTK_CLIST (mpe->clist), i, rdata);
	names_changed_cb (G_OBJECT (env->q), mpe);
	

	/* Signal from QueryEnv */
	g_signal_connect (G_OBJECT (env), "name_changed",
			  G_CALLBACK (names_changed_cb), mpe);

	/* Signal from Query */
	if (! g_slist_find (mpe->queries, env->q)) {
		mpe->queries = g_slist_append (mpe->queries, env->q);

		g_signal_connect (G_OBJECT (env->q), "name_changed",
				  G_CALLBACK (names_changed_cb), mpe);
	}
}

static void
main_page_env_drop_cb (GObject   * obj, QueryEnv * env, MainPageEnv *mpe)
{
	gint i = 0;
	gboolean found = FALSE;
	Row_Data *rdata;


	while ((i < GTK_CLIST (mpe->clist)->rows) && !found) {
		rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpe->clist), i);
		if (rdata->env == env) {
			gint j = 0, nbq = 0;
			Row_Data *rdata2;
			
			/* How many times does the Query represented by QueryEnv appear in the
			   CList ? */
			while (j < GTK_CLIST (mpe->clist)->rows) {
				rdata2 = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpe->clist), j);
				if ((rdata != rdata2) && (rdata2->env->q == rdata->env->q))
					nbq++;
				j++;
			}
			if (! nbq) {
				mpe->queries = g_slist_remove (mpe->queries, rdata->env->q);
				g_signal_handlers_disconnect_by_func (G_OBJECT (rdata->env->q), 
								      G_CALLBACK (names_changed_cb), mpe);
			}

			/* removing the CList entry */
			found = TRUE;
			g_free (rdata);
			gtk_clist_remove (GTK_CLIST (mpe->clist), i);
			if (mpe->sel_row == i) {
				mpe->sel_row = -1;
				gtk_widget_set_sensitive (mpe->remove_env, FALSE);
			}
		}
		i++;
	}
}

static void 
names_changed_cb (GObject   *obj, MainPageEnv *mpe)
{
	gint i = 0;
	Row_Data *rdata;
	Query *q;

	if (IS_QUERY_ENV (obj))
		q = QUERY_ENV (obj)->q;
	else
		q = QUERY (obj);

	g_assert (q);
	
	while (i < GTK_CLIST (mpe->clist)->rows) {
		rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpe->clist), i);
		if (rdata->env->q == q) {
			gchar *col[3];

			if (rdata->env->name && *(rdata->env->name))
				col[0] = rdata->env->name;
			else
				col[0] = _("<No name>");
			if (rdata->env->descr)
				col[1] = rdata->env->descr;
			else
				col[1] = "";
			col[2] = q->name;
			gtk_clist_set_text (GTK_CLIST (mpe->clist), i, 0, col[0]);
			gtk_clist_set_text (GTK_CLIST (mpe->clist), i, 1, col[1]);
			gtk_clist_set_text (GTK_CLIST (mpe->clist), i, 2, col[2]);
		}
		i++;
	}
	
}
