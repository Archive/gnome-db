/* query-join.c
 *
 * Copyright (C) 2002 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "query.h"
#include "marshal.h"

#define T_OR_F(bool) bool ? "t" : "f"

#define bT_OR_F(str) (str[0] == 't') ? TRUE : FALSE

static void query_join_class_init (QueryJoinClass * class);
static void query_join_init (QueryJoin * qj);
static void query_join_dispose (GObject   * object);
static void query_join_finalize (GObject   * object);

static QueryJoinPair *query_join_find_pair (QueryJoin *qj, GObject   *ant_field, GObject   *suc_field);

GObject   *query_join_new_from_xml (ConfManager *conf, xmlNodePtr node);
xmlNodePtr query_join_save_to_xml (QueryJoin *qj);

/*
 * static variables 
 */

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass   *parent_class = NULL;

/* get a pointer on the class of QueryJoin for static data access */
static QueryJoinClass *query_join_class = NULL;

enum
{
	TYPE_CHANGED,
	CARD_CHANGED,
	PAIR_ADDED,
	PAIR_REMOVED,
	LAST_SIGNAL
};

static gint query_join_signals[LAST_SIGNAL] = { 0, 0, 0, 0 };


guint
query_join_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryJoinClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_join_class_init,
			NULL,
			NULL,
			sizeof (QueryJoin),
			0,
			(GInstanceInitFunc) query_join_init
		};		

		type = g_type_register_static (G_TYPE_OBJECT, "QueryJoin", &info, 0);
	}
	return type;
}

static void
query_join_class_init (QueryJoinClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);
	query_join_class = class;

	query_join_signals[TYPE_CHANGED] =
		g_signal_new ("type_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryJoinClass, type_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);

	query_join_signals[CARD_CHANGED] =
		g_signal_new ("card_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryJoinClass, card_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);

	query_join_signals[PAIR_ADDED] =
		g_signal_new ("pair_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryJoinClass, pair_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);

	query_join_signals[PAIR_REMOVED] =
		g_signal_new ("pair_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryJoinClass, pair_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);

	class->type_changed = NULL;
	class->pair_added = NULL;
	class->pair_removed = NULL;

	object_class->dispose = query_join_dispose;
	object_class->finalize = query_join_finalize;
	/* FIXME: support change of integrity and of condition fields */
}


static void
query_join_init (QueryJoin * qj)
{
	qj->query = NULL;
	qj->join_type = QUERY_JOIN_INNER;
	qj->ant_view = NULL;
	qj->suc_view = NULL;
	qj->pairs = NULL;
	qj->weak_ref_objects = NULL;
	qj->card = QUERY_JOIN_UNDEFINED;
	qj->condition = g_new0 (gchar, 1);
	qj->cond_modified = FALSE;
	qj->skip_view = FALSE;
}


static void view_object_destroy_cb (QueryJoin *qj, GObject   *obj);
GObject   *
query_join_new (Query *q,  QueryView *ant_view, QueryView *suc_view)
{
	GObject   *obj;
	QueryJoin *qj;

	/* lots of tests */
	g_assert (q != NULL);
	g_assert (IS_QUERY (q));

	g_assert (IS_QUERY_VIEW (ant_view));
	g_assert (IS_QUERY_VIEW (suc_view));
	g_assert (ant_view != suc_view);

	obj = g_object_new (QUERY_JOIN_TYPE, NULL);
	qj = QUERY_JOIN (obj);

	qj->query = q;
	qj->ant_view = ant_view;
	qj->suc_view = suc_view;

	/* views destruction signals */
	g_object_weak_ref (G_OBJECT (ant_view), (GWeakNotify) view_object_destroy_cb, qj);
	qj->weak_ref_objects = g_slist_append (qj->weak_ref_objects, ant_view);
	g_object_weak_ref (G_OBJECT (suc_view), (GWeakNotify) view_object_destroy_cb, qj);
	qj->weak_ref_objects = g_slist_append (qj->weak_ref_objects, suc_view);

	/* FIXME FER: your init code here */

	return obj;
}

void 
query_join_free (QueryJoin *qj)
{
	g_return_if_fail (qj && IS_QUERY_JOIN (qj));
	g_object_unref (G_OBJECT (qj));
}

static void 
view_object_destroy_cb (QueryJoin *qj, GObject   *obj)
{
	/* removing the QueryView weak reference */
	qj->weak_ref_objects = g_slist_remove (qj->weak_ref_objects, obj);

	query_join_free (qj);
}

static void 
query_join_dispose (GObject   * object)
{
	QueryJoin *qj;
	GSList *list;

	g_return_if_fail (object != NULL);
        g_return_if_fail (IS_QUERY_JOIN (object));

	qj = QUERY_JOIN (object);

	/* join pairs cleanup */
	list = qj->pairs;
	while (list) {
		QueryJoinPair *pair;

		pair = QUERY_JOIN_PAIR_CAST (list->data);
		query_join_del_pair (qj, pair);
		
		list = qj->pairs;
	}

	/* weak unref the ant and suc views */
	if (g_slist_find (qj->weak_ref_objects, qj->ant_view)) {
		g_object_weak_unref (G_OBJECT (qj->ant_view), (GWeakNotify) view_object_destroy_cb, qj);
		qj->weak_ref_objects = g_slist_remove (qj->weak_ref_objects, qj->ant_view);
	}

	if (g_slist_find (qj->weak_ref_objects, qj->suc_view)) {
		g_object_weak_unref (G_OBJECT (qj->suc_view), (GWeakNotify) view_object_destroy_cb, qj);
		qj->weak_ref_objects = g_slist_remove (qj->weak_ref_objects, qj->suc_view);
	}

	qj->ant_view = NULL;
	qj->suc_view = NULL;
	qj->query = NULL;
	qj->join_type = QUERY_JOIN_INNER;
	qj->card = QUERY_JOIN_UNDEFINED;
	if (qj->weak_ref_objects) {
		g_slist_free (qj->weak_ref_objects);
		qj->weak_ref_objects = NULL;
	}
	qj->skip_view = FALSE;

	/* parent class */
	parent_class->dispose (object);
}


static void 
query_join_finalize (GObject   * object)
{
	QueryJoin *qj;

	g_return_if_fail (object != NULL);
        g_return_if_fail (IS_QUERY_JOIN (object));

	qj = QUERY_JOIN (object);

	/* FIXME FER: check this: cleanup code here */
	g_free (qj->condition);
	qj->condition = NULL;

	parent_class->finalize (object);
}


gchar *
query_join_render_as_sql   (QueryJoin *qj)
{
	return NULL;
}


xmlNodePtr 
query_join_render_as_xml   (QueryJoin *qj)
{
	return NULL;
}
					  
xmlNodePtr 
query_join_list_save_to_xml     (GSList *jlist)
{
	xmlNodePtr node;
	xmlNodePtr pn;
	QueryJoin *qj;

	node = xmlNewNode (NULL, "QueryJoinList");

	/* joins */
	while (jlist) {
		qj = QUERY_JOIN (jlist->data);
		pn = query_join_save_to_xml (qj);
		xmlAddChild (node, pn);
		jlist = g_slist_next (jlist);
	}

	return node;
}

static gchar *char_get_join_type (QueryJoinType jt);
static gchar *char_get_join_card (QueryJoinCard card);

xmlNodePtr 
query_join_save_to_xml     (QueryJoin *qj)
{
	xmlNodePtr node;
	gchar *str;
	GSList *list;

	node = xmlNewNode (NULL, "QueryJoin");

	str = query_view_get_xml_id (qj->ant_view);
	xmlSetProp (node, "ant_view", str);
	g_free (str);

	str = query_view_get_xml_id (qj->suc_view);
	xmlSetProp (node, "suc_view", str);
	g_free (str);

	str = char_get_join_type (qj->join_type);
	xmlSetProp (node, "join_type", str);

	str = char_get_join_card (qj->card);
	xmlSetProp (node, "join_card", str);

	xmlSetProp (node, "condition", qj->condition);

	xmlSetProp (node, "ref_integrity", T_OR_F(qj->ref_integrity));
	xmlSetProp (node, "cascade_update", T_OR_F(qj->cascade_update));
	xmlSetProp (node, "cascade_delete", T_OR_F(qj->cascade_delete));
	xmlSetProp (node, "skip_view", T_OR_F(qj->skip_view));

	/* join pairs */
	list = qj->pairs;
	while (list) {
		xmlNodePtr pn;
		QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list->data);
		pn = xmlNewChild (node, NULL, "QueryJoinPair", NULL);
		
		if (IS_DB_FIELD (pair->ant_field))
			str = db_field_get_xml_id (DB_FIELD (pair->ant_field), qj->query->conf->db);
		else
			str = query_field_get_xml_id (QUERY_FIELD (pair->ant_field));
		xmlSetProp (pn, "ant_field", str);
		g_free (str);

		if (IS_DB_FIELD (pair->suc_field))
			str = db_field_get_xml_id (DB_FIELD (pair->suc_field), qj->query->conf->db);
		else
			str = query_field_get_xml_id (QUERY_FIELD (pair->suc_field));
		xmlSetProp (pn, "suc_field", str);
		g_free (str);

		list = g_slist_next (list);
	}

	return node;
}

static gchar *
char_get_join_type (QueryJoinType jt)
{
	gchar *str;

	switch (jt) {
	case QUERY_JOIN_INNER:
		str = "INNER_JOIN";
		break;
	case QUERY_JOIN_LEFT_OUTER:
		str = "LEFT_JOIN";
		break;
	case QUERY_JOIN_RIGHT_OUTER:
		str = "RIGHT_JOIN";
		break;
	case QUERY_JOIN_FULL_OUTER:
		str = "FULL_JOIN";
		break;
	case QUERY_JOIN_CROSS:
		str = "CROSS_JOIN";
		break;
	default:
		str = "???";
		break;
	}

	return str;
}

static gchar *
char_get_join_card (QueryJoinCard card)
{
	gchar *str;

	switch (card) {
	case QUERY_JOIN_1_1:
		str = "1_1";
		break;
	case QUERY_JOIN_1_N:
		str = "1_N";
		break;
	case QUERY_JOIN_N_1:
		str = "N_1";
		break;
	case QUERY_JOIN_UNDEFINED:
		str = "UNDEF";
		break;
	default:
		str = "???";
		break;
	}

	return str;
}


static QueryJoinType enum_get_join_type (gchar *str);
static QueryJoinCard enum_get_join_card (gchar *str);
GObject   *
query_join_new_from_xml   (ConfManager *conf, xmlNodePtr node)
{
	QueryJoin *qj = NULL;
	QueryView *ant_view = NULL;
	QueryView *suc_view = NULL;
	gchar *str;

	str = xmlGetProp (node, "ant_view");
	if (str) {
		ant_view = query_view_find_from_xml_name (conf, NULL, str);
		g_free (str);
	}

	str = xmlGetProp (node, "suc_view");
	if (str) {
		suc_view = query_view_find_from_xml_name (conf, NULL, str);
		g_free (str);
	}

	if (ant_view && suc_view) {
		xmlNodePtr tree;
		qj = QUERY_JOIN (query_join_new (ant_view->query, ant_view, suc_view));
		str = xmlGetProp (node, "join_type");
		if (str) {
			query_join_set_join_type (qj, enum_get_join_type (str));
			g_free (str);
		}
		
		tree = node->xmlChildrenNode;

		while (tree) {
			if (!strcmp (tree->name, "QueryJoinPair")) {
				gpointer ptr;
				GObject   *ant_field = NULL;
				GObject   *suc_field = NULL;

				str = xmlGetProp (tree, "ant_field");
				if (str) {
					ptr = database_find_field_from_xml_name (conf->db, str);
					if (ptr)
						ant_field = G_OBJECT (ptr);
					else {
						ptr = query_get_field_by_xmlid (qj->query, str);
						if (ptr)
							ant_field = G_OBJECT (ptr);
					}
					g_free (str);
				}

				str = xmlGetProp (tree, "suc_field");
				if (str) {
					ptr = database_find_field_from_xml_name (conf->db, str);
					if (ptr)
						suc_field = G_OBJECT (ptr);
					else {
						ptr = query_get_field_by_xmlid (qj->query, str);
						if (ptr)
							suc_field = G_OBJECT (ptr);
					}
					g_free (str);
				}


				if (ant_field && suc_field) 
					query_join_add_pair (qj, ant_field, suc_field);
			}
			tree = tree->next;
		}

		/* set the cardinality after adding the pairs because adding pairs sets default values
		   which need to be overridden by the XML file */
		str = xmlGetProp (node, "join_card");
		if (str) {
			query_join_set_card (qj, enum_get_join_card (str));
			g_free (str);
		}
		str = xmlGetProp (node, "condition");
		if (str) {
			g_free (qj->condition);
			qj->condition = str;
		}
		str = xmlGetProp (node, "cond_modified");
		if (str) {
			qj->cond_modified = bT_OR_F(str);
			g_free (str);
		}
		str = xmlGetProp (node, "ref_integrity");
		if (str) {
			qj->ref_integrity = bT_OR_F(str);
			g_free (str);
		}
		str = xmlGetProp (node, "cascade_update");
		if (str) {
			qj->cascade_update = bT_OR_F(str);
			g_free (str);
		}
		str = xmlGetProp (node, "cascade_delete");
		if (str) {
			qj->cascade_delete = bT_OR_F(str);
			g_free (str);
		}
		str = xmlGetProp (node, "skip_view");
		if (str) {
			qj->skip_view = bT_OR_F(str);
			g_free (str);
		}
	}

	if (qj)
		return G_OBJECT (qj);
	else
		return NULL;
}


static 
QueryJoinType enum_get_join_type (gchar *str)
{
	QueryJoinType jt;

	switch (*str) {
	case 'L':
		jt = QUERY_JOIN_LEFT_OUTER;
		break;
	case 'R':
		jt = QUERY_JOIN_RIGHT_OUTER;
		break;
	case 'F':
		jt = QUERY_JOIN_FULL_OUTER;
		break;
	case 'C':
		jt = QUERY_JOIN_CROSS;
		break;
	case 'I':
	default:
		jt = QUERY_JOIN_INNER;
		break;
	}
	
	return jt;
}

static QueryJoinCard 
enum_get_join_card (gchar *str)
{
	QueryJoinCard card;

	switch (*str) {
	case '1':
		if (*(str+2) == '1')
			card = QUERY_JOIN_1_1;
		else
			card = QUERY_JOIN_1_N;
		break;
	case 'N':
		card = QUERY_JOIN_N_1;
		break;
	default:
		card = QUERY_JOIN_UNDEFINED;
		break;
	}
	
	return card;
}


gchar	*
query_join_get_join_type_sql (QueryJoin *qj)
{
	gchar *str;

	switch (qj->join_type) {
	case QUERY_JOIN_INNER:
		str = "INNER JOIN";
		break;
	case QUERY_JOIN_LEFT_OUTER:
		str = "LEFT JOIN";
		break;
	case QUERY_JOIN_RIGHT_OUTER:
		str = "RIGHT JOIN";
		break;
	case QUERY_JOIN_FULL_OUTER:
		str = "FULL JOIN";
		break;
	case QUERY_JOIN_CROSS:
		str = "CROSS JOIN";
		break;
	default:
		str = "???";
		break;
	}

	return str;
}


void
query_join_set_join_type (QueryJoin *qj, QueryJoinType jt)
{
	if (jt != qj->join_type) {
		qj->join_type = jt;

#ifdef debug_signal
		g_print (">> 'TYPE_CHANGED' from query_join_set_type\n");
#endif
		g_signal_emit (G_OBJECT (qj), query_join_signals[TYPE_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'TYPE_CHANGED' from query_join_set_type\n");
#endif	
	}
}

void        
query_join_set_card (QueryJoin *qj, QueryJoinCard card) 
{
	if (card != qj->card) {
		qj->card = card;

#ifdef debug_signal
		g_print (">> 'CARD_CHANGED' from query_join_set_card\n");
#endif
		g_signal_emit (G_OBJECT (qj), query_join_signals[CARD_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'CARD_CHANGED' from query_join_set_card\n");
#endif	
	}
}

static  QueryJoinPair *
query_join_find_pair (QueryJoin *qj, GObject   *ant_field, GObject   *suc_field)
{
	QueryJoinPair *pair = NULL;
	GSList *pairs = qj->pairs;
	
	while (pairs && !pair) {
		if ((ant_field == QUERY_JOIN_PAIR_CAST (pairs->data)->ant_field) &&
		    (suc_field == QUERY_JOIN_PAIR_CAST (pairs->data)->suc_field))
			pair = QUERY_JOIN_PAIR_CAST (pairs->data);
		pairs = g_slist_next (pairs);
	}

	return pair;
}

/* Try to set the default to the best query join cardinality as possible */
static QueryJoinCard
compute_best_appropriate_card (QueryJoin *qj)
{
	QueryJoinCard card = QUERY_JOIN_UNDEFINED;
	GSList *list;
	QueryJoinPair *pair;
	gboolean ant_key = TRUE;
	gboolean suc_key = TRUE;

	/* see if we have a key on the ant_field and suc_field */
	list = qj->pairs;
	while (list) {
		pair = QUERY_JOIN_PAIR_CAST (list->data);
		if (IS_QUERY_FIELD (pair->ant_field) ||
		    (IS_DB_FIELD (pair->ant_field) && !DB_FIELD (pair->ant_field)->is_key))
			ant_key = FALSE;

		if (IS_QUERY_FIELD (pair->suc_field) ||
		    (IS_DB_FIELD (pair->suc_field) && !DB_FIELD (pair->suc_field)->is_key))
			suc_key = FALSE;

		list = g_slist_next (list);
	}

	if (ant_key) {
		if (suc_key)
			card = QUERY_JOIN_1_1;
		else
			card = QUERY_JOIN_1_N;
	}
	else {
		if (suc_key)
			card = QUERY_JOIN_N_1;
		else
			card = QUERY_JOIN_UNDEFINED;
	}

	/* if we have undefined, see if we can have something better with
	   not NULL values instead of keys (being less exigent) */
	if (card == QUERY_JOIN_UNDEFINED) {
		ant_key = TRUE;
		suc_key = TRUE;
		
		/* see if we have a non NULL value on the ant_field and suc_field */
		list = qj->pairs;
		while (list) {
			pair = QUERY_JOIN_PAIR_CAST (list->data);
			if (IS_QUERY_FIELD (pair->ant_field) ||
			    (IS_DB_FIELD (pair->ant_field) && DB_FIELD (pair->ant_field)->null_allowed))
				ant_key = FALSE;
			
			if (IS_QUERY_FIELD (pair->suc_field) ||
			    (IS_DB_FIELD (pair->suc_field) && DB_FIELD (pair->suc_field)->null_allowed))
				suc_key = FALSE;
			
			list = g_slist_next (list);
		}
		
		if (ant_key) {
			if (suc_key)
				card = QUERY_JOIN_1_1;
			else
				card = QUERY_JOIN_1_N;
		}
		else {
			if (suc_key)
				card = QUERY_JOIN_N_1;
			else
				card = QUERY_JOIN_UNDEFINED;
		}
	}

	return card;
}

static guint is_pair_object_in_pairs (QueryJoin *qj, GObject   *obj);

static void pair_object_destroy_cb (QueryJoin *join, GObject   *obj);

void
query_join_add_pair (QueryJoin *qj, GObject   *field1, GObject   *field2)
{
	QueryJoinPair *pair;	
	GObject   *ant_field=NULL, *suc_field=NULL;
	gchar *str;

	g_return_if_fail (IS_QUERY_JOIN (qj));
	g_return_if_fail (G_IS_OBJECT (field1) && G_IS_OBJECT (field2));

	/* matches each field with is own view */
	if (query_view_contains_field (qj->ant_view, field1)) {
		ant_field = field1;
		if (query_view_contains_field (qj->suc_view, field2))
			suc_field = field2;
	}
	else if (query_view_contains_field (qj->ant_view, field2)) {
		ant_field = field2;
		if (query_view_contains_field (qj->suc_view, field1))
			suc_field = field1;
	}

	g_assert (ant_field);
	g_assert (suc_field);

	pair = query_join_find_pair (qj, ant_field, suc_field);
	if (pair)
		return;

	/* create a new pair */
	pair = g_new0 (QueryJoinPair, 1);
	pair->qj = qj;
	pair->ant_field = ant_field;
	pair->suc_field = suc_field;

	/* case when one of the two objects is destroyed */
	if (!g_slist_find (qj->weak_ref_objects, ant_field)) {
		g_object_weak_ref (G_OBJECT (ant_field), (GWeakNotify) pair_object_destroy_cb, qj);
		qj->weak_ref_objects = g_slist_append (qj->weak_ref_objects, ant_field);
	}
	if (!g_slist_find (qj->weak_ref_objects, suc_field)) {
		g_object_weak_ref (G_OBJECT (suc_field), (GWeakNotify) pair_object_destroy_cb, qj);
		qj->weak_ref_objects = g_slist_append (qj->weak_ref_objects, suc_field);
	}

	qj->pairs = g_slist_append (qj->pairs, pair);

	/* add corresponding default join condition */
	if (qj->condition[0]) /* condition is not empty */
		str = g_strdup_printf ("(%s AND (%s.%s = %s.%s))",
					qj->condition,
					query_view_get_name (qj->ant_view),
					"f1", /*query_field_render_as_sql(QUERY_FIELD(ant_field), NULL),*/
					query_view_get_name (qj->suc_view),
					"f2"); /* query_field_render_as_sql(QUERY_FIELD(suc_field), NULL));*/
	else
		str = g_strdup_printf ("(%s.%s = %s.%s)",
					query_view_get_name (qj->ant_view),
					"f1", /*query_field_render_as_sql(QUERY_FIELD(ant_field), NULL),*/
					query_view_get_name (qj->suc_view),
					"f2"); /* query_field_render_as_sql(QUERY_FIELD(suc_field), NULL));*/
	g_free (qj->condition);
	qj->condition = str;


#ifdef debug_signal
	g_print (">> 'PAIR_ADDED' from query_join_add_pair\n");
#endif
	g_signal_emit (G_OBJECT (qj), query_join_signals[PAIR_ADDED], 0, pair);
#ifdef debug_signal
	g_print ("<< 'PAIR_ADDED' from query_join_add_pair\n");
#endif	

	/* try to guess the best cardinality as possible */
	query_join_set_card (qj, compute_best_appropriate_card (qj));
}

static guint 
is_pair_object_in_pairs (QueryJoin *qj, GObject   *obj)
{
	guint count = 0;
	GSList *list;

	list = qj->pairs;
	while (list) {
		if ((QUERY_JOIN_PAIR_CAST (list->data)->ant_field == (gpointer) obj) ||
		    (QUERY_JOIN_PAIR_CAST (list->data)->suc_field == (gpointer) obj))
			count++;
		list = g_slist_next (list);
	}

	return count;
}

static void 
pair_object_destroy_cb (QueryJoin *join, GObject   *obj)
{
	GSList *list;

	g_print ("pair_object_destroy_cb (%p, %p)\n", join, obj);
	join->weak_ref_objects = g_slist_remove (join->weak_ref_objects, obj);

	/* remove all the pairs with the obj as ant or suc fields */
	list = join->pairs;
	while (list) {
		if ((QUERY_JOIN_PAIR_CAST (list->data)->ant_field == (gpointer) obj) ||
		    (QUERY_JOIN_PAIR_CAST (list->data)->suc_field == (gpointer) obj)) {
			query_join_del_pair (join, QUERY_JOIN_PAIR_CAST (list->data));
			list = join->pairs;
		}
		else
			list = g_slist_next (list);
	}
}


void 
query_join_del_pair (QueryJoin *qj,  QueryJoinPair *pair)
{
	g_return_if_fail (IS_QUERY_JOIN (qj));

	if (g_slist_find (qj->pairs, pair)) {
		/* remove the pair now so it does not appear in the following tests */
		qj->pairs = g_slist_remove (qj->pairs, pair);
		
#ifdef debug
		g_print ("DELETING PAIR %p (%p -> %p)\n", pair, pair->ant_field, pair->suc_field);
		g_print ("\tis_pair_object_in_pairs (qj, pair->ant_field) = %d\n", 
			 is_pair_object_in_pairs (qj, pair->ant_field));
		g_print ("\tg_slist_find (qj->weak_ref_objects, pair->ant_field) = %p\n", 
			 g_slist_find (qj->weak_ref_objects, pair->ant_field));
		g_print ("\tis_pair_object_in_pairs (qj, pair->suc_field) = %d\n", 
			 is_pair_object_in_pairs (qj, pair->suc_field));
		g_print ("\tg_slist_find (qj->weak_ref_objects, pair->suc_field) = %p\n", 
			 g_slist_find (qj->weak_ref_objects, pair->suc_field));
#endif

		/* disconnect the signals connected at add_pair() */
		if (g_slist_find (qj->weak_ref_objects, pair->ant_field) &&
		    !is_pair_object_in_pairs (qj, pair->ant_field)) {
			g_object_weak_unref (G_OBJECT (pair->ant_field), (GWeakNotify) pair_object_destroy_cb, qj);
			qj->weak_ref_objects = g_slist_remove (qj->weak_ref_objects, pair->ant_field);
		}

		if (g_slist_find (qj->weak_ref_objects, pair->suc_field) &&
		    !is_pair_object_in_pairs (qj, pair->suc_field)) {
			g_object_weak_unref (G_OBJECT (pair->suc_field), (GWeakNotify) pair_object_destroy_cb, qj);
			qj->weak_ref_objects = g_slist_remove (qj->weak_ref_objects, pair->suc_field);
		}

		g_print ("--------- %d pairs left in QJ %p\n", g_slist_length (qj->pairs), qj);

		/* FIXME: VIV: implement correct joining conditions */
		/* update join condition */
 		if (0 && qj->pairs && !qj->cond_modified) {
			char *str, *pos;
			gchar *new_cond;
			str = g_strdup_printf ("(%s.%s = %s.%s)",
					query_view_get_name (qj->ant_view),
					"f1", /*query_field_render_as_sql(QUERY_FIELD(ant_field), NULL),*/
					query_view_get_name (qj->suc_view),
					"f2"); /* query_field_render_as_sql(QUERY_FIELD(suc_field), NULL));*/
			/* removes str from the condition */
			pos = strstr (qj->condition, str);
			g_assert(pos);
			*pos = '\0';
			pos += strlen(str) + 5; /* pos jumps over "condition AND "*/
			new_cond = g_strdup_printf ("%s%s", qj->condition, pos);
			g_free (str);
			g_free (qj->condition);
			qj->condition = new_cond;
		}
		else {
			if (qj->condition) {
				g_free (qj->condition);
				qj->condition = NULL;
			}
		}

#ifdef debug_signal
		g_print (">> 'PAIR_REMOVED' from query_join_del_pair\n");
#endif
		g_signal_emit (G_OBJECT (qj), query_join_signals[PAIR_REMOVED], 0, pair);
#ifdef debug_signal
		g_print ("<< 'PAIR_REMOVED' from query_join_del_pair\n");
#endif	

		g_free (pair);

		/* Destroys itself if there are no more pair */
 		/* if (qj->pairs == NULL) */
 		/*	g_object_unref (G_OBJECT (qj));*/
	}
}


QueryJoin *query_join_copy (QueryJoin *qj)
{
	/* FER FIXME - deep copy: needed to copy global relationships to
	 * a query when a new table is added */

	g_warning ("query_join_copy () not yet implemented, will return NULL");
	return NULL;
}


/* query_join_swap_views 
 *
 * Swaps the left view with the right view (it also swaps the field pairs
 * and updates the cardinality accordingly (1-n changes to n-1 and vice-versa)
 *
 * See also the dfinition of a Join list in query_add_join()
 * 
 * It should be used carefully to avoid inadvertent introduction of
 * inconsistencies. Like the following example where ij means INNER JOIN
 * and the conditions over the fiels is omited for the sake of simplicity:
 * 
 * The query FROM (A ij B) ij C is represented as (A,B) (B,C). A single
 * swap of (B,C) to (C,B) would give the sequence (A,B) (C,B) which will
 * not be properly translated to the intended, original, query. We would
 * obtain FROM (A ij B) ij B
 *
 */

void query_join_swap_views (QueryJoin *qj)
{
	GSList *pl;
	QueryView *tmp;
	gchar *str=NULL;

	/* swap views */
	tmp = qj->suc_view;
	qj->suc_view = qj->ant_view;
	qj->ant_view = tmp;
	
	/* reverse relation cardinality */
	if (qj->card == QUERY_JOIN_1_N)
		qj->card = QUERY_JOIN_N_1;
	else if (qj->card == QUERY_JOIN_N_1)
		qj->card = QUERY_JOIN_1_N;
	
	/* swap fields in each field pair */
	pl = qj->pairs;
	while (pl) {
		QueryJoinPair *p;
		GObject   *f;
		p = (QueryJoinPair *)(pl->data);
		f = p->ant_field;
		p->ant_field = p->suc_field;
		p->suc_field = f;
		pl = g_slist_next(pl);
		if (!qj->cond_modified) {
			/* swap also the join conditions*/
			if (str) /* condition is not empty */
				str = g_strdup_printf ("(%s AND (%s.%s = %s.%s))",
					qj->condition,
					query_view_get_name (qj->ant_view),
					"f1", /*query_field_render_as_sql(QUERY_FIELD(p->ant_field), NULL),*/
					query_view_get_name (qj->suc_view),
					"f2"); /* query_field_render_as_sql(QUERY_FIELD(p->suc_field), NULL));*/
			else
				str = g_strdup_printf ("(%s.%s = %s.%s)",
					query_view_get_name (qj->ant_view),
					"f1", /*query_field_render_as_sql(QUERY_FIELD(p->ant_field), NULL),*/
					query_view_get_name (qj->suc_view),
					"f2"); /* query_field_render_as_sql(QUERY_FIELD(p->suc_field), NULL));*/
		}
	}

	if (str) {
		g_free (qj->condition);
		qj->condition = str;
	}
}


QueryView *query_join_get_ant_view (QueryJoin *qj)
{
	return qj->ant_view;
}


QueryView *query_join_get_suc_view (QueryJoin *qj)
{
	return qj->suc_view;
}

/* FIXME: should we duplicate the strings? (rather than duplicate pointers) */
void query_join_set_condition (QueryJoin *qj, gchar *cond)
{
	g_free (qj->condition);
	qj->condition = cond;
}


gchar *query_join_get_condition (QueryJoin *qj)
{
	return qj->condition;
}

#ifdef debug
void query_join_print (QueryJoin *qj)
{
  if (qj==NULL) g_print("NULL\n");
  g_print("(%s, %s) ON (%s)", query_view_get_name(qj->ant_view),
			query_view_get_name(qj->suc_view),
			qj->condition);
}
#endif

void query_join_set_skip_view (QueryJoin *qj)
{
	qj->skip_view = TRUE;
}

void query_join_unset_skip_view (QueryJoin *qj)
{
	qj->skip_view = TRUE;
}

gboolean query_join_skip_view (QueryJoin *qj)
{
	return qj->skip_view;
}

