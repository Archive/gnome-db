/* mainpageseq.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mainpageseq.h"
#include "interface_cb.h"

static void main_page_seq_class_init (MainPageSeqClass * class);
static void main_page_seq_init (MainPageSeq * wid);
static void main_page_seq_finalize (GObject *object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

typedef struct
{
	ConfManager *conf;
	DbSequence *seq;
	void *query;		/* to be a Query object */
}
Row_Data;

/*
 * static functions 
 */
static void selection_made (GtkWidget * wid, gint row, gint column,
			    GdkEventButton * event, gpointer data);
static void selection_unmade (GtkWidget * wid, gint row, gint column,
			      GdkEventButton * event, gpointer data);
static void remove_seq_cb (GObject   * obj, gpointer data);
static void main_page_seq_add_cb (GObject   * obj, DbSequence * seq,
				  gpointer data);
static void main_page_seq_drop_cb (GObject   * obj, DbSequence * seq,
				   gpointer data);
static void main_page_db_updated_cb (Database * db, MainPageSeq * mps);
static void main_page_seq_conn_close_cb (GObject   * obj, MainPageSeq * mps);
static void database_added_cb (ConfManager *conf, Database *db, MainPageSeq * mps);
static void database_removed_cb (ConfManager *conf, Database *db, MainPageSeq * mps);

guint
main_page_seq_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MainPageSeqClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) main_page_seq_class_init,
			NULL,
			NULL,
			sizeof (MainPageSeq),
			0,
			(GInstanceInitFunc) main_page_seq_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "MainPageSeq", &info, 0);
	}
	return type;
}

static void
main_page_seq_class_init (MainPageSeqClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = main_page_seq_finalize;
}



static void
main_page_seq_init (MainPageSeq * wid)
{
	GtkWidget *sw, *bb;
	gint i;

	/* setting spaces,... */
	gtk_container_set_border_width (GTK_CONTAINER (wid), GNOME_PAD / 2);

	/* Scrolled Window fro CList */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);

	/* CList */
	wid->clist = gtk_clist_new (3);
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 0, _("Sequence"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 1, _("Owner"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 2, _("Comments"));
	gtk_clist_set_selection_mode (GTK_CLIST (wid->clist),
				      GTK_SELECTION_SINGLE);
	for (i = 0; i < 3; i++)
		gtk_clist_set_column_auto_resize (GTK_CLIST (wid->clist), i,
						  TRUE);
	gtk_clist_column_titles_show (GTK_CLIST (wid->clist));
	gtk_clist_column_titles_passive (GTK_CLIST (wid->clist));
	gtk_container_add (GTK_CONTAINER (sw), wid->clist);
	gtk_widget_show (wid->clist);
	g_signal_connect (G_OBJECT (wid->clist), "select_row",
			    G_CALLBACK (selection_made), wid);
	g_signal_connect (G_OBJECT (wid->clist), "unselect_row",
			    G_CALLBACK (selection_unmade), wid);

	/* Button Box */
	bb = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_end (GTK_BOX (wid), bb, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (bb);

	/* Remove Seq Button */
	wid->remove_seq = gtk_button_new_with_label (_("Drop Sequence"));
	gtk_container_add (GTK_CONTAINER (bb), wid->remove_seq);
	gtk_widget_show (wid->remove_seq);
	g_signal_connect (G_OBJECT (wid->remove_seq), "clicked",
			    G_CALLBACK (remove_seq_cb), wid);

	/* New Seq Button */
	wid->new_seq = gtk_button_new_with_label (_("Create Sequence"));
	gtk_container_add (GTK_CONTAINER (bb), wid->new_seq);
	gtk_widget_show (wid->new_seq);
	/* FIXME: use bonobo controls to create new sequences */
	/*g_signal_connect(G_OBJECT(wid->new_seq), "clicked",
	   G_CALLBACK(create_seq_cb), wid); */


	wid->sel_row = -1;
}


GtkWidget *
main_page_seq_new (ConfManager * conf)
{
	GObject   *obj;
	MainPageSeq *wid;

	obj = g_object_new (MAIN_PAGE_SEQ_TYPE, NULL);
	wid = MAIN_PAGE_SEQ (obj);
	wid->conf = conf;

	g_signal_connect (G_OBJECT (conf), "database_added",
			  G_CALLBACK (database_added_cb), wid);

	g_signal_connect (G_OBJECT (conf), "database_removed",
			  G_CALLBACK (database_removed_cb), wid);


	gtk_widget_set_sensitive (wid->remove_seq, FALSE);
	gtk_widget_set_sensitive (wid->new_seq, FALSE);
	conf_manager_register_sensitive_on_connect (wid->conf,
						    GTK_WIDGET (wid->new_seq));

	g_signal_connect (G_OBJECT (conf->srv), "conn_closed",
			  G_CALLBACK (main_page_seq_conn_close_cb), obj);

	return GTK_WIDGET (obj);
}

static void main_page_seq_finalize (GObject *object)
{
	parent_class->finalize (object);
}

static void database_added_cb (ConfManager *conf, Database *db, MainPageSeq * mps)
{
	g_signal_connect (G_OBJECT (conf->db), "seq_created",
			  G_CALLBACK (main_page_seq_add_cb),
			  mps);
	g_signal_connect (G_OBJECT (conf->db), "seq_dropped",
			  G_CALLBACK (main_page_seq_drop_cb),
			  mps);
	g_signal_connect (G_OBJECT (conf->db), "updated",
			  G_CALLBACK (main_page_db_updated_cb),
			  mps);
}

static void database_removed_cb (ConfManager *conf, Database *db, MainPageSeq * mps)
{
	/* nothing to do about it */
}


static void
selection_made (GtkWidget * wid,
		gint row, gint column, GdkEventButton * event, gpointer data)
{
	MainPageSeq *mps = MAIN_PAGE_SEQ (data);

	mps->sel_row = row;
	gtk_widget_set_sensitive (mps->remove_seq, TRUE);
}

static void
selection_unmade (GtkWidget * wid,
		gint row, gint column, GdkEventButton * event, gpointer data)
{
	MainPageSeq *mps = MAIN_PAGE_SEQ (data);

	mps->sel_row = -1;
	gtk_widget_set_sensitive (mps->remove_seq, FALSE);
}


static void remove_seq_answer_cb (gint reply, GObject   * obj);
static void
remove_seq_cb (GObject   * obj, gpointer data)
{
	Row_Data *rdata = NULL;
	MainPageSeq *wid = MAIN_PAGE_SEQ (data);
	gchar *txt;

	if (wid->sel_row >= 0)
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (wid->clist),
						wid->sel_row);
	if (rdata) {
		txt = g_strdup_printf (_("Do you really want to remove\n"
					 "the sequence '%s'?"),
				       rdata->seq->name);
		g_object_set_data (obj, "conf", wid->conf);
		g_object_set_data (obj, "seq", rdata->seq);
		gnome_question_dialog (txt,
				       (GnomeReplyCallback)
				       remove_seq_answer_cb, obj);
		g_free (txt);
	}
}

static void
remove_seq_answer_cb (gint reply, GObject   * obj)
{
	ConfManager *conf;
	DbSequence *seq;
	gchar *txt;

	conf = (ConfManager *) g_object_get_data (obj, "conf");
	seq = DB_SEQUENCE (g_object_get_data (obj, "seq"));

	if (reply == 0) {
		txt = g_strdup_printf ("DROP sequence %s", seq->name);
		server_access_do_query (conf->srv, txt, SERVER_ACCESS_QUERY_SQL);
		sql_mem_update_cb (NULL, conf);
		g_free (txt);
	}
}



/* this CB is intended to be connected to the "seq_created" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting MainPageSeq object 
*/
static void
main_page_seq_add_cb (GObject   * obj, DbSequence * seq, gpointer data)
{
	MainPageSeq *wid = MAIN_PAGE_SEQ (data);
	gchar *col[3];
	gchar *str;
	gint i = 0;
	gboolean stop = FALSE;
	Row_Data *rdata;

	col[0] = seq->name;
	col[1] = seq->owner;
	if (seq->comments)
		col[2] = seq->comments;
	else
		col[2] = "";
	/* where to put it ? */
	while ((i < GTK_CLIST (wid->clist)->rows) && !stop) {
		gtk_clist_get_text (GTK_CLIST (wid->clist), i, 0, &str);
		if (strcmp (str, seq->name) > 0) {
			stop = TRUE;
		}
		else
			i++;
	}

	gtk_clist_insert (GTK_CLIST (wid->clist), i, col);

	rdata = (Row_Data *) g_malloc (sizeof (Row_Data));
	rdata->conf = wid->conf;
	rdata->seq = seq;
	rdata->query = NULL;
	gtk_clist_set_row_data (GTK_CLIST (wid->clist), i, rdata);
}

/* this CB is intended to be connected to the "seq_dropped" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting MainPageSeq object 
*/
static void
main_page_seq_drop_cb (GObject   * obj, DbSequence * seq, gpointer data)
{
	MainPageSeq *wid = MAIN_PAGE_SEQ (data);
	gchar *str;
	gint i = 0;
	gboolean found = FALSE;
	Row_Data *rdata;

	while ((i < GTK_CLIST (wid->clist)->rows) && !found) {
		gtk_clist_get_text (GTK_CLIST (wid->clist), i, 0, &str);
		if (strcmp (str, seq->name) == 0) {
			found = TRUE;
			rdata = (Row_Data *)
				gtk_clist_get_row_data (GTK_CLIST
							(wid->clist), i);
			g_free (rdata);
			gtk_clist_remove (GTK_CLIST (wid->clist), i);
			if (wid->sel_row == i) {
				wid->sel_row = -1;
				gtk_widget_set_sensitive (wid->remove_seq,
							  FALSE);
			}
		}
		i++;
	}
}

static void
main_page_db_updated_cb (Database * db, MainPageSeq * mps)
{
	gint row;
	Row_Data *rd;
	for (row = 0; row < GTK_CLIST (mps->clist)->rows; row++) {
		rd = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (mps->clist), row);
		if (rd->seq->owner)
			gtk_clist_set_text (GTK_CLIST (mps->clist), row, 1,
					    rd->seq->owner);
		else
			gtk_clist_set_text (GTK_CLIST (mps->clist), row, 1,
					    "");
	}
}


static void
main_page_seq_conn_close_cb (GObject   * obj, MainPageSeq * mps)
{
	Row_Data *rdata;
	gint i, nb = GTK_CLIST (mps->clist)->rows;

	for (i = 0; i < nb; i++) {
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (mps->clist), 0);
		g_free (rdata);
		gtk_clist_remove (GTK_CLIST (mps->clist), 0);
	}
	gtk_widget_set_sensitive (mps->remove_seq, FALSE);
	gtk_widget_set_sensitive (mps->new_seq, FALSE);
	mps->sel_row = -1;
}
