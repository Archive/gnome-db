/* canvas-query-join.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __CANVAS_QUERY_JOIN__
#define __CANVAS_QUERY_JOIN__

#include <gnome.h>
#include "canvas-base.h"
#include "query.h"
#include "config.h"

G_BEGIN_DECLS


/* QueryJoin item for the canvas.  
 *
 * In addition to the GnomeCanvasGroup and CanvasBase arguments, the following object arguments 
 * are available::
 *
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * query                pointer                 RW              The Query to which the QueryView is attached
 * query_join           pointer                 RW              The QueryJoin being displayed
 * 
 * 
 * NOTE: the "query" and "query_join" arguments are required.
 * 
 */


#define CQJ_DEFAULT_VIEW_FONT "Sans 10"
#define CQJ_DEFAULT_VIEW_FONT_BOLD "Sans Bold 10"

#define CANVAS_QUERY_JOIN_TYPE          (canvas_query_join_get_type())
#define CANVAS_QUERY_JOIN(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, canvas_query_join_get_type(), CanvasQueryJoin)
#define CANVAS_QUERY_JOIN_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, canvas_query_join_get_type (), CanvasQueryJoinClass)
#define IS_CANVAS_QUERY_JOIN(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, canvas_query_join_get_type ())


typedef struct _CanvasQueryJoin CanvasQueryJoin;
typedef struct _CanvasQueryJoinClass CanvasQueryJoinClass;


/* struct for the object's data */
struct _CanvasQueryJoin
{
	CanvasBase          object;

	/* objects being represented */
	Query              *query;
	QueryJoin          *join;
	GSList             *weak_ref_objects;

	/* presentation parameters */
	gdouble             x_text_space;
	gdouble             y_text_space;

	/* UI building information */
	GnomeCanvasItem    *bg_frame;
	GSList             *items;

	/* References to other GnomeCanvasItems */
	GnomeCanvasItem    *ant_cview;
	GnomeCanvasItem    *suc_cview;

	/* Dialog for the properties */
	GtkDialog          *props_dlg;
};

/* struct for the object's class */
struct _CanvasQueryJoinClass
{
	CanvasBaseClass     parent_class;
};


guint      canvas_query_join_get_type (void);

void       canvas_query_join_free     (CanvasQueryJoin *cqj);

G_END_DECLS

#endif
