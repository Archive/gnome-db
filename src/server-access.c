/* serveraccess.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "server-access.h"
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <gmodule.h>
#include <libgda/libgda.h>
#include "marshal.h"


/* signals */
enum
{
	CONN_OPENED,
	CONN_TO_CLOSE,
	CONN_CLOSED,
	DATA_TYPES_UPDATED,
	DATA_FUNCTION_ADDED,
	DATA_FUNCTION_REMOVED,
	DATA_FUNCTION_UPDATED,
	DATA_AGGREGATE_ADDED,
	DATA_AGGREGATE_REMOVED,
	DATA_AGGREGATE_UPDATED,
	PROGRESS,
	OBJECTS_BINDINGS_UPDATED,
	LAST_SIGNAL
};

static gint server_access_signals[LAST_SIGNAL] = { 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0
};
static void server_access_class_init (ServerAccessClass * class);
static void server_access_init (ServerAccess * srv, ServerAccessClass *klass);
static void server_access_finalize (GObject * object);
static void m_conn_closed (ServerAccess * srv);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
server_access_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ServerAccessClass),
			(GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
			(GClassInitFunc) server_access_class_init,
			NULL,
			NULL,
			sizeof (ServerAccess),
			0,
			(GInstanceInitFunc) server_access_init
		};

		type = g_type_register_static (GDA_TYPE_CLIENT, "ServerAccess", &info, 0);
	}

	return type;
}

static void
server_access_class_init (ServerAccessClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	server_access_signals[CONN_OPENED] =
		g_signal_new ("conn_opened",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, conn_opened),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[CONN_TO_CLOSE] =
		g_signal_new ("conn_to_close", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, conn_to_close),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[CONN_CLOSED] =	/* runs after user handlers */
		g_signal_new ("conn_closed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, conn_closed),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[DATA_TYPES_UPDATED] =
		g_signal_new ("data_types_updated", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_types_updated),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[DATA_FUNCTION_ADDED] =
		g_signal_new ("data_function_added", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_function_added),
			      NULL, NULL,
			      marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
	server_access_signals[DATA_FUNCTION_REMOVED] =
		g_signal_new ("data_function_removed", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_function_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
	server_access_signals[DATA_FUNCTION_UPDATED] =
		g_signal_new ("data_function_updated", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_function_updated),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[DATA_AGGREGATE_ADDED] =
		g_signal_new ("data_aggregate_added", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_aggregate_added),
			      NULL, NULL,
			      marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
	server_access_signals[DATA_AGGREGATE_REMOVED] =
		g_signal_new ("data_aggregate_removed", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_aggregate_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
	server_access_signals[DATA_AGGREGATE_UPDATED] =
		g_signal_new ("data_aggregate_updated", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, data_aggregate_updated),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	server_access_signals[PROGRESS] =
		g_signal_new ("progress", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, progress),
			      NULL, NULL,
			      marshal_VOID__POINTER_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_UINT);
	server_access_signals[OBJECTS_BINDINGS_UPDATED] =
		g_signal_new ("objects_bindings_updated", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ServerAccessClass, objects_bindings_updated),
			      NULL, NULL,
			      marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	class->conn_opened = NULL;
	class->conn_to_close = NULL;
	class->conn_closed = m_conn_closed;
	class->data_types_updated = NULL;
	class->data_function_added = NULL;
	class->data_function_removed = NULL;
	class->data_function_updated = NULL;
	class->data_aggregate_added = NULL;
	class->data_aggregate_removed = NULL;
	class->data_aggregate_updated = NULL;
	class->progress = NULL;
	class->objects_bindings_updated = NULL;
	object_class->finalize = server_access_finalize;
}

static void
server_access_init (ServerAccess * srv, ServerAccessClass *klass)
{
	srv->gda_datasource = g_string_new ("");
	srv->user_name = g_string_new (getenv ("USER"));
	srv->password = g_string_new ("");
	srv->description = NULL;
	srv->cnc = NULL;

	/* creation of the data types known by postgreSQL */
	srv->data_types = NULL;
	srv->data_functions = NULL;
	srv->data_aggregates = NULL;

	/* display plugins */
	if (!g_module_supported ()) {
		g_warning ("GModule is required for GNOME-DB to run!");
		exit (1);
	}
	srv->data_types_display = sql_data_display_get_initial_list ();
	srv->types_objects_hash = g_hash_table_new (NULL, NULL);	/* pointers! */
	srv->bindable_objects = NULL;

	/* init the features */
	srv->features.sequences = FALSE;
	srv->features.procs = FALSE;
	srv->features.inheritance = FALSE;
	srv->features.xml_queries = FALSE;

	/* declare the binding functions for Functions and aggregates here */
	server_access_declare_object_bindable (srv, server_function_binding_func);
}


ServerAccess *
server_access_new (void)
{
	ServerAccess *obj;

	obj = g_object_new (SERVER_ACCESS_TYPE, NULL);

	return obj;
}

static void
server_access_finalize (GObject * object)
{
	ServerAccess *srv;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_ACCESS (object));

	srv = SERVER_ACCESS (object);

	if (srv->cnc) 
		server_access_close_connect (srv);


	if (srv->description)
		g_free (srv->description);
	g_string_free (srv->gda_datasource, TRUE);
	srv->gda_datasource = NULL;
	g_string_free (srv->user_name, TRUE);
	srv->user_name = NULL;
	g_string_free (srv->password, TRUE);
	srv->password = NULL;

	/* data types */
	if (srv->data_types) {
		list = srv->data_types;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_types);
	}
	srv->data_types = NULL;

	/* data functions */
	if (srv->data_functions) {
		list = srv->data_functions;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_functions);
	}
	srv->data_functions = NULL;

	/* data aggregates */
	if (srv->data_aggregates) {
		list = srv->data_aggregates;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_aggregates);
	}
	srv->data_aggregates = NULL;

	/* hash table */
	if (srv->types_objects_hash)
		g_hash_table_destroy (srv->types_objects_hash);

	/* Display functions and Plugins */
	while (srv->data_types_display) {
		SqlDataDisplayFns *fns;
		fns = (SqlDataDisplayFns *) (srv->data_types_display->data);
		if (fns->plugin_file) {	/* it is a plugin */
			void (*release_struct) (SqlDataDisplayFns * fns);
			g_print ("Freeing plugin %s\n", fns->descr);
			g_free (fns->plugin_file);
			g_module_symbol (fns->lib_handle, "release_struct", (gpointer *) &release_struct);
			if (release_struct)
				(release_struct) (fns);
			g_module_close (fns->lib_handle);
			if (release_struct)
				fns = NULL;
		}
		else {		/* built in data type display */
			g_print ("Freeing builtin %s\n", fns->descr);
			sql_data_display_free_display_fns (fns);
			fns = NULL;
		}
		if (fns)
			g_free (fns);
		list = srv->data_types_display;
		srv->data_types_display = g_slist_remove_link (srv->data_types_display,
							       srv->data_types_display);
		g_slist_free_1 (list);
	}

	/* chain to parent class */
	parent_class->finalize (object);
}




/*
 * open/close and query functions
 */

void
server_access_open_connect (ServerAccess * srv)
{
	GdaDataSourceInfo *dsn;

	if (srv->cnc)
		server_access_close_connect (srv);

	dsn = gnome_db_config_find_data_source (srv->gda_datasource->str);
	if (!dsn) {
		GdaError *error;
		gchar *str;

		error = gda_error_new ();
		str = g_strdup_printf (_
				       ("No datasource '%s' defined in your GDA configuration"),
				       srv->gda_datasource->str);
		gda_error_set_description (error, str);
		g_free (str);

		gda_error_set_source (error, _("[GNOME DB Client Widgets]"));
		gda_connection_add_error (srv->cnc, error);
		return;
	}

	srv->cnc = gda_client_open_connection (GDA_CLIENT (srv), dsn->name,
					       srv->user_name->str, srv->password->str);

	gnome_db_config_free_data_source_info (dsn);
	if (srv->cnc) { 
		/* connection is now opened */
		srv->features.sequences = gda_connection_supports
			(srv->cnc, GDA_CONNECTION_FEATURE_SEQUENCES);
		srv->features.procs = gda_connection_supports
			(srv->cnc, GDA_CONNECTION_FEATURE_PROCEDURES);
		srv->features.inheritance = gda_connection_supports
			(srv->cnc, GDA_CONNECTION_FEATURE_INHERITANCE);
		srv->features.xml_queries = gda_connection_supports
			(srv->cnc, GDA_CONNECTION_FEATURE_XML_QUERIES);		
#ifdef debug_signal
		g_print (">> 'CONN_OPENED' from server_access_open_connect()\n");
#endif
		g_signal_emit (G_OBJECT (srv), server_access_signals[CONN_OPENED], 0);
#ifdef debug_signal
		g_print ("<< 'CONN_OPENED' from server_access_open_connect\n");
#endif		
	}
}

gboolean
server_access_is_open (ServerAccess * srv)
{
	return (srv->cnc) ? TRUE : FALSE;
}

void
server_access_close_connect (ServerAccess * srv)
{
	g_return_if_fail (srv && IS_SERVER_ACCESS (srv));

	if (srv->cnc) {
#ifdef debug_signal
		g_print (">> 'CONN_TO_CLOSE' from server_access_close_connect\n");
#endif
		g_signal_emit (G_OBJECT (srv), server_access_signals[CONN_TO_CLOSE], 0);
#ifdef debug_signal
		g_print ("<< 'CONN_TO_CLOSE' from server_access_close_connect\n");
#endif
		gda_client_close_all_connections (GDA_CLIENT (srv));
		srv->cnc = NULL;
#ifdef debug_signal
		g_print (">> 'CONN_CLOSED' from server_access_close_connect\n");
#endif
		g_signal_emit (G_OBJECT (srv), server_access_signals[CONN_CLOSED], 0);
#ifdef debug_signal
		g_print ("<< 'CONN_CLOSED' from server_access_close_connect\n");
#endif
	}
}

void
server_access_close_connect_no_warning (ServerAccess * srv)
{
	if (srv->cnc) {
		gda_client_close_all_connections (GDA_CLIENT (srv));
		srv->cnc = NULL;
#ifdef debug_signal
		g_print (">> 'CONN_CLOSED' from server_access_close_connect_no_warning\n");
#endif
		g_signal_emit (G_OBJECT (srv), server_access_signals[CONN_CLOSED], 0);
#ifdef debug_signal
		g_print ("<< 'CONN_CLOSED' from server_access_close_connect_no_warning\n");
#endif
	}
}

static void
m_conn_closed (ServerAccess * srv)
{
	GSList *list, *hold;

	/* Note: empty the hash table of used plugins is done automatically through 
	 * callbacks ((update_hash_table_as_objects_destroyed_cb...) */
	/* FIXME on the above! */

	/* destroy all information lists */
	if (srv->data_types) {
		list = srv->data_types;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_types = NULL;
#ifdef debug_signal
		g_print (">> 'DATA_TYPES_UPDATED' from m_conn_closed\n");
#endif
		g_signal_emit_by_name (G_OBJECT (srv), "data_types_updated");
#ifdef debug_signal
		g_print ("<< 'DATA_TYPES_UPDATED' from m_conn_closed\n");
#endif
	}
	if (srv->data_functions) {
		list = srv->data_functions;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_functions = NULL;
	}
	if (srv->data_aggregates) {
		list = srv->data_aggregates;
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_aggregates = NULL;
	}
}


ServerResultset *
server_access_do_query (ServerAccess * srv, const gchar * query, ServerAccessQueryType type)
{
	ServerResultset *qres = NULL;

	if (srv->cnc) {
		GdaDataModel *res;
		GdaCommandType gtype;
		GdaCommand *cmd;

		switch (type) {
		case SERVER_ACCESS_QUERY_XML:
			gtype = GDA_COMMAND_TYPE_XML;
			break;
		default:
			gtype = GDA_COMMAND_TYPE_SQL;
			break;
		}

		cmd = gda_command_new (query, gtype, GDA_COMMAND_OPTION_STOP_ON_ERRORS);

		res = gda_connection_execute_single_command (srv->cnc, cmd, NULL);

		if (res)
			qres = SERVER_RESULTSET (server_resultset_new (res, cmd));
	}

	return qres;
}


/*
 * data type, etc lookup and management
 */

void
server_access_refresh_datas (ServerAccess * srv)
{
	server_data_type_update_list (srv);
	server_function_update_list (srv);
	server_aggregate_update_list (srv);
}

gchar *
server_access_get_data_type (ServerAccess * srv, const gchar * oid)
{
	ServerDataType *dt;
	dt = server_data_type_get_from_server_type (srv->data_types, atoi (oid));
	if (dt)
		return dt->sqlname;
	else
		return NULL;
}

GList *
server_access_get_data_type_list (ServerAccess * srv)
{
	return server_data_type_get_name_list (srv->data_types);
}

ServerDataType *
server_access_get_type_from_name (ServerAccess * srv, const gchar * name)
{
	return server_data_type_get_from_name (srv->data_types, name);
}

ServerDataType *
server_access_get_type_from_oid (ServerAccess * srv, const gchar * oid)
{
	return server_data_type_get_from_server_type (srv->data_types, atoi (oid));
}


void
server_access_build_xml_tree (ServerAccess * srv, xmlDocPtr doc)
{
	xmlNodePtr toptree, tree, subtree, subsubtree;
	GSList *list, *list2;
	gchar *str;

	/* main node */
	toptree = xmlNewChild (doc->xmlRootNode, NULL, "SERVER", NULL);
	xmlSetProp (toptree, "descr", srv->description);

	/* data types */
	tree = xmlNewChild (toptree, NULL, "DATATYPES", NULL);
	list = srv->data_types;
	while (list) {
		ServerDataType *dt;
		SqlDataDisplayFns *fns;

		dt = SERVER_DATA_TYPE (list->data);
		subtree = xmlNewChild (tree, NULL, "type", NULL);
		str = g_strdup_printf ("DT%s", dt->sqlname);
		xmlSetProp (subtree, "name", str);
		g_free (str);
		xmlSetProp (subtree, "descr", dt->descr);
		str = g_strdup_printf ("%d", dt->numparams);
		xmlSetProp (subtree, "nparam", str);
		g_free (str);
		str = g_strdup_printf ("%d", dt->server_type);
		xmlSetProp (subtree, "sqltype", str);
		g_free (str);
		str = g_strdup_printf ("%d", dt->gda_type);
		xmlSetProp (subtree, "gdatype", str);
		g_free (str);
		/* if there is a plugin for that data type, write it */
		if ((fns = g_hash_table_lookup (srv->types_objects_hash, dt))) {
			xmlSetProp (subtree, "plugin", fns->plugin_name);
		}
		list = g_slist_next (list);
	}
	/* functions */
	tree = xmlNewChild (toptree, NULL, "PROCEDURES", NULL);
	list = srv->data_functions;
	while (list) {
		ServerFunction *df;
		df = SERVER_FUNCTION (list->data);
		subtree = xmlNewChild (tree, NULL, "function", NULL);
		xmlSetProp (subtree, "name", df->sqlname);
		str = g_strdup_printf ("PR%s", df->objectid);
		xmlSetProp (subtree, "id", str);
		g_free (str);
		xmlSetProp (subtree, "descr", df->descr);
		if (df->is_user)
			xmlSetProp (subtree, "user", "t");
		else
			xmlSetProp (subtree, "user", "f");
		/* params */
		if (df->result_type) {
			subsubtree =
				xmlNewChild (subtree, NULL, "param", NULL);
			str = g_strdup_printf ("DT%s",
					       df->result_type->sqlname);
			xmlSetProp (subsubtree, "type", str);
			g_free (str);
			xmlSetProp (subsubtree, "way", "out");
		}
		list2 = df->args;
		while (list2) {
			subsubtree =
				xmlNewChild (subtree, NULL, "param", NULL);
			str = g_strdup_printf ("DT%s",
					       SERVER_DATA_TYPE (list2->data)->
					       sqlname);
			xmlSetProp (subsubtree, "type", str);
			g_free (str);
			xmlSetProp (subsubtree, "way", "in");
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}

	/* aggregates */
	tree = xmlNewChild (toptree, NULL, "AGGREGATES", NULL);
	list = srv->data_aggregates;
	while (list) {
		ServerAggregate *da;
		da = SERVER_AGGREGATE (list->data);
		subtree = xmlNewChild (tree, NULL, "agg", NULL);
		xmlSetProp (subtree, "name", da->sqlname);
		xmlSetProp (subtree, "descr", da->descr);
		str = g_strdup_printf ("AG%s", da->objectid);
		xmlSetProp (subtree, "id", str);
		g_free (str);
		/* params */
		if (da->arg_type) {
			str = g_strdup_printf ("DT%s", da->arg_type->sqlname);
			xmlSetProp (subtree, "arg", str);
			g_free (str);
		}
		list = g_slist_next (list);
	}
}


/* WARNING: the XML doc MUST be a valid one with regards to the DTD,
   otherwise, the result is not known! */
gboolean
server_access_build_from_xml_tree (ServerAccess * srv, xmlNodePtr node)
{
	gboolean ok = TRUE;
	xmlNodePtr tree, subtree;
	gchar *str;

	if (srv->data_types || srv->data_functions || srv->data_aggregates) {
		gnome_error_dialog (_
				    ("INTERNAL ERROR:\nTrying to load an ServerAccess object "
				     "which already have some data inside. Clean it "
				     "before."));
		return FALSE;
	}

	tree = node->xmlChildrenNode;
	while (tree) {
		if (!strcmp (tree->name, "DATATYPES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				ServerDataType *dt;
				if (!strcmp (subtree->name, "type")) {
					dt = SERVER_DATA_TYPE (server_data_type_new ());
					str = xmlGetProp (subtree, "name");
					server_data_type_set_sqlname (dt, str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_data_type_set_descr (dt, str);
						g_free (str);
					}

					str = xmlGetProp (subtree, "nparam");
					dt->numparams = atoi (str);
					g_free (str);

					str = xmlGetProp (subtree, "sqltype");
					dt->server_type = atoi (str);
					g_free (str);

					str = xmlGetProp (subtree, "gdatype");
					dt->gda_type = atoi (str);
					g_free (str);

					/* default display function */
					dt->display_fns =
						server_access_get_display_fns_from_gda (srv, dt);
					srv->data_types =
						g_slist_append (srv->data_types, dt);

					/* plugin for this data type */
					if ((str =
					     xmlGetProp (subtree,
							 "plugin"))) {
						GSList *plist;
						gboolean found = FALSE;
						SqlDataDisplayFns *fns;
						plist = srv->
							data_types_display;
						while (plist && !found) {
							fns = (SqlDataDisplayFns *) (plist->data);
							if ((fns->plugin_name)
							    && !strcmp (fns->plugin_name, str))
							{
								server_access_bind_object_display (srv, G_OBJECT (dt),
												   fns);
								found = TRUE;
							}
							plist = g_slist_next (plist);
						}
						g_free (str);
					}
				}
				subtree = subtree->next;
			}
#ifdef debug_signal
			g_print (">> 'DATA_TYPES_UPDATED' from server_access_build_from_xml_tree\n");
#endif
			g_signal_emit_by_name (G_OBJECT (srv), "data_types_updated");
#ifdef debug_signal
			g_print ("<< 'DATA_TYPES_UPDATED' from server_access_build_from_xml_tree\n");
#endif
		}

		if (!strcmp (tree->name, "PROCEDURES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				xmlNodePtr node;
				ServerFunction *df;
				if (!strcmp (subtree->name, "function")) {
					df = SERVER_FUNCTION
						(server_function_new ());
					str = xmlGetProp (subtree, "name");
					server_function_set_sqlname (df, str);
					g_free (str);

					str = xmlGetProp (subtree, "id");
					df->objectid = g_strdup (str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_function_set_descr (df, str);
						g_free (str);
					}

					str = xmlGetProp (subtree, "user");
					if (*str == 't')
						df->is_user = TRUE;
					else
						df->is_user = FALSE;
					g_free (str);

					/* parameters */
					node = subtree->xmlChildrenNode;
					while (node) {
						if (!strcmp
						    (node->name, "param")) {
							ServerDataType *dt;
							str = xmlGetProp
								(node,
								 "type");
							dt = server_data_type_get_from_xml_id (srv, str);
							g_free (str);
							if (!dt)
								ok = FALSE;
							else {
								str = xmlGetProp (node, "way");
								if (!strcmp (str, "out"))
									df->result_type = dt;
								else
									df->args = g_slist_append (df->args, dt);
								g_free (str);
							}
						}
						if (ok)
							node = node->next;
						else
							node = NULL;
					}
					if (ok)
						srv->data_functions =
							g_slist_append (srv->data_functions, df);
				}
				subtree = subtree->next;
			}
		}

		if (!strcmp (tree->name, "AGGREGATES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				ServerAggregate *da;
				ServerDataType *dt;
				if (!strcmp (subtree->name, "agg")) {
					da = SERVER_AGGREGATE
						(server_aggregate_new ());
					str = xmlGetProp (subtree, "name");
					server_aggregate_set_sqlname (da,
									str);
					g_free (str);

					str = xmlGetProp (subtree, "id");
					da->objectid = g_strdup (str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_aggregate_set_descr
							(da, str);
						g_free (str);
					}

					if ((str =
					     xmlGetProp (subtree, "arg"))) {
						dt = server_data_type_get_from_xml_id (srv, str);
						if (dt)
							da->arg_type = dt;
						else
							ok = FALSE;
						g_free (str);
					}
					else
						da->arg_type = NULL;

					if (ok)
						srv->data_aggregates =
							g_slist_append (srv->
									data_aggregates,
									da);
				}
				subtree = subtree->next;
			}
		}

		if (tree)
			tree = tree->next;
	}

	return ok;
}

SqlDataDisplayFns *
server_access_get_display_fns_from_gda (ServerAccess * srv, ServerDataType * dt)
{
	SqlDataDisplayFns *fns = NULL, *ptr;
	gint i;
	GSList *list;

	list = srv->data_types_display;	/* SHOULD NEVER BE NULL! */
	while (list && !fns) {
		ptr = (SqlDataDisplayFns *) (list->data);
		if (ptr->nb_gda_type == 0)	/* 0 => ALL gda types */
			fns = ptr;
		i = 0;
		while (!fns && (i < ptr->nb_gda_type)) {
			if (ptr->valid_gda_types[i] == dt->gda_type)
				fns = ptr;
			i++;
		}

		list = g_slist_next (list);
	}

	return fns;
}

SqlDataDisplayFns *
server_access_get_object_display_fns (ServerAccess * srv, GObject * obj)
{
	SqlDataDisplayFns *fns = NULL;
	GSList *list;
	gpointer data;

	if (obj && G_IS_OBJECT (obj) && srv) {
		/* check to see if there is a specified bind for that object */
		fns = g_hash_table_lookup (srv->types_objects_hash, obj);

		/* if no fns has been found, we get the default ones */
		if (!fns && IS_SERVER_DATA_TYPE (obj))
			fns = SERVER_DATA_TYPE (obj)->display_fns;

		/* if no function found, then let's try to find a plugin for the 
		   object, using the declared functions to bind objects */
		list = srv->bindable_objects;
		while (list && !fns) {
			data = ((gpointer (*)(GObject *)) (list->data))
				(obj);
			if (data) {
				fns = g_hash_table_lookup (srv->types_objects_hash, data);

				/* if no fns has been found, we get the default ones */
				if (!fns && G_IS_OBJECT (data)
				    && IS_SERVER_DATA_TYPE (data)) {
					fns = SERVER_DATA_TYPE (data)->display_fns;
				}
			}
			list = g_slist_next (list);
		}
	}
	if (!fns)		/* default basic functions for every object */
		fns = (SqlDataDisplayFns *) srv->data_types_display->data;
	return fns;
}

static void bound_object_destroy_cb (GObject * obj, ServerAccess * srv);
void
server_access_bind_object_display (ServerAccess * srv,
				   GObject * obj, SqlDataDisplayFns * fns)
{
	g_hash_table_insert (srv->types_objects_hash, obj, fns);
	g_signal_connect (G_OBJECT (obj), "destroy", G_CALLBACK (bound_object_destroy_cb), srv);
	g_signal_emit (G_OBJECT (srv), server_access_signals[OBJECTS_BINDINGS_UPDATED], 0);
}

static void
bound_object_destroy_cb (GObject * obj, ServerAccess * srv)
{
	server_access_unbind_object_display (srv, obj);
}


void
server_access_unbind_object_display (ServerAccess * srv, GObject * obj)
{
	g_hash_table_remove (srv->types_objects_hash, obj);
	g_signal_handlers_disconnect_by_func (obj, G_CALLBACK (bound_object_destroy_cb), srv);
	g_signal_emit (G_OBJECT (srv), server_access_signals[OBJECTS_BINDINGS_UPDATED], 0);
}


struct foreach_hash_struct
{
	GHashTable *hash;
	gpointer rem_value;
	gpointer repl_value;	/* if not NULL; for every node with rem_value, 
				   create a new node with repl_value instead */
};

static void foreach_hash_cb (gpointer key, gpointer value,
			     struct foreach_hash_struct *user_data);
void
server_access_rescan_display_plugins (ServerAccess * srv, gchar * path)
{
	struct dirent *dir;
	DIR *dstream;
	gchar *str, *key, *key2;
	GSList *list, *list2, *newones = NULL;
	struct foreach_hash_struct *fhs;

	/* FIXME: it appears that the program hangs if we try to access
	   not a lib but an executable program. maybe try to use the "standard"
	   libtool calls. */
	dstream = opendir (path);
	if (!dstream) {
		g_print ("Cannot open %s\n", path);
	}
	else {
		dir = readdir (dstream);
		while (dir) {
			if ((strlen (dir->d_name) > 3) &&	/* only .so files */
			    !strcmp (dir->d_name + strlen (dir->d_name) - 3,
				     ".so")) {
				GModule *lib;
				SqlDataDisplayFns *fns;
				SqlDataDisplayFns *(*fpi) ();

				str = g_strdup_printf ("%s/%s", path,
						       dir->d_name);
				if ((lib = g_module_open (str, G_MODULE_BIND_LAZY))) {
				        g_module_symbol (lib,
							"fetch_plugin_interface",
							 (gpointer *) &fpi);
					if (fpi) {
						fns = (fpi) ();
						g_print ("\tNew plugin (%p/%p): %s(V%s)\n", fns, 
							 fns->gdavalue_to_widget, fns->plugin_name, fns->version);
						fns->lib_handle = lib;
						fns->plugin_file =
							g_strdup (str);
						srv->data_types_display =
							g_slist_append (srv->
									data_types_display,
									fns);
						newones =
							g_slist_append
							(newones, fns);
					}
					else {
						g_module_close (lib);
					}
				}
				g_free (str);
			}
			dir = readdir (dstream);
		}
		closedir (dstream);

		/*
		 * Now removing any old plugin (reloaded ones get replaced into the
		 * hash table)
		 */
		fhs = g_new (struct foreach_hash_struct, 1);
		fhs->hash = srv->types_objects_hash;
		list = srv->data_types_display;
		while (list) {
			if ((((SqlDataDisplayFns *) (list->data))->plugin_name) && !g_slist_find (newones, list->data)) {	/* only old plugins */
				void (*release_struct) (SqlDataDisplayFns *
							fns);
				SqlDataDisplayFns *fns;
				gboolean found;

				fns = (SqlDataDisplayFns *) (list->data);
				fhs->rem_value = fns;
				fhs->repl_value = NULL;

				/* is there a new plugin with the same key? */
				key = (fns->get_unique_key) ();
				list2 = newones;
				found = FALSE;
				while (list2 && !found) {
					key2 = (((SqlDataDisplayFns
						  *) (list2->data))->
						get_unique_key)
						();
					if (!strcmp (key, key2)) {
						fhs->repl_value = list2->data;
						found = TRUE;
					}
					g_free (key2);
					list2 = g_slist_next (list2);
				}
				g_free (key);

				g_hash_table_foreach (srv->types_objects_hash,
						      (GHFunc)
						      (foreach_hash_cb), fhs);
				srv->data_types_display = g_slist_remove_link (srv->data_types_display,
									       list);
				g_slist_free_1 (list);
				/* freeing the ressources for that plugin */
				g_free (fns->plugin_file);
				g_module_symbol (fns->lib_handle,
						 "release_struct",
						 (gpointer *) &release_struct);
				if (release_struct)
					(release_struct) (fns);
				g_module_close (fns->lib_handle);

				list = srv->data_types_display;
			}
			else
				list = g_slist_next (list);
		}
		g_free (fhs);
		g_slist_free (newones);
	}
}

static void
foreach_hash_cb (gpointer key, gpointer value,
		 struct foreach_hash_struct *user_data)
{
	if (value == user_data->rem_value) {
		g_hash_table_remove (user_data->hash, key);
		if (user_data->repl_value)
			g_hash_table_insert (user_data->hash, key,
					     user_data->repl_value);
	}
}


void
server_access_declare_object_bindable (ServerAccess * srv,
				       gpointer (*func) (GObject *))
{
	if (!g_slist_find (srv->bindable_objects, func))
		srv->bindable_objects =	g_slist_append (srv->bindable_objects, func);
}
