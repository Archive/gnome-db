/* relship.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __RELSHIP__
#define __RELSHIP__

#include <gnome.h>
#include "relship-view.h"

G_BEGIN_DECLS

#define RELSHIP_TYPE          (relship_get_type())
#define RELSHIP(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, relship_get_type(), RelShip)
#define RELSHIP_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, relship_get_type (), RelShipClass)
#define IS_RELSHIP(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, relship_get_type ())
#define RELSHIP_ITEM_DATA_CAST(id) ((RelShipItemData *)id)

typedef struct _RelShip              RelShip;
typedef struct _RelShipClass         RelShipClass;
typedef struct _RelShipItemData      RelShipItemData;

struct _RelShipItemData {
	GObject            *obj; /* QueryView, etc */
	gdouble             x;
	gdouble             y;
};

/* struct for the object's data */
struct _RelShip
{
	GObject             object;

	Query              *query;
	gboolean            is_query_destroyed;
	GSList             *views; /* list of RelShipView */
	GSList             *items; /* list of RelShipItemData */
};

/* struct for the object's class */
struct _RelShipClass
{
	GObjectClass         parent_class;
};

/* generic object's functions */
guint            relship_get_type            (void);
GObject         *relship_find                (Query *q);

/* Creates a new RelShip for 'q', from the one of 'original' where hash holds the
   corresponding objects between the two queries (hash MUST be a valid hash table) */
void             relship_copy                (Query *q, Query *original, GHashTable *hash);
	
/* get a RelShipView in a scrolled window */
GtkWidget       *relship_get_sw_view         (RelShip *rs);

	
/* get the data associated to an item, will never return NULL:
   a new entry is created if necessary */
RelShipItemData *relship_find_item           (RelShip *rs, GObject   *obj);

/* manually set the position of an item */
void             relship_item_set_position   (RelShip *rs, GObject   *obj, gdouble x, gdouble y);


/* XML Saving and loading is handled from here (helper functions) */
void             relship_build_xml_tree      (Query *start_query, xmlNodePtr toptree, ConfManager * conf);
RelShip         *relship_build_from_xml_tree (ConfManager * conf, xmlNodePtr node);

G_END_DECLS

#endif
