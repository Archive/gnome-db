/* canvas-query-view.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-query-view.h"
#include "canvas-field.h"
#include "relship.h"

static void canvas_query_view_class_init (CanvasQueryViewClass * class);
static void canvas_query_view_init (CanvasQueryView * item);
static void canvas_query_view_dispose (GObject   * object);

static void canvas_query_view_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);
static void canvas_query_view_set_property    (GObject              *object,
					 guint                 param_id,
					 const GValue         *value,
					 GParamSpec           *pspec);
static void canvas_query_view_get_property    (GObject              *object,
					 guint                 param_id,
					 GValue               *value,
					 GParamSpec           *pspec);

enum
{
	PROP_0,
	PROP_QUERY,
	PROP_QUERY_VIEW
};

/* for the parent object */
static GObjectClass *parent_class = NULL;

guint
canvas_query_view_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (CanvasQueryViewClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) canvas_query_view_class_init,
			NULL,
			NULL,
			sizeof (CanvasQueryView),
			0,
			(GInstanceInitFunc) canvas_query_view_init
		};	       
		
		type = g_type_register_static (CANVAS_BASE_TYPE, "CanvasQueryView", &info, 0);
	}

	return type;
}

static void
canvas_query_view_class_init (CanvasQueryViewClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	GnomeCanvasItemClass *item_class = (GnomeCanvasItemClass *) class;

	parent_class = g_type_class_peek_parent (class);
	object_class->dispose = canvas_query_view_dispose;
	item_class->bounds = canvas_query_view_bounds;

	/* Properties */
	object_class->set_property = canvas_query_view_set_property;
	object_class->get_property = canvas_query_view_get_property;

	g_object_class_install_property
                (object_class, PROP_QUERY,
                 g_param_spec_pointer ("query", NULL, NULL,  (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
                (object_class, PROP_QUERY_VIEW,
                 g_param_spec_pointer ("query_view", NULL, NULL,  (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}


static void
canvas_query_view_init (CanvasQueryView * cqv)
{
	cqv->query = NULL;
	cqv->view = NULL;
	cqv->view_destroyed = FALSE;

	cqv->x_text_space = 3.;
	cqv->y_text_space = 3.;

	cqv->title_text_height = 0.;
	cqv->title_text_width = 0.;
	cqv->max_text_width = 0.;
	cqv->field_items = NULL;
	cqv->bg_frame = NULL;
	cqv->fields_text_height = 0.;

	cqv->xmouse = 50.;
	cqv->ymouse = 50.;
}

static void view_destroyed_cb (CanvasQueryView *cqv, GObject   *obj);
static void contents_changed_cb  (GObject   *obj, GObject   *obj2, CanvasQueryView *cqv);
static void
canvas_query_view_dispose (GObject   * object)
{
	CanvasQueryView *cqv;
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_QUERY_VIEW (object));

	cqv = CANVAS_QUERY_VIEW (object);

	if (cqv->query && cqv->view) {
		if (! cqv->view_destroyed) 
			g_object_weak_unref (G_OBJECT (cqv->view), (GWeakNotify) view_destroyed_cb, cqv);
		
		if (cqv->view->obj)
			g_signal_handlers_disconnect_by_func (G_OBJECT (cqv->view->obj),
							      G_CALLBACK (contents_changed_cb), cqv);
		cqv->query = NULL;
		cqv->view = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

void
canvas_query_view_free (CanvasQueryView *cqv)
{
	g_return_if_fail (cqv && IS_CANVAS_QUERY_VIEW (cqv));
	gtk_object_destroy (GTK_OBJECT (cqv));

	/*g_object_unref (G_OBJECT (cqv));*/
}

static void post_init (CanvasQueryView * cqv);
static void
canvas_query_view_set_property    (GObject              *object,
				   guint                 param_id,
				   const GValue         *value,
				   GParamSpec           *pspec)
{
	CanvasQueryView *cqv;
	gpointer ptr;

	cqv = CANVAS_QUERY_VIEW (object);

	switch (param_id) {
	case PROP_QUERY:
		ptr = g_value_get_pointer (value);
		g_assert (IS_QUERY (ptr));
		cqv->query = QUERY (ptr);
		break;
	case PROP_QUERY_VIEW:
		ptr = g_value_get_pointer (value);
		if (ptr)
			cqv->view = QUERY_VIEW (ptr);
		break;
	}

	if (cqv->query && cqv->view)
		post_init (cqv);

}

static void 
canvas_query_view_get_property    (GObject              *object,
				   guint                 param_id,
				   GValue               *value,
				   GParamSpec           *pspec)
{
	g_print ("Get Property\n");
}


static void redraw_view_contents (CanvasQueryView *cqv);
static void 
post_init (CanvasQueryView * cqv)
{
	GnomeCanvasItem *item;
	gchar *str;
	double x1, y1, x2, y2;

	/* Title of the Table or query */
	str = query_view_get_textual (cqv->view);
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_TEXT,
				     "font", CQV_DEFAULT_VIEW_FONT_BOLD,
				     "text", str,
				     "x", cqv->x_text_space, 
				     "y", cqv->y_text_space,
				     "fill_color", "black",
				     "justification", GTK_JUSTIFY_RIGHT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST, 
				     NULL);
	g_free (str);
	gnome_canvas_item_get_bounds (item, &x1, &y1, &x2, &y2);

	/* Getting text metrics */
	cqv->title_text_height = y2 - y1;
	cqv->title_text_width = x2 - x1;
	cqv->max_text_width = x2 - x1;

	/* Drawing the contents */
	redraw_view_contents (cqv);

	/* Signals to keep the display up to date */
	g_signal_connect (G_OBJECT (cqv->view->obj), "field_created",
			  G_CALLBACK (contents_changed_cb), cqv);
	g_signal_connect (G_OBJECT (cqv->view->obj), "field_dropped",
			  G_CALLBACK (contents_changed_cb), cqv);

	/* Keep a weak ref on the view, in case it disappears;
	 no need to do the same for the query since when the query disappears, so does the view. */
	g_object_weak_ref (G_OBJECT (cqv->view), (GWeakNotify) view_destroyed_cb, cqv);

	/* VMA: the following line is necessary if we want to get the bounding box of a CanvasQueryView 
	   Updated: no more necessary because bouding box method created for the CanvasQueryView item */
	/*gnome_canvas_update_now (GNOME_CANVAS_ITEM (cqv)->canvas);*/
}

static void 
contents_changed_cb  (GObject   *obj, GObject   *obj2, CanvasQueryView *cqv)
{
	redraw_view_contents (cqv);
}

static void
view_destroyed_cb (CanvasQueryView *cqv, GObject   *obj)
{
	cqv->view_destroyed = TRUE;
	canvas_query_view_free (CANVAS_QUERY_VIEW (cqv));
}


static void field_drag_action_cb (CanvasBase *cb, CanvasBase *drag_from, CanvasBase *drag_to, CanvasQueryView *cqv);
static gdouble compute_y (CanvasQueryView * cqv, gpointer entry);
static int button_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryView *cqv);
static void 
redraw_view_contents (CanvasQueryView *cqv)
{
	GSList *list;
	gboolean is_table;
	GnomeCanvasItem *item;
	gchar *str;
	gdouble x, sqsize, radius;

	/* Destroy any existing GnomeCanvasItem for the fields */
	list = cqv->field_items;
	while (list) {
                gtk_object_destroy (GTK_OBJECT (list->data));
                list = g_slist_next (list);
        }
	g_slist_free (cqv->field_items);
        cqv->field_items = NULL;

	/* Building new fields */
	is_table = IS_DB_TABLE (cqv->view->obj);
	if (is_table)
		list = DB_TABLE (cqv->view->obj)->fields;
	else
		list = QUERY (cqv->view->obj)->fields;
	
	sqsize = cqv->title_text_height * 0.6;
	while (list) {
		if (IS_DB_FIELD (list->data) || 
		    (IS_QUERY_FIELD (list->data) && QUERY_FIELD (list->data)->is_printed)) {
			item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
						     CANVAS_FIELD_TYPE,
						     "x", cqv->x_text_space, 
						     "y", compute_y (cqv, list->data),
						     "canvas_query_view", cqv,
						     "field", list->data,
						     NULL);

			g_object_set_data (G_OBJECT (item), "fieldptr", list->data);
			cqv->field_items = g_slist_append (cqv->field_items, item);
			
			g_signal_connect (G_OBJECT (item), "drag_action",
					  G_CALLBACK (field_drag_action_cb), cqv);
		}

		list = g_slist_next (list);
	}

	/* "button" to close the QueryView and top little frame */
	sqsize = cqv->title_text_height * 0.8;
	x = cqv->max_text_width + cqv->x_text_space;
	if (x - sqsize - 2*cqv->x_text_space < cqv->title_text_width)
		x = cqv->title_text_width + sqsize + 2*cqv->x_text_space;

	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_RECT,
				     "x1", x - sqsize,
				     "y1", cqv->y_text_space,
				     "x2", x,
				     "y2", cqv->y_text_space + sqsize,
				     "fill_color", "white",
				     "outline_color", "black",
				     "width_units", 1.0,
				     NULL);
	gnome_canvas_item_raise_to_top (item);
	g_signal_connect(G_OBJECT(item),"event",
			 G_CALLBACK (button_item_event), cqv);

	if (x - cqv->x_text_space > cqv->max_text_width)
		cqv->max_text_width = x - cqv->x_text_space;

	str = "";
	if (is_table) {
		if (DB_TABLE (cqv->view->obj)->is_view)
			str = CQV_VIEW_COLOR;
		else
			str = CQV_TABLE_COLOR;
	}
	else 
		str = CQV_QUERY_COLOR;

	radius = sqsize * .2;
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_ELLIPSE,
				     "x1", x - sqsize/2. - radius,
				     "y1", cqv->y_text_space + sqsize/2. - radius,
				     "x2", x - sqsize/2. + radius,
				     "y2", cqv->y_text_space + sqsize/2. + radius,
				     "fill_color", str,
				     "outline_color", "black",
				     "width_units", 1.0,
				     NULL);
	gnome_canvas_item_raise_to_top (item);
	g_signal_connect(G_OBJECT(item),"event",
			 G_CALLBACK (button_item_event), cqv);

	/* Top little frame */
	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqv),
				      GNOME_TYPE_CANVAS_RECT,
				      "x1", (double) 0,
				      "y1", (double) 0,
				      "x2", cqv->max_text_width + 2. * cqv->x_text_space,
				      "y2", cqv->title_text_height + 2 * cqv->y_text_space,
				      "outline_color", "black",
				      "fill_color", str, 
				      "width_units", 1.0, 
				      NULL);
	gnome_canvas_item_lower_to_bottom (item);

	/* Outline frame */
	if (cqv->bg_frame)
                gtk_object_destroy (GTK_OBJECT (cqv->bg_frame));

	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqv),
				      GNOME_TYPE_CANVAS_RECT,
				      "x1", (double) 0,
				      "y1", (double) 0,
				      "x2", cqv->max_text_width + 2. * cqv->x_text_space,
				      "y2", compute_y (cqv, NULL),
				      "outline_color", "black",
				      "fill_color", "white",
				      "width_units", 1.0, NULL);
	cqv->bg_frame = item;
	gnome_canvas_item_lower_to_bottom (item);

	/* All the fields have the same width */
	list = cqv->field_items;
	while (list) {
		gnome_canvas_item_set (GNOME_CANVAS_ITEM (list->data), "outline_witdh", 
				       cqv->max_text_width, NULL);
		list = g_slist_next (list);
	}
}

static void 
canvas_query_view_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2)
{
	CanvasQueryView *cqv;

	cqv = CANVAS_QUERY_VIEW (item);

	if (! cqv->bg_frame) {
		*x1 = *y1 = *x2 = *y2 = 0.0;
	}
	else {
		*x1 = 0.;
		*y1 = 0.;
		*x2 = cqv->max_text_width + 2. * cqv->x_text_space;
		*y2 = compute_y (cqv, NULL);
	}
}

static void 
field_drag_action_cb (CanvasBase *cb, CanvasBase *drag_from, CanvasBase *drag_to, CanvasQueryView *cqv)
{
#ifdef debug_signal
	g_print (">> 'DRAG_ACTION' from %s::field_drag_action_cb()\n", __FILE__);
#endif
	g_signal_emit_by_name (G_OBJECT (cqv), "drag_action", drag_from, drag_to);
#ifdef debug_signal
	g_print ("<< 'DRAG_ACTION' from %s::field_drag_action_cb()\n", __FILE__);
#endif
}

/* if entry is NULL, then it gives the "would be" position of the after
   last field in the Query or DbTable */
static gdouble
compute_y (CanvasQueryView * cqv, gpointer entry)
{
	double calc;
	gint pos;
	GSList *list;
	gboolean list_to_free = FALSE;

	if (IS_DB_TABLE (cqv->view->obj))
		list = DB_TABLE (cqv->view->obj)->fields;
	else {
		GSList *iter;
		
		list = NULL;
		iter = QUERY (cqv->view->obj)->fields;
		while (iter) {
			if (QUERY_FIELD (iter->data)->is_printed)
				list = g_slist_append (list, iter->data);
			iter = g_slist_next (iter);
		}

		list_to_free = TRUE;
	}

	if (entry && g_slist_find (list, entry))
		pos = g_slist_index (list, entry);
	else
		pos = g_slist_length (list);

	if (list_to_free)
		g_slist_free (list);

	calc = (3 * (cqv->y_text_space) + cqv->title_text_height) +
                pos * (cqv->y_text_space + cqv->fields_text_height);

        return calc;
}


static void create_alias_cb(GtkWidget * button, CanvasQueryView *cqv);
static void delete_view_cb(GtkWidget * button, CanvasQueryView *cqv);
static int 
button_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryView *cqv)
{
	gboolean done = TRUE;
	GtkWidget *menu, *entry;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		cqv->xmouse = ((GdkEventButton *) event)->x;
		cqv->ymouse = ((GdkEventButton *) event)->y;

		menu = gtk_menu_new ();
		entry = gtk_menu_item_new_with_label (_("Create alias"));
		g_signal_connect (G_OBJECT (entry), "activate",
				  G_CALLBACK (create_alias_cb), cqv);
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		entry = gtk_menu_item_new_with_label (_("Delete"));
		g_signal_connect (G_OBJECT (entry), "activate",
				  G_CALLBACK (delete_view_cb), cqv);
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				NULL, NULL, ((GdkEventButton *)event)->button,
				((GdkEventButton *)event)->time);
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void 
create_alias_cb(GtkWidget * button, CanvasQueryView *cqv)
{
	QueryView *qv;

	qv = query_add_view_with_obj (cqv->query, cqv->view->obj);

	/* set the item at the position the mouse was when the context menu was opened */
	relship_item_set_position (RELSHIP (relship_find (cqv->query)), G_OBJECT (qv), 
				   cqv->xmouse, cqv->ymouse);
}

static void 
delete_view_cb(GtkWidget * button, CanvasQueryView *cqv)
{
	query_del_view (cqv->query, cqv->view);
}


GnomeCanvasItem *
canvas_query_view_find_field (CanvasQueryView *cqv, GObject   *field)
{
	GnomeCanvasItem *found = NULL;
	GSList *list;

	g_return_val_if_fail (IS_CANVAS_QUERY_VIEW (cqv), NULL);
	list = cqv->field_items;
	while (list && !found) {
		if (CANVAS_FIELD (list->data)->field == field)
			found = GNOME_CANVAS_ITEM (list->data);
		list = g_slist_next (list);
	}

	return found;
}
