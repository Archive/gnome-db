/* server-rs.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __SERVER_RESULTSET__
#define __SERVER_RESULTSET__

#include <gtk/gtksignal.h>
#include <gtk/gtkobject.h>
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define SERVER_RESULTSET_TYPE          (server_resultset_get_type())
#define SERVER_RESULTSET(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, server_resultset_get_type(), ServerResultset)
#define SERVER_RESULTSET_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, server_resultset_get_type (), ServerResultsetClass)
#define IS_SERVER_RESULTSET(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, server_resultset_get_type ())


typedef struct _ServerResultset ServerResultset;
typedef struct _ServerResultsetClass ServerResultsetClass;

/* struct for the object's data */
struct _ServerResultset
{
	GObject       object;

	GdaDataModel *model;
	gint          current_row;
	GdaCommand   *cmd;
};

/* struct for the object's class */
struct _ServerResultsetClass
{
	GObjectClass   parent_class;

	void         (*dummy) (ServerResultset * rs);	/* unused */
};

/* generic widget's functions */
guint            server_resultset_get_type     (void);
GObject         *server_resultset_new          (GdaDataModel * model, GdaCommand *cmd);

/* other functions for this object */
gint             server_resultset_get_nbtuples (ServerResultset * rs);
gint             server_resultset_get_nbcols   (ServerResultset * rs);

/* rows in [1..N] and cols in [0..N-1] */
/* gchar* IS allocated! */
gchar           *server_resultset_get_item     (ServerResultset * rs, gint row, gint col);
const GdaValue  *server_resultset_get_gdavalue (ServerResultset * rs, gint row, gint col);

/* convenience function, mem allocated or NULL */
gchar           *server_resultset_stringify    (const GdaValue * value);
const gchar     *server_resultset_get_col_name (ServerResultset * rs, gint col);
void             server_resultset_dump         (ServerResultset * rs);

G_END_DECLS

#endif
