/* canvas-query-view.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __CANVAS_QUERY_VIEW__
#define __CANVAS_QUERY_VIEW__

#include <gnome.h>
#include "canvas-base.h"
#include "query.h"
#include "config.h"

G_BEGIN_DECLS


/* QueryView item for the canvas.  
 *
 * In addition to the GnomeCanvasGroup and CanvasBase arguments, the following object arguments 
 * are available::
 *
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * query                pointer                 RW              The Query to which the QueryView is attached
 * query_view           pointer                 RW              The QueryView being displayed
 * 
 * 
 * NOTE: the "query" and "query_view" arguments are required.
 * 
 */


#define CQV_DEFAULT_VIEW_FONT "Sans 10"
#define CQV_DEFAULT_VIEW_FONT_BOLD "Sans Bold 10"
#define CQV_TABLE_COLOR "lightblue"
#define CQV_VIEW_COLOR  "lightgreen"
#define CQV_QUERY_COLOR "gold"

#define CANVAS_QUERY_VIEW_TYPE          (canvas_query_view_get_type())
#define CANVAS_QUERY_VIEW(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, canvas_query_view_get_type(), CanvasQueryView)
#define CANVAS_QUERY_VIEW_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, canvas_query_view_get_type (), CanvasQueryViewClass)
#define IS_CANVAS_QUERY_VIEW(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, canvas_query_view_get_type ())


typedef struct _CanvasQueryView CanvasQueryView;
typedef struct _CanvasQueryViewClass CanvasQueryViewClass;


/* struct for the object's data */
struct _CanvasQueryView
{
	CanvasBase          object;

	/* objects being represented */
	Query              *query;
	QueryView          *view;
	gboolean            view_destroyed; /* to have a clean finalize */

	/* presentation parameters */
	gdouble             x_text_space;
	gdouble             y_text_space;

	/* UI building information */
	gdouble             title_text_height;
	gdouble             title_text_width;
	gdouble             max_text_width;
	gdouble             fields_text_height;
	GSList             *field_items;
	GnomeCanvasItem    *bg_frame;

	/* pointer position when a context menu was last opened */
	gdouble             xmouse;
	gdouble             ymouse;
};

/* struct for the object's class */
struct _CanvasQueryViewClass
{
	CanvasBaseClass     parent_class;
};

/* generic widget's functions */
guint            canvas_query_view_get_type   (void);

void             canvas_query_view_free       (CanvasQueryView *cqv);

GnomeCanvasItem *canvas_query_view_find_field (CanvasQueryView *cqv, GObject   *field);

G_END_DECLS

#endif
