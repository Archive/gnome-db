/* server-rs.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <config.h>
#include "server-rs.h"
#include <string.h>
#include "marshal.h"

static void server_resultset_class_init (ServerResultsetClass * class);
static void server_resultset_init (ServerResultset * srv);
static void server_resultset_finalize (GObject   * object);
static void server_resultset_res_destroyed (ServerResultset * qres, GdaDataModel * res); /* GWeakNotify */

/* signals */
enum
{
	DUMMY,
	LAST_SIGNAL
};

static gint server_resultset_signals[LAST_SIGNAL] = { 0 };


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass   *parent_class = NULL;


guint
server_resultset_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ServerResultsetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) server_resultset_class_init,
			NULL,
			NULL,
			sizeof (ServerResultset),
			0,
			(GInstanceInitFunc) server_resultset_init
		};		
		
		type = g_type_register_static (G_TYPE_OBJECT, "ServerResultset", &info, 0);
	}
	return type;
}

static void
server_resultset_class_init (ServerResultsetClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	server_resultset_signals[DUMMY] =
		g_signal_new ("dummy",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (ServerResultsetClass, dummy),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE,
				0);

	class->dummy = NULL;
	object_class->finalize = server_resultset_finalize;
}

static void
server_resultset_init (ServerResultset * rs)
{
	rs->cmd = NULL;
	rs->model = NULL;
	rs->current_row = 0;
}

GObject   *
server_resultset_new (GdaDataModel * model, GdaCommand *cmd)
{
	GObject   *obj;
	ServerResultset *rs;

	g_return_val_if_fail (model && GDA_IS_DATA_MODEL (model), NULL);

	obj = g_object_new (SERVER_RESULTSET_TYPE, NULL);
	rs = SERVER_RESULTSET (obj);
	rs->model = model;
	
	g_object_weak_ref (G_OBJECT (model), (GWeakNotify) server_resultset_res_destroyed, rs);

	rs->current_row = 0;
	rs->cmd = cmd;

	return obj;
}

static void
server_resultset_res_destroyed (ServerResultset * rs, GdaDataModel * res)
{
	/* no more data available */
	rs->model = NULL;
	rs->current_row = 0;
	if (rs->cmd) {
		gda_command_free (rs->cmd);
		rs->cmd = NULL;
	}
}

static void
server_resultset_finalize (GObject   * object)
{
	ServerResultset *rs;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_RESULTSET (object));

	rs = SERVER_RESULTSET (object);

	if (rs->model) {
		g_object_weak_unref (G_OBJECT (rs->model), (GWeakNotify) server_resultset_res_destroyed, rs);
		g_object_unref (G_OBJECT (rs->model));
		rs->model = NULL;
	}
	
	if (rs->cmd) {
		gda_command_free (rs->cmd);
		rs->cmd = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

gint
server_resultset_get_nbtuples (ServerResultset * rs)
{
	g_return_val_if_fail (rs && IS_SERVER_RESULTSET (rs), 0);
	g_return_val_if_fail (SERVER_RESULTSET (rs)->model, 0);
	return gda_data_model_get_n_rows (rs->model);
}

gint
server_resultset_get_nbcols (ServerResultset * rs)
{
	g_return_val_if_fail (rs && IS_SERVER_RESULTSET (rs), 0);
	g_return_val_if_fail (SERVER_RESULTSET (rs)->model, 0);
	return gda_data_model_get_n_columns (rs->model);
}



/* 
 * ANY returned string MUST then be freed!
 */
gchar *
server_resultset_get_item (ServerResultset * rs, gint row, gint col)
{
	const GdaValue *value;

	g_return_val_if_fail (rs && IS_SERVER_RESULTSET (rs), NULL);
	g_return_val_if_fail (SERVER_RESULTSET (rs)->model, NULL);

	value = gda_data_model_get_value_at (rs->model, row, col);
	return gda_value_stringify (value);
}

/* warning: rows start at 1 and cols start at 0 */
const GdaValue *
server_resultset_get_gdavalue (ServerResultset * rs, gint row, gint col)
{
	const GdaValue *value;

	g_return_val_if_fail (rs && IS_SERVER_RESULTSET (rs), NULL);
	g_return_val_if_fail (SERVER_RESULTSET (rs)->model, NULL);

	value = gda_data_model_get_value_at (rs->model, row, col);
	return value;
}

const gchar *
server_resultset_get_col_name (ServerResultset * rs, gint col)
{
	const gchar *str;

	g_return_val_if_fail (rs && IS_SERVER_RESULTSET (rs), NULL);
	g_return_val_if_fail (SERVER_RESULTSET (rs)->model, NULL);

	str = gda_data_model_get_column_title (rs->model, col);
	return str;
}

void
server_resultset_dump (ServerResultset * rs)
{
	guint nbtup, nbcols;
	guint *max, i, j, total;
	gchar *str;

	nbtup = server_resultset_get_nbtuples (rs);
	nbcols = server_resultset_get_nbcols (rs);

	g_print ("-- ServerResultset DUMP --\n");
	g_print ("* nbtuples: %d\n", nbtup);
	g_print ("* nbcols:   %d\n", nbcols);
	if (rs->model) {
		max = (guint *) g_malloc (sizeof (guint) * nbcols);
		for (i = 0; i < nbcols; i++)
			max[i] = 0;
		/* max size for the columns */
		for (j = 0; j < nbtup; j++)
			for (i = 0; i < nbcols; i++) {
				str = server_resultset_get_item (rs, j, i);
				if (strlen (str) > max[i])
					max[i] = strlen (str);
				g_free (str);
			}
		total = 1;
		for (i = 0; i < nbcols; i++)
			total += max[i] + 3;

		/* top line */
		g_print ("/");
		for (i = 0; i < total - 2; i++)
			g_print ("-");
		g_print ("\\\n");

		/* printing */
		for (j = 0; j < nbtup; j++) {
			for (i = 0; i < nbcols; i++) {
				str = server_resultset_get_item (rs, j, i);
				g_print ("| %-*s ", max[i], str);
				g_free (str);
			}
			g_print ("|\n");
		}

		/* bottom line */
		g_print ("\\");
		for (i = 0; i < total - 2; i++)
			g_print ("-");
		g_print ("/\n");

		g_free (max);
	}
}

/* creates a GSList with each node pointing to a string which appears
   in the tabular.
   the created list will have to be freed, as well as its strings contents.
   WARNING: only on dimension tabulars allowed! */
GSList *
server_resultset_convert_tabular_to_list (gchar * tab)
{
	GSList *list;
	gchar *wtab, *ptr, *ptr2;

	list = NULL;
	if (tab && (*tab == '{') && (tab[strlen (tab) - 1] == '}')) {
		wtab = g_strdup (tab);
		wtab[strlen (wtab) - 1] = '\0';
		ptr = wtab + 1;
		ptr = strtok (ptr, ",");
		while (ptr) {
			ptr2 = g_strdup (ptr);
			list = g_slist_append (list, ptr2);
			ptr = strtok (NULL, ",");
		}
		g_free (wtab);
	}
	else {
		if (!tab || (*tab != '\0'))
			g_print ("Field %s is not a tabular\n", tab);
	}

	return list;
}

gchar *
server_resultset_stringify (const GdaValue * value)
{
	gchar *retval;

	g_return_val_if_fail (value, NULL);

	retval = gda_value_stringify (value);
	return retval;
}
