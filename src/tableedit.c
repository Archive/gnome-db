/* tableedit.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "tableedit.h"

static void table_edit_class_init (TableEditClass * class);
static void table_edit_init (TableEdit * wid);
static void table_edit_initialize (TableEdit * wid);
static void table_edit_build (TableEdit * wid);
static void table_edit_finalize (GObject   * object);

static void sequence_combo_box_cb (GtkWidget * wid, gpointer data);
static void edit_parent_table_cb (GtkWidget * wid, gpointer data);
static void field_selected_changed_cb (GtkWidget * wid, gint row, gint col,
				       GdkEventButton * event, TableEdit * te);
static void field_selected_selrow_cb (GtkWidget * wid, gint row, gint col,
				      GdkEventButton * event, TableEdit * te);
static void field_selected_unselrow_cb (GtkWidget * wid, gint row, gint col,
					GdkEventButton * event, TableEdit * te);
static void table_comments_changed_cb (GtkWidget * wid, TableEdit * te);

static void update_seq_list_cb (GObject   * obj,
				DbSequence * seq, gpointer data);
static void update_from_seq_cb (GObject   * obj, gpointer data);

/* callbacks for signals emitted by the Database object */
static void field_created_cb (Database * db, DbField * field, gpointer data);
static void field_dropped_cb (Database * db, DbField * field, gpointer data);
static void update_parent_tables_cb (GObject   * obj, gpointer data);

/* for the dialog opened to add a field */
static void add_field_to_table_cb (GtkWidget * wid, gpointer data);
static GtkWidget *add_field_create_dlg (TableEdit * te);
static void add_field_dialog_cb (GnomeDialog * wid, gint button_number,
				 gpointer * data);
static void add_field_dlg_destroy_cb (GObject   * obj, gpointer data);
static void add_field_type_changed_cb (GObject   * obj, gpointer data);
static void add_field_name_changed_cb (GObject   * obj, gpointer data);

static void todo_cb (GtkWidget * widget, ConfManager * conf);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
table_edit_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (TableEditClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) table_edit_class_init,
			NULL,
			NULL,
			sizeof (TableEdit),
			0,
			(GInstanceInitFunc) table_edit_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "TableEdit", &info, 0);
	}
	return type;
}

static void
table_edit_class_init (TableEditClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = table_edit_finalize;
	
}

static void
table_edit_init (TableEdit * wid)
{
	wid->box_table_parents = NULL;
}

GtkWidget *
table_edit_new (ConfManager * conf, DbTable * t)
{
	GObject   *obj;
	TableEdit *wid;

	obj = g_object_new (TABLE_EDIT_TYPE, NULL);
	wid = TABLE_EDIT (obj);
	wid->conf = conf;
	wid->table = t;
	wid->field = NULL;
	wid->frame = NULL;
	wid->box_table_parents = NULL;
	gtk_box_set_homogeneous (GTK_BOX (wid), FALSE);
	gtk_box_set_spacing (GTK_BOX (wid), GNOME_PAD);
	table_edit_build (wid);
	table_edit_initialize (wid);

	return GTK_WIDGET (obj);
}

static void table_edit_finalize (GObject   * object)
{
	TableEdit *te;
	
	g_return_if_fail (object && IS_TABLE_EDIT (object));
	te = TABLE_EDIT (object);
	
	g_signal_handlers_disconnect_by_func (G_OBJECT (te->table),
					      G_CALLBACK (field_created_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (te->table), 
					      G_CALLBACK (field_dropped_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (te->conf->db), 
					      G_CALLBACK (update_parent_tables_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (te->conf->db), 
					      G_CALLBACK (update_from_seq_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (te->conf->db),
					      G_CALLBACK (update_seq_list_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (te->conf->db),
					      G_CALLBACK (update_seq_list_cb),te);


	/* parent_class */
	parent_class->finalize (object);
}

static void
table_edit_build (TableEdit * wid)
{
	GtkWidget *sw, *hb, *hb2, *vb, *vb2, *button, *label, *te, *clist;
	GtkWidget *arrow, *frame, *frame_box;
	gchar *titles[6] =
		{ N_("Field"), N_("Type"), N_("Length"), N_("Null Allowed"),
		N_("Key"), N_("Default value")
	};
	guint i;
	GSList *sens = NULL;

	/* global table properties */
	if (wid->table->is_view)
		frame = gtk_frame_new (_("Global properties for this view:"));
	else
		frame = gtk_frame_new (_
				       ("Global properties for this table:"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), 0);
	gtk_box_pack_start (GTK_BOX (wid), frame, TRUE, TRUE, 0);
	gtk_widget_show (frame);

	frame_box = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_set_border_width (GTK_CONTAINER (frame_box), GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (frame), frame_box);
	gtk_widget_show (frame_box);

	/* Table name */
	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (frame_box), hb, FALSE, TRUE, 0);
	gtk_widget_show (hb);
	if (wid->table->is_view)
		label = gtk_label_new (_("View:"));
	else
		label = gtk_label_new (_("Table:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hb), label, FALSE, FALSE, 0);
	te = gtk_entry_new ();
	gtk_widget_set_sensitive (te, FALSE);
	gtk_box_pack_start (GTK_BOX (hb), te, FALSE, FALSE, 0);
	gtk_widget_show (te);
	wid->te_table_name = te;
	/* TRUE to edit the table's name */
	gtk_widget_set_sensitive (te, FALSE);

	/* Button to add a field to the table */
	button = gtk_button_new_with_label (_("Modify structure"));
	gtk_box_pack_end (GTK_BOX (hb), button, FALSE, TRUE, 0);
	gtk_widget_show (button);
	sens = g_slist_append (sens, button);
	g_signal_connect (G_OBJECT (button), "clicked",
			    G_CALLBACK (todo_cb), wid->conf);
	/* g_signal_connect(G_OBJECT(button), "clicked", */
/* 		     G_CALLBACK(add_field_to_table_cb), wid); */

	/* Table comments */
	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (frame_box), hb, FALSE, TRUE, 0);
	gtk_widget_show (hb);
	label = gtk_label_new (_("Comments:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hb), label, FALSE, FALSE, 0);
	te = gtk_entry_new ();
	wid->te_table_comments = te;
	gtk_box_pack_start (GTK_BOX (hb), te, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (te), "changed",
			  G_CALLBACK (table_comments_changed_cb), wid);
	gtk_widget_show (te);

	/* GtkCList to display the various fields */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (frame_box), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	clist = gtk_clist_new_with_titles (6, titles);
	for (i = 0; i < 6; i++) {
		gtk_clist_set_column_title (GTK_CLIST (clist), i,
					    _(titles[i]));
		gtk_clist_set_column_auto_resize (GTK_CLIST (clist), i, TRUE);
	}
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	g_signal_connect (G_OBJECT (clist), "select_row",
			    G_CALLBACK (field_selected_changed_cb), wid);
	g_signal_connect (G_OBJECT (clist), "select_row",
			    G_CALLBACK (field_selected_selrow_cb), wid);
	g_signal_connect (G_OBJECT (clist), "unselect_row",
			    G_CALLBACK (field_selected_unselrow_cb), wid);
	wid->clist_table_fields = clist;
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_widget_show (clist);

	/* parent tables */
	if ((wid->conf->srv->features.inheritance) && (!wid->table->is_view)) {
		frame = gtk_frame_new (_("Inheritance"));
		gtk_box_pack_start (GTK_BOX (wid), frame, FALSE, TRUE, 0);
		gtk_widget_show (frame);

		hb = gtk_hbox_new (FALSE, GNOME_PAD);
		gtk_container_set_border_width (GTK_CONTAINER (hb),
						GNOME_PAD);
		gtk_container_add (GTK_CONTAINER (frame), hb);
		label = gtk_label_new (_("Parent tables :"));
		gtk_widget_show (label);
		gtk_box_pack_start (GTK_BOX (hb), label, FALSE, TRUE, 0);
		gtk_widget_show (hb);
		hb2 = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (hb), hb2, FALSE, FALSE, 0);
		gtk_widget_show (hb2);
		wid->box_table_parents = hb2;
	}

	/* Sequences links */
	if (wid->conf->srv->features.sequences) {
		if (wid->table->is_view)
			frame = gtk_frame_new (_("Sequence links for this view:"));
		else
			frame = gtk_frame_new (_("Sequence links for this table:"));
		gtk_container_set_border_width (GTK_CONTAINER (frame), 0);
		gtk_box_pack_start (GTK_BOX (wid), frame, FALSE, TRUE, 0);
		gtk_widget_show (frame);

		frame_box = gtk_vbox_new (FALSE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame_box), GNOME_PAD);
		gtk_container_add (GTK_CONTAINER (frame), frame_box);
		gtk_widget_show (frame_box);
		wid->frame = frame_box;


		/* hbox for the 5 main items */
		hb = gtk_hbox_new (FALSE, GNOME_PAD);
		gtk_widget_show (hb);
		gtk_box_pack_start (GTK_BOX (frame_box), hb, TRUE, TRUE, GNOME_PAD);

		/* A VBOX to hold the sequence */
		vb2 = gtk_vbox_new (FALSE, GNOME_PAD);
		gtk_widget_show (vb2);
		gtk_box_pack_start (GTK_BOX (hb), vb2, TRUE, TRUE, 0);

		/* Sequence list */
		vb = gtk_vbox_new (FALSE, GNOME_PAD);
		gtk_widget_show (vb);
		gtk_box_pack_start (GTK_BOX (vb2), vb, TRUE, TRUE, 0);
		button = gtk_combo_new ();
		wid->combobox = button;
		gtk_box_pack_start (GTK_BOX (vb), button, TRUE, TRUE, 0);
		gtk_widget_show (button);
		wid->seqlist = NULL;
		gtk_combo_set_use_arrows (GTK_COMBO (button), TRUE);
		gtk_combo_set_case_sensitive (GTK_COMBO (button), FALSE);
		gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (button)->entry), FALSE);
		gtk_widget_show (vb);
		sens = g_slist_append (sens, button);


		/* middle widgets */
		arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
		gtk_box_pack_start (GTK_BOX (hb), arrow, FALSE, FALSE, 0);
		gtk_widget_show (arrow);
		
		te = gtk_entry_new ();
		gtk_widget_set_sensitive (te, FALSE);
		gtk_box_pack_start (GTK_BOX (hb), te, FALSE, TRUE, 0);
		gtk_entry_set_editable (GTK_ENTRY (te), FALSE);
		gtk_widget_show (te);
		gtk_widget_set_usize (te, 60, -1);
		wid->te_field = te;

		g_object_set_data (G_OBJECT (wid), "sensitives", sens);
		gtk_widget_set_sensitive (wid->frame, FALSE);
	}
}

static void
table_edit_initialize (TableEdit * wid)
{
	GSList *list;
	gchar *text[6];
	guint row;
	DbSequence *seq;

	/* name of the table */
	gtk_entry_set_text (GTK_ENTRY (wid->te_table_name), wid->table->name);
	if (wid->table->comments)
		gtk_entry_set_text (GTK_ENTRY (wid->te_table_comments),
				    wid->table->comments);

	/* Contents of the CList holding all the fields of the table */
	list = wid->table->fields;
	while (list) {
		text[0] = DB_FIELD (list->data)->name;
		text[1] = DB_FIELD (list->data)->type->sqlname;
		if (DB_FIELD (list->data)->length < 0)
			text[2] = g_strdup (_("variable"));
		else
			text[2] =
				g_strdup_printf ("%d",
						 DB_FIELD (list->data)->
						 length);
		if (DB_FIELD (list->data)->null_allowed)
			text[3] = _("Yes");
		else
			text[3] = _("No");
		if (DB_FIELD (list->data)->is_key)
			text[4] = _("Yes");
		else
			text[4] = _("No");

		text[5] = NULL;
		/* a hard coded default value is NOT stronger than an egnima value! */
		/* is there any sequence to that field? */
		seq = database_find_sequence_to_field (wid->conf->db,
						     DB_FIELD (list->
								    data));
		if (seq) {
			text[5] =
				g_strdup_printf ("nextval('%s')", seq->name);
		}
		else {
			if (DB_FIELD (list->data)->default_val)
				text[5] =
					g_strdup (DB_FIELD (list->data)->
						  default_val);
		}
		if (!text[5])
			text[5] = g_strdup ("");
		row = gtk_clist_append (GTK_CLIST (wid->clist_table_fields),
					text);
		g_free (text[2]);
		g_free (text[5]);
		/* DbField pointers are attached to the rows */
		gtk_clist_set_row_data (GTK_CLIST (wid->clist_table_fields),
					row, list->data);
		list = g_slist_next (list);
	}

	/* tables hierarchy */
	if ((wid->conf->srv->features.inheritance) && (!wid->table->is_view))
		update_parent_tables_cb (NULL, wid);

	/* combo box of the known sequences */
	update_seq_list_cb (NULL, NULL, wid);

	/* SIGNALS */
	g_signal_connect (G_OBJECT (wid->table), "field_created",
			  G_CALLBACK (field_created_cb), wid);

	g_signal_connect (G_OBJECT (wid->table), "field_dropped",
			  G_CALLBACK (field_dropped_cb), wid);

	g_signal_connect (G_OBJECT (wid->conf->db), "updated",
			  G_CALLBACK (update_parent_tables_cb), wid);

	g_signal_connect (G_OBJECT (wid->conf->db), "updated",
			  G_CALLBACK (update_from_seq_cb), wid);

	g_signal_connect (G_OBJECT (wid->conf->db), "seq_created",
			  G_CALLBACK (update_seq_list_cb), wid);

	g_signal_connect (G_OBJECT (wid->conf->db),	"seq_dropped",
			  G_CALLBACK (update_seq_list_cb),wid);


	g_signal_connect (G_OBJECT (GTK_COMBO (wid->combobox)->entry), "changed",
			  G_CALLBACK (sequence_combo_box_cb), wid);

	/* widgets for which the sensitiveness changes */
	list = g_object_get_data (G_OBJECT (wid), "sensitives");
	g_object_set_data (G_OBJECT (wid), "sensitives", NULL);
	while (list) {
		GSList *hold;
		conf_manager_register_sensitive_on_connect (wid->conf, GTK_WIDGET (list->data));
		hold = list;
		list = g_slist_remove_link (list, list);
		g_slist_free_1 (hold);
	}
}

static void
table_comments_changed_cb (GtkWidget * wid, TableEdit * te)
{
	db_table_set_comments (te->table, te->conf, gtk_entry_get_text (GTK_ENTRY (wid)), TRUE);
}

/***************************************************************************/
/*                                                                         */
/* Management of the links                                                 */
/*                                                                         */
/***************************************************************************/
/* DO NOT USE "obj", sometimes called with obj = NULL.
   data is a pointer to TableEdit */
static void
update_from_seq_cb (GObject   * obj, gpointer data)
{
	TableEdit *te = TABLE_EDIT (data);
	gboolean found = FALSE;

	if (!te->conf->srv->features.sequences)
		return;

	/* this function must prevent the GtkEntry in the Combo box to
	   emit a "changed" signal, because it is not a change resulting
	   from an action of the user on a selected field, but a change for the
	   needs of the GUI */
	g_signal_handlers_block_by_func (G_OBJECT (GTK_COMBO (te->combobox)->entry),
					 G_CALLBACK (sequence_combo_box_cb), te);

	if (te->field) {
		GSList *seq, *list;

		seq = te->conf->db->sequences;
		while (seq && !found) {
			list = DB_SEQUENCE (seq->data)->field_links;
			while (list && !found) {
				if (list->data == te->field) {
					found = TRUE;
					gtk_entry_set_text (GTK_ENTRY
							    (GTK_COMBO
							     (te->combobox)->
							     entry),
							    DB_SEQUENCE (seq->
									 data)->
							    name);
				}
				list = g_slist_next (list);
			}
			seq = g_slist_next (seq);
		}
	}
	if (!te->field || !found) {
		/* set the "<NO SEQUENCE>" in the text entry */
		gtk_entry_set_text (GTK_ENTRY
				    (GTK_COMBO (te->combobox)->entry),
				    (gchar *) (te->seqlist->data));
	}


	/* unblocking the signal for the sake of the GUI */
	g_signal_handlers_unblock_by_func (G_OBJECT (GTK_COMBO (te->combobox)->entry),
					   G_CALLBACK (sequence_combo_box_cb), te);
}

/* Updates the GList of the availables sequences, nothing else 
   DO NOT USE "obj", sometimes called with obj = NULL.
   data is a pointer to TableEdit */
static void
update_seq_list_cb (GObject   * obj, DbSequence * seq, gpointer data)
{
	GSList *list;
	GList *dlist;
	TableEdit *te;
	gboolean found = FALSE;

	te = TABLE_EDIT (data);

	/* removing the existing contents of the combo list will be done 
	   automatically by the GtkCombo object when passing a new list */
	list = te->conf->db->sequences;
	if (te->seqlist) {
		g_list_free (te->seqlist);
		te->seqlist = NULL;
	}
	te->seqlist = g_list_append (te->seqlist, _("<NO SEQUENCE>"));
	while (list) {
		te->seqlist =
			g_list_append (te->seqlist,
				       DB_SEQUENCE (list->data)->name);
		list = g_slist_next (list);
	}
	gtk_combo_set_popdown_strings (GTK_COMBO (te->combobox), te->seqlist);

	/* if the value of the TextEntry element of the combo does not exist
	   anymore in the sequence, we put the "<NO SEQUENCE>" one */
	dlist = te->seqlist;
	while (dlist && !found) {
		if (!strcmp ((gchar *) (dlist->data),
			     gtk_entry_get_text (GTK_ENTRY
						 (GTK_COMBO (te->combobox)->
						  entry))))
			found = TRUE;
		else
			dlist = g_list_next (dlist);
	}
	if (!found)
		gtk_entry_set_text (GTK_ENTRY
				    (GTK_COMBO (te->combobox)->entry),
				    (gchar *) (te->seqlist->data));

}


/***************************************************************************/
/*                                                                         */
/* Dialog created to add a field to a table                                */
/*                                                                         */
/***************************************************************************/
/* Cb called when we want to add a field to a table
   data is a pointer to TableEdit */
static void
add_field_to_table_cb (GtkWidget * wid, gpointer data)
{
	TableEdit *te = TABLE_EDIT (data);
	GtkWidget *dlg;
	/* will create a new dialog */

	dlg = add_field_create_dlg (te);
	if (dlg) {
		gtk_widget_show (dlg);
	}
}

/* FIXME: this function needs to be re-written when implementing schemas edition */
static GtkWidget *
add_field_create_dlg (TableEdit * te)
{
	GtkWidget *dlg, *wid, *tab;
	GtkObject   *adj;
	gchar *str;
	GList *list;

	str = g_strdup_printf (_("Add a field to table %s"), te->table->name);
	dlg = gtk_dialog_new_with_buttons (str, NULL, 0,
					   GTK_STOCK_OK, GTK_RESPONSE_OK,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
	g_free (str);
	g_object_set_data (G_OBJECT (dlg), "conf", te->conf);	/* DATA */

	/* small text at the top */
	str = g_strdup_printf (_("Enter the attributes of the field\n"
				 "you wish to add to table '%s'"),
			       te->table->name);
	wid = gtk_label_new (str);
	g_free (str);
	gtk_widget_show (wid);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), wid,
			    FALSE, FALSE, 0);

	/* table to hold all the elements */
	tab = gtk_table_new (3, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), tab,
			    FALSE, FALSE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (tab), GNOME_PAD);
	gtk_table_set_col_spacings (GTK_TABLE (tab), GNOME_PAD);
	gtk_widget_show (tab);

	/* field name */
	wid = gtk_label_new (_("Field name:"));
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 0, 1, 0, 1);
	gtk_widget_show (wid);

	wid = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 1, 2, 0, 1);
	g_object_set_data (G_OBJECT (dlg), "fieldname", wid);	/* DATA */
	g_signal_connect (G_OBJECT (wid), "changed",
			    G_CALLBACK (add_field_name_changed_cb), dlg);
	gtk_widget_show (wid);

	/* field type */
	wid = gtk_label_new (_("Field type:"));
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 0, 1, 1, 2);
	gtk_widget_show (wid);

	wid = gtk_combo_new ();
	list = server_access_get_data_type_list (te->conf->srv);
	gtk_combo_set_popdown_strings (GTK_COMBO (wid), list);
	g_list_free (list);
	gtk_combo_set_use_arrows (GTK_COMBO (wid), TRUE);
	gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (wid)->entry), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 1, 2, 1, 2);
	g_object_set_data (G_OBJECT (dlg), "fieldtype", GTK_COMBO (wid)->entry);	/* DATA */
	g_signal_connect (G_OBJECT (GTK_COMBO (wid)->entry), "changed",
			    G_CALLBACK (add_field_type_changed_cb), dlg);
	gtk_widget_show (wid);

	/* field length (optionnal function of the type) */
	wid = gtk_label_new (_("Length:"));
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 0, 1, 2, 3);
	gtk_widget_show (wid);

	adj = gtk_adjustment_new (30, 1, 10000, 1, 10, 10);

	wid = gtk_spin_button_new (GTK_ADJUSTMENT (adj), 0.5, 0);
	gtk_table_attach_defaults (GTK_TABLE (tab), wid, 1, 2, 2, 3);
	g_object_set_data (G_OBJECT (dlg), "fieldlength", wid);	/* DATA */
	gtk_widget_show (wid);

	/* signals */
	g_signal_connect (G_OBJECT (dlg), "clicked",
			    G_CALLBACK (add_field_dialog_cb), te);

	/* FIXME: remove this handler if dlg is closed */
	g_signal_connect (G_OBJECT (g_object_get_data (G_OBJECT (te), "dlg")), "destroy",
			  G_CALLBACK (add_field_dlg_destroy_cb), dlg);

	/* if the server disconnects, the DLG is removed */
	/* FIXME: remove that signal connect if dlg is closed */
	g_signal_connect (G_OBJECT (te->conf->srv), "conn_closed",
			  G_CALLBACK (add_field_dlg_destroy_cb), dlg);

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dlg), GTK_RESPONSE_OK, FALSE);
	add_field_type_changed_cb (NULL, dlg);
	return dlg;
}

/* data is a pointer on TableEdit */
static void
add_field_dialog_cb (GnomeDialog * wid, gint button_number, gpointer * data)
{
	TableEdit *te = TABLE_EDIT (data);
	GtkWidget *type, *name, *length;
	ConfManager *conf;
	GString *str;
	ServerDataType *stype;

	switch (button_number) {
	case 0:		/* OK button */
		conf = (ConfManager *)
			g_object_get_data (G_OBJECT (wid), "conf");
		name = GTK_WIDGET (g_object_get_data
				   (G_OBJECT (wid), "fieldname"));
		type = GTK_WIDGET (g_object_get_data
				   (G_OBJECT (wid), "fieldtype"));
		length = GTK_WIDGET (g_object_get_data
				     (G_OBJECT (wid), "fieldlength"));
		/* TODO: test to see if there is no bad characters in name: ',\... */
		stype = server_access_get_type_from_name (conf->srv,
							  gtk_entry_get_text
							  (GTK_ENTRY (type)));
		str = g_string_new (NULL);
		if (stype->numparams)
			g_string_sprintf (str,
					  "alter table %s add column %s %s (%s)",
					  te->table->name,
					  gtk_entry_get_text (GTK_ENTRY
							      (name)),
					  stype->sqlname,
					  gtk_entry_get_text (GTK_ENTRY
							      (length)));
		else
			g_string_sprintf (str,
					  "alter table %s add column %s %s",
					  te->table->name,
					  gtk_entry_get_text (GTK_ENTRY
							      (name)),
					  stype->sqlname);
		/* now do the query */
		server_access_do_query (conf->srv, str->str, SERVER_ACCESS_QUERY_SQL);
		g_string_free (str, TRUE);
		database_refresh (conf->db, conf->srv);
		break;
	case 1:		/* Cancel button */
		break;
	}
	gnome_dialog_close (GNOME_DIALOG (wid));
}

/* DO NOT use obj because some calls are made with obj=NULL */
static void
add_field_type_changed_cb (GObject   * obj, gpointer data)
{
	GtkWidget *combote, *flength;
	ServerDataType *type;
	ConfManager *conf;

	conf = (ConfManager *) g_object_get_data (G_OBJECT (data),
							  "conf");
	combote =
		GTK_WIDGET (g_object_get_data
			    (G_OBJECT (data), "fieldtype"));
	flength =
		GTK_WIDGET (g_object_get_data
			    (G_OBJECT (data), "fieldlength"));
	type = server_access_get_type_from_name (conf->srv,
					      gtk_entry_get_text (GTK_ENTRY
								  (combote)));
	if (type->numparams)
		gtk_widget_set_sensitive (flength, TRUE);
	else
		gtk_widget_set_sensitive (flength, FALSE);
}

static void
add_field_name_changed_cb (GObject   * obj, gpointer data)
{
	GtkWidget *namete;

	namete = GTK_WIDGET (g_object_get_data
			     (G_OBJECT (data), "fieldname"));
	if (*gtk_entry_get_text (GTK_ENTRY (namete)) == '\0')
		gnome_dialog_set_sensitive (GNOME_DIALOG (data), 0, FALSE);
	else
		gnome_dialog_set_sensitive (GNOME_DIALOG (data), 0, TRUE);
}

static void
add_field_dlg_destroy_cb (GObject   * obj, gpointer data)
{
	gnome_dialog_close (GNOME_DIALOG (data));
}

/***************************************************************************/
/*                                                                         */
/* CBs to catch the signals emitted by the Database and DbTable objects   */
/*                                                                         */
/***************************************************************************/
/* data must be a pointer to TableEdit */
static void
update_parent_tables_cb (GObject   * obj, gpointer data)
{
	TableEdit *te = TABLE_EDIT (data);
	GSList *list;
	GList *dlist;

	if (te->conf->srv->features.inheritance) {
		/* first clear all the existing buttons to tables */
		dlist = gtk_container_children (GTK_CONTAINER
						(te->box_table_parents));
		while (dlist) {
			GList *hold;
			gtk_container_remove (GTK_CONTAINER (te->box_table_parents),
					      GTK_WIDGET (dlist->data));
			hold = dlist;
			dlist = g_list_remove_link (dlist, dlist);
			g_list_free_1 (hold);
		}

		/* then refill everything */
		list = te->table->parents;
		if (list)
			while (list) {
				GtkWidget *button;

				button = gtk_button_new_with_label
					(DB_TABLE (list->data)->name);
				g_object_set_data (G_OBJECT (button), "user_data", te->conf);
				g_signal_connect (G_OBJECT (button),
						    "clicked",
						    G_CALLBACK
						    (edit_parent_table_cb),
						    list->data);
				gtk_box_pack_start (GTK_BOX
						    (te->box_table_parents),
						    button, FALSE, FALSE, 0);
				gtk_widget_show (button);
				list = g_slist_next (list);
			}
		else {
			GtkWidget *label;
			label = gtk_label_new (_("NONE"));
			gtk_box_pack_start (GTK_BOX (te->box_table_parents),
					    label, FALSE, FALSE, 0);
			gtk_widget_show (label);
		}
	}
}

/* data must be a pointer to TableEdit */
static void
field_created_cb (Database * db, DbField * field, gpointer data)
{
	TableEdit *te;
	gchar *text[6];
	gint row;
	DbSequence *seq;

	te = TABLE_EDIT (data);
	/* the created field will be in the end */
	text[0] = field->name;
	text[1] = field->type->sqlname;
	if (field->length < 0)
		text[2] = g_strdup (_("variable"));
	else
		text[2] = g_strdup_printf ("%d", field->length);
	if (field->null_allowed)
		text[3] = _("Yes");
	else
		text[3] = _("No");
	if (field->is_key)
		text[4] = _("Yes");
	else
		text[4] = _("No");

	/* a hard coded default value is NOT stronger than an egnima value! */
	/* is there any sequence to that field? */
	seq = database_find_sequence_to_field (te->conf->db, field);
	text[5] = NULL;
	if (seq) {
		text[5] = g_strdup_printf ("nextval('%s')", seq->name);
	}
	else {
		if (field->default_val)
			text[5] = g_strdup (field->default_val);
	}
	if (!text[5])
		text[5] = g_strdup ("");
	row = gtk_clist_append (GTK_CLIST (te->clist_table_fields), text);
	g_free (text[2]);
	g_free (text[5]);
	/* DbField pointers are attached to the rows */
	gtk_clist_set_row_data (GTK_CLIST (te->clist_table_fields), row,
				field);
}

/* data must be a pointer to TableEdit */
static void
field_dropped_cb (Database * db, DbField * field, gpointer data)
{
	TableEdit *te = TABLE_EDIT (data);
	gint row;

	row = gtk_clist_find_row_from_data (GTK_CLIST
					    (te->clist_table_fields), field);
	if (row >= 0) {
		gtk_clist_remove (GTK_CLIST (te->clist_table_fields), row);
		/* in case the selected field is the one removed */
		if (te->field == field) {
			te->field = NULL;
			gtk_entry_set_text (GTK_ENTRY (te->te_field), "");
			/* links on the right: calling CB with NULL obj */
			update_from_seq_cb (NULL, te);
		}
	}
}

/***************************************************************************/
/*                                                                         */
/* Other CALLBACKS                                                         */
/*                                                                         */
/***************************************************************************/
/* CB called from the CList containing the table's fields.
   data must be a pointer to TableEntry */
static void
field_selected_changed_cb (GtkWidget * wid, gint row, gint col,
			   GdkEventButton * event, TableEdit *te)
{
	DbField *field;

	field = DB_FIELD (gtk_clist_get_row_data (GTK_CLIST (wid), row));
	te->field = field;
	/* name of the field */
	gtk_entry_set_text (GTK_ENTRY (te->te_field), field->name);
	/* links on the right: calling CB with NULL obj */
	update_from_seq_cb (NULL, te);
}

static void field_selected_selrow_cb (GtkWidget * wid, gint row, gint col,
				      GdkEventButton * event, TableEdit * te)
{
	gtk_widget_set_sensitive (te->frame, TRUE);
}

static void field_selected_unselrow_cb (GtkWidget * wid, gint row, gint col,
					GdkEventButton * event, TableEdit * te)
{
	gtk_widget_set_sensitive (te->frame, FALSE);
}


/* CB to open another dialog of this type if we click on a parent's name
   to have some information. data points onto a DbTable */
static void
edit_parent_table_cb (GtkWidget * wid, gpointer data)
{
	GtkWidget *dlg;
	gpointer user_data;

	user_data = g_object_get_data (G_OBJECT (wid), "user_data");
	if (user_data) {
		dlg = table_edit_dialog_new ((ConfManager *) user_data,
					     DB_TABLE (data));
		if (dlg)
			gtk_widget_show (dlg);
	}
}

/* CB to change the sequence linked to the field.
   data is a TableEdit*  */
static void
sequence_combo_box_cb (GtkWidget * wid, gpointer data)
{
	TableEdit *te = TABLE_EDIT (data);
	DbSequence *seq;
	gchar *seq_name;
	gint i;
	gboolean found;
	gchar *str;

	if (te->field) {
		/* we need to copy the name of the sequence in seq_name because the content
		   of the combobox will be set to "<NO SEQUENCE>" when the Database object
		   is updated and sends the "updated" signal */
		seq_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (wid)));
		database_delete_seq_field_link (te->conf->db, NULL, te->field);
		seq = database_find_sequence_by_name (te->conf->db, seq_name);
		if (seq) {
			database_insert_seq_field_link (te->conf->db, seq,
						      te->field);
		}
		g_free (seq_name);

		/* updating what is displayed in the clist for the default value */
		found = FALSE;
		i = 0;
		while (!found) {
			if (te->field ==
			    gtk_clist_get_row_data (GTK_CLIST
						    (te->clist_table_fields),
						    i)) {
				found = TRUE;
			}
			else
				i++;
		}
		str = NULL;
		if (seq) {
			str = g_strdup_printf ("nextval('%s')", seq->name);
		}
		else {
			if (te->field->default_val)
				str = g_strdup (te->field->default_val);
		}
		if (!str)
			str = g_strdup ("");
		gtk_clist_set_text (GTK_CLIST (te->clist_table_fields), i, 5,
				    str);
		g_free (str);
	}
}

/* if the dropped table is ours then we close the Dialog which must
   be passed as data. data must contain a user_data on TableEdit object */
static void table_edit_drop_table_cb (GObject   * obj, DbTable * table, TableEdit *te);
void
table_edit_drop_table_cb (GObject   * obj, DbTable * table, TableEdit *te)
{
	GtkWidget *dlg;

	if (te->table == table) {
		dlg = GTK_WIDGET (g_object_get_data
				  (G_OBJECT (te), "dlg"));
		gtk_widget_destroy (GTK_WIDGET (dlg));
	}
}

/*********************************************************************/
/*                                                                   */
/* functions for the integration of the object into a GNOME Dialog   */
/*                                                                   */
/*********************************************************************/
static void
catch_conn_to_close_cb (GObject   * obj, GtkDialog * dlg)
{
	gtk_widget_destroy (GTK_WIDGET (dlg));
}

static void table_edit_dlg_destroy_cb (GtkWidget * wid, ConfManager *conf);
static void table_edit_dlg_response_cb (GtkDialog * wid, gint button_number,
					ConfManager *conf);
GtkWidget *
table_edit_dialog_new (ConfManager * conf, DbTable * t)
{
	GtkWidget *dlg = NULL, *content;
	gchar *title;
	gint row;

	row = main_page_table_get_row (MAIN_PAGE_TABLE (conf->tables_page),
				       t);
	if (! (dlg = main_page_table_get_data_dlg (MAIN_PAGE_TABLE
						   (conf->tables_page), row))) {
		if (t->is_view)
			title = g_strdup_printf (_("Properties for view '%s'"), t->name);
		else
			title = g_strdup_printf (_("Properties for table '%s'"), t->name);
		dlg = gtk_dialog_new_with_buttons (title, NULL, 0,
						   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
						   NULL);
		gtk_widget_set_usize (dlg, 500, 500);
		gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
		g_free (title);

		content = table_edit_new (conf, t);

		g_object_set_data (G_OBJECT (content), "dlg", dlg);

		g_signal_connect (G_OBJECT (conf->db), "table_dropped",
				  G_CALLBACK (table_edit_drop_table_cb), content);

		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox),
				    content, TRUE, TRUE, GNOME_PAD/2.);

		gtk_widget_show (content);

		/* set some data to DbTable */
		g_object_set_data (G_OBJECT (dlg), "user_data", t);
		g_object_set_data (G_OBJECT (dlg), "te", content);

		g_signal_connect (G_OBJECT (dlg), "response",
				  G_CALLBACK (table_edit_dlg_response_cb), conf);
		g_signal_connect (G_OBJECT (dlg), "destroy",
				  G_CALLBACK (table_edit_dlg_destroy_cb), conf);
		
		g_signal_connect (G_OBJECT (conf->srv), "conn_to_close",
				  G_CALLBACK (catch_conn_to_close_cb), dlg);

		main_page_table_set_data_dlg (MAIN_PAGE_TABLE (conf->tables_page), row, dlg);
	}
	else
		gdk_window_raise (dlg->window);
	return dlg;
}

/* data is a pointer on ConfManager */
static void
table_edit_dlg_destroy_cb (GtkWidget * wid, ConfManager *conf)
{
	gint row;
	TableEdit *te;

	te = TABLE_EDIT (g_object_get_data (G_OBJECT (wid), "te"));

	/* releasing the pointer on the dialog to be able to reopen one later */
	row = main_page_table_get_row (MAIN_PAGE_TABLE (conf->tables_page),
				       g_object_get_data (G_OBJECT (wid), "user_data"));
	main_page_table_set_data_dlg (MAIN_PAGE_TABLE (conf->tables_page),
				      row, NULL);

	/* disconnect signal */
	g_signal_handlers_disconnect_by_func (G_OBJECT (conf->db),
					      G_CALLBACK (table_edit_drop_table_cb), te);

	g_signal_handlers_disconnect_by_func (G_OBJECT (conf->srv), 
					      G_CALLBACK (catch_conn_to_close_cb), wid);
}


/* data is a pointer on ConfManager */
static void
table_edit_dlg_response_cb (GtkDialog * wid, gint button_number, ConfManager *conf)
{
	gtk_widget_destroy (GTK_WIDGET (wid));
}

static void
todo_cb (GtkWidget * widget, ConfManager * conf)
{
	gnome_ok_dialog_parented (_("Function still non implemented\n"),
				  GTK_WINDOW (conf->app));
}
