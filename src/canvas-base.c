/* canvas-base.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "marshal.h"
#include "canvas-base.h"
#include "database.h"
#include "query.h"

/*
 * 
 * CanvasBase object
 * 
 */


static void canvas_base_class_init (CanvasBaseClass * class);
static void canvas_base_init       (CanvasBase * item);
static void canvas_base_finalize    (GObject   * object);

static void canvas_base_set_property    (GObject              *object,
					 guint                 param_id,
					 const GValue         *value,
					 GParamSpec           *pspec);
static void canvas_base_get_property    (GObject              *object,
					 guint                 param_id,
					 GValue               *value,
					 GParamSpec           *pspec);

enum
{
	MOVED,
	MOVED_CONT,
	DRAG_ACTION,
	ENTER_NOTIFY,
	LEAVE_NOTIFY,
	LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_ALLOW_MOVE,
	PROP_ALLOW_DRAG,
	PROP_TOOLTIP_OBJ
};

static gint canvas_base_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *base_parent_class = NULL;

guint
canvas_base_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (CanvasBaseClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) canvas_base_class_init,
			NULL,
			NULL,
			sizeof (CanvasBase),
			0,
			(GInstanceInitFunc) canvas_base_init
		};
		type = g_type_register_static (GNOME_TYPE_CANVAS_GROUP, "CanvasBase", &info, 0);
	}

	return type;
}


static void
canvas_base_class_init (CanvasBaseClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	base_parent_class = g_type_class_peek_parent (class);

	canvas_base_signals[MOVED] =
		g_signal_new ("moved",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (CanvasBaseClass, moved),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE,
				0);
	canvas_base_signals[MOVED_CONT] =
		g_signal_new ("moved_cont",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (CanvasBaseClass, moved_cont),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE,
				0);
	canvas_base_signals[DRAG_ACTION] =
		g_signal_new ("drag_action",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (CanvasBaseClass, drag_action),
				NULL, NULL,
				marshal_VOID__POINTER_POINTER,
				G_TYPE_NONE, 2, G_TYPE_POINTER,
				G_TYPE_POINTER);
	canvas_base_signals[ENTER_NOTIFY] =
		g_signal_new ("enter_notify",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (CanvasBaseClass, enter_notify),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE,
				0);
	canvas_base_signals[LEAVE_NOTIFY] =
		g_signal_new ("leave_notify",
				G_TYPE_FROM_CLASS (object_class),
				G_SIGNAL_RUN_FIRST,
				G_STRUCT_OFFSET (CanvasBaseClass, leave_notify),
				NULL, NULL,
				marshal_VOID__VOID, G_TYPE_NONE,
				0);

	class->moved = NULL;
	class->moved_cont = NULL;
	class->drag_action = NULL;
	class->enter_notify = NULL;
	class->leave_notify = NULL;
	object_class->finalize = canvas_base_finalize;

	/* Properties */
	object_class->set_property = canvas_base_set_property;
	object_class->get_property = canvas_base_get_property;

	g_object_class_install_property
                (object_class, PROP_ALLOW_MOVE,
                 g_param_spec_boolean ("allow_move", NULL, NULL, TRUE, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
                (object_class, PROP_ALLOW_DRAG,
                 g_param_spec_boolean ("allow_drag", NULL, NULL, FALSE, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
                (object_class, PROP_TOOLTIP_OBJ,
                 g_param_spec_pointer ("tooltip_object", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static int item_event(CanvasBase *cb, GdkEvent *event, gpointer data);

static void
canvas_base_init (CanvasBase * item)
{
	item->moving = FALSE;
	item->xstart = 0;
	item->ystart = 0;
	item->allow_move = TRUE;
	item->allow_drag = FALSE;
	item->tooltip = NULL;
	
	g_signal_connect(G_OBJECT(item), "event",
			 G_CALLBACK (item_event), NULL);
}


static void
canvas_base_finalize (GObject   * object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_BASE (object));

	/* for the parent class */
	base_parent_class->finalize (object);
}


static void tooltip_destroy_cb (CanvasBase *cb, GObject *tooltip);
static void 
canvas_base_set_property    (GObject              *object,
			     guint                 param_id,
			     const GValue         *value,
			     GParamSpec           *pspec)
{
	CanvasBase *cb;
	gpointer obj;

	cb = CANVAS_BASE (object);

	switch (param_id) {
	case PROP_ALLOW_MOVE:
		cb->allow_move = g_value_get_boolean (value);
		if (cb->allow_move && cb->allow_drag)
			cb->allow_drag = FALSE;
		break;
	case PROP_ALLOW_DRAG:
		cb->allow_drag = g_value_get_boolean (value);
		if (cb->allow_drag && cb->allow_move)
			cb->allow_move = FALSE;
		break;
	case PROP_TOOLTIP_OBJ:
		obj = g_value_get_pointer (value);
		if (!obj) {
			if (cb->tooltip) {
				g_object_weak_unref (G_OBJECT (cb->tooltip), (GWeakNotify) tooltip_destroy_cb, cb);
				cb->tooltip = NULL;
			}
		}
		else {
			if (G_IS_OBJECT (obj)) {
				cb->tooltip = G_OBJECT (obj);
				g_object_weak_ref (G_OBJECT (obj), (GWeakNotify) tooltip_destroy_cb, cb);
			}
		}
	}

}

static void 
tooltip_destroy_cb (CanvasBase *cb, GObject *tooltip)
{
	cb->tooltip = NULL;
}

static void
canvas_base_get_property    (GObject              *object,
			     guint                 param_id,
			     GValue               *value,
			     GParamSpec           *pspec)
{
	g_print("Get Property\n");
}



static void end_of_drag_cb (CanvasBase *cb, GObject *cursor);
static void end_of_drag_cb_d (GnomeCanvasItem *ci, CanvasBase *cb);
static gboolean add_tip_timeout (CanvasBase *cb);
static gboolean display_tip_timeout (CanvasBase *cb);
static int 
item_event(CanvasBase *cb, GdkEvent *event, gpointer data)
{
	gboolean done = TRUE;
	GnomeCanvasItem *ci;
	GObject   *obj;
	guint id;
	gdouble pos;

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		/* Drag management */
		ci = g_object_get_data (G_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)),
					  "dragged_from");
		if (ci && IS_CANVAS_BASE (ci)) {
#ifdef debug_signal
			g_print (">> 'DRAG_ACTION' from %s::item_event()\n", __FILE__);
#endif
			g_signal_emit (G_OBJECT (cb), canvas_base_signals[DRAG_ACTION], 0, ci, cb);
#ifdef debug_signal
			g_print ("<< 'DRAG_ACTION' from %s::item_event()\n", __FILE__);
#endif
			g_object_set_data (G_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)),
					   "dragged_from", NULL);
		}
		g_signal_emit (G_OBJECT (cb), canvas_base_signals[ENTER_NOTIFY], 0);
		done = FALSE;
		break;
	case GDK_LEAVE_NOTIFY:
		/* remove tooltip */
		obj = g_object_get_data (G_OBJECT (cb), "tip");
                if (obj) {
                        canvas_tip_free (CANVAS_TIP (obj));
			g_object_set_data (G_OBJECT (cb), "tip", NULL);
		}

                id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "addtipid"));
                if (id != 0) {
                        g_source_remove (id);
                        g_object_set_data (G_OBJECT (cb), "addtipid", NULL);
                }
                id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "displaytipid"));
                if (id != 0) {
                        g_source_remove (id);
                        g_object_set_data (G_OBJECT (cb), "displaytipid", NULL);
                }
		g_signal_emit (G_OBJECT (cb), canvas_base_signals[LEAVE_NOTIFY], 0);
		done = FALSE;
		break;
	case GDK_BUTTON_PRESS:
		switch (((GdkEventButton *) event)->button) {
		case 1:
			if (cb->allow_move) {
				/* movement management */
				gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (cb));
				cb->xstart = ((GdkEventMotion *) event)->x;
				cb->ystart = ((GdkEventMotion *) event)->y;
				cb->moving = TRUE;
			}
			if (cb->allow_drag) {
				/* remove tooltip */
				obj = g_object_get_data (G_OBJECT (cb), "tip");
				if (obj) {
					canvas_tip_free (CANVAS_TIP (obj));
					g_object_set_data (G_OBJECT (cb), "tip", NULL);
				}
				id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "addtipid"));
				if (id != 0) {
					g_source_remove (id);
					g_object_set_data (G_OBJECT (cb), "addtipid", NULL);
				}
				id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "displaytipid"));
				if (id != 0) {
					g_source_remove (id);
					g_object_set_data (G_OBJECT (cb), "displaytipid", NULL);
				}

				/* start dragging */
				g_signal_emit (G_OBJECT (cb), canvas_base_signals[LEAVE_NOTIFY], 0);
				ci = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cb),
							    CANVAS_DRAG_CURSOR_TYPE,
							    "x", ((GdkEventButton *) event)->x - 5.,
							    "y", ((GdkEventButton *) event)->y - 5.,
							    "fill_color", "white",
							    NULL);
				g_object_weak_ref (G_OBJECT (ci), (GWeakNotify) end_of_drag_cb, cb);

				/* this weak ref is in case cb is destroyed before ci */
				g_object_weak_ref (G_OBJECT (cb), (GWeakNotify) end_of_drag_cb_d, ci);
				
				CANVAS_BASE (ci)->xstart = ((GdkEventButton *) event)->x;
				CANVAS_BASE (ci)->ystart = ((GdkEventButton *) event)->y;
				CANVAS_BASE (ci)->moving = TRUE;
				gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (ci));
				gnome_canvas_item_reparent (ci, gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas));
				gnome_canvas_item_grab (ci, GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
							NULL, GDK_CURRENT_TIME);
			}
			break;
		default:
			break;
		}
		break;
	case GDK_BUTTON_RELEASE:
		if (cb->allow_move) {
			if (cb->moving)
				cb->moving = FALSE;
#ifdef debug_signal
			g_print (">> 'MOVED' from %s::item_event()\n", __FILE__);
#endif
			g_signal_emit (G_OBJECT (cb), canvas_base_signals[MOVED], 0);
#ifdef debug_signal
			g_print ("<< 'MOVED' from %s::item_event()\n", __FILE__);
#endif
			done = FALSE;
		}	
		break;
	case GDK_MOTION_NOTIFY:
		if (cb->moving) {
			GdkEventMotion *evm = (GdkEventMotion *) (event);

			g_assert (IS_CANVAS_BASE (cb));
			gnome_canvas_item_move (GNOME_CANVAS_ITEM (cb), 
						(double) evm->x - cb->xstart, 
						(double) evm->y - cb->ystart);
			cb->xstart = ((GdkEventMotion *) event)->x;
			cb->ystart = ((GdkEventMotion *) event)->y;
			g_signal_emit (G_OBJECT (cb), canvas_base_signals[MOVED_CONT], 0);
		}
		else {
			if (! g_object_get_data (G_OBJECT (cb), "tip")) {
				/* set tooltip */
				id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "addtipid"));
				if (id != 0) {
					g_source_remove (id);
					g_object_set_data (G_OBJECT (cb), "addtipid", NULL);
				}
				id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (cb), "displaytipid"));
				if (id != 0) {
					g_source_remove (id);
					g_object_set_data (G_OBJECT (cb), "displaytipid", NULL);
				}
				id = g_timeout_add (200, (GSourceFunc) add_tip_timeout, cb);
				g_object_set_data (G_OBJECT (cb), "addtipid", GUINT_TO_POINTER (id));
				pos = ((GdkEventMotion *)(event))->x;
				g_object_set_data (G_OBJECT (cb), "mousex", GINT_TO_POINTER ((gint) pos));
				pos = ((GdkEventMotion *)(event))->y;
				g_object_set_data (G_OBJECT (cb), "mousey", GINT_TO_POINTER ((gint) pos));
			}
		}
		break;

	default:
		done = FALSE;
		break;
	}

	return done;
}

static void 
end_of_drag_cb (CanvasBase *cb, GObject *cursor)
{
	g_object_set_data (G_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)), 
			   "dragged_from", cb);
	g_object_weak_unref (G_OBJECT (cb), (GWeakNotify) end_of_drag_cb_d, cursor);
}

static void
end_of_drag_cb_d (GnomeCanvasItem *ci, CanvasBase *cb)
{
	g_object_weak_unref (G_OBJECT (ci), (GWeakNotify) end_of_drag_cb, cb);
}

static gboolean add_tip_timeout (CanvasBase *cb)
{
        guint id;

        id = gtk_timeout_add (1000, (GSourceFunc) display_tip_timeout, cb);
        g_object_set_data (G_OBJECT (cb), "addtipid", NULL);
        g_object_set_data (G_OBJECT (cb), "displaytipid", GUINT_TO_POINTER (id));
        return FALSE;
}


static void tip_destroy (CanvasBase *cb, GObject *tip);
static gboolean display_tip_timeout (CanvasBase *cb)
{
        GnomeCanvasItem *tip;
        gdouble x, y;

	if (cb->tooltip) {
		/* display the tip */
		g_object_set_data (G_OBJECT (cb), "displaytipid", NULL);
		x = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cb), "mousex"));
		y = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cb), "mousey"));
		
		tip = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas),
					     CANVAS_TIP_TYPE,
					     "x", x+7.,
					     "y", y+3.,
					     "tooltip_object", cb->tooltip,
					     NULL);

		g_object_weak_ref (G_OBJECT (tip), (GWeakNotify) tip_destroy, cb);

		g_object_set_data (G_OBJECT (cb), "tip", tip);
	}
	return FALSE;
}

static void tip_destroy (CanvasBase *cb, GObject *tip)
{
        g_object_set_data (G_OBJECT (cb), "tip", NULL);
}












/*
 * 
 * CanvasDragCursor object
 * 
 */

static void canvas_drag_cursor_class_init (CanvasDragCursorClass * class);
static void canvas_drag_cursor_init       (CanvasDragCursor * drag);
static void canvas_drag_cursor_finalize    (GObject   * object);

static void canvas_drag_cursor_set_property    (GObject              *object,
						guint                 param_id,
						const GValue         *value,
						GParamSpec           *pspec);
static void canvas_drag_cursor_get_property    (GObject              *object,
						guint                 param_id,
						GValue               *value,
						GParamSpec           *pspec);


enum
{
	PROP_00,
	PROP_FILLCOLOR
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *drag_parent_class = NULL;

guint
canvas_drag_cursor_get_type (void)
{
	static GType type = 0;

        if (!type) {
		static const GTypeInfo info = {
			sizeof (CanvasDragCursorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) canvas_drag_cursor_class_init,
			NULL,
			NULL,
			sizeof (CanvasDragCursor),
			0,
			(GInstanceInitFunc) canvas_drag_cursor_init
		};		

		type = g_type_register_static (CANVAS_BASE_TYPE, "CanvasDragCursor", &info, 0);
	}

	return type;
}

	

static void
canvas_drag_cursor_class_init (CanvasDragCursorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	drag_parent_class = g_type_class_peek_parent (class);

	object_class->finalize = canvas_drag_cursor_finalize;

	/* Properties */
	object_class->set_property = canvas_drag_cursor_set_property;
	object_class->get_property = canvas_drag_cursor_get_property;

	g_object_class_install_property
                (object_class, PROP_FILLCOLOR,
                 g_param_spec_pointer ("fill_color", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static int cursor_item_event(CanvasDragCursor *cur, GdkEvent *event, gpointer data);

static void
canvas_drag_cursor_init (CanvasDragCursor * drag)
{
	drag->item = NULL;

	g_signal_connect(G_OBJECT(drag),"event",
			 G_CALLBACK (cursor_item_event), NULL);
}

static int 
cursor_item_event(CanvasDragCursor *cur, GdkEvent *event, gpointer data)
{
	gboolean done = TRUE;
	
	switch (event->type) {
	case GDK_BUTTON_RELEASE:
		canvas_drag_cursor_free (CANVAS_DRAG_CURSOR (cur));
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void
canvas_drag_cursor_finalize (GObject   * object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_DRAG_CURSOR (object));

	/* for the parent class */
	drag_parent_class->finalize (object);
}

void
canvas_drag_cursor_free (CanvasDragCursor *cursor)
{
	g_return_if_fail (cursor && IS_CANVAS_DRAG_CURSOR (cursor));
	gnome_canvas_item_ungrab (GNOME_CANVAS_ITEM (cursor), GDK_CURRENT_TIME);	
	gtk_object_destroy (GTK_OBJECT (cursor));

	/*g_object_unref (G_OBJECT (cursor));*/
}

static void post_init (CanvasDragCursor * drag);

static void 
canvas_drag_cursor_set_property    (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec)
{
	CanvasDragCursor *cd;
	gchar *str;

	cd = CANVAS_DRAG_CURSOR (object);
	if (!cd->item)
		post_init (cd);

	switch (param_id) {
	case PROP_FILLCOLOR:
		str = g_value_get_pointer (value);
		gnome_canvas_item_set (cd->item, "fill_color", str, NULL);
		break;
	}

}

static void 
canvas_drag_cursor_get_property    (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec)
{
	g_print ("Get Property\n");
}



static void 
post_init (CanvasDragCursor * drag)
{
	drag->item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (drag),
					   GNOME_TYPE_CANVAS_RECT,
					   "x1", (double) 0,
					   "y1", (double) 0,
					   "x2", (double) 20,
					   "y2", (double) 7,
					   "outline_color", "black",
					   "fill_color", "white",
					   "width_pixels", 2,
					   NULL);
}













/*
 * 
 * CanvasTip object
 * 
 */


static void canvas_tip_class_init (CanvasTipClass * class);
static void canvas_tip_init       (CanvasTip * tip);
static void canvas_tip_finalize    (GObject   * object);

static void canvas_tip_set_property    (GObject              *object,
					guint                 param_id,
					const GValue         *value,
					GParamSpec           *pspec);
static void canvas_tip_get_property    (GObject              *object,
					guint                 param_id,
					GValue               *value,
					GParamSpec           *pspec);

static void canvas_tip_post_init  (CanvasTip * tip);

enum
{
	PROP_TIP0,
	PROP_OBJ
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *tip_parent_class = NULL;


guint
canvas_tip_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (CanvasTipClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) canvas_tip_class_init,
			NULL,
			NULL,
			sizeof (CanvasTip),
			0,
			(GInstanceInitFunc) canvas_tip_init
		};		

		type = g_type_register_static (GNOME_TYPE_CANVAS_GROUP, "CanvasTip", &info, 0);
	}

	return type;
}

static void
canvas_tip_class_init (CanvasTipClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	tip_parent_class = g_type_class_peek_parent (class);

	object_class->finalize = canvas_tip_finalize;

	/* Properties */
	object_class->set_property = canvas_tip_set_property;
	object_class->get_property = canvas_tip_get_property;

	g_object_class_install_property
                (object_class, PROP_OBJ,
                 g_param_spec_pointer ("tooltip_object", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
canvas_tip_init (CanvasTip * tip)
{
	tip->tip_obj = NULL;

	tip->x_text_space = 3.;
	tip->y_text_space = 3.;
}


static void
canvas_tip_finalize (GObject   * object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_TIP (object));

	/* for the parent class */
	tip_parent_class->finalize (object);
}

void 
canvas_tip_free (CanvasTip *tip)
{
	g_return_if_fail (tip && IS_CANVAS_TIP (tip));
	gtk_object_destroy (GTK_OBJECT (tip));
	/* REM: don't use g_object_unref (G_OBJECT (tip)) since it makes it crash! */
}

static void 
canvas_tip_set_property    (GObject              *object,
			    guint                 param_id,
			    const GValue         *value,
			    GParamSpec           *pspec)
{
	CanvasTip *tip;
	gpointer obj;

	tip = CANVAS_TIP (object);

	switch (param_id) {
	case PROP_OBJ:
		obj = g_value_get_pointer (value);
		if (!obj) {
			g_warning ("Tooltip object set to NULL!");
		}
		else {
			if (G_IS_OBJECT (obj)) 
				tip->tip_obj = G_OBJECT (obj);
		}
		break;
	}

	if (tip->tip_obj)
		canvas_tip_post_init (tip);
}

static void 
canvas_tip_get_property    (GObject              *object,
			    guint                 param_id,
			     GValue              *value,
			    GParamSpec           *pspec)
{
	g_print("Get Property\n");
}


static void build_dbfield_tip (CanvasTip * tip);
static void build_queryjoin_tip (CanvasTip * tip);

static void 
canvas_tip_post_init  (CanvasTip * tip)
{
	gboolean done = FALSE;

	g_return_if_fail (tip->tip_obj);
	if (IS_DB_FIELD (tip->tip_obj)) {
		build_dbfield_tip (tip);
		done = TRUE;
	}
	if (!done && IS_QUERY_JOIN (tip->tip_obj)) {
		build_queryjoin_tip (tip);
		done = TRUE;
	}
	
	if (!done)
		g_warning ("Tooltip: unknown object type\n");
}


#define TIP_DEFAULT_FONT "Sans 10"

static void 
build_dbfield_tip (CanvasTip * tip)
{
	GnomeCanvasItem *text, *bg;
	DbField *field;
	gchar *str;
	double x1, y1, x2, y2;

	field =  DB_FIELD (tip->tip_obj);
	if (field->default_val)
		str = g_strdup_printf (_("Field type: %s\n"
				       "Def. value: %s"), field->type->sqlname,
				       field->default_val);
	else
		str = g_strdup_printf (_("Field type: %s"), field->type->sqlname);

	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", tip->x_text_space,
				     "y", tip->y_text_space,
				     "font", TIP_DEFAULT_FONT,
				     "text", str,
				     "justification", GTK_JUSTIFY_LEFT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST,
				     NULL);
	g_free (str);
	gnome_canvas_item_get_bounds (text, &x1, &y1, &x2, &y2);

	bg = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				   GNOME_TYPE_CANVAS_RECT,
				   "x1", (double) 0,
				   "y1", (double) 0,
				   "x2", x2 - x1 + 2*tip->x_text_space,
				   "y2", y2 - y1 + 2*tip->y_text_space,
				   "outline_color", "black",
				   "fill_color", "lightyellow",
				   "width_pixels", 1,
				   NULL);

	gnome_canvas_item_lower_to_bottom (bg);
}

static void 
build_queryjoin_tip (CanvasTip * tip)
{
	GnomeCanvasItem *text, *bg;
	QueryJoin *qj;
	gchar *str;
	double x1, y1, x2, y2;

	qj =  QUERY_JOIN (tip->tip_obj);

	str = g_strdup ("Join tooltip:\nTODO from FM's implementation\n"
			"of the char * functions\n\n"
			"Translate the symbols");
	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", tip->x_text_space,
				     "y", tip->y_text_space,
				     "font", TIP_DEFAULT_FONT,
				     "text", str,
				     "justification", GTK_JUSTIFY_LEFT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST,
				     NULL);
	g_free (str);
	gnome_canvas_item_get_bounds (text, &x1, &y1, &x2, &y2);

	bg = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				   GNOME_TYPE_CANVAS_RECT,
				   "x1", (double) 0,
				   "y1", (double) 0,
				   "x2", x2 - x1 + 2*tip->x_text_space,
				   "y2", y2 - y1 + 2*tip->y_text_space,
				   "outline_color", "black",
				   "fill_color", "lightyellow",
				   "width_pixels", 1,
				   NULL);

	gnome_canvas_item_lower_to_bottom (bg);
}
