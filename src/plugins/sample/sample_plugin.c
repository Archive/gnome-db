#include <config.h>
#include "baseplugin.h"

#define PLUG_NAME "Test"
#define PLUG_VERSION "0.8.193"

/* functions declaration */
static gchar *server_access_escape_chars (gchar * str);
static gchar *str_from_value (const GdaValue * value);
static DataEntry *widget_from_value (const GdaValue * value);
static void widget_update (DataEntry * wid, const GdaValue * value);
static gchar *sql_from_value (const GdaValue * value);
static GdaValue *value_from_widget (DataEntry * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

DataDisplayFns *
fetch_plugin_interface ()
{
	DataDisplayFns *fns;

	fns = g_new (DataDisplayFns, 1);
	fns->descr = _("Sample plugin");
	fns->detailled_descr = _("Not to be used anywhere, really!");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types = g_new0 (GdaValueType, 1);
	fns->valid_gda_types[0] = GDA_VALUE_TYPE_STRING;
	fns->expand_widget = FALSE;
	fns->widget_from_value = widget_from_value;
	fns->value_from_widget = value_from_widget;
	fns->widget_update = widget_update;
	fns->sql_from_value = sql_from_value;
	fns->str_from_value = str_from_value;
	return fns;
}

void
release_struct (DataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void
contents_changed_cb (GtkObject * obj, DataEntry * dd)
{
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}

static DataEntry *
widget_from_value (const GdaValue * value)
{
	DataEntry *dd;
	GtkWidget *wid;

	dd = DATA_ENTRY (data_entry_new ());
	wid = gtk_label_new ("Sample plugin used!!!");
	data_entry_pack_default (dd, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new ();
	data_entry_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->children = g_slist_append (NULL, wid);
	if (value)
		widget_update (dd, value);
	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

static GdaValue *
value_from_widget (DataEntry * wid)
{
	GdaValue *value;

	g_return_val_if_fail (wid && IS_DATA_ENTRY (wid), NULL);
	value = gda_value_new_string (gtk_entry_get_text (GTK_ENTRY (wid->children->data)));
	return value;
}

static void
widget_update (DataEntry * dd, const GdaValue * value)
{
	gchar *str;
	if (value) {
		str = str_from_value (value);
		if (str) {
			gtk_entry_set_text (GTK_ENTRY (dd->children->data), str);
			g_free (str);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (dd->children->data), "");
	}
	else
		gtk_entry_set_text (GTK_ENTRY (dd->children->data), "");
}

static gchar *
sql_from_value (const GdaValue * value)
{
	gchar *str, *str2, *retval;

	if ((value->type == GDA_VALUE_TYPE_STRING) &&
            !gda_value_get_string (value))
		return NULL;

	if (gda_value_is_null (value))
		retval = NULL;
	else {
		str = gda_value_stringify (value);
		str2 = server_access_escape_chars (str);
		retval = g_strdup_printf ("'%s'", str2);
		g_free (str2);
	}
	return retval;
}

static gchar *
str_from_value (const GdaValue * value)
{
	if (value) {
		if (((value->type == GDA_VALUE_TYPE_STRING)) &&
	            !gda_value_get_string (value))
			return NULL;


		return gda_value_stringify (value);
	}
	else
		return g_strdup ("");
}

static gchar *
server_access_escape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}
