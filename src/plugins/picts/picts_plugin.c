#include <config.h>
#include "baseplugin.h"
#include <gdk-pixbuf/gdk-pixbuf.h>

#define PLUG_NAME "Picts"
#define PLUG_VERSION "0.8.193"
#define IMAGE_X_SIZE 150
#define IMAGE_Y_SIZE 200

/* functions declaration */
static gchar *server_access_escape_chars (gchar * str);
static gchar *str_from_value (const GdaValue * value);
static DataEntry *widget_from_value (const GdaValue * value);
static void widget_update (DataEntry * wid, const GdaValue * value);
static gchar *sql_from_value (const GdaValue * value);
static GdaValue *value_from_widget (DataEntry * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

DataDisplayFns *
fetch_plugin_interface ()
{
	DataDisplayFns *fns;

	fns = g_new (DataDisplayFns, 1);
	fns->descr = _("Pictures plugin");
	fns->detailled_descr = _("Displays images from file names!");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types = g_new0 (GdaValueType, 1);
	fns->valid_gda_types[0] = GDA_VALUE_TYPE_STRING;
	fns->expand_widget = TRUE;
	fns->widget_from_value = widget_from_value;
	fns->value_from_widget = value_from_widget;
	fns->widget_update = widget_update;
	fns->sql_from_value = sql_from_value;
	fns->str_from_value = str_from_value;
	return fns;
}

void
release_struct (DataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void widget_update_str (DataEntry * dd, gchar * str);

static void
filesel_cancel_cb (GtkWidget * btn, DataEntry * dd)
{
	GtkWidget *dlg;
	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");
	if (dlg) {
		gtk_object_set_data (GTK_OBJECT (dd), "seldlg", NULL);
		gtk_widget_destroy (dlg);
	}
}

static void
filesel_ok_cb (GtkWidget * btn, DataEntry * dd)
{
	GtkWidget *dlg;
	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");

	if (dlg) {
		const gchar *str;
		str = gtk_file_selection_get_filename (GTK_FILE_SELECTION
						       (dlg));
		widget_update_str (dd, g_strdup (str));	/* allocate str! */
		gtk_signal_emit_by_name (GTK_OBJECT (dd),
					 "contents_modified");
	}
	filesel_cancel_cb (NULL, dd);
}

static void
dd_destroy_cb (GtkWidget * wid, DataEntry * dd)
{
	filesel_cancel_cb (NULL, dd);
}

static void
change_btn_clicked_cb (GtkWidget * btn, DataEntry * dd)
{
	GtkWidget *dlg;

	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");
	if (!dlg) {
		dlg = gtk_file_selection_new (_("Select an image file"));
		if (dd->children->next) {
			gchar *str;
			str = (gchar *) (dd->children->next->data);
			gtk_file_selection_set_filename (GTK_FILE_SELECTION
							 (dlg), str);
		}
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (dlg)->ok_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (filesel_ok_cb),
				    (gpointer) dd);
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (dlg)->cancel_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (filesel_cancel_cb),
				    (gpointer) dd);
		gtk_object_set_data (GTK_OBJECT (dd), "seldlg", dlg);
		gtk_widget_show (dlg);
	}
	else {
		gdk_window_raise (dlg->window);
	}
}

static gchar *
str_from_value (const GdaValue * value)
{
	if (value) {
		if ((value->type == GDA_VALUE_TYPE_STRING) &&
	            !gda_value_get_string (value))
			return NULL;

		return gda_value_stringify (value);
	}
	else
		return g_strdup ("");
}

static DataEntry *
widget_from_value (const GdaValue * value)
{
	DataEntry *dd;
	GtkWidget *wid, *sw, *btn;

	dd = DATA_ENTRY (data_entry_new ());
	wid = gtk_hbox_new (FALSE, GNOME_PAD);
	data_entry_pack_default (dd, wid);
	gtk_widget_show (wid);

	/* SW for the pictures */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (sw);
	dd->children = g_slist_append (NULL, sw);

	/* button to change it */
	btn = gtk_button_new_with_label ("...");
	gtk_box_pack_start (GTK_BOX (wid), btn, FALSE, TRUE, 0);
	gtk_widget_show (btn);

	if (value)
		widget_update (dd, value);

	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (change_btn_clicked_cb), dd);

	gtk_signal_connect (GTK_OBJECT (dd), "destroy",
			    GTK_SIGNAL_FUNC (dd_destroy_cb), dd);
	return dd;
}

static GdaValue *
value_from_widget (DataEntry * wid)
{
	GdaValue *value;

	g_return_val_if_fail (wid && IS_DATA_ENTRY (wid), NULL);
	if (wid->children->next)
		value = gda_value_new_string ((gchar *)(wid->children->next->data));
	else
		value = gda_value_new_string ("");
	return value;
}

static void
widget_update (DataEntry * dd, const GdaValue * value)
{
	gchar *str = NULL;

	if (value)
		str = str_from_value (value);
	widget_update_str (dd, str);
}

/* WARNING: str needs to be allocated because it will be freed when not used
   anymore */
static void
widget_update_str (DataEntry * dd, gchar * str)
{
	GtkWidget *wid = NULL;

	if (dd->children->next) {	/* there is already an str */
		g_free (dd->children->next->data);
		dd->children = g_slist_remove_link (dd->children, dd->children->next);
	}

	if (str) {
		gint width = IMAGE_X_SIZE, height = IMAGE_Y_SIZE;

		wid = gtk_image_new_from_file (str);

		/* if (gdk_pixbuf_get_height (gdkpix) < IMAGE_Y_SIZE) */
/* 			height = gdk_pixbuf_get_height (gdkpix); */
/* 		if (gdk_pixbuf_get_width (gdkpix) < IMAGE_X_SIZE) */
/* 			width = gdk_pixbuf_get_width (gdkpix); */
		gtk_widget_set_usize (GTK_WIDGET (dd->children->data),
				      width + GNOME_PAD, height + GNOME_PAD);
	}

	if (!wid) {
		if (str)
			wid = gnome_pixmap_new_from_file
				(gnome_pixmap_file ("gnome-who.png"));
		else
			wid = gnome_pixmap_new_from_file
				(gnome_pixmap_file ("BulletHole.xpm"));
	}

	if (wid) {
		GtkWidget *viewport, *sw;

		sw = GTK_WIDGET (dd->children->data);
		if (GTK_BIN (sw)->child) {
			gtk_container_remove (GTK_CONTAINER (sw),
					      GTK_BIN (sw)->child);
		}
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW
						       (sw), wid);
		viewport = GTK_BIN (sw)->child;
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (viewport),
					      GTK_SHADOW_NONE);
		gtk_widget_show (wid);
	}
	if (str)
		dd->children = g_slist_append (dd->children, str);
}

static gchar *
sql_from_value (const GdaValue * value)
{
	gchar *str, *str2, *retval;

	if ((value->type == GDA_VALUE_TYPE_STRING) &&
            !gda_value_get_string (value))
		return NULL;

	if (gda_value_is_null (value))
		retval = NULL;
	else {
		str = gda_value_stringify (value);
		str2 = server_access_escape_chars (str);
		retval = g_strdup_printf ("'%s'", str2);
		g_free (str2);
	}
	return retval;
}


static gchar *
server_access_escape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}
