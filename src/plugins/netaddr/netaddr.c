#include <config.h>
#include "baseplugin.h"

#define PLUG_NAME "NetAddr"
#define PLUG_VERSION "0.8.193"

/* functions declaration */
static gchar *str_from_value (const GdaValue * value);
static DataEntry *widget_from_value (const GdaValue * value);
static void widget_update (DataEntry * wid, const GdaValue * value);
static gchar *sql_from_value (const GdaValue * value);
static GdaValue *value_from_widget (DataEntry * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

DataDisplayFns *
fetch_plugin_interface ()
{
	DataDisplayFns *fns;

	fns = g_new (DataDisplayFns, 1);
	fns->descr = _("IP addresses ex:172.16.49.19");
	fns->detailled_descr = _("Designed to be used with the\n"
				 "Postgres cidr data type");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types = g_new0 (GdaValueType, 1);
	fns->valid_gda_types[0] = GDA_VALUE_TYPE_STRING;
	fns->expand_widget = FALSE;
	fns->widget_from_value = widget_from_value;
	fns->value_from_widget = value_from_widget;
	fns->widget_update = widget_update;
	fns->sql_from_value = sql_from_value;
	fns->str_from_value = str_from_value;

	return fns;
}

void
release_struct (DataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void
contents_changed_cb (GtkObject * obj, DataEntry * dd)
{
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}

static gchar *
str_from_value (const GdaValue * value)
{
	if (value) {
		if ((value->type == GDA_VALUE_TYPE_STRING) &&
	            !gda_value_get_string (value))
			return NULL;


		if (gda_value_is_null (value))
			return NULL;
		else
			return gda_value_stringify (value);
	}
	else
		return NULL;
}

static DataEntry *any_to_widget ();
static DataEntry *
widget_from_value (const GdaValue * value)
{
	DataEntry *dd;

	dd = any_to_widget ();
	if (value)
		widget_update (dd, value);
	return dd;
}

static GdaValue *
value_from_widget (DataEntry * wid)
{
	GdaValue *value;
	gint i, j, nb, p;
        GSList *list;
        GString *string; 
        gboolean first;
        const gchar *strc;
        gchar *mask, *str;

	g_return_val_if_fail (wid && IS_DATA_ENTRY (wid), NULL);

	list = wid->children; 
        string = g_string_new ("'");
        first = TRUE;
        for (i = 0; i < 4; i++) {
                strc = gtk_entry_get_text (GTK_ENTRY (list->data));
                if (first)
                        first = FALSE;
                else
                        g_string_append (string, ".");
                if (!strc || (strc && (*strc == '\0')))
                        g_string_append (string, "0"); 
                else
                        g_string_append (string, strc);
                list = g_slist_next (list);
        }

        mask = g_new0 (gchar, 33);
        for (i = 0; i < 4; i++) {
                nb = atoi (gtk_entry_get_text (GTK_ENTRY (list->data)));
                p = 128;
                for (j = i * 8; j <= i * 8 + 7; j++) {
                        if (nb / p == 1)
                                mask[j] = '1';
                        else
                                mask[j] = '0';
                        nb = nb % p;
                        p = p / 2;
                }
                list = g_slist_next (list);
        }

        str = mask;
        nb = 0;
        while (*str == '1') {
                nb++;
                str++;
        }
        g_free (mask);
        g_string_sprintfa (string, "/%d'", nb);

	value = gda_value_new_string (string->str);
        g_string_free (string, TRUE);

	return value;
}

static void widget_update_str (DataEntry * dd, gchar * str);
static DataEntry *
any_to_widget ()
{
	DataEntry *dd;
	GtkWidget *hb, *wid, *table;
	GSList *list;

	dd = DATA_ENTRY (data_entry_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_entry_pack_default (dd, hb);
	gtk_widget_show (hb);
	table = gtk_table_new (2, 7, FALSE);
	gtk_box_pack_start (GTK_BOX (hb), table, FALSE, FALSE, 0);
	gtk_widget_show (table);

	/* filling the top table row with entries */
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
	dd->children = g_slist_append (NULL, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 0, 1);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 4, 5, 0, 1);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 6, 7, 0, 1);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 3, 4, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 5, 6, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);

	/* filling the bottom table row with entries */
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 1, 2);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 4, 5, 1, 2);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 6, 7, 1, 2);
	dd->children = g_slist_append (dd->children, wid);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 3, 4, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 5, 6, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);

	list = dd->children;
	while (list) {
		/* FIXME: sth better with fonts handling for size... */
		gtk_widget_set_usize (GTK_WIDGET (list->data), 30, 16);
		gtk_signal_connect (GTK_OBJECT (list->data), "changed",
				    GTK_SIGNAL_FUNC (contents_changed_cb),
				    dd);
		list = g_slist_next (list);
	}

	return dd;
}

static void
widget_update_str (DataEntry * dd, gchar * str)
{
	gchar *ptr, *mask, *ip;
	GSList *list;
	gint i, j, nb, p;

	if (str) {
		list = dd->children;

		ip = strtok (str, "/");
		mask = strtok (NULL, "/");
		i = 0;
		if (ip) {
			ptr = strtok (ip, ".");
			while (ptr) {
				gtk_entry_set_text (GTK_ENTRY (list->data),
						    ptr);
				list = g_slist_next (list);
				ptr = strtok (NULL, ".");
				i++;
			}
		}
		for (j = i; j < 4; j++) {
			gtk_entry_set_text (GTK_ENTRY (list->data), "0");
			list = g_slist_next (list);
		}

		if (mask) {
			nb = atoi (mask);
			mask = g_new0 (gchar, 33);
			for (i = 0; i < nb; i++)
				mask[i] = '1';
			for (; i < 32; i++)
				mask[i] = '0';
			for (i = 0; i < 4; i++) {
				nb = 0;
				p = 1;	/* 2^0 */
				for (j = i * 8 + 7; j >= i * 8; j--) {
					if (mask[j] == '1')
						nb += p;
					p *= 2;
				}
				ptr = g_strdup_printf ("%d", nb);
				gtk_entry_set_text (GTK_ENTRY (list->data),
						    ptr);
				g_free (ptr);
				list = g_slist_next (list);
			}
			g_free (mask);
		}
	}
}

static void
widget_update (DataEntry * dd, const GdaValue * value)
{
	gchar *str;

	if (value) {
		str = str_from_value (value);
		widget_update_str (dd, str);
		g_free (str);
	}
}

static gchar *
sql_from_value (const GdaValue * value)
{
	gchar *str, *retval;

	if ((value->type == GDA_VALUE_TYPE_STRING) &&
            !gda_value_get_string (value))
		return NULL;

	str = gda_value_stringify (value);
	retval = g_strdup_printf ("'%s'", str);
	g_free (str);
	return retval;
}

