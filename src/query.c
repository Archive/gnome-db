/* query.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2001 - 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query.h"
#include "query-env.h"
#include "relship.h"
#include "marshal.h"

/* 
 * Main static functions 
 */
static void query_class_init (QueryClass * class);
static void query_init (Query * srv);
static void query_dispose (GObject   * object);
static void query_finalize (GObject   * object);
static void m_changed (Query * q, gpointer data);

static GObject   *query_new_id (const gchar * name, Query *parent_query, ConfManager * conf, guint id);

static gchar *get_from_part_of_sql (Query * q);

static void query_list_joins (Query *q);

#ifdef debug
static void query_print_joins (Query *q);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

enum
{
	CHANGED,
	NAME_CHANGED,
	TYPE_CHANGED,
	FIELD_CREATED,
	FIELD_DROPPED,
	FIELD_MODIFIED,
	FIELD_NAME_MODIFIED,
	FIELD_ALIAS_MODIFIED,
	JOIN_CREATED,
	JOIN_DROPPED,
	JOIN_MODIFIED,
	COND_CREATED,
	COND_DROPPED,
	COND_MODIFIED,
	COND_MOVED,
	ENV_CREATED,
	ENV_DROPPED,
	QUERY_VIEW_ADDED,
	QUERY_VIEW_REMOVED,
	QUERY_CREATED,
	QUERY_DROPPED,
	LAST_SIGNAL
};

static gint query_signals[LAST_SIGNAL] =
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


guint
query_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_class_init,
			NULL,
			NULL,
			sizeof (Query),
			0,
			(GInstanceInitFunc) query_init
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "Query", &info, 0);
	}
	return type;
}

static void
query_class_init (QueryClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	query_signals[CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	query_signals[NAME_CHANGED] =
		g_signal_new ("name_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, name_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	query_signals[TYPE_CHANGED] =
		g_signal_new ("type_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, type_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	query_signals[FIELD_CREATED] =
		g_signal_new ("field_created",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, field_created),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[FIELD_DROPPED] =
		g_signal_new ("field_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, field_dropped),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[FIELD_MODIFIED] =
		g_signal_new ("field_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, field_modified),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[FIELD_NAME_MODIFIED] =
		g_signal_new ("field_name_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, field_name_modified),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[FIELD_ALIAS_MODIFIED] =
		g_signal_new ("field_alias_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, field_alias_modified),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[JOIN_CREATED] =
		g_signal_new ("join_created",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, join_created),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[JOIN_DROPPED] =
		g_signal_new ("join_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, join_dropped),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[JOIN_MODIFIED] =
		g_signal_new ("join_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, join_modified),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[COND_CREATED] =
		g_signal_new ("cond_created",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, cond_created),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[COND_DROPPED] =
		g_signal_new ("cond_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, cond_dropped),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[COND_MODIFIED] =
		g_signal_new ("cond_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, cond_modified),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[COND_MOVED] =
		g_signal_new ("cond_moved",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, cond_moved),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	
	query_signals[ENV_CREATED] =
		g_signal_new ("env_created",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, env_created),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	query_signals[ENV_DROPPED] =
		g_signal_new ("env_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, env_dropped),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	
	query_signals[QUERY_VIEW_ADDED] =
		g_signal_new ("query_view_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, query_view_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	
	query_signals[QUERY_VIEW_REMOVED] =
		g_signal_new ("query_view_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, query_view_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	
	query_signals[QUERY_CREATED] =
		g_signal_new ("query_created",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, query_created),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);
	
	query_signals[QUERY_DROPPED] =
		g_signal_new ("query_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryClass, query_dropped),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE, 1,
			      G_TYPE_POINTER);

	class->field_created =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->field_dropped =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->field_modified =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->join_created = 
		(void (*)(Query * q, QueryJoin * qj))	m_changed;
	class->join_dropped = 
		(void (*)(Query * q, QueryJoin * qj)) m_changed;
	class->join_modified = 
		(void (*)(Query * q, QueryJoin * qj)) m_changed;
	class->changed = NULL;
	class->name_changed = NULL;
	class->type_changed = (void (*)(Query * q)) m_changed;
	class->cond_created =
		(void (*)(Query * q, QueryCond * node)) m_changed;
	class->cond_dropped =
		(void (*)(Query * q, QueryCond * node)) m_changed;
	class->cond_modified =
		(void (*)(Query * q, QueryCond * node)) m_changed;
	class->cond_moved =
		(void (*)(Query * q, QueryCond * node)) m_changed;

	class->query_view_added = NULL;
	class->query_view_removed = NULL;

	class->query_created = NULL;
	class->query_dropped = NULL;

	class->env_created = NULL;
	class->env_dropped = NULL;

	object_class->dispose = query_dispose;
	object_class->finalize = query_finalize;
}

static void
query_init (Query * q)
{
	q->weak_ref_objects = NULL;
	q->parent = NULL;
	q->sub_queries = NULL;

	q->name = NULL;
	q->descr = NULL;
	q->type = QUERY_TYPE_STD;

	q->views = NULL;
	q->fields_counter = 0;
	q->fields = NULL;
	q->joins = NULL;
	q->where_cond = NULL;

	q->group_by_list = NULL;
	q->order_by_list = NULL;

	q->text_sql = NULL;

	q->envs = NULL;
}


GObject   *
query_new (const gchar * name, Query *parent_query, ConfManager * conf)
{
	return query_new_id (name, parent_query, conf, conf_manager_get_id_serial (conf));
}

static GObject   *
query_new_id (const gchar * name, Query *parent_query, ConfManager * conf, guint id)
{
	GObject   *obj;

	Query *q;
	obj = g_object_new (QUERY_TYPE, NULL);
	q = QUERY (obj);

	q->conf = conf;
	q->id = id;
	q->name = g_strdup (name);

	if (parent_query) {
		g_return_val_if_fail (IS_QUERY (parent_query), obj);
		query_add_sub_query (parent_query, NULL, q);
		/* otherwise we have a non referenced query */
	}

	return obj;
}

static void do_copy_query_cond (Query * newq, Query * oldq);
static void query_activate_all_fields (Query *q);
static GObject   *query_copy_real (Query * q);

GObject   *
query_new_copy (Query * q)
{
	Query *newq;
	newq = QUERY (query_copy_real (q));
	g_assert (newq);
	query_activate_all_fields (newq);

	return G_OBJECT (newq);
}

/* Only make empty queries with the right structure of sub queries */
static Query *do_copy_bare_queries (Query * q, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryView objects */
static void   do_copy_query_views  (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryField objects */
static void   do_copy_query_fields (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryJoin objects */
static void   do_copy_query_joins (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), make sure the RelShip object associated,
   if it exists, is copied as well */
static void   do_copy_query_relships (Query *q, Query *original, GHashTable *hash);
static GObject   *
query_copy_real (Query * q)
{
	Query *newquery;
	GHashTable *htable;

	htable = g_hash_table_new (NULL, NULL);
	
	/* New empty queries tree */
	newquery = do_copy_bare_queries (q, htable);

	/* Copy of the QueryView objects */
	do_copy_query_views (newquery, q, htable);

	/* Copy of the QueryFields */
	do_copy_query_fields (newquery, q, htable);

	/* Copy of the QueryJoins */
	do_copy_query_joins (newquery, q, htable);

	/* Copy of the QueryCond conditions FIXME: see if it needs to be changed */
	do_copy_query_cond (newquery, q);

	/* Other Query attributes */
	if (q->text_sql)
		newquery->text_sql = g_strdup (q->text_sql);

	/* Then the RelShip objects which hold the graphical parameters */
	do_copy_query_relships (newquery, q, htable);
	
	g_hash_table_destroy (htable);

	return G_OBJECT (newquery);
}

static Query *
do_copy_bare_queries (Query * q, GHashTable *hash)
{
	gchar *str;
	GSList *list;
	Query *newquery;

	str = g_strdup (q->name);
	newquery = QUERY (query_new (str, q->parent, q->conf));
	g_free (str);
	g_hash_table_insert (hash, q, newquery);
	
	/* sub queries */
	list = q->sub_queries;
	while (list) {
		Query *childq;

		childq = do_copy_bare_queries (QUERY (list->data), hash);
		query_add_sub_query (newquery, NULL, childq);
		list = g_slist_next (list);
	}

	return newquery;
}

static void
do_copy_query_views (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));

	list = original->views;
	while (list) {
		QueryView *qv;
		gpointer data;

		qv = QUERY_VIEW (query_view_new_copy (QUERY_VIEW (list->data)));
		g_hash_table_insert (hash, list->data, qv);
		/* changing the query to the new one */
		query_view_set_query (qv, q);

		/* if the obj attribute has been changed, do it as well */
		if ((data = g_hash_table_lookup (hash, qv->obj))) 
			query_view_set_view_obj (qv, G_OBJECT (data));

		query_add_view (q, qv);
		list = g_slist_next (list);	
	}

	/* sub queries */
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_views (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void
do_copy_query_fields (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));

	list = original->fields;
	while (list) {
		QueryField *qf, *nqf;
		GSList *views;

		qf = QUERY_FIELD (list->data);
		nqf = QUERY_FIELD (query_field_new_copy (qf));

		g_hash_table_insert (hash, list->data, nqf);

		/* changing the query to the new one */
		query_field_replace_ref_ptr (nqf, G_OBJECT (original), G_OBJECT (q));

		views = original->views;
		while (views) {
			QueryView *qv;
			
			qv = QUERY_VIEW (g_hash_table_lookup (hash, views->data));
			if (!qv)
				g_warning ("Can't find corresponding QueryView for original QueryView %p\n", views->data);
			else
				query_field_replace_ref_ptr (nqf, G_OBJECT (views->data), G_OBJECT (qv));

			views = g_slist_next (views);
		}

		query_add_field (q, nqf);
		list = g_slist_next (list);
	}

	/* sub queries */
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_fields (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void
do_copy_query_joins (Query *q, Query *original, GHashTable *hash)
{
	/* TODO */
}

static void
do_copy_query_relships (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;

	relship_copy (q, original, hash);
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_relships (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void 
query_activate_all_fields (Query *q)
{
	GSList *list;
	
	/* activate all the fields */
	list = q->fields;
	while (list) {
		query_field_activate (QUERY_FIELD (list->data));
		if (! QUERY_FIELD (list->data)->activated)
			g_warning ("QueryField %s.%s can't be activated (%s line %d)\n", 
				   q->name, QUERY_FIELD (list->data)->name, __FILE__, __LINE__);

		list = g_slist_next (list);
	}
	
	/* for the sub queries as well */
	list = q->sub_queries;
	while (list) {
		query_activate_all_fields (QUERY (list->data));
		list = g_slist_next (list);
	}
}

gboolean
query_fields_activated   (Query *q)
{
	gboolean retval = TRUE;
	GSList *list;

	list = q->fields;
	while (list && retval) {
		retval = QUERY_FIELD (list->data)->activated;
		if (! retval)
			g_warning ("QueryField %s.%s is not activated (%s line %d)\n", 
				   q->name, QUERY_FIELD (list->data)->name, __FILE__, __LINE__);

		list = g_slist_next (list);
	}
	
	if (retval) {
		/* for the sub queries as well */
		list = q->sub_queries;
		while (list && retval) {
			retval = query_fields_activated (QUERY (list->data));

			list = g_slist_next (list);
		}
	}

	return retval;
}


void query_get_missing_value_fields_recurs (QueryField *qf, GSList **missing, GSList **not_missing);
GSList *query_get_missing_value_fields (Query *q)
{
	GSList *list, *missing = NULL, *not_missing = NULL;

	g_return_val_if_fail (q && IS_QUERY (q), NULL);

	list = q->fields;
	while (list) {
		query_get_missing_value_fields_recurs (QUERY_FIELD (list->data), &missing, &not_missing);
		list = g_slist_next (list);
	}

	if (not_missing)
		g_slist_free (not_missing);
	return missing;
}

/* populates the two lists recursively from one QF, the lists help in not interrogating the QF
   several times to see if they have any missing values */
void 
query_get_missing_value_fields_recurs (QueryField *qf, GSList **missing, GSList **not_missing)
{
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));
	
	if (!g_slist_find (*missing, qf) && !g_slist_find (*not_missing, qf)) {
		GSList *objects, *list;

		/* looking into qf */
		if ((qf->field_type == QUERY_FIELD_VALUE) && query_field_value_input_required (qf))
			*missing = g_slist_append (*missing, qf);
		else
			*not_missing = g_slist_append (*not_missing, qf);
		
		/* looking recursively into the QueryField objects used by qf */
		objects = query_field_get_monitored_objects (qf);
		list = objects;
		while (list) {
			if (IS_QUERY_FIELD (list->data))
				query_get_missing_value_fields_recurs (QUERY_FIELD (list->data), 
								       missing, not_missing);
			list = g_slist_next (list);
		}
		if (objects)
			g_slist_free (objects);
	}
}



/* for this recursive function, we assume it is called with 
   existing GNodes (and their data correctely set), 
   and it creates and update the GNodes and data of the new
   query for all the children of the GNodes 
*/
static void
do_copy_query_cond (Query * newq, Query * oldq)
{
	g_return_if_fail (newq && IS_QUERY (newq));
	g_return_if_fail (oldq && IS_QUERY (oldq));

	if (oldq->where_cond)
		newq->where_cond = QUERY_COND (query_cond_new_copy (oldq->where_cond));
}

static void
query_dispose (GObject   * object)
{
	Query *q;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY (object));

	q = QUERY (object);

	/* parent query */
	q->parent = NULL;

	/* sub queries */
	list = q->sub_queries;
	while (list) {
		GObject   *obj;

		obj = G_OBJECT (list->data);
		g_object_unref (G_OBJECT (obj));
		list = g_slist_next (list);
	}
	if (q->sub_queries) {
		g_slist_free (q->sub_queries);
		q->sub_queries = NULL;
	}

	/* Joins */
	while (q->joins) {
		GSList *sub_list;
		sub_list = (GSList *) q->joins->data;
		g_print ("Del join %p for query %d\n", sub_list->data, q->id);
		query_del_join (q, QUERY_JOIN (sub_list->data));
	}

	/* Query Fields */
	while (q->fields) {
		GObject   *obj = G_OBJECT (q->fields->data);

		query_del_field (q, QUERY_FIELD (obj));
	}
	q->fields_counter = 0;

	/* Query Envs */
	while (q->envs) {
		query_del_env (q, G_OBJECT (q->envs->data));
	}

	/* Query Views */
	while (q->views) {
		query_del_view (q, QUERY_VIEW (q->views->data));
	}

	/* Query Cond area */
	/* FIXME: part is already done, but need to finish the job */

	/* various attributes */
	q->type = QUERY_TYPE_STD;

	if (q->group_by_list) {
		g_slist_free (q->group_by_list);
		q->group_by_list = NULL;
	}

	if (q->order_by_list) {
		g_slist_free (q->order_by_list);
		q->order_by_list = NULL;
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
query_finalize (GObject   * object)
{
	Query *q;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY (object));

	q = QUERY (object);

	/* minor memory allocations */
	if (q->name) {
		g_free (q->name);
		q->name = NULL;
	}
	if (q->descr) {
		g_free (q->descr);
		q->descr = NULL;
	}

	if (q->text_sql) {
		g_free (q->text_sql);
		q->text_sql = NULL;
	}

	parent_class->finalize (object);
}

static void
m_changed (Query * q, gpointer data)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'CHANGED' from query->m_changed\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'CHANGED' from query->m_changed\n");
#endif
}



/*
 * Helper functions
 */

void
query_set_name (Query * q, const gchar * name, const gchar * descr)
{
	gboolean changed = FALSE;

	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));

	if (name) {
		if (q->name) {
			changed = strcmp (q->name, name) ? TRUE : FALSE;
			g_free (q->name);
		}
		else
			changed = TRUE;
		q->name = g_strdup (name);
	}

	if (descr) {
		if (q->descr) {
			changed = strcmp (q->descr, descr) ? TRUE : changed;
			g_free (q->descr);
		}
		else
			changed = TRUE;
		q->descr = g_strdup (descr);
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'NAME_CHANGED' from query_set_name (%s/%s)\n", q->name, q->descr);
#endif
		g_signal_emit (G_OBJECT (q), query_signals[NAME_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'NAME_CHANGED' from query_set_name (%s/%s)\n", q->name, q->descr);
#endif
	}
}

void 
query_set_query_type (Query * q, QueryType type)
{
	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));
	if (q->type != type) {
		q->type = type;
#ifdef debug_signal
		g_print (">> 'TYPE_CHANGED' from query_set_query_type\n");
#endif
		g_signal_emit (G_OBJECT (q), query_signals[TYPE_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'TYPE_CHANGED' from query_set_query_type\n");
#endif
	}
}

void
query_set_text_sql (Query * q, gchar * sql)
{
	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));

	if (q->text_sql) {
		g_free (q->text_sql);
		q->text_sql = NULL;
	}

	if (sql) {
		q->text_sql = g_strdup (sql);
		query_set_query_type (q, QUERY_TYPE_SQL);
	}

#ifdef debug_signal
	g_print (">> 'CHANGED' from query_set_text_sql\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'CHANGED' from query_set_text_sql\n");
#endif
}


/*
 * saving as XML
 */

static void build_cond_xml_recursive (Query * q, GNode * node,
				       xmlNodePtr tree);
void
query_build_xml_tree (Query * q, xmlNodePtr maintree, ConfManager * conf)
{
	xmlNodePtr toptree, tree;
	GSList *list;
	gint order;
	gchar *str;

	toptree = xmlNewChild (maintree, NULL, "Query", NULL);
	str = query_get_xml_id (q);
	xmlSetProp (toptree, "id", str);
	g_free (str);
	xmlSetProp (toptree, "name", q->name);
	if (q->descr)
		xmlSetProp (toptree, "descr", q->descr);
	switch (q->type) {
	case QUERY_TYPE_STD:
		str = "std";
		break;
	case QUERY_TYPE_UNION:
		str = "union";
		break;
	case QUERY_TYPE_INTERSECT:
		str = "intersect";
		break;
	case QUERY_TYPE_SQL:
		str = "SQL";
		break;
	default:
		str = "unknown";
	}
	xmlSetProp (toptree, "type", str);

	/* Query views */
	list = q->views;
	while (list) {
		QueryView *qv = QUERY_VIEW (list->data);

		tree = query_view_save_to_xml (qv);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* Query fields */
	list = q->fields;
	order = 0;
	while (list) {
		QueryField *qf;

		qf = QUERY_FIELD (list->data);
		tree = query_field_save_to_xml (qf);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* Query Joins Lists */
	list = q->joins;
	while (list) {
		GSList *jlist;
		jlist = (GSList *) (list->data);
		tree = query_join_list_save_to_xml (jlist);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* tree of cond clauses */
	if (q->where_cond) {
		tree = query_cond_save_to_xml (q->where_cond);
		xmlAddChild (toptree, tree);
	}


	/* The text of the query if specified */
	if (q->text_sql)
		tree = xmlNewChild (toptree, NULL, "QueryText", q->text_sql);

	/* the Query environments */
	list = q->envs;
	while (list) {
		query_env_build_xml_tree (QUERY_ENV (list->data), toptree);
		list = g_slist_next (list);
	}

	/* sub queries */
	list = q->sub_queries;
	while (list) {
		query_build_xml_tree (QUERY (list->data), toptree, conf);
		
		list = g_slist_next (list);
	}
}

gchar * 
query_get_xml_id (Query *q)
{
	gchar *str;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	str = g_strdup_printf ("QU%d", q->id);
	return str;
}


/* if start_query is NULL then start from the top level query */
Query *
query_find_from_xml_name (ConfManager * conf, Query * start_query, gchar *xmlname)
{
	Query *q, *retval = NULL;
	gchar *str;

	/* test if current query */
	if (start_query)
		q = start_query;
	else
		q = QUERY (conf->top_query);

	g_assert (q);
	str = query_get_xml_id (q);
	if (!strcmp (str, xmlname))
		retval = q;
	g_free (str);

	/* if not found, see children queries */
	if (!retval) { 
		GSList *list;

		list = q->sub_queries;
		while (list && !retval) {
			retval = query_find_from_xml_name (conf, QUERY (list->data), xmlname);
			list = g_slist_next (list);
		}
	}

	return retval;
}



/*
 * loading from XML
 */

static Query *real_query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent);
static void activate_pending_query_views (Query *q);

Query *
query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent)
{
	Query *q;

	q = real_query_build_from_xml_tree (conf, node, parent);

	/* Some Query views can't be useable right out of the box because they reference 
	   other queries which do not exist when the query to which they belong is parsed,
	   so we try to render them useable now that all the queries have been loaded */
	activate_pending_query_views (q);	
	
	return q;
}

static void 
activate_pending_query_views (Query *q)
{
	GSList *list;
	QueryView *qv;
	GObject   *obj;

	list = g_object_get_data (G_OBJECT (q), "pending_views");
	while (list) {
		qv = QUERY_VIEW (list->data);
		if (!qv->obj) {
			gpointer ptr;

			g_assert (qv->ref);
			ptr = query_find_from_xml_name (q->conf, NULL, qv->ref);
			obj = NULL;
			if (ptr)
				obj = G_OBJECT (ptr);
			else {
				ptr = database_find_table_from_xml_name (q->conf->db, qv->ref);
				if (ptr)
					obj = G_OBJECT (ptr);
			}

			g_free (qv->ref);
			qv->ref = NULL;

			g_assert (obj);
			query_view_set_view_obj (qv, obj);
		}
		query_add_view (q, qv);
		list = g_slist_next (list);
	}

	list = g_object_get_data (G_OBJECT (q), "pending_views");
	g_slist_free (list);
	g_object_set_data (G_OBJECT (q), "pending_views", NULL);

	list = q->sub_queries;
	while (list) {
		activate_pending_query_views (QUERY (list->data));
		list = g_slist_next (list);
	}

}

static void query_join_list_load_from_xml (Query *q, ConfManager *conf, xmlNodePtr node);
static Query *
real_query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent)
{
	xmlNodePtr tree;
	gchar *str;
	guint id;
	gboolean ok = TRUE;
	Query *q;
	GSList *list;

	str = xmlGetProp (node, "id");
	id = atoi (str + 2);
	
	g_free (str);

	/* new query: when parent is NULL, then we know it is the top level query to be used
	   and it can be found in the ConfManager object, so we don't build one in that case */
	if (parent) {
		str = xmlGetProp (node, "name");
		
		q = QUERY (query_new_id (str, parent, conf, id));
		g_free (str);
	}
	else {
		g_assert (conf->top_query);
		q = QUERY (conf->top_query);
		q->id = id;
	}

	str = xmlGetProp (node, "descr");
	if (str) {
		query_set_name (q, NULL, str);
		g_free (str);
	}

	str = xmlGetProp (node, "type");
	if (str) {
		switch (*str) {
		case 'u':
			q->type = QUERY_TYPE_UNION;
			break;
		case 'i':
			q->type = QUERY_TYPE_INTERSECT;
			break;
		case 'S':
			q->type = QUERY_TYPE_SQL;
			break;
		default:
			q->type = QUERY_TYPE_STD;
		}
		g_free (str);
	}

	/* First pass: load all the QueryViews and QueryFields */
	tree = node->xmlChildrenNode;
	while (tree) {
		if (!strcmp (tree->name, "QueryView")) {
			QueryView *qv;

			qv = QUERY_VIEW (query_view_new (q));
			query_view_load_from_xml (qv, tree);

			if (qv->obj)
				query_add_view (q, qv);
			else {
				/* make a list of QueryViews to be "activated" later */
				GSList *pending;

				pending = g_object_get_data (G_OBJECT (q), "pending_views");
				pending = g_slist_append (pending, qv);
				g_object_set_data (G_OBJECT (q), "pending_views", pending);
			}
		}

		if (!strcmp (tree->name, "QueryField")) {
			QueryField *qf;
			
			str = xmlGetProp (tree, "type");
			qf = QUERY_FIELD (query_field_new_objref (q, NULL, str));
			g_free (str);

			if (qf) {
				query_field_load_from_xml (qf, tree);
				query_add_field (q, qf);
			}
			else {
				/* Fixme: signal the error */
			}
		}
		tree = tree->next;
	}

	/* Activate all the QueryFields which are not activated */
	list = q->fields;
	while (list) {
		query_field_activate (QUERY_FIELD (list->data));
		list = g_slist_next (list);
	}


	
	/* Second pass: for the sub queries, joins, environments and cond conditions */
	tree = node->xmlChildrenNode;

	while (tree) {
		/* Sub queries */
		if (!strcmp (tree->name, "Query")) {
			query_build_from_xml_tree (conf, tree, q);
		}

		/* Joins */
		if (!strcmp (tree->name, "QueryJoinList")) {
			query_join_list_load_from_xml (q, conf, tree);
			g_print("\n%s\n", get_from_part_of_sql(q)); /* DEBUG FER */
		}

		/* Where condition */
		if (!strcmp (tree->name, "QueryCond"))
			q->where_cond = QUERY_COND (query_cond_new_from_xml (q->conf, q, tree));

		/* Envs */
		if (!strcmp (tree->name, "QueryEnv")) 
			query_env_build_from_xml_tree (conf, tree);
		
		tree = tree->next;
	}

	if (!ok) {
		g_object_unref (G_OBJECT (q));
		q = NULL;
	} 
	else 
		query_activate_all_fields (q);

	return q;
}

static void real_query_add_join (Query * q, QueryJoin *join);
static void
query_join_list_load_from_xml   (Query *q, ConfManager *conf, xmlNodePtr node)
{
	xmlNodePtr jnode;

	jnode = node->xmlChildrenNode;

	while (jnode) {
		if (!strcmp (jnode->name, "QueryJoin")) {
			QueryJoin *qj;
			qj = QUERY_JOIN (query_join_new_from_xml (conf, jnode));
			real_query_add_join(q, qj);
			g_print("Query join %p added\n", QUERY_JOIN (qj));
		}
		jnode = jnode->next;
	}
}


/*
 * QueryViews management
 */

static void query_add_view_check (Query *q, QueryView *qv);

QueryView *
query_add_view_with_obj (Query *q, GObject   *obj)
{
	QueryView *qv;

	g_return_val_if_fail (q && IS_QUERY (q), NULL);
	g_return_val_if_fail (obj, NULL);
	g_return_val_if_fail (IS_QUERY (obj) || IS_DB_TABLE (obj), NULL);
	
	qv = QUERY_VIEW (query_view_new (q));
	query_view_set_view_obj (qv, obj);

	query_add_view_check (q, qv);
	query_add_view (q, qv);

	return qv;
}


/* query_add_view_check
 *
 * checks the view list to see if it already exists one (or more) instances of
 * the view. If so, defines alias information: number of occurrence and alias name
 * which is table_occ
 *
 */

static void
query_add_view_check (Query *q, QueryView *qv)
{
	GSList *list;
	gint found;

	list = q->views;
	found = -1;
	while (list) {
		QueryView *list_qv;
		list_qv = QUERY_VIEW (list->data);
		if ((list_qv->obj == qv->obj) && (found < list_qv->occ))
			found = list_qv->occ;
		list = g_slist_next (list);
	}
	if (found > -1) {
		qv->occ = found + 1;
		qv->alias = g_strdup_printf ("%s_%d", query_view_get_real_name (qv), qv->occ);
	}
}

static void query_view_finalize_cb (Query *q, GObject   *obj);
void 
query_add_view (Query *q, QueryView *qv)
{
	g_return_if_fail (q);
	g_return_if_fail (qv);
	g_return_if_fail (G_IS_OBJECT (qv->obj));

	g_assert (qv->obj);
	g_assert (IS_QUERY (qv->obj) || IS_DB_TABLE (qv->obj));		

	q->views = g_slist_append (q->views, qv);
	g_object_weak_ref (G_OBJECT (qv), (GWeakNotify) query_view_finalize_cb, q);
	q->weak_ref_objects = g_slist_append (q->weak_ref_objects, qv);

#ifdef debug_signal
	g_print (">> 'QUERY_VIEW_ADDED' from query_add_view\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[QUERY_VIEW_ADDED], 0, qv);
#ifdef debug_signal
	g_print ("<< 'QUERY_VIEW_ADDED' from query_add_view\n");
#endif	


	/* FIXME: look into the 'top level' query and make a copy
	   of the relevant joins into this query.
	*/
}

static void 
query_view_finalize_cb (Query *q, GObject *obj)
{
	QueryView *qv = QUERY_VIEW (obj);
	
	if (qv) {
		q->weak_ref_objects = g_slist_remove (q->weak_ref_objects, qv);
		query_del_view (q, qv);	
	}
}

void 
query_del_view (Query *q, QueryView *qv)
{
	GSList *fields;
	gboolean do_free_qv = FALSE;
	
	g_return_if_fail (q && IS_QUERY(q));
	g_return_if_fail (qv && IS_QUERY_VIEW (qv));
	g_return_if_fail (g_slist_find (q->views, qv));

	/* Weak unref */
	if (g_slist_find (q->weak_ref_objects, qv)) {
		g_object_weak_unref (G_OBJECT (qv), (GWeakNotify) query_view_finalize_cb, q);		
		q->weak_ref_objects = g_slist_remove (q->weak_ref_objects, qv);
		do_free_qv = TRUE;
	}

	/* When a query view is removed, remove any QueryField which depended
	   on this query view */
	fields = q->fields;
	while (fields) {
		GSList *list;

		list = query_field_get_monitored_objects (QUERY_FIELD (fields->data));
		if (g_slist_find (list, qv->obj)) {
			query_del_field (q, QUERY_FIELD (fields->data));
			fields = q->fields;
		}
		else
			fields = g_slist_next (fields);
		g_slist_free (list);
	}
	

	q->views = g_slist_remove (q->views, qv);
#ifdef debug_signal
	g_print (">> 'QUERY_VIEW_REMOVED' from query_del_view\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[QUERY_VIEW_REMOVED], 0, qv);
#ifdef debug_signal
	g_print ("<< 'QUERY_VIEW_REMOVED' from query_del_view\n");
#endif	
	if (do_free_qv)
		query_view_free (qv);
}




/*
 * QueryFields Management
 */

/* gets the newt ID which is free in the query. I proposed != 0,
   then the ID will either be the one proposed, or the next one free 
   (with a warning, because it should not happen) */
guint
query_get_unique_field_id (Query *q, guint proposed_id)
{
	guint id = 0;

	if (proposed_id) {
		if (query_get_field_by_id (q, proposed_id)) {
			g_warning ("query_get_unique_field_id(): id %d is already used!", proposed_id);
			return query_get_unique_field_id (q, 0);
		}
		else {
			id = proposed_id;
			if (id > q->fields_counter)
				q->fields_counter = id + 1;
		}
	}
	else {
		id = q->fields_counter;
		q->fields_counter ++;
	}

	return id;
}

static void field_destroy_cb (Query *q, QueryField *field);
static void field_field_modified_cb (QueryField *field, Query *q);
static void field_name_modified_cb (QueryField *field, Query *q);
static void field_alias_modified_cb (QueryField *field, Query *q);
void 
query_add_field (Query *q, QueryField *field)
{
	g_return_if_fail (q);
	g_return_if_fail (field);

	/* name duplication issues: we append _num to avoid duplication */
	if (field->name) {
		gchar *name;
		guint i = 1;
		GSList *list;

		name = g_strdup (field->name);
		list = q->fields;
		while (list) {
			if (QUERY_FIELD (list->data)->name &&
			    !strcmp (name, QUERY_FIELD (list->data)->name)) {
				g_free (name);
				name = g_strdup_printf ("%s_%d", field->name, i++);
				list = q->fields;
			}
			else
				list = g_slist_next (list);
		}
		if (strcmp (name, field->name)) 
			query_field_set_name (field, name);
		g_free (name);
	}

	/* alias duplication issues: we append _num to avoid duplication */
	if (field->alias) {
		gchar *name;
		guint i = 1;
		GSList *list;

		name = g_strdup (field->alias);
		list = q->fields;
		while (list) {
			if (QUERY_FIELD (list->data)->alias &&
			    !strcmp (name, QUERY_FIELD (list->data)->alias)) {
				g_free (name);
				name = g_strdup_printf ("%s_%d", field->alias, i++);
				list = q->fields;
			}
			else
				list = g_slist_next (list);
		}
		if (strcmp (name, field->alias)) 
			query_field_set_alias (field, name);
		g_free (name);
	}

	q->fields = g_slist_append (q->fields, field);

#ifdef debug_signal
	g_print (">> 'FIELD_CREATED' from query_add_field\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[FIELD_CREATED], 0, field);
#ifdef debug_signal
	g_print ("<< 'FIELD_CREATED' from query_add_field\n");
#endif

	/* Weak ref on the QueryField */
	g_object_weak_ref (G_OBJECT (field), (GWeakNotify) field_destroy_cb, q);

	/* fetch the "field_modified" and "field_type_changed" signals */
	g_signal_connect (G_OBJECT (field), "field_modified",
			  G_CALLBACK (field_field_modified_cb), q);
	g_signal_connect (G_OBJECT (field), "field_type_changed",
			  G_CALLBACK (field_field_modified_cb), q);

	/* fetch the "name_modified" signal */
	g_signal_connect (G_OBJECT (field), "name_changed",
			  G_CALLBACK (field_name_modified_cb), q);

	/* fetch the "alias_modified" signal */
	g_signal_connect (G_OBJECT (field), "alias_changed",
			  G_CALLBACK (field_alias_modified_cb), q);
}


static void 
field_destroy_cb (Query *q, QueryField *field)
{
	g_return_if_fail (q);
	g_return_if_fail (field);

	if (g_slist_find (q->fields, field)) {
		q->fields = g_slist_remove (q->fields, field);
		
#ifdef debug_signal
		g_print (">> 'FIELD_DROPPED' from field_destroy_cb\n");
#endif
		g_signal_emit (G_OBJECT (q), query_signals[FIELD_DROPPED], 0, field);
#ifdef debug_signal
		g_print ("<< 'FIELD_DROPPED' from field_destroy_cb\n");
#endif
	}
}


static void 
field_field_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from field_field_modified_cb\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[FIELD_MODIFIED], 0, field);
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from field_field_modified_cb\n");
#endif
}


static void 
field_name_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_NAME_MODIFIED' from field_name_modified_cb\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[FIELD_NAME_MODIFIED], 0, field);
#ifdef debug_signal
	g_print ("<< 'FIELD_NAME_MODIFIED' from field_name_modified_cb\n");
#endif
}

static void 
field_alias_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_ALIAS_MODIFIED' from field_alias_modified_cb\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[FIELD_ALIAS_MODIFIED], 0, field);
#ifdef debug_signal
	g_print ("<< 'FIELD_ALIAS_MODIFIED' from field_alias_modified_cb\n");
#endif
}


void 
query_del_field (Query *q, QueryField *field)
{
	query_field_free (field);
}

QueryField *
query_get_field_by_name (Query * q, gchar * name)
{
	QueryField *qf = NULL;
	GSList *list;

	list = q->fields;
	while (list && !qf) {
		if (!strcmp ( QUERY_FIELD(list->data)->name, name))
			qf = QUERY_FIELD (list->data);
		else
			list = g_slist_next (list);
	}

	return qf;
}

QueryField *
query_get_field_by_xmlid(Query * q, gchar * xmlid)
{
	/* The XML ID string is provided by each QueryField
	   in the QueryField::query_field_get_xml_id() function
	*/
	
	QueryField *qf = NULL;
	gchar *str, *ptr;
	guint id;
	GSList *token;

	g_return_val_if_fail (xmlid, NULL);

	str = g_strdup (xmlid);
	ptr = strtok (str, ":");
	ptr += 2;
	id = atoi (ptr);
	if (id != q->id)
		return NULL;
	
	ptr = strtok (NULL, ":");
	ptr +=2;
	id = atoi (ptr);
	g_free(str);
	token = q->fields;
	while (token && !qf) {
		if (QUERY_FIELD (token->data)->id == id)
			qf = QUERY_FIELD (token->data);
		token = g_slist_next (token);
	}

	return qf;
}


QueryField *
query_get_field_by_id (Query * q, guint id)
{
	QueryField *qf = NULL;
	GSList *list = q->fields;

	/* looking in this query first */
	while (list && !qf) {
		if (QUERY_FIELD(list->data)->id == id)
			qf = QUERY_FIELD (list->data);
		else
			list = g_slist_next (list);
	}
	
	return qf;
}


void
query_swap_fields (Query * q, QueryField *f1, QueryField *f2)
{
	GSList *list1, *list2;
	gpointer data;

	list1 = g_slist_find (q->fields, f1);
	list2 = g_slist_find (q->fields, f2);

	if (list1 && list2) {
		/* swapping list1->data and list2->data */
		data = list1->data;
		list1->data = list2->data;
		list2->data = data;

#ifdef debug_signal
		g_print (">> 'CHANGED' from query_swap_fields\n");
#endif
		g_signal_emit (G_OBJECT (q), query_signals[CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'CHANGED' from query_swap_fields\n");
#endif
	}
}


static void sub_query_created (Query *sub_query, Query *new_query, Query *parent_query);
static void sub_query_dropped (Query *sub_query, Query *old_query, Query *parent_query);
void 
query_add_sub_query (Query *q, Query *sibling, Query *sub_query)
{
	gint pos = -1;

	g_return_if_fail (q);
	g_return_if_fail (sub_query);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (IS_QUERY (sub_query));

	/* test for incompatibilities between the queries */
	if (! query_is_query_compatible (q, sub_query))
		return;
 
	if (sibling) {
		g_return_if_fail (IS_QUERY (sibling));
		if (!g_slist_find (q->sub_queries, sibling)) {
			g_warning ("sibling not child of query in %s line %d", __FILE__, __LINE__);
			return;
		}
		pos = g_slist_index (q->sub_queries, sibling);
	}
	/* if the query already has a parent then we remove that 
	   dependency */
	if (sub_query->parent) {
		g_object_ref (G_OBJECT (sub_query)); /* to avoid destruction */
		query_del_sub_query (sub_query->parent, sub_query);
	}
	
	/* make sure the sub query is not yet there */
	g_assert (!g_slist_find (q->sub_queries, sub_query));

	/* add the query to the list */
	q->sub_queries = g_slist_insert (q->sub_queries, sub_query, pos);
	sub_query->parent = q;

	g_print ("sub query %s added to query %s at pos %d\n", sub_query->name, q->name, pos);
	
#ifdef debug_signal
	g_print (">> 'QUERY_CREATED' from query_add_sub_query (Query=%s)\n", q->name);
#endif
	g_signal_emit (G_OBJECT (q), query_signals[QUERY_CREATED], 0, sub_query);
#ifdef debug_signal
	g_print ("<< 'QUERY_CREATED' from query_add_sub_query (Query=%s)\n", q->name);
#endif	

	/* to have a propagation of the signal */
	g_signal_connect (G_OBJECT (sub_query), "query_created",
			    G_CALLBACK (sub_query_created), q);
	g_signal_connect (G_OBJECT (sub_query), "query_dropped",
			    G_CALLBACK (sub_query_dropped), q);
}

/* test for incompatibilities between the queries
   - sub query with fields linked to an inaccessible query
   - sub query which does not have the right kind of fields for UNION, etc queries
   WARNING: q can be NULL, to test for a query
            to add as a top query (immediate child of conf->top_query).
*/
static GSList * query_fields_list (Query *q, GSList *sofar);
gboolean 
query_is_query_compatible (Query *q, Query *sub_query)
{
	gboolean comp = TRUE;
	GSList *list, *hold;
	QueryField *qf;

	/* sub query with fields linked to an inaccessible query */
	list = query_fields_list (sub_query, NULL);
	while (list && comp) {
		gboolean test = TRUE;
		qf = QUERY_FIELD (list->data);
		if (qf->field_type == QUERY_FIELD_QUERY) {
			Query *query;
			/* test if the query represented by the field is present among the sub queries
			   of sub_query */
			query = query_field_query_get_query (qf);
			if (! g_slist_find (sub_query->sub_queries, query))
				test = FALSE;
#ifdef debug
			g_warning ("Rejected sub query insertion: query incompatible test 1\n");
#endif
		}
		if (test && (qf->field_type == QUERY_FIELD_QUERY_FIELD)) {

		}

		if (!test)
			comp = FALSE;
		hold = list;
		list = g_slist_remove_link (list, list);
		g_slist_free_1 (hold);
	}
	/* if there is a remaining in list, free it */
	if (list)
		g_slist_free (list);

	return comp;
}

static GSList *
query_fields_list (Query *q, GSList *sofar)
{
	GSList *list, *list2=sofar;

	if (q->fields) {
		list = g_slist_copy (q->fields);
		list2 = g_slist_concat (sofar, list);
	}

	list = q->sub_queries;
	while (list) {
		list2 = query_fields_list (QUERY (list->data), list2);
		list = g_slist_next (list);
	}

	return list2;
}

/* this is called when a sub query Q2 of the query parent_query Q1 emits a signal
   to tell a sub query Q3 has been added to Q2, so that Q1 can emit the same
   signal which gets propagated to the top level query */
static void 
sub_query_created (Query *sub_query, Query *new_query, Query *parent_query)
{
#ifdef debug_signal
		g_print (">> 'QUERY_CREATED' from sub_query_created\n");
#endif
		g_signal_emit (G_OBJECT (parent_query), query_signals[QUERY_CREATED], 0, new_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_CREATED' from sub_query_created\n");
#endif		

}

/* same as the sub_query_added() function but for the removal of queries */
static void 
sub_query_dropped (Query *sub_query, Query *old_query, Query *parent_query)
{
#ifdef debug_signal
		g_print (">> 'QUERY_DROPPED' from sub_query_dropped\n");
#endif
		g_signal_emit (G_OBJECT (parent_query), query_signals[QUERY_DROPPED], 0, old_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_DROPPED' from sub_query_dropped\n");
#endif		
}

void 
query_del_sub_query (Query *q, Query *sub_query)
{
	g_return_if_fail (q);
	g_return_if_fail (sub_query);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (IS_QUERY (sub_query));

	if (g_slist_find (q->sub_queries, sub_query)) {
		q->sub_queries = g_slist_remove (q->sub_queries, sub_query);
		
		/* to stop getting signals from that sub query */
		g_signal_handlers_disconnect_by_func (G_OBJECT (sub_query),
						      G_CALLBACK (sub_query_created), q);
		g_signal_handlers_disconnect_by_func (G_OBJECT (sub_query),
						      G_CALLBACK (sub_query_dropped), q);
		
		sub_query->parent = NULL;
#ifdef debug_signal
		g_print (">> 'QUERY_DROPPED' from query_del_sub_query\n");
#endif
		g_signal_emit (G_OBJECT (q), query_signals[QUERY_DROPPED], 0, sub_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_DROPPED' from query_del_sub_query\n");
#endif	
		g_object_unref (G_OBJECT (sub_query));
	}
}


/*
 * Query joins management
 */


static void
query_place_list (Query *query, GSList *list, QueryView *view);

static guint
find_views_in_joins_lists (QueryView *vant, QueryView *vsuc, GSList **plists, GSList **pjoins);

/* return codes for the find_views* function */
#define FOUND_NONE 0	/* no view found */
#define FOUND_VANT 1	/* ant view was found */
#define FOUND_VSUC 2	/* suc view was found */

static GSList *g_slist_set_append (GSList *list, gpointer elem);

/* query_list_joins
 * print the lists of joins list
 * as pointers
 */
static void 
query_list_joins (Query *q)
{
	GSList *list, *sub_list;

	list = q->joins;
	while (list) {
		g_print ("Master list %p (->%p)\n", list, list->data);

		sub_list = (GSList *)(list->data);
		while (sub_list) {
			g_print ("\tSub list %p (->%p)\n", sub_list, sub_list->data);
			sub_list = g_slist_next (sub_list);
		}
		list = g_slist_next (list);
	}
}

#ifdef debug
/* query_print_joins
 *
 * prints the list of joins lists 
 *  (for debug purposes)
 *
 */

static void 
query_print_joins (Query *q)
{
	GSList *list1, *list2;

	list1 = q->joins;
	while (list1) {
		list2 = (GSList *) (list1->data);
		while (list2) {
			query_join_print(QUERY_JOIN(list2->data));
			list2 = g_slist_next(list2);
			if (list2 != NULL) g_print(" ");
		}
		g_print("\n");
		list1 = g_slist_next(list1);
	}
}
#endif


/* find_views_in_joins_lists
 *
 * Auxiliary function to find a view in a list of joins lists.
 * It looks for two views (vant, vsuc) simultaneously.
 * One of the two views can be NULL.
 * It assumes the list is properly organised (see query_add_join)
 * plists is a pointer to the main list pointer and there should be a sub-list
 * pjoins can be NULL or point to an element of the first sublist
 * *plists will return the main lis element refering to the sub-list
 * cond the view was found (if so) and *pjoins will point to the
 * element in the sub-list cond the join was found.
 * 
 */

static guint
find_views_in_joins_lists (QueryView *vant, QueryView *vsuc, GSList **plists, GSList **pjoins)
{
	GSList *lists = *plists;
	GSList *joins = *pjoins;
	QueryJoin *jelem;
	QueryView *el_ant, *el_suc;
	guint found;

	if (*plists == NULL) return FOUND_NONE;

	found = FOUND_NONE;
	do {
		if (joins == NULL) { /* search a new sub-list */
	    		joins = (GSList *) (lists->data);
			jelem = QUERY_JOIN (joins->data);
			el_ant = query_join_get_ant_view (jelem);
			if (vant == el_ant)
				found = FOUND_VANT;
			else if (vsuc == el_ant)
				found = FOUND_VSUC;
		}
		else {
			jelem = QUERY_JOIN (joins->data);
			el_suc = query_join_get_suc_view (jelem);
			if (vant == el_suc)
				found = FOUND_VANT;
			else if (vsuc == el_suc)
				found = FOUND_VSUC;
			if (!found) { /* progress to next element and/or sub-list*/
				joins = g_slist_next (joins);
				if (joins == NULL) {
					lists = g_slist_next (lists);
				}
			}
		}
	} while (lists && !found) ;

	*plists = lists;	/* sub-list cond view was found */
	*pjoins = joins;	/* join cond view was found */
	return found;
}


/* g_slist_set_append 
 *
 * appends the element elem to end of the list, except if it is 
 * already in the list
 *
 */

static GSList *g_slist_set_append (GSList *list, gpointer elem)
{
	GSList *aux;

	if (list == NULL) {
		list = g_slist_prepend(NULL, elem);
	}
	else {
		aux = list;
		while ((aux->next != NULL) && (elem != aux->data)) {
			aux = aux->next;
		}

		if (elem != aux->data) {
			aux->next = g_slist_prepend(NULL, elem);
		}
	}

	return list;
}


static void query_place_join (Query *q, QueryJoin *join);

/* query_place_list
 *
 * Place joins from list starting with the one having view
 *
 * Algorithm:
 * create a queue having "view" as initial element
 * for each view in the queue
 *   for each join having this view in list
 *      remove the join from list and place it in the query
 *      get the other view in the join and queue it (no duplicates)
 *   
 */

static void
query_place_list (Query *query, GSList *list, QueryView *view)
{
	GSList *queue = NULL;	/* view queue */
	GSList *q;		/* auxiliary to queue */
	GSList *lst;		/* list iterator */
	GSList *ant;		/* iterator going before lst */
	QueryJoin *join;
	gboolean merge;

	queue = g_slist_prepend(queue, view);
	lst = list;
	while (lst != NULL) {
		/* pop view from the queue */
		view = queue->data;
		q = queue;
		queue = g_slist_remove_link (queue, q);
		g_slist_free_1 (q);

		/* place joins in list having view as a member */
		lst = list;
		ant = list;
		while (lst != NULL) {
			QueryView *vant, *vsuc;
			join = QUERY_JOIN(lst->data);
			vant = query_join_get_ant_view (join);
			vsuc = query_join_get_suc_view (join);
			merge = TRUE;
			if (view == vsuc) {
				query_join_swap_views (join);
				queue = g_slist_set_append (queue, vant);
			}
			else if (view == vant)
				queue = g_slist_set_append (queue, vsuc);
			else
				merge = FALSE;

			if (merge) {
				GSList *hold;
				/* remove join (lst2) from list2 */
				hold = lst;
				if (lst == list) { /* join is first elem of list2 */
					ant = lst->next;
					list = ant;
					lst = ant;
				}
				else {
					ant->next = lst->next;
					lst = lst->next;
				}
				hold->next = NULL;

				/* insert the join (hold) after view in list1 */
				query_place_join (query, QUERY_JOIN (hold->data));
			}
			else { /* go to next elem in lst2 */
				ant = lst;
				lst = lst->next;
			}
		}
	}

	/* release leftovers in queue */
	while (queue != NULL) {
		q = queue;
		queue = g_slist_remove_link (queue, q);
		g_slist_free_1 (q);
	}
}


/* query_place_join
 *
 * Inserts a join object in the query joins list. This operation ensures
 * the resulting list represents a valid join sequence i.e., the SQL FROM 
 * statement can be derived from it just by following the list sequence.
 *
 * After inserting a join, its ant_view holds the left side view and the
 * suc_view holds the right side view. Note that the concept of left/right
 * only makes sense inside a concrete SQL statement string. In a visual
 * query, there is no such concept. A visual indication needs to be used
 * in order to signal that the non-joined records of a table should appear.
 * For instance, an asterisk * (a la Oracle) or a small circle, could appear
 * near the table's end of the join line.
 * 
 * Duplicate joins are not added.
 * 
 * The views in the join must already exist in the query views list. This is
 * needed because if a table needs to appear twice in a join sequence, one
 * of the tables needs to be referred by an alias. This alias should already
 * exist in the visual part (the user can modify the name), hence it should
 * exist in the views list.
 *
 * Disconnected join list are supported. This allows two or more separate
 * groups of tables (now links between groups). This is a cartesian product
 * between the groups). Each group is represented by a G_SList. The query
 * contains a G_SList of these sub-lists.
 * 
 * Join list organization: (a join is (ant_view, suc_view, condition) )
 * - 1st join is (t1, t2, c2) and represents "t1 JOIN t2 ON c2"
 * - 2nd join is (t1 or t2, t3, c3)
 * - nth join is (one of t1..tn-1, tn, cn)
 *
 * Notes:
 * - in the 2nd to nth join, the left view must exist in a previous join.
 * - when a loop is created in a query, the corresponding join has two
 *   views who already exist in two separate joins. The new join is then
 *   placed imediately after the second of the existing joins.
 * 
 * A disconnected query join list contains more than one join sub-list. In
 * this case, no view from each sub-list appears in the other sub-list. 
 * These sub-lists are separated by commas in the corresponding SQL statement.
 * If a new join is added with one view in a 1st group and the other in a
 * 2nd group, then the two sub-lists have to be properly merged (the order
 * of the joins in the 2nd list usually change and sometimes two views in
 * the same join have to be swapped (in which case a LEFT JOIN becomes a
 * RIGHT JOIN, for instance).
 *
 */

static void 
query_place_join (Query *q, QueryJoin *join)
{
	QueryView *vant, *vsuc;

	g_return_if_fail (q);
	g_return_if_fail (join);
	vant = query_join_get_ant_view (join);
	g_return_if_fail (vant);
	vsuc = query_join_get_suc_view (join);
	g_return_if_fail (vsuc);
	g_return_if_fail (vant != vsuc);

	if (q->joins == NULL) {
		q->joins = g_slist_prepend (NULL, g_slist_prepend (NULL, join));
	}
	else {
		/* see if one of the join's views are in the joins lists */
		GSList *list1, *list2;
		GSList *joins=NULL, *joins2;
		guint found;

		list1 = q->joins;
		found = find_views_in_joins_lists (vant, vsuc, &list1, &joins);
		if (found == FOUND_NONE) {
			/* append a new joins list having this join */
			q->joins = g_slist_append (q->joins,
						g_slist_prepend (NULL, join));
		}
		else {
			/* swap views if needed */
			GSList *elem;
			if (found == FOUND_VSUC) {
				query_join_swap_views (join);
				vsuc = vant; /* No need for old vsuc! */
			}

			/* search for the 2nd view, vsuc */
			joins2 = joins->next;
			if (joins2 == NULL) {
				list2 = g_slist_next(list1);
			}
			else
				list2 = list1;
			if (list2 == NULL)
				found = FOUND_NONE;
			else {
				joins2 = (GSList *) list2->data;
				found = find_views_in_joins_lists (NULL, vsuc, &list2, &joins2);
			}

			if ((list1 == list2) && joins2) { /* found loop in the list */
				query_join_set_skip_view(join);
				joins = joins2;
			}

			/* - insert new join after joins position */
			elem = g_slist_prepend (NULL, join);
			elem->next = joins->next;
			joins->next = elem;

			if (found != FOUND_NONE) {
				if (list1 != list2) { /* have merge two sub-lists */
					q->joins = g_slist_remove_link (q->joins, list2);
					query_place_list (q, list2->data, vsuc);
					g_slist_free_1 (list2);
				}
			}
		}
	}
}

static void join_destroy_cb (Query *q, QueryJoin *join);
static void join_modified_cb (QueryJoin *join, Query *q);
static void join_pairs_modified_cb (QueryJoin *join, QueryJoinPair *pair, Query *q);
void 
query_add_join (Query *q, QueryView *v1, QueryField *f1, QueryView  *v2, QueryField *f2)
{
	QueryJoin *qj = NULL;
	QueryView *qj_vant = NULL, *qj_vsuc = NULL;
	GSList *list;

	query_list_joins (q);

	/* Look for an existing QueryJoin or define one */
	list = q->joins;
	while (list && !qj) {
		GSList *list2;
		list2 = (GSList *) (list->data);
		while (list2 && !qj) {
			qj_vant = QUERY_JOIN (list2->data)->ant_view;
			qj_vsuc = QUERY_JOIN (list2->data)->suc_view;
			if (((qj_vant == v1) && (qj_vsuc == v2)) ||
			    ((qj_vant == v2) && (qj_vsuc == v1)))
				qj = QUERY_JOIN (list2->data);
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}
			
	if (!qj) {
		qj = QUERY_JOIN (query_join_new (q, v1, v2));
		real_query_add_join (q, qj);
	}

	query_join_add_pair (qj, G_OBJECT (f1), G_OBJECT (f2));
}


static void 
real_query_add_join (Query * q, QueryJoin *join)
{
	query_place_join (q, join);

#ifdef debug_signal
	g_print (">> 'JOIN_CREATED' from real_query_add_join\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[JOIN_CREATED], 0, join);
#ifdef debug_signal
	g_print ("<< 'JOIN_CREATED' from real_query_add_join\n");
#endif
	
	/* Weak ref on the join */
	g_object_weak_ref (G_OBJECT (join), (GWeakNotify) join_destroy_cb, q);
	
	/* fetch the "type_changed" signal */
	g_signal_connect (G_OBJECT (join), "type_changed",
			  G_CALLBACK (join_modified_cb), q);
	g_signal_connect (G_OBJECT (join), "pair_added",
			  G_CALLBACK (join_pairs_modified_cb), q);
	g_signal_connect (G_OBJECT (join), "pair_removed",
			  G_CALLBACK (join_pairs_modified_cb), q);

	query_list_joins (q);
}


static void 
join_destroy_cb (Query *q, QueryJoin *join)
{
	GSList *lists, *antl;
	GSList *joins, *antj;
	GSList *slist2, *tmp;

	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (join && IS_QUERY_JOIN (join));
	if (! q->joins) /* FIXME: here should always be not NULL */
		return;

	query_list_joins (q);

	/* search for the join in q->joins */
	lists = q->joins;	/* points to a sub-list (lists->data) */
	antl = lists;		/* points to the sub-list before lists */
	joins = (GSList *) (lists->data);
	antj = joins;		/* points to the element before joins */

	while (lists && (join != joins->data)) {
		antj = joins;
		joins = g_slist_next (joins);
		if (! joins) { /* search in a new sub-list */
			antl = lists;
			lists = g_slist_next (lists);
			if (lists) {
				joins = (GSList *) (lists->data);
				antj = joins;
			}
		}
	}

	/* g_return_if_fail (lists);*/
	/* sometimes this callback is called twice and lists is empty */
	if (!lists) return;

	/* remove the join from the list */
	slist2 = joins->next;	/* holds join sub-list after the join item */
	if (antj == joins) { /* it's the first join in the list */ 
		/* the sub-list becomes empty and has to be removed */
		if (antl == lists) 	/* it's the first sub-list */
			q->joins = lists->next;
		else
			antl->next = lists->next;
		g_slist_free_1 (lists);
	}

	if (query_join_skip_view (QUERY_JOIN(joins->data))) {
		/* the join _identifies_ a loop; just remove it */
		antj->next = joins->next;
	}
	else if (slist2 && query_join_skip_view (QUERY_JOIN(slist2->data))) {
		/* the join is _part_ of a loop; just remove it */
		antj->next = joins->next;
		/* the next join identifies a loop; unset it */
		query_join_unset_skip_view (QUERY_JOIN(slist2->data));
	}
	else {
		/* split the list and re-place the 2nd half in the query */
		antj->next = NULL;
		while (slist2) {
			query_join_unset_skip_view (QUERY_JOIN (slist2->data));
			query_place_join (q, QUERY_JOIN (slist2->data));
			tmp = slist2;
			slist2 = g_slist_next (slist2);
			g_slist_free_1 (tmp);
		}
	}

	g_slist_free_1 (joins);


#ifdef debug_signal
	g_print (">> 'JOIN_DROPPED' from query_del_join\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[JOIN_DROPPED], 0, join);
#ifdef debug_signal
	g_print ("<< 'JOIN_DROPPED' from query_del_join\n");
#endif

	query_list_joins (q);
}

static void 
join_modified_cb (QueryJoin *join, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'JOIN_MODIFIED' from join_modified_cb\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[JOIN_MODIFIED], 0, join);
#ifdef debug_signal
	g_print ("<< 'JOIN_MODIFIED' from join_modified_cb\n");
#endif
}

static void 
join_pairs_modified_cb (QueryJoin *join, QueryJoinPair *pair, Query *q)
{
	join_modified_cb (join, q);	
}


void 
query_del_join (Query * q, QueryJoin *join)
{
	query_join_free (join);
}


/*
 * Query COND/HAVING management
 */

/* WARNING: cond MUST not already be the child of another QueryCond */
void 
query_add_cond (Query * q, QueryCond *cond, QueryCond * parent, QueryCond * sibling)
{
	gint pos = -1;

	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (cond && IS_QUERY_COND (cond));
	g_return_if_fail (cond->query == q);

	if (parent) {
		g_return_if_fail (IS_QUERY_COND (parent));
		g_return_if_fail (parent->query == q);
		g_return_if_fail (q->where_cond && query_cond_is_ancestor (parent, q->where_cond));
	}

	if (sibling) {
		g_return_if_fail (parent);
		g_return_if_fail (IS_QUERY_COND (sibling));
		g_return_if_fail (sibling->query == q);
		g_return_if_fail (g_slist_find (parent->children, sibling));
		pos = g_slist_index (parent->children, sibling);
	}

	if (parent) 
		query_cond_add_to_node (parent, cond, pos);
	else {
		if (q->where_cond)
			query_cond_add_to_node (q->where_cond, cond, -1);
		else
			q->where_cond = cond;
	}

	
#ifdef debug_signal
	g_print (">> 'COND_CREATED' from query_add_cond\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[COND_CREATED], 0, cond);
#ifdef debug_signal
	g_print ("<< 'COND_CREATED' from query_add_cond\n");
#endif	
	/* FIXME: catch signals from the QueryCond object */
}

static void query_del_cond_real (Query * q, QueryCond *cond, gboolean do_destroy);
void 
query_del_cond (Query * q, QueryCond *cond)
{
	query_del_cond_real (q, cond, TRUE);
}

void 
query_del_cond_real (Query * q, QueryCond *cond, gboolean do_destroy)
{
	QueryCond *parent;

	g_return_if_fail (q && IS_QUERY (cond));
	g_return_if_fail (cond && IS_QUERY_COND (cond));
	g_return_if_fail (cond->query == q);
	g_return_if_fail (q->where_cond);

	parent = query_cond_get_parent (cond, q->where_cond);
	g_return_if_fail (parent);

	query_cond_del_from_node (parent, cond, do_destroy);
		
#ifdef debug_signal
	g_print (">> 'COND_DROPPED' from query_del_cond\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[COND_DROPPED], 0, cond);
#ifdef debug_signal
	g_print ("<< 'COND_DROPPED' from query_del_cond\n");
#endif
}

void 
query_move_cond (Query * q, QueryCond * cond, QueryCond * to_parent, QueryCond * to_sibling)
{
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (cond && IS_QUERY_COND (cond));
	g_return_if_fail (cond->query == q);
	
	query_del_cond_real (q, cond, FALSE);
	query_add_cond (q, cond, to_parent, to_sibling);

#ifdef debug_signal
	g_print (">> 'COND_MOVED' from query_move_cond\n");
#endif
	g_signal_emit (G_OBJECT (q), query_signals[COND_MOVED], 0, cond);
#ifdef debug_signal
	g_print ("<< 'COND_MOVED' from query_move_cond\n");
#endif
}




/*
 * Management of the Envs
 */
void 
query_add_env (Query *q, GObject   *env)
{
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (env && IS_QUERY_ENV (env));

	/* make sure the env is not yet there */
	g_assert (!g_slist_find (q->envs, env));

	/* add the query to the list */
	q->envs = g_slist_append (q->envs, env);
	
#ifdef debug_signal
	g_print (">> 'ENV_CREATED' from query_add_env (Query=%s)\n", q->name);
#endif
	g_signal_emit (G_OBJECT (q), query_signals[ENV_CREATED], 0, env);
#ifdef debug_signal
	g_print ("<< 'ENV_CREATED' from query_add_env (Query=%s)\n", q->name);
#endif	

	q->conf->save_up_to_date = FALSE;
}

void 
query_del_env (Query *q, GObject   *env)
{
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (env && IS_QUERY_ENV (env));

	if (g_slist_find (q->envs, env)) {
		q->envs = g_slist_remove (q->envs, env);
		
#ifdef debug_signal
		g_print (">> 'ENV_DROPPED' from query_del_env\n");
#endif
		g_signal_emit (G_OBJECT (q), query_signals[ENV_DROPPED], 0, env);
#ifdef debug_signal
		g_print ("<< 'ENV_DROPPED' from query_del_env\n");
#endif	
		g_object_unref (env);
	}

	q->conf->save_up_to_date = FALSE;
}

/*
 * Interrogation helper functions 
 */

gint
query_is_table_field_printed (Query * q, DbField * field)
{
	gint result = -1, i;
	GSList *list;
	QueryField *qf;
	DbTable *table;

	table = database_find_table_from_field (q->conf->db, field);
	if (!table)
		return result;

	list = q->fields;
	i = 0;
	while (list && (result == -1)) {
		qf = QUERY_FIELD (list->data);
		if ((qf->field_type == QUERY_FIELD_FIELD) && 
		    qf->is_printed) {
			GSList *objs;
			
			objs = query_field_get_monitored_objects (qf);
			if (g_slist_find (objs, table))
				result = i;
			g_slist_free (objs);

			i++;
		}
		list = g_slist_next (list);
	}

	return result;
}

QueryField *
query_get_field_by_pos (Query * q, gint pos)
{
	QueryField *qf, *retval = NULL;
	gint i;
	GSList *list;
	gboolean error = FALSE;

	g_return_val_if_fail (q && IS_QUERY (q), NULL);
	g_return_val_if_fail (pos >= 0, NULL);

	list = q->fields;
	i = -1;
	while (list && !error && !retval) {
		qf = QUERY_FIELD (list->data);
		if (qf->is_printed) {
			if (qf->field_type == QUERY_FIELD_ALLFIELDS) {
				/* this QF spans more than one column */
				gint j;
				j = query_field_allfields_get_span (qf);
				if (j < 0)
					error = TRUE;
				else {
					if (pos <= i+j)
						retval = qf;
					i += j;
				}
			}
			else {
				i++;
				if (i == pos)
					retval = qf;
			}
		}
		list = g_slist_next (list);
	}

	return retval;
}

gint
query_get_pos_by_field (Query * q, QueryField *fqf)
{
	gint i = -1, retval = -1;
	QueryField *qf;
	GSList *list;
	gboolean error = FALSE;

	g_return_val_if_fail (q && IS_QUERY (q), -1);
	g_return_val_if_fail (fqf && IS_QUERY_FIELD (fqf), -1);
	g_return_val_if_fail (fqf->query != q, -1);

	list = q->fields;
	while (list && !error && (retval==-1)) {
		qf = QUERY_FIELD (list->data);
		if (qf->is_printed) {
			if (qf == fqf)
				retval = i+1;
			if (qf->field_type == QUERY_FIELD_ALLFIELDS) {
				/* this QF spans more than one column */
				gint j;
				j = query_field_allfields_get_span (qf);
				if (j < 0)
					error = TRUE;
				else 
					i += j;
			}
			else
				i++;
		}
		list = g_slist_next (list);
	}

	return retval;
}

GSList *
query_find_equi_join_with_end   (Query * q, QueryField *qf)
{
	GSList *list = NULL;
	
	g_return_val_if_fail (q && IS_QUERY (q), NULL);
	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);
	g_return_val_if_fail (qf->query != q, NULL);

	/* FIXME */
	return list;
}

static gchar *get_select_part_of_sql (Query * q, GSList * missing_values);
static gchar *get_from_part_of_sql (Query * q);
static gchar *get_cond_part_of_sql (Query * q, GSList * missing_values);

/*
 * Getting the SQL version of the query
 */
gchar *
query_get_select_query (Query * q, GSList * missing_values)
{
	gchar *str, *retval = NULL;
	GString *SQL;

	if (q->type == QUERY_TYPE_SQL)
		retval = g_strdup (q->text_sql);
	else {
		SQL = g_string_new ("SELECT\n");

		/* SELECT objects */
		str = get_select_part_of_sql (q, missing_values);
		if (str) {
			g_string_append (SQL, str);
			g_free (str);
		}

		/* FROM relations */
		str = get_from_part_of_sql (q);
		if (str) {
			g_string_sprintfa (SQL, "\nFROM\n%s", str);
			g_free (str);
		}

		/* COND conditions */
		str = get_cond_part_of_sql (q, missing_values);
		if (str) {
			g_string_sprintfa (SQL, "\nCOND\n%s", str);
			g_free (str);
		}

		retval = SQL->str;
		g_string_free (SQL, FALSE);
	}

	return retval;
}


/* creates a new string with the objects that go in the SELECT part to SQL */
static gchar *
get_select_part_of_sql (Query * q, GSList * missing_values)
{
	gchar *str, *retval = NULL;
	GString *objects;
	gboolean first;
	GSList *fields;

	g_return_val_if_fail (q != NULL, NULL);

	fields = q->fields;
	first = TRUE;
	objects = g_string_new ("");
	while (fields) {
		QueryField *qf;
		qf = QUERY_FIELD (fields->data);
		
		if (qf->alias) {
			if (first)
				first = FALSE;
			else
				g_string_append (objects, ",\n");

			str = query_field_render_as_sql (qf, missing_values);
			g_string_sprintfa (objects, "%s", str);
			g_free (str);
		}
		fields = g_slist_next (fields);
	}

	if (!first) {
		retval = objects->str;
		g_string_free (objects, FALSE);
	}
	else
		g_string_free (objects, TRUE);

	return retval;
}


/* returns the COND part of a query while taking care of the automatic links
   and any other COND constraint imposed to the query 
   The allocated string has to be freed */
static gchar *
get_cond_part_of_sql (Query * q, GSList * missing_values)
{
	gboolean firstnode = TRUE;
	GString *gstr;
	gchar *wh = NULL;

	gstr = g_string_new ("");
	/* FIXME: VIV if (q->where_tree->children) { */
/* 		GNode *gnode = q->where_tree->children; */

/* 		while (gnode) { */
/* 			if (firstnode) */
/* 				firstnode = FALSE; */
/* 			else */
/* 				g_string_append (gstr, "AND "); */
/* 			wh = query_cond_render_as_sql (QUERY_COND (gnode->data), */
/* 							    missing_values); */
/* 			g_string_append (gstr, wh); */
/* 			g_free (wh); */
/* 			g_string_append (gstr, "\n"); */
/* 			gnode = gnode->next; */
/* 		} */
/* 	} */

	if (!firstnode) {
		wh = gstr->str;
		g_string_free (gstr, FALSE);
	}
	else 
		g_string_free (gstr, TRUE);

	return wh;
}


/* Creates the FROM part of the SQL statement
 * The general case is a comma separated list of relations (views) cond a
 * relation can be a table or colection of joined tables.
 * The token "FROM" is not returned, only the list of tables
 *
 * ex: tblAlone, tblA INNER JOIN tblB ON tblA.ID=tblB.AID 
 *     LEFT JOIN tblC ON tblB.ID=tblC.BID
 *
 * Restrictions:
 *  - only equi-joins; TODO FM: introduce theta joins (add new condition 
 *    column in the Joins' clist and edit it in a dialog box, or inline
 *    if widget supports it)
 *   
 * Note: those remaining tables (without join conditions) will create a
 *   cartesian product (CROSS), unless the user manually specifys joining
 *   conditions in the COND part of the query). The join conditions in the
 *   COND part as not automatically identified as such. This means they are
 *   not extracted and converted to the FROM part.
 *
 */

static gchar *
get_from_part_of_sql (Query * q)
{
	GSList *lists, *list, *views;
	QueryJoin *jelem;
	QueryView *el_ant, *el_suc;
	gboolean first;
	GString *joins=NULL, *all_joins=NULL;
	gchar *retval;

	views = q->views;
	while (views) {
		lists = q->joins;
		list = NULL;
		el_ant = QUERY_VIEW (views->data);
		if (FOUND_NONE == find_views_in_joins_lists (el_ant, NULL, &lists, &list)) {
			if (all_joins)
				g_string_append (all_joins, ", ");
			else
				all_joins = g_string_new ("");
			g_string_append (all_joins, query_view_get_alias_exp (el_ant));
		}
		views = g_slist_next (views);
	}

	lists = q->joins;
	while (lists) {
		list = (GSList *) (lists->data);
		first = TRUE;
		joins = g_string_new ("");
		while (list) {
			jelem = QUERY_JOIN (list->data);
			el_ant = query_join_get_ant_view (jelem);
			el_suc = query_join_get_suc_view (jelem);
			if (first) {	/* Ex: "tableAnt INNER JOIN tableSuc ON condition" */
				first = FALSE;
				g_string_sprintf (joins, "%s %s %s ON %s",
						query_view_get_alias_exp (el_ant),
 						query_join_get_join_type_sql (jelem),
						query_view_get_alias_exp (el_suc),
						query_join_get_condition (jelem));
			}
			else if (query_join_skip_view (jelem))
				/* Ex: "... AND condition" */
				g_string_sprintfa (joins, " AND %s",
						query_join_get_condition (jelem));
			else
				/* Ex: "... INNER_JOIN tableSuc ON condition" */
				g_string_sprintfa (joins, " %s %s ON %s",
 					   	query_join_get_join_type_sql (jelem),
						query_view_get_alias_exp (el_suc),
						query_join_get_condition (jelem));
			list = g_slist_next (list);
		}
		if (all_joins)
			g_string_append (all_joins, ", ");
		else
			all_joins = g_string_new ("");
		g_string_append (all_joins, joins->str);
		g_string_free (joins, TRUE);
		lists = g_slist_next (lists);
	}

	if (all_joins) {
		retval = all_joins->str;
		g_string_free (all_joins, FALSE);
	}
	else
		retval = NULL;

	return retval;
}


#ifdef debug
static gchar* query_type_to_str (Query *q)
{
	gchar *str;
	switch (q->type) {
	case QUERY_TYPE_STD:
		str = "Standard";
		break;
	case QUERY_TYPE_UNION:
		str = "Union";
		break;
	case QUERY_TYPE_INTERSECT:
		str = "Intersect";
		break;
	default:
		str = "Unknown!!!";
	}

	return str;
}

static gchar* field_type_to_str (QueryFieldType t) 
{
	gchar *str;

	switch (t) {
	case QUERY_FIELD_FIELD:
		str = "Table's field";
		break;
	case QUERY_FIELD_ALLFIELDS:
		str = "Table.*";
		break;
	case QUERY_FIELD_AGGREGATE:
		str = "Aggregate";
		break;
	case QUERY_FIELD_FUNCTION:
		str = "Function";
		break;
	case QUERY_FIELD_VALUE:
		str = "Constant value";
		break;
	case QUERY_FIELD_QUERY:
		str = "Whole query";
		break;
	case QUERY_FIELD_QUERY_FIELD:
		str = "Other query's field";
		break;
	default:
		str = "UNKNOWN TYPE";
	}
	return str;
}

/* structure debug function */
static void static_query_dump_contents (Query *q, guint offset);
void 
query_dump_contents (Query *q)
{
	g_return_if_fail (q);
	static_query_dump_contents (q, 0);
}

static void 
static_query_dump_contents (Query *q, guint offset)
{
	GSList *list, *sub_list;
	gchar *str;
	guint i;

	str = g_new0 (gchar, offset+1);
	for (i=0; i<offset; i++)
		str[i] = ' ';
	str[offset] = 0;

	g_print ("%s" D_COL_H1 "==== QUERY DEBUG DUMP -> %p (parent = %p) ====\n" D_COL_NOR, str, q, q->parent);
	g_print ("%s" D_COL_H2 "Id:" D_COL_NOR "    %d\n", str, q->id);
	g_print ("%s" D_COL_H2 "Name:" D_COL_NOR "  %s\n", str, q->name);
	g_print ("%s" D_COL_H2 "Descr:" D_COL_NOR " %s\n", str, q->descr);
	g_print ("%s" D_COL_H2 "Type:" D_COL_NOR "  %s\n", str, query_type_to_str (q));
	g_print ("%s" D_COL_H2 "State:" D_COL_NOR, str);
	if (query_fields_activated (q))
		g_print (D_COL_OK " All fields activated\n" D_COL_NOR);
	else
		g_print (D_COL_ERR " NON ACTIVATED\n" D_COL_NOR);

	/* QueryViews */
	g_print ("%s" D_COL_H2 "Query Views:\n" D_COL_NOR, str);
	list = q->views;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			QueryView *qv;
			qv = (QueryView *)(list->data);
			if (IS_DB_TABLE (qv->obj))
				g_print ("%s\ttable: %s ", str, DB_TABLE (qv->obj)->name);
			else {
				if (IS_QUERY (qv->obj))
					g_print ("%s\tquery: %s (id=%d) ", str,QUERY (qv->obj)->name, 
						 QUERY (qv->obj)->id);
				else
					g_print (D_COL_ERR "%s\tUNKNOWN!!! " D_COL_NOR, str);
			}
			g_print ("%sOCC=%d ", str, qv->occ);
			if (qv->alias)
				g_print ("%salias=%s\n", str, qv->alias);
			else
				g_print ("\n");
			list = g_slist_next (list);
		}
	}

	/* Query Fields */
	g_print ("%s" D_COL_H2 "Query fields:\n" D_COL_NOR, str);
	list = q->fields;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			QueryField *qf;
			GSList *mo, *iter;

			qf = QUERY_FIELD (list->data);
			g_print ("%s  " D_COL_H2 "* field:" D_COL_NOR " %s (@ %p)\n", str, qf->name, qf);
			g_print ("%s\tid: %d\n", str, qf->id);
			if (qf->is_printed)
				g_print ("%s\tPrinted, alias: %s\n", str, qf->alias);
			else
				g_print ("%s\tNot printed:\n", str);
			g_print ("%s\tfield type:     %s\n", str, field_type_to_str (qf->field_type));
			mo = query_field_get_monitored_objects (qf);
			g_print ("%s\tmonitored objs: ", str);
			iter = mo;
			while (iter) {
				g_print ("%p ", iter->data);
				iter = g_slist_next (iter);
			}
			if (mo) {
				g_print ("\n");
				g_slist_free (mo);
			}
			else
				g_print ("NONE\n");

			if (qf->activated) {
				gchar *sql;

				sql = query_field_render_as_sql (qf, NULL);
				g_print ("%s\tSQL= %s\n", str, sql);
				g_free (sql);
				g_print ("%s\t" D_COL_OK "activated\n" D_COL_NOR , str);
			}
			else
				g_print("%s\t" D_COL_ERR "not activated\n" D_COL_NOR, str);
			list = g_slist_next (list);
		}
	}


	/* QueryJoins */
	g_print ("%s" D_COL_H2 "Query Joins:\n" D_COL_NOR, str);
	list = q->joins;
	if (!list)
		g_print ("%s\tNONE\n", str);
	while (list) {
		g_print ("%s" D_COL_H2 "\tMaster list " D_COL_NOR "%p (->%p)\n", str, list, list->data);
		sub_list = (GSList*)(list->data);
		while (sub_list) {
			QueryJoin *qj;
			GSList *list2;
			qj = QUERY_JOIN (sub_list->data);
			g_print ("%s\t%p->Join with %d pair(s)\n", str, sub_list, g_slist_length (qj->pairs));
			list2 = qj->pairs;
			while (list2) {
				QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list2->data);
				g_print ("%s\t\t* ", str);
				if (IS_DB_FIELD (pair->ant_field))
					g_print ("DbField %s", DB_FIELD (pair->ant_field)->name);
				else
					g_print ("QueryField %s", QUERY_FIELD (pair->ant_field)->name);
				if (IS_DB_FIELD (pair->suc_field))
					g_print (" -- DbField %s\n", DB_FIELD (pair->suc_field)->name);
				else
					g_print (" -- QueryField %s\n", QUERY_FIELD (pair->suc_field)->name);
				
				list2 = g_slist_next (list2);
			}
			sub_list = g_slist_next (sub_list);
		}

		list = g_slist_next (list);
	} 

	/* Query Cond */
	g_print ("%s" D_COL_H2 "Query Where tree:\n" D_COL_NOR, str);
	if (!q->where_cond)
		g_print ("%s\tNONE\n", str);
	else 
		query_cond_dump_contents (q->where_cond, offset);


	/* Sub Queries */
	g_print ("%s" D_COL_H2 "Sub queries:\n" D_COL_NOR, str);
	list = q->sub_queries;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			Query *sq;
			sq = QUERY (list->data);
			static_query_dump_contents (sq, offset+5);
			list = g_slist_next (list);
		}
	}
	
	g_free (str);
}

#endif
