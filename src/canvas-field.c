/* canvas-field.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-field.h"

static void canvas_field_class_init (CanvasFieldClass * class);
static void canvas_field_init (CanvasField * item);
static void canvas_field_finalize (GObject   * object);

static void canvas_field_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);
static void canvas_field_set_property    (GObject              *object,
					  guint                 param_id,
					  const GValue         *value,
					  GParamSpec           *pspec);
static void canvas_field_get_property    (GObject              *object,
					  guint                 param_id,
					  GValue               *value,
					  GParamSpec           *pspec);

enum
{
	PROP_0,
	PROP_CANVAS_QUERY_VIEW,
	PROP_FIELD,
	PROP_OUT_WIDTH
};

static GObjectClass *parent_class = NULL;

guint
canvas_field_get_type (void)
{
static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (CanvasFieldClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) canvas_field_class_init,
			NULL,
			NULL,
			sizeof (CanvasField),
			0,
			(GInstanceInitFunc) canvas_field_init
		};		
		
		type = g_type_register_static (CANVAS_BASE_TYPE, "CanvasField", &info, 0);
	}
	
	return type;
}

static void
canvas_field_class_init (CanvasFieldClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	GnomeCanvasItemClass *item_class = (GnomeCanvasItemClass *) class;

	parent_class = g_type_class_peek_parent (class);
	object_class->finalize = canvas_field_finalize;
	item_class->bounds = canvas_field_bounds;

	/* Properties */
	object_class->set_property = canvas_field_set_property;
	object_class->get_property = canvas_field_get_property;

	g_object_class_install_property
                (object_class, PROP_CANVAS_QUERY_VIEW,
                 g_param_spec_pointer ("canvas_query_view", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property
                (object_class, PROP_FIELD,
                 g_param_spec_pointer ("field", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property
                (object_class, PROP_OUT_WIDTH,
                 g_param_spec_double ("outline_witdh", NULL, NULL, 
				      0.0, G_MAXDOUBLE, 0.0, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}


static void enter_notify_cb (CanvasField * cf, gpointer data);
static void leave_notify_cb (CanvasField * cf, gpointer data);
static void
canvas_field_init (CanvasField * cf)
{
	cf->cqv = NULL;
	cf->bg = NULL;
	cf->text = NULL;
	cf->outline_width = 0.;
	cf->outline_height = 0.;

	/* connect to the "enter_notify" & "leave_notify" signals" */
	g_signal_connect (G_OBJECT (cf), "enter_notify",
			  G_CALLBACK (enter_notify_cb), NULL);
	g_signal_connect (G_OBJECT (cf), "leave_notify",
			  G_CALLBACK (leave_notify_cb), NULL);
}

static void 
enter_notify_cb (CanvasField * cf, gpointer data)
{
	canvas_field_set_highlight (cf, TRUE);
}

static void 
leave_notify_cb (CanvasField * cf, gpointer data)
{
	canvas_field_set_highlight (cf, FALSE);
}

static void
canvas_field_finalize (GObject   * object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_FIELD (object));

	canvas_field_init (CANVAS_FIELD (object));
	
	/* for the parent class */
	parent_class->finalize (object);
}

static void post_init (CanvasField * cf);
static void 
canvas_field_set_property    (GObject              *object,
			      guint                 param_id,
			      const GValue         *value,
			      GParamSpec           *pspec)
{
	CanvasField *cf;
	gpointer ptr;

	cf = CANVAS_FIELD (object);

	switch (param_id) {
	case PROP_CANVAS_QUERY_VIEW:
		ptr = g_value_get_pointer (value);
		if (ptr)
			cf->cqv = CANVAS_QUERY_VIEW (ptr);
		break;
		if (cf->cqv && cf->field)
			post_init (cf);
	case PROP_FIELD:
		ptr = g_value_get_pointer (value);
		if (ptr) { 
			if (IS_QUERY_FIELD (ptr) || IS_DB_FIELD (ptr)) {
				cf->field = ptr;
				if (IS_DB_FIELD (ptr))
					gnome_canvas_item_set (GNOME_CANVAS_ITEM (cf),
							       "tooltip_object", ptr,
							       NULL);
			}
			else
				g_warning ("PROP_FIELD is of unknown type!\n");
		}
		if (cf->cqv && cf->field)
			post_init (cf);
		break;
	case PROP_OUT_WIDTH:
		cf->outline_width = g_value_get_double (value);
		if (cf->bg) 
			gnome_canvas_item_set (cf->bg, "x2", cf->outline_width, NULL);
		break;
	}

}

static void 
canvas_field_get_property    (GObject              *object,
			      guint                 param_id,
			      GValue               *value,
			      GParamSpec           *pspec)
{
	g_print ("Get Property\n");
}


static int field_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasField *cf);
static void 
post_init (CanvasField * cf)
{
	GnomeCanvasItem *item, *text;
	gchar *str, *font;
	double x1, y1, x2, y2;

	/* Building new field */
	if (IS_DB_FIELD (cf->field))
		str = DB_FIELD (cf->field)->name;
	else
		str = QUERY_FIELD (cf->field)->name;
	
	gnome_canvas_item_set (GNOME_CANVAS_ITEM (cf), 
			       "allow_move", FALSE,
			       "allow_drag", TRUE,
			       NULL);

	/* text: field's name */
	if (IS_DB_FIELD (cf->field) && DB_FIELD (cf->field)->is_key)
		font = CQV_DEFAULT_VIEW_FONT_BOLD;
	else
		font = CQV_DEFAULT_VIEW_FONT;

	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", cf->cqv->x_text_space,
				     "y", 0.,
				     "font", font,
				     "text", str,
				     "fill_color", "black",
				     "justification", GTK_JUSTIFY_RIGHT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST, 
				     NULL);

	cf->text = text;
	/* UI metrics */
	gnome_canvas_item_get_bounds (text, &x1, &y1, &x2, &y2);

	cf->cqv->fields_text_height = y2 - y1;
	if (x2 - x1 + cf->cqv->x_text_space > cf->cqv->max_text_width)
		cf->cqv->max_text_width = x2 - x1 + cf->cqv->x_text_space;

	/* underline for non null values */
	if (IS_DB_FIELD (cf->field) && !(DB_FIELD (cf->field)->null_allowed)) {
		GnomeCanvasPoints *points;
		points = gnome_canvas_points_new (2);

		/* fill out the points */
		points->coords[0] = cf->cqv->x_text_space;
		points->coords[1] = y2 - y1;
		points->coords[2] = cf->cqv->x_text_space + x2 - x1;
		points->coords[3] = points->coords[1];
		gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				      gnome_canvas_line_get_type(),
				      "points", points,
				      "fill_color", "black",
				      "width_units", 1.,
				      NULL);
		gnome_canvas_points_free(points);
	}

	/* background */
	cf->outline_width = cf->cqv->x_text_space + x2 - x1;
	cf->outline_height = y2 - y1;
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				     GNOME_TYPE_CANVAS_RECT,
				     "x1", cf->cqv->x_text_space,
				     "y1", 0.,
				     "x2", cf->outline_width,
				     "y2", cf->outline_height,
				     "fill_color", "white",
				     NULL);

	cf->bg = item;
	gnome_canvas_item_lower_to_bottom (item);	


	/* Signals */
	g_signal_connect(G_OBJECT (cf), "event",
			 G_CALLBACK (field_item_event), cf);
}

static void 
canvas_field_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2)
{
	CanvasField *cf;

	cf = CANVAS_FIELD (item);

	*x1 = *y1 = 0.;
	*x2 = cf->outline_width - cf->cqv->x_text_space;
	*y2 = cf->outline_height;
}
static int 
field_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasField *cf)
{
	gboolean done = TRUE;

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		canvas_field_set_highlight (cf, TRUE); 
		break;
	case GDK_BUTTON_PRESS:
		if (((GdkEventButton *) event)->button != 1) {
			done = TRUE;
			break;
		}
	case GDK_LEAVE_NOTIFY:
		canvas_field_set_highlight (cf, FALSE); 
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

void 
canvas_field_set_highlight (CanvasField *cf, gboolean highlight)
{

	g_return_if_fail (IS_CANVAS_FIELD (cf));

	if (highlight) {
		gchar *str;
		DbTable *table;

		if (IS_DB_FIELD (cf->field)) {
			table = database_find_table_from_field (cf->cqv->query->conf->db, DB_FIELD (cf->field));
			g_assert (table);
			if (table->is_view)
				str = CQV_VIEW_COLOR;
			else
				str = CQV_TABLE_COLOR;
		}
		else 
			str = CQV_QUERY_COLOR;

		gnome_canvas_item_set (cf->bg, "fill_color", str, NULL);
	}
	else 
		gnome_canvas_item_set (cf->bg, "fill_color", "white", NULL);
}
