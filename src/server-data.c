/* sqldata.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "server-access.h"
#include "default-display.h"
#include <libgda/libgda.h>

/*
 *
 * ServerDataType object
 *
 */


static void server_data_type_class_init (ServerDataTypeClass * class);
static void server_data_type_init (ServerDataType * dt);
static void server_data_type_finalize (GObject   * object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *sdt_parent_class = NULL;


GType
server_data_type_get_type (void) 
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ServerDataTypeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) server_data_type_class_init,
			NULL,
			NULL,
			sizeof (ServerDataType),
			0,
			(GInstanceInitFunc) server_data_type_init
		};

		type = g_type_register_static (G_TYPE_OBJECT, "ServerDataType", &info, 0);
	}
	return type;
}

static void
server_data_type_class_init (ServerDataTypeClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	sdt_parent_class = g_type_class_peek_parent (class);

	object_class->finalize = server_data_type_finalize;
}

static void
server_data_type_init (ServerDataType * dt)
{
	dt->descr = NULL;
	dt->sqlname = NULL;
	dt->numparams = 0;
	dt->server_type = 0;
	dt->gda_type = 0;
	dt->display_fns = NULL;
	dt->updated = FALSE;
}


GObject   *
server_data_type_new (void)
{
	GObject   *obj;

	obj = g_object_new (SERVER_DATA_TYPE_TYPE, NULL);
	return obj;
}

static void
server_data_type_finalize (GObject   * object)
{
	ServerDataType *dt;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_DATA_TYPE (object));

	dt = SERVER_DATA_TYPE (object);
	if (dt->descr)
		g_free (dt->descr);
	if (dt->sqlname)
		g_free (dt->sqlname);


	/* parent class */
	sdt_parent_class->finalize (object);
}

void
server_data_type_set_sqlname (ServerDataType * dt, gchar * name)
{
	if (dt->sqlname)
		g_free (dt->sqlname);
	dt->sqlname = g_strdup (name);
}

void
server_data_type_set_descr (ServerDataType * dt, gchar * name)
{
	if (dt->descr)
		g_free (dt->descr);
	dt->descr = g_strdup (name);
}

void
server_data_type_update_list (ServerAccess * srv)
{
	GSList *dtl = srv->data_types;
	ServerDataType *dt;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;

	/* here we get the complete list of types, and for each type, update or
	   create the entry in the list if not yet there. */
	rs = gda_connection_get_schema (GDA_CONNECTION (srv->cnc),
					GDA_CONNECTION_SCHEMA_TYPES, NULL);

	if (rs) {
		total = gda_data_model_get_n_rows (rs);
		now = 0;		
		while (now < total) {
			const GdaValue *value;

			value = gda_data_model_get_value_at (rs, now, 0);
			str = gda_value_stringify (value);
			dt = server_data_type_get_from_name (dtl, str);
			if (!dt) {
				gchar *str2;

				dt = SERVER_DATA_TYPE (server_data_type_new ());
				server_data_type_set_sqlname (dt, str);
				/* FIXME: number of params */
				dt->numparams = 0;

				/* description */
				value = gda_data_model_get_value_at (rs, now, 2);
				str2 = gda_value_stringify (value);
				server_data_type_set_descr (dt, str2);
				g_free (str2);
				srv->data_types = g_slist_append (srv->data_types, dt);
			}
			g_free (str);

			dt->updated = TRUE;
			
			/* server_type */
			value = gda_data_model_get_value_at (rs, now, 4);
			str = gda_value_stringify (value);
			dt->server_type = atoi (str);
			g_free (str);

			/* gda_type */
			value = gda_data_model_get_value_at (rs, now, 3);
			str = gda_value_stringify (value);
			dt->gda_type = atoi (str);
			g_free (str);

			/* functions to display the data type */
			dt->display_fns = server_access_get_display_fns_from_gda (srv, dt);

			g_signal_emit_by_name (G_OBJECT (srv), "progress",
					       _("Updating the list of data types..."),
					       now, total);
			now++;
		}
		g_object_unref (G_OBJECT (rs));

		/* remove the data types not existing anymore */
		dtl = srv->data_types;
		while (dtl) {
			if (SERVER_DATA_TYPE (dtl->data)->updated) {
				SERVER_DATA_TYPE (dtl->data)->updated = FALSE;
				dtl = g_slist_next (dtl);
			}
			else {
				GSList *hlist, *hold;
				dt = SERVER_DATA_TYPE (dtl->data);
				g_object_unref (G_OBJECT (dt));

				hold = dtl;
				hlist = g_slist_next (dtl);
				srv->data_types = g_slist_remove_link (srv->data_types, dtl);
				g_slist_free_1 (hold);
				dtl = hlist;
			}
		}

#ifdef debug_signal
		g_print (">> 'DATA_TYPES_UPDATED' from  server_data_type_update_list\n");
#endif
		g_signal_emit_by_name (G_OBJECT (srv), "data_types_updated");
#ifdef debug_signal
		g_print ("<< 'DATA_TYPES_UPDATED' from  server_data_type_update_list\n");
#endif
	}
	else
		g_error (_
			 ("This database provider does not support data types retreival which is "
			  "needed."));
	g_signal_emit_by_name (G_OBJECT (srv), "progress", NULL, 0, 0);
}


#ifdef debug
void
server_data_type_show_types (GSList * dtl)
{
	GSList *list = dtl;
	guint maxlen = 0;

	g_print (D_COL_H1 "\n==== SQL Server known types ====\n" D_COL_NOR);
	while (list) {
		if (strlen (SERVER_DATA_TYPE (list->data)->sqlname) > maxlen)
			maxlen = strlen (SERVER_DATA_TYPE (list->data)->sqlname);
		list = g_slist_next (list);
	}
	list = dtl;
	while (list) {
		g_print ("%-*s (sql:%5d gda:%2d) - %d param - %s\n",
			 maxlen,
			 SERVER_DATA_TYPE (list->data)->sqlname,
			 SERVER_DATA_TYPE (list->data)->server_type,
			 SERVER_DATA_TYPE (list->data)->gda_type,
			 SERVER_DATA_TYPE (list->data)->numparams,
			 SERVER_DATA_TYPE (list->data)->descr);
		list = g_slist_next (list);
	}
}
#endif


/* 
 * data types lookup
 */

ServerDataType *
server_data_type_get_from_server_type (GSList * dtl, gint server_type)
{
	ServerDataType *found = NULL;
	GSList *list = dtl;

	while (list && !found) {
		if (SERVER_DATA_TYPE (list->data)->server_type == server_type)
			found = SERVER_DATA_TYPE (list->data);
		list = g_slist_next (list);
	}

	return found;
}

ServerDataType *
server_data_type_get_from_name (GSList * dtl, const gchar * name)
{
	ServerDataType *found = NULL;
	GSList *list = dtl;

	while (list && !found) {
		if (!g_strcasecmp (SERVER_DATA_TYPE (list->data)->sqlname, name))
			found = SERVER_DATA_TYPE (list->data);
		list = g_slist_next (list);
	}

	return found;
}

/*
 * Builds a list with strings pointing to the types names.
 * the built list must be freed by the caller, and not its contents 
 */
GList *
server_data_type_get_name_list (GSList * dtl)
{
	GList *list = NULL;
	GSList *data;

	data = dtl;
	while (data) {
		if (g_slist_next (data) != NULL)	/* to avoid the UNKNOWN type */
			list = g_list_append (list,
					      SERVER_DATA_TYPE (data->data)->
					      sqlname);
		data = g_slist_next (data);
	}
	return list;
}

gchar *
server_data_type_get_xml_id (ServerDataType * dt)
{
	gchar *str;
	g_return_val_if_fail (dt && IS_SERVER_DATA_TYPE (dt), NULL);
	
	str = g_strdup_printf ("DT%s", dt->sqlname);
	return str;
}

ServerDataType *
server_data_type_get_from_xml_id (ServerAccess * srv, const gchar * id)
{
	ServerDataType *type = NULL;

	g_return_val_if_fail ((srv != NULL), NULL);
	type = server_data_type_get_from_name (srv->data_types, id + 2);
	return type;
}





/*
 *
 * ServerFunction object
 *
 */

static void server_function_class_init (ServerFunctionClass * class);
static void server_function_init (ServerFunction * dt);
static void server_function_finalize (GObject   * object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *sf_parent_class = NULL;


GType
server_function_get_type (void) 
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ServerFunctionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) server_function_class_init,
			NULL,
			NULL,
			sizeof (ServerFunction),
			0,
			(GInstanceInitFunc) server_function_init
		};

		type = g_type_register_static (G_TYPE_OBJECT, "ServerFunction", &info, 0);
	}
	return type;
}

static void
server_function_class_init (ServerFunctionClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	sf_parent_class = g_type_class_peek_parent (class);
	object_class->finalize = server_function_finalize;
}

static void
server_function_init (ServerFunction * dt)
{
	dt->descr = NULL;
	dt->sqlname = NULL;
	dt->result_type = NULL;
	dt->args = NULL;
	dt->updated = FALSE;
	dt->is_user = FALSE;
	dt->objectid = NULL;
}


GObject   *
server_function_new (void)
{
	GObject   *obj;

	obj = g_object_new (SERVER_FUNCTION_TYPE, NULL);
	return obj;
}


static void
server_function_finalize (GObject   * object)
{
	ServerFunction *df;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_FUNCTION (object));

	df = SERVER_FUNCTION (object);
	if (df->descr)
		g_free (df->descr);
	if (df->sqlname)
		g_free (df->sqlname);
	if (df->objectid)
		g_free (df->objectid);

	if (df->args)
		g_slist_free (df->args);

	/* parent class */
	sf_parent_class->finalize (object);
}

void
server_function_set_sqlname (ServerFunction * df, gchar * name)
{
	if (df->sqlname)
		g_free (df->sqlname);
	if (name)
		df->sqlname = g_strdup (name);
	else
		df->sqlname = NULL;
}

void
server_function_set_descr (ServerFunction * df, gchar * name)
{
	if (df->descr)
		g_free (df->descr);
	if (name)
		df->descr = g_strdup (name);
	else
		df->descr = NULL;
}

void
server_function_update_list (ServerAccess * srv)
{
	GSList *list, *hlist, *hold;
	gint i;
	gboolean insert, found;
	ServerDataType *dt;
	ServerFunction *df = NULL;
	gchar *str;
	GdaDataModel *rs, *rs2;
	guint now, total;

	rs = gda_connection_get_schema (GDA_CONNECTION (srv->cnc),
					GDA_CONNECTION_SCHEMA_PROCEDURES, NULL); /* FIXME: we need the extra info */
	
	if (rs) {
		total = gda_data_model_get_n_rows (rs);
		now = 0;
		while (now < total) {
			ServerDataType *rettype = NULL;	/* return type for the function */
			GSList *dtl = NULL;	/* list of params for the function */
			const GdaValue *value;

			/* do we have to insert that function in the list ? */
			value = gda_data_model_get_value_at (rs, now, 1);
			str = gda_value_stringify (value);
			df = server_function_get_from_objid (srv->data_functions, str);

			/* build the parameters list for the function */
			/* FIXME: we need a GdaConnectionSchema to get the parameters of each procedure
			   we just got */
			rs2 = gda_connection_get_schema (GDA_CONNECTION (srv->cnc),
							 GDA_CONNECTION_SCHEMA_PROCEDURES, NULL); 
			g_free (str);
			insert = TRUE;
			if (rs2) {
				guint now2, total2;

				total2 = gda_data_model_get_n_rows (rs2);
				now2 = 0;
				while ((now2 < total2) && insert) {
					gboolean isin;
					/* at the same time, get the result type and the list of 
					   params type */
					
					value = gda_data_model_get_value_at (rs2, now2, 0);
					str = gda_value_stringify (value);
					if (!strcmp (str, "in"))
						isin = TRUE;
					else
						isin = FALSE;
					g_free (str);

					value = gda_data_model_get_value_at (rs2, now2, 1);
					str = gda_value_stringify (value);
					if (! (dt = server_data_type_get_from_name (srv->data_types, str)))
						insert = FALSE;
					g_free (str);
					if (dt) {
						if (!isin)
							rettype = dt;
						else
							dtl = g_slist_append (dtl, dt);
					}
					now2 ++;
				}
				g_object_unref (G_OBJECT (rs2));
			}
			else
				insert = FALSE;


			/* does the function we found have the same rettype and params 
			   as the one we have now? */
			if (insert && df) {
				gboolean isequal = TRUE;
				list = df->args;
				hlist = dtl;
				while (list && hlist && isequal) {
					if (list->data != hlist->data)
						isequal = FALSE;
					list = g_slist_next (list);
					hlist = g_slist_next (hlist);
				}
				if (isequal && (df->result_type != rettype))
					isequal = FALSE;
				insert = !isequal;	/* insert only if different */
				if (isequal)
					df->updated = TRUE;
			}


			/* actual creation step */
			if (insert) {
				/* creating new ServerFunction object */
				df = SERVER_FUNCTION (server_function_new ());
				df->result_type = rettype;
				df->args = dtl;
				value = gda_data_model_get_value_at (rs, now, 3);
				if (* gda_value_get_string(value)) {
					str = gda_value_stringify (value);
					server_function_set_descr (df, str);
					g_free (str);
				}
				else
					server_function_set_descr (df, NULL);

				value =  gda_data_model_get_value_at (rs, now, 0);
				str = gda_value_stringify (value);
				server_function_set_sqlname (df, str);
				g_free (str);

				value = gda_data_model_get_value_at (rs, now, 1);
				str = gda_value_stringify (value);
				df->objectid = str;

				value = gda_data_model_get_value_at (rs, now, 2);
				str = gda_value_stringify (value);
				if (strcmp (str, srv->user_name->str))
					df->is_user = FALSE;
				else
					df->is_user = TRUE;
				g_free (str);


				/* insertion in the list */
				/* finding where to insert the function */
				list = srv->data_functions;
				i = 0;
				found = FALSE;
				while (list && !found) {
					if (strcmp (df->sqlname, SERVER_FUNCTION (list->data)->sqlname) < 0)
						found = TRUE;
					else
						i++;
					list = g_slist_next (list);
				}
				srv->data_functions = g_slist_insert (srv->data_functions, df, i);
#ifdef debug_signal
				g_print (">> 'DATA_FUNCTION_ADDED' from "
					 "server_function_update_list\n");
#endif
				g_signal_emit_by_name (G_OBJECT (srv), "data_function_added", df);
#ifdef debug_signal
				g_print ("<< 'DATA_FUNCTION_ADDED' from "
					 "server_function_update_list\n");
#endif
				df->updated = TRUE;
			}
			else 	/* we do not insert the function, so free dtl */
				g_slist_free (dtl);

			g_signal_emit_by_name (G_OBJECT (srv), "progress",
						 _("Updating functions list..."),
						 now, total);
			now ++;
		}
		g_object_unref (G_OBJECT (rs));
	}


	/* cleanup for the functions which do not exist anymore */
	list = srv->data_functions;
	while (list) {
		if (SERVER_FUNCTION (list->data)->updated) {
			SERVER_FUNCTION (list->data)->updated = FALSE;
			list = g_slist_next (list);
		}
		else {
			hlist = g_slist_next (list);
			df = SERVER_FUNCTION (list->data);
			hold = list;
			srv->data_functions = g_slist_remove_link (srv->data_functions, list);
			g_slist_free_1 (hold);
#ifdef debug_signal
			g_print (">> 'DATA_FUNCTION_REMOVED' from "
				 "server_function_update_list\n");
#endif
			g_signal_emit_by_name (G_OBJECT (srv),
						 "data_function_removed", df);
#ifdef debug_signal
			g_print ("<< 'DATA_FUNCTION_REMOVED' from "
				 "server_function_update_list\n");
#endif
			g_object_unref (G_OBJECT (df));
			list = hlist;
		}
	}

	/* tell the world that we updated the functions */
#ifdef debug_signal
	g_print (">> 'DATA_FUNCTION_UPDATED' from "
		 "server_function_update_list\n");
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_function_updated");
#ifdef debug_signal
	g_print ("<< 'DATA_FUNCTION_UPDATED' from "
		 "server_function_update_list\n");
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "progress", NULL, 0, 0);
}

#ifdef debug
void
server_function_show_functions (GSList * dfl)
{
	GSList *list = dfl, *list2;
	guint maxlen = 0;
	gboolean first;

	g_print (D_COL_H1 "\n==== SQL Server known functions ====\n" D_COL_NOR);
	while (list) {
		if (strlen (SERVER_FUNCTION (list->data)->sqlname) > maxlen)
			maxlen = strlen (SERVER_FUNCTION (list->data)->
					 sqlname);
		list = g_slist_next (list);
	}
	list = dfl;
	while (list) {
		g_print ("%-*s id=%s (",
			 maxlen,
			 SERVER_FUNCTION (list->data)->sqlname,
			 SERVER_FUNCTION (list->data)->objectid);
		list2 = SERVER_FUNCTION (list->data)->args;
		first = TRUE;
		while (list2) {
			if (first) {
				first = FALSE;
				g_print ("%s",
					 SERVER_DATA_TYPE (list2->data)->
					 sqlname);
			}
			else
				g_print (" %s",
					 SERVER_DATA_TYPE (list2->data)->
					 sqlname);
			list2 = g_slist_next (list2);
		}
		g_print (") - ret %s - DESCR:%s",
			 SERVER_FUNCTION (list->data)->result_type->sqlname,
			 SERVER_FUNCTION (list->data)->descr);
		if (SERVER_FUNCTION (list->data)->is_user)
			g_print (" Owner: User\n");
		else
			g_print (" Owner: System\n");
		list = g_slist_next (list);
	}
}
#endif


/* 
 * functions lookup
 */

GSList *
server_function_get_list_from_name (GSList * dfl, gchar * name)
{
	GSList *newlist = NULL;	/* ServerFunction list */
	GSList *list = dfl;

	while (list) {
		if (!g_strcasecmp
		    (SERVER_FUNCTION (list->data)->sqlname, name))
			newlist = g_slist_append (newlist, list->data);
		list = g_slist_next (list);
	}

	return newlist;
}

ServerFunction *
server_function_get_from_name (GSList * dfl, gchar * name,
				 GSList * argtypes)
{
	ServerFunction *ret = NULL;
	GSList *list, *args;
	gint i, nb, nbmax;
	gboolean still_ok;

	list = dfl;
	nbmax = g_slist_length (argtypes);
	while (list && !ret) {
		if (!strcmp (SERVER_FUNCTION (list->data)->sqlname, name)) {
			args = SERVER_FUNCTION (list->data)->args;
			nb = g_slist_length (args);
			if (nb == nbmax)
				still_ok = TRUE;
			else
				still_ok = FALSE;
			i = 0;
			while ((i < nb) && still_ok) {
				if (g_slist_nth (args, i)->data !=
				    g_slist_nth (argtypes, i)->data)
					still_ok = FALSE;
				i++;
			}
			if (still_ok)
				ret = SERVER_FUNCTION (list->data);
		}
		list = g_slist_next (list);
	}

	return ret;
}

ServerFunction *
server_function_get_from_objid (GSList * dfl, gchar * id)
{
	GSList *list = dfl;
	ServerFunction *df = NULL;

	while (list && !df) {
		if (!g_strcasecmp
		    (SERVER_FUNCTION (list->data)->objectid, id))
			df = list->data;
		list = g_slist_next (list);
	}

	return df;
}

gchar *
server_function_get_xml_id (ServerFunction * df)
{
	g_return_val_if_fail (df && IS_SERVER_FUNCTION (df), NULL);

	return g_strdup_printf ("PR%s", df->objectid);
}

ServerFunction *
server_function_get_from_xml_id (ServerAccess * srv, const gchar * id)
{
	ServerFunction *func = NULL;

	g_return_val_if_fail (srv && IS_SERVER_ACCESS (srv), NULL);
	func = server_function_get_from_objid (srv->data_functions, id + 2);
	return func;
}

gpointer
server_function_binding_func (GObject   * obj)
{
	g_return_val_if_fail ((obj != NULL), NULL);
	if (IS_SERVER_FUNCTION (obj))
		return (gpointer) SERVER_FUNCTION (obj)->result_type;
	else
		return NULL;
}




/*
 *
 * ServerAggregate object
 *
 */


static void server_aggregate_class_init (ServerAggregateClass * class);
static void server_aggregate_init (ServerAggregate * doo);
static void server_aggregate_finalize (GObject   * object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *sa_parent_class = NULL;

GType  
server_aggregate_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ServerAggregateClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) server_aggregate_class_init,
			NULL,
			NULL,
			sizeof (ServerAggregate),
			0,
			(GInstanceInitFunc) server_aggregate_init
		};

		type = g_type_register_static (G_TYPE_OBJECT, "ServerAggregate", &info, 0);
	}
	return type;
}

static void
server_aggregate_class_init (ServerAggregateClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	sa_parent_class = g_type_class_peek_parent (class);
	object_class->finalize = server_aggregate_finalize;
}

static void
server_aggregate_init (ServerAggregate * da)
{
	da->descr = NULL;
	da->sqlname = NULL;
	da->arg_type = NULL;
	da->updated = FALSE;
	da->objectid = NULL;
}


GObject   *
server_aggregate_new (void)
{
	GObject   *obj;

	obj = g_object_new (SERVER_AGGREGATE_TYPE, NULL);
	return obj;
}

static void
server_aggregate_finalize (GObject   * object)
{
	ServerAggregate *df;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_AGGREGATE (object));

	df = SERVER_AGGREGATE (object);
	if (df->descr)
		g_free (df->descr);
	if (df->sqlname)
		g_free (df->sqlname);

	/* parent class */
	sa_parent_class->finalize (object);
}

void
server_aggregate_set_sqlname (ServerAggregate * da, gchar * name)
{
	if (da->sqlname)
		g_free (da->sqlname);
	da->sqlname = g_strdup (name);
}

void
server_aggregate_set_descr (ServerAggregate * da, gchar * name)
{
	if (da->descr)
		g_free (da->descr);
	da->descr = g_strdup (name);
}


void
server_aggregate_update_list (ServerAccess * srv)
{
	GSList *list, *hlist, *hold;
	gint j;
	ServerDataType *dt;
	ServerAggregate *da;
	gboolean found;
	GdaDataModel *rs;
	const GdaValue *value;
	gchar *str;
	guint now, total;

	rs = gda_connection_get_schema (GDA_CONNECTION (srv->cnc),
					GDA_CONNECTION_SCHEMA_AGGREGATES, NULL); /* FIXME: wee need the extra info */
	if (rs) {
		total = gda_data_model_get_n_rows (rs);
		now = 0;
		while (now < total) {
			value = gda_data_model_get_value_at (rs, now, 2);	/* in type */
			str = gda_value_stringify (value);
			dt = server_data_type_get_from_name (srv->data_types, str);
			g_free (str);

			value = gda_data_model_get_value_at (rs, now, 1);	/* agg oid */
			str = gda_value_stringify (value);
			da = server_aggregate_get_from_objid (srv->data_aggregates, str);
			g_free (str);

			if ((da && (da->arg_type != dt)) || (!da)) {
				/* create a new DataAggregate */
				da = SERVER_AGGREGATE (server_aggregate_new ());
				da->arg_type = dt;

				value = gda_data_model_get_value_at (rs, now, 0);
				str = gda_value_stringify (value);
				da->sqlname = str;

				value = gda_data_model_get_value_at (rs, now, 1);
				str = gda_value_stringify (value);
				da->objectid = str;

				value = gda_data_model_get_value_at (rs, now, 4);
				if (* gda_value_get_string (value)) {
					str = gda_value_stringify (value);
					da->descr = str;
				}

				/* insertion into the list */
				j = 0;
				found = FALSE;
				list = srv->data_aggregates;
				while (list && !found) {
					if (strcmp (da->sqlname, SERVER_AGGREGATE (list->data)->sqlname) > 0)
						j++;
					else
						found = TRUE;
					list = g_slist_next (list);
				}
				srv->data_aggregates = g_slist_insert (srv->data_aggregates, da, j);
#ifdef debug_signal
				g_print (">> 'DATA_AGGREGATE_ADDED' from "
					 "server_aggregate_update_list\n");
#endif
				g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_added", da);
#ifdef debug_signal
				g_print ("<< 'DATA_AGGREGATE_ADDED' from "
					 "server_aggregate_update_list\n");
#endif
				da->updated = TRUE;
			}
			else {
				if (da && (da->arg_type == dt))	/* mark as updated */
					da->updated = TRUE;
			}
			g_signal_emit_by_name (G_OBJECT (srv), "progress",
						 _("Updating aggregates list..."),
						 now, total);
			now++;
		}
		g_object_unref (G_OBJECT (rs));
	}


	/* cleanup for the aggregates which do not exist anymore */
	list = srv->data_aggregates;
	while (list) {
		if (SERVER_AGGREGATE (list->data)->updated) {
			SERVER_AGGREGATE (list->data)->updated = FALSE;
			list = g_slist_next (list);
		}
		else {
			da = SERVER_AGGREGATE (list->data);
			hlist = g_slist_next (list);
			hold = list;
			srv->data_aggregates = g_slist_remove_link (srv->data_aggregates, list);
			g_slist_free_1 (hold);
#ifdef debug_signal
			g_print (">> 'DATA_AGGREGATE_REMOVED' from "
				 "server_aggregate_update_list\n");
#endif
			g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_removed", da);
#ifdef debug_signal
			g_print ("<< 'DATA_AGGREGATE_REMOVED' from "
				 "server_aggregate_update_list\n");
#endif
			g_object_unref (G_OBJECT (da));
			list = hlist;
		}
	}

#ifdef debug_signal
	g_print (">> 'DATA_AGGREGATE_UPDATED' from "
		 "server_aggregate_update_list\n");
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_updated");
#ifdef debug_signal
	g_print ("<< 'DATA_AGGREGATE_UPDATED' from "
		 "server_aggregate_update_list\n");
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "progress", NULL, 0, 0);
}

#ifdef debug
void
server_aggregate_show_aggregates (GSList * dal)
{
	GSList *list;

	g_print (D_COL_H1 "\n==== SQL Server known Aggregates ====\n" D_COL_NOR);

	list = dal;
	while (list) {
		if (SERVER_AGGREGATE (list->data)->arg_type)
			g_print ("%10s(%s) - %s\n",
				 SERVER_AGGREGATE (list->data)->sqlname,
				 SERVER_AGGREGATE (list->data)->arg_type->
				 sqlname,
				 SERVER_AGGREGATE (list->data)->descr);
		else
			g_print ("%10s(ANY) - %s\n",
				 SERVER_AGGREGATE (list->data)->sqlname,
				 SERVER_AGGREGATE (list->data)->descr);
		list = g_slist_next (list);
	}
}
#endif

/*
 * aggregates lookup
 */

ServerAggregate *
server_aggregate_get_from_name (GSList * dal, gchar * name,
				  ServerDataType * arg_type)
{
	ServerAggregate *found = NULL;
	GSList *list = dal;

	while (list && !found) {
		if (!g_strcasecmp
		    (SERVER_AGGREGATE (list->data)->sqlname, name)) {
			if (!arg_type
			    && !SERVER_AGGREGATE (list->data)->arg_type)
				found = SERVER_AGGREGATE (list->data);
			else if (SERVER_AGGREGATE (list->data)->arg_type ==
				 arg_type)
				found = SERVER_AGGREGATE (list->data);
		}
		list = g_slist_next (list);
	}

	return found;
}

ServerAggregate *
server_aggregate_get_from_objid (GSList * dal, gchar * oid)
{
	ServerAggregate *found = NULL;
	GSList *list = dal;

	while (list && !found) {
		if (!g_strcasecmp
		    (SERVER_AGGREGATE (list->data)->objectid, oid))
			found = SERVER_AGGREGATE (list->data);
		list = g_slist_next (list);
	}
	
	return found;
}

gchar *
server_aggregate_get_xml_id (ServerAggregate * da)
{
	g_return_val_if_fail (da && IS_SERVER_AGGREGATE (da), NULL);
	return g_strdup_printf ("AG%s", da->objectid);
}

ServerAggregate *
server_aggregate_get_from_xml_id (ServerAccess * srv, const gchar * id)
{
	ServerAggregate *agg = NULL;

	g_return_val_if_fail ((srv != NULL), NULL);
	agg = server_aggregate_get_from_objid (srv->data_aggregates, id + 2);
	return agg;
}
