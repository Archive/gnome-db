/* relship.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "relship.h"
#include "canvas-query-view.h"

/*
 * 
 * RelShip object
 * 
 */



static void relship_class_init (RelShipClass * class);
static void relship_init       (RelShip * rs);
static void relship_dispose    (GObject   * object);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;


guint
relship_get_type (void) 
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (RelShipClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) relship_class_init,
			NULL,
			NULL,
			sizeof (RelShip),
			0,
			(GInstanceInitFunc) relship_init
		};		

		type = g_type_register_static (G_TYPE_OBJECT, "RelShip", &info, 0);
	}
	return type;
}

static void
relship_class_init (RelShipClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = relship_dispose;
}


static void
relship_init (RelShip * rs)
{
	rs->query = NULL;
	rs->views = NULL;
	rs->items = NULL;
	rs->is_query_destroyed = FALSE;
}



static void query_destroy_cb (RelShip *rs, Query *q);

GObject   *
relship_find (Query *q) 
{
	GObject   *obj;
	RelShip *rs;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	if ((obj = g_object_get_data (G_OBJECT (q), "RelShip"))) 
		return obj;

	obj = g_object_new (RELSHIP_TYPE, NULL);
	rs = RELSHIP (obj);

	rs->query = q;

	g_object_weak_ref (G_OBJECT (q), (GWeakNotify) query_destroy_cb, rs);

	g_object_set_data (G_OBJECT (q), "RelShip", rs);
	
	return obj;
}

void relship_copy (Query *q, Query *original, GHashTable *hash)
{
	GObject   *obj;
	RelShip *rs = NULL;

	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));
	g_return_if_fail (hash);

	if ((obj = g_object_get_data (G_OBJECT (q), "RelShip"))) {
		g_object_unref (obj);
		g_object_set_data (G_OBJECT (q), "RelShip", NULL);
	}

	if ((obj = g_object_get_data (G_OBJECT (original), "RelShip"))) {
		GSList *list;

		RelShip *rso = RELSHIP (obj);
		rs = RELSHIP (relship_find (q));
		
		/* Copy of the list of items */
		list = rso->items;
		while (list) {
			RelShipItemData *rsi = RELSHIP_ITEM_DATA_CAST (list->data);

			obj = g_hash_table_lookup (hash, rsi->obj);
			if (obj) {
				RelShipItemData *nrsi;
				
				nrsi = relship_find_item (rs, obj);
				nrsi->x = rsi->x;
				nrsi->y = rsi->y;
			}
			
			list = g_slist_next(list);
		}
	}
}


static void item_moved_cb (RelShipView *rsv, CanvasBase *item, RelShip *rs);
static void view_destroy_cb (GObject   *obj, RelShip *rs);
static void object_item_destroy_cb (RelShip *rs, GObject   *obj); /* GWeakNotify */

static void
relship_dispose (GObject   * object)
{
	RelShip *rs;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_RELSHIP (object));

	rs = RELSHIP (object);
	list = rs->items;
	while (list) {
		g_free (list->data);
		g_object_weak_unref (((RelShipItemData*)(list->data))->obj, (GWeakNotify) object_item_destroy_cb, rs);
		list = g_slist_next (list);
	}
	if (rs->items) {
		g_slist_free (rs->items);
		rs->items = NULL;
	}

	list = rs->views;
	while (list) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), 
						      G_CALLBACK (item_moved_cb), rs);
		g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), 
						      G_CALLBACK (view_destroy_cb), rs);
		
		list = g_slist_next (list);
	}
	if (rs->views) {
		g_slist_free (rs->views);
		rs->views = NULL;
	}


	if (rs->query && !rs->is_query_destroyed)
		g_object_weak_unref (G_OBJECT (rs->query), (GWeakNotify) query_destroy_cb, rs);
	rs->query = NULL;
	rs->is_query_destroyed = FALSE;

	/* for the parent class */
	parent_class->dispose (object);
}



static void 
query_destroy_cb (RelShip *rs, Query *q)
{
	rs->is_query_destroyed = TRUE;
	g_object_unref (G_OBJECT (rs));
}



/*
 * Getting a new RelShipView
 */
static void adj_changed_cb (GtkAdjustment *adj, RelShip *rs);
GtkWidget *
relship_get_sw_view (RelShip *rs)
{
	GtkWidget *sw, *rels;
	GtkAdjustment *adj;

	g_return_val_if_fail (rs, NULL);
	g_return_val_if_fail (IS_RELSHIP (rs), NULL);

	sw = gtk_scrolled_window_new(NULL, NULL);
	rels = relship_view_new (rs->query);
	gnome_canvas_set_scroll_region(GNOME_CANVAS (rels), -750, -750, 750, 750);
	
	gtk_widget_set_usize(rels, 500, 500);
	gtk_container_add(GTK_CONTAINER(sw), rels);
	gtk_widget_show (rels);

	adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));
	g_signal_connect (G_OBJECT (adj), "changed",
			  G_CALLBACK (adj_changed_cb), rs);
	adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));
	g_signal_connect (G_OBJECT (adj), "changed",
			  G_CALLBACK (adj_changed_cb), rs);

	g_signal_connect (G_OBJECT (rels), "item_moved",
			  G_CALLBACK (item_moved_cb), rs);

	g_signal_connect (G_OBJECT (rels), "destroy",
			  G_CALLBACK (view_destroy_cb), rs);

	rs->views = g_slist_append (rs->views, rels);

	return sw;
}

static void 
adj_changed_cb (GtkAdjustment *adj, RelShip *rs)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (adj), G_CALLBACK (adj_changed_cb), rs);
	gtk_adjustment_set_value (adj, (adj->upper - adj->lower) / 2.);
}

static void 
item_moved_cb (RelShipView *rsv, CanvasBase *item, RelShip *rs)
{
	if (IS_CANVAS_QUERY_VIEW (item)) {
		QueryView *qv;
		RelShipItemData *id;
		gdouble x, y;
		GSList *list;

		/* FIXME: implement this with the "query_view" property */
		qv = CANVAS_QUERY_VIEW (item)->view;
		
		id = relship_find_item (rs, G_OBJECT (qv));
		gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (item), &(id->x), &(id->y), &x, &y);

		/* Forward the movement */
		list = rs->views;
		while (list) {

			if (list->data != rsv) {
				g_signal_handlers_block_by_func (G_OBJECT (list->data),
								 G_CALLBACK (item_moved_cb), rs);
				relship_view_refresh_items (RELSHIP_VIEW (list->data));
				g_signal_handlers_unblock_by_func (G_OBJECT (list->data),
								   G_CALLBACK (item_moved_cb), rs);
			}
			list = g_slist_next (list);
		}
	}
}

static void 
view_destroy_cb (GObject   *obj, RelShip *rs)
{
	rs->views = g_slist_remove (rs->views, obj);
}

/*
 * Finding an item 
 */
RelShipItemData *
relship_find_item (RelShip *rs, GObject   *obj)
{
	RelShipItemData *id = NULL;
	GSList *list;

	list = rs->items;
	while (list && !id) {
		if (RELSHIP_ITEM_DATA_CAST (list->data)->obj == obj)
			id = RELSHIP_ITEM_DATA_CAST (list->data);
		list = g_slist_next (list);
	}

	if (!id) {
		id = g_new0 (RelShipItemData, 1);
		id->obj = obj;
		id->x = 50.;
		id->y = 50.;
		rs->items = g_slist_append (rs->items, id);
		
		g_object_weak_ref (obj, (GWeakNotify) object_item_destroy_cb, rs);
	}

	return id;
}

void
relship_item_set_position   (RelShip *rs, GObject *obj, gdouble x, gdouble y)
{
	RelShipItemData *id;
	GSList *list;

	g_return_if_fail (rs && IS_RELSHIP (rs));
	g_return_if_fail (obj && G_IS_OBJECT (obj));
	
	id = relship_find_item (rs, obj);
	id->x = x;
	id->y = y;

	/* Forward the movement */
	list = rs->views;
	while (list) {
		g_signal_handlers_block_by_func (G_OBJECT (list->data),
						 G_CALLBACK (item_moved_cb), rs);
		relship_view_refresh_items (RELSHIP_VIEW (list->data));
		g_signal_handlers_unblock_by_func (G_OBJECT (list->data),
						   G_CALLBACK (item_moved_cb), rs);
		list = g_slist_next (list);
	}	
}

static void 
object_item_destroy_cb (RelShip *rs, GObject   *obj)
{
	RelShipItemData *id;
	id = relship_find_item (rs, obj);
	if (id) {
		rs->items = g_slist_remove (rs->items, id);
		g_free (id);
	}
}


/*
 * XML saving and loading 
 */
void 
relship_build_xml_tree (Query *start_query, xmlNodePtr toptree, ConfManager * conf)
{
	RelShip *rs;
	GSList *list;
	Query *query;

	if (!start_query)
		query = QUERY (conf->top_query);
	else
		query = start_query;

	rs = g_object_get_data (G_OBJECT (query), "RelShip");
	if (rs && IS_RELSHIP (rs)) {
		xmlNodePtr tree;	
		gchar *str;

		tree = xmlNewChild (toptree, NULL, "RelShip", NULL);
		str = query_get_xml_id (query);
		xmlSetProp (tree, "queryid", str);
		g_free (str);

		list = rs->items;
		while (list) {
			RelShipItemData *id;

			id = RELSHIP_ITEM_DATA_CAST (list->data);

			str = NULL;
			if (IS_QUERY_VIEW (id->obj))
				str = query_view_get_xml_id (QUERY_VIEW (id->obj));

			if (str) {
				xmlNodePtr tree2;
				tree2 = xmlNewChild (tree, NULL, "RelShipItem", NULL);
				xmlSetProp (tree2, "object", str);
				g_free(str);

				str = g_strdup_printf ("%d.%d", (gint) id->x, (gint) (id->x - (gint) id->x) * 100);
				xmlSetProp (tree2, "xpos", str);
				g_free(str);

				str = g_strdup_printf ("%d.%d", (gint) id->y, (gint) (id->y - (gint) id->y) * 100);
				xmlSetProp (tree2, "ypos", str);
				g_free(str);
			}
			list = g_slist_next (list);
		}
	}

	
	list = query->sub_queries;
	while (list) {
		relship_build_xml_tree (QUERY (list->data), toptree, conf);
		list = g_slist_next (list);
	}
}

RelShip *
relship_build_from_xml_tree (ConfManager * conf, xmlNodePtr node)
{
	RelShip *rs = NULL;
	gchar *str;
	
	str = xmlGetProp (node, "queryid");
	if (str) {
		Query *q;

		q = query_find_from_xml_name (conf, NULL, str);
		if (q) {
			xmlNodePtr tree;
			rs = RELSHIP (relship_find (q));

			tree = node->xmlChildrenNode;
			while (tree) {
				if (!strcmp (tree->name, "RelShipItem")) {
					gchar *str2;

					str2 = xmlGetProp (tree, "object");
					if (str2) {
						gpointer ptr;
						ptr = query_view_find_from_xml_name (conf, NULL, str2);
						g_free (str2);
						if (ptr && G_IS_OBJECT (ptr)) {
							RelShipItemData *id;
							gchar *str3;

							id = relship_find_item (rs, G_OBJECT (ptr));

							str3 = xmlGetProp (tree, "xpos");
							if (str3) {
								id->x = atof (str3);
								g_free (str3);
							}
							str3 = xmlGetProp (tree, "ypos");
							if (str3) {
								id->y = atof (str3);
								g_free (str3);
							}
						}
					}
				}
				tree = tree->next;
			}
			
		}
		g_free (str);
	}

	return rs;
}
