/* query-create-druid.c
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "query-create-druid.h"
#include "libgnomedb/libgnomedb.h"
#include <config.h>
#include "packedclist.h"
#include "marshal.h"

static void query_create_druid_class_init (QueryCreateDruidClass *klass);
static void query_create_druid_dispose (GObject   *object);
static void query_create_druid_init (QueryCreateDruid *druid);
static void query_create_druid_post_init (QueryCreateDruid *druid);


enum
{
        FINISH,
	LAST_SIGNAL
};

static gint druid_signals[LAST_SIGNAL] = {0};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/*
 * QueryCreateDruid interface
 */
GType  
query_create_druid_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryCreateDruidClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_create_druid_class_init,
			NULL,
			NULL,
			sizeof (QueryCreateDruid),
			0,
			(GInstanceInitFunc) query_create_druid_init
		};

		type = g_type_register_static (GNOME_TYPE_DRUID, "QueryCreateDruid", &info, 0);
	}

	return type;
}


static void
query_create_druid_class_init (QueryCreateDruidClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	druid_signals[FINISH] =
                g_signal_new ("finish",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryCreateDruidClass, finish),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);

	class->finish = NULL;

	object_class->dispose = query_create_druid_dispose;
}


static void fields_page_table_dropped_cb (Database *db, DbTable *table, QueryCreateDruid *druid);
static void fields_page_field_dropped_cb (Database *db, DbTable *table, DbField *field, QueryCreateDruid *druid);
static void
query_create_druid_dispose (GObject   *object)
{
	QueryCreateDruid *druid;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_CREATE_DRUID (object));

	druid = QUERY_CREATE_DRUID (object);

	/* disconnect */
	g_signal_handlers_disconnect_by_func (G_OBJECT (druid->conf->db),
					      G_CALLBACK (fields_page_table_dropped_cb), druid);
	g_signal_handlers_disconnect_by_func (G_OBJECT (druid->conf->db),
					      G_CALLBACK (fields_page_field_dropped_cb), druid);

	/* free memory */
	if (druid->q) {
		g_object_unref (G_OBJECT (druid->q));
		druid->q = NULL;
	}
	
	/* tooltips */
	if (druid->tips) {
		g_object_unref (G_OBJECT (druid->tips));
		druid->tips = NULL;
	}

	/* parent class */
	parent_class->dispose (object);
}

static void cancel_druid_cb (GnomeDruid *gnome_druid, QueryCreateDruid *qdruid);
static void druid_finished_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid);

static void info_page_prepare_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid);
static gboolean info_page_next_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid);
static void info_page_name_changed_cb (GnomeDruidPage *druid_page, QueryCreateDruid *druid);

static void fields_page_table_selected_cb (GtkWidget *wid, DbTable *table, QueryCreateDruid *druid);
static void fields_page_field_selected_cb (GtkWidget *wid, DbTable *table, DbField *field,
					   QueryCreateDruid *druid);
static void fields_page_sels_select_cb (GtkCList *clist, gint row,
					gint column, GdkEventButton *event, 
					QueryCreateDruid *druid);
static void fields_page_sels_unselect_cb (GtkCList *clist, gint row,
					  gint column, GdkEventButton *event, 
					  QueryCreateDruid *druid);

static void fields_page_add_field_cb (GtkWidget *button, QueryCreateDruid *druid);
static void fields_page_del_field_cb (GtkWidget *button, QueryCreateDruid *druid);

static void finish_page_prepare_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid);
static gboolean finish_page_back_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid);

static void
query_create_druid_init (QueryCreateDruid *druid)
{
	druid->conf = NULL;
	druid->q = NULL;
	druid->tips = gtk_tooltips_new();
	gtk_tooltips_enable (druid->tips);
}

static void 
query_create_druid_post_init (QueryCreateDruid *druid)
{
	GdkPixbuf *logo = NULL;
	gchar *pathname, *str, *tmpstr;
	GtkWidget *table;
	GtkWidget *evb, *label;
	GtkWidget *menu, *menu_item;
	GtkWidget *wid, *cdlist, *bb, *button, *arrow;
	gchar *titles1[] = {N_("Fields in query")};

	pathname = g_strdup (PIXMAPDIR "/egnima.png");
	logo = gdk_pixbuf_new_from_file (pathname, NULL);
	g_free(pathname);

	/* create the start page */
	druid->start_page = GNOME_DRUID_PAGE_EDGE (gnome_druid_page_edge_new (GNOME_EDGE_START));
	if (logo)
		gnome_druid_page_edge_set_logo(druid->start_page, logo);
	gnome_druid_page_edge_set_title(druid->start_page,_("Create a new query..."));
	gnome_druid_page_edge_set_text(druid->start_page,
					_("This wizard will guide you through the process of\n"
					  "creating a new query.\n\n"
					  "Just follow the steps!"));
	gtk_widget_show_all(GTK_WIDGET(druid->start_page));




	/* 
	 * create the general query info page 
	 */
	if (logo)
		druid->info_page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new_with_vals 
							      (_("General Query Information"), logo, NULL));
	else
		druid->info_page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new ());
	table = gtk_table_new (4, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);

	gtk_box_pack_start (GTK_BOX(druid->info_page->vbox), table, FALSE, TRUE, 0);


	evb = gtk_event_box_new ();
	gtk_table_attach_defaults (GTK_TABLE(table), evb, 0, 1, 0, 1);
	label = gtk_label_new (_("Name:"));
	gtk_container_add (GTK_CONTAINER (evb), label);
	gtk_tooltips_set_tip (druid->tips, evb,
			      _("Name of the query, must be set"),
			      _("This is the name of the query, "
				"it must be unique for all the queries."));
	druid->name_entry = gtk_entry_new ();
	gtk_table_attach_defaults(GTK_TABLE (table), druid->name_entry, 1, 2, 0, 1);


	evb = gtk_event_box_new ();
	gtk_table_attach_defaults (GTK_TABLE(table), evb, 0, 1, 1, 2);
	label = gtk_label_new (_("Description:"));
	gtk_container_add (GTK_CONTAINER (evb), label);
	gtk_tooltips_set_tip (druid->tips, evb,
			      _("A short description, may be left empty"),
			      _("A description to remember what the query does, etc"));
	druid->descr_entry = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), druid->descr_entry, 1, 2, 1, 2);


	evb = gtk_event_box_new ();
	gtk_table_attach_defaults (GTK_TABLE(table), evb, 0, 1, 2, 3);
	label = gtk_label_new (_("Type of query:"));
	gtk_container_add (GTK_CONTAINER (evb), label);
	gtk_tooltips_set_tip (druid->tips, evb,
			      _("The following allows you to make composed queries;\n"
				"leaving the default is the safe choice."),
			      _("For advanced users only..."));
	druid->query_type = gtk_option_menu_new ();
	gtk_table_attach_defaults(GTK_TABLE(table), druid->query_type, 1, 2, 2, 3);

	menu = gtk_menu_new ();
	menu_item = gtk_menu_item_new_with_label (_("non composed query"));
	gtk_menu_append (GTK_MENU (menu), menu_item);
	gtk_widget_show (menu_item);
	g_object_set_data (G_OBJECT (menu_item), "qt", GINT_TO_POINTER (QUERY_TYPE_STD));
	menu_item = gtk_menu_item_new_with_label (_("Union query"));
	gtk_menu_append (GTK_MENU (menu), menu_item);
	g_object_set_data (G_OBJECT (menu_item), "qt", GINT_TO_POINTER (QUERY_TYPE_UNION));
	menu_item = gtk_menu_item_new_with_label (_("Intersect query"));
	gtk_menu_append (GTK_MENU (menu), menu_item);
	g_object_set_data (G_OBJECT (menu_item), "qt", GINT_TO_POINTER (QUERY_TYPE_INTERSECT));
	gtk_option_menu_set_menu (GTK_OPTION_MENU (druid->query_type), menu);
	
	evb = gtk_event_box_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), evb, 0, 1, 3, 4);

	label = gtk_label_new (_("Type of wizard:"));
	gtk_container_add (GTK_CONTAINER (evb), label);

	str = g_strdup (_("Don't use the wizard"));
	tmpstr = g_strdup_printf (_("If you want to stop using the wizard at this stage,\n"
				    "select the '%s' choice and click on 'Next'"), str);
	gtk_tooltips_set_tip (druid->tips, evb, tmpstr,
			      _("This is just in case you don't want to use this wizard..."));
			      
	druid->wizard_type = gtk_option_menu_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), druid->wizard_type, 1, 2, 3, 4);

	menu = gtk_menu_new ();
	menu_item = gtk_menu_item_new_with_label (_("Continue with wizard"));
	gtk_menu_append (GTK_MENU (menu), menu_item);
	gtk_widget_show (menu_item);
	g_object_set_data (G_OBJECT (menu_item), "bool", GINT_TO_POINTER (1));
	menu_item = gtk_menu_item_new_with_label (str);
	g_free (str);
	gtk_menu_append (GTK_MENU (menu), menu_item);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (druid->wizard_type), menu);
	g_object_set_data (G_OBJECT (menu_item), "bool", GINT_TO_POINTER (0));

	gtk_widget_show_all (GTK_WIDGET(druid->info_page));
	
	
	
	
	/* 
	 * create the Query fields selection page 
	 */
	if (logo)
		druid->fields_page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new_with_vals
								(_("Query's fields selection"), logo, NULL));
	else
		druid->fields_page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new ());
	
	table = gtk_table_new (2, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX(druid->fields_page->vbox), table, TRUE, TRUE, 0);
	
	label = gtk_label_new (_("Select the fields you want to have in the query"));
	gtk_misc_set_alignment (GTK_MISC(label), 0.5, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 3, 0, 1);
	
	/* PORTING FIXME: use another widget */
	wid = gtk_label_new ("FIXME");
	/* wid = sql_wid_db_tree_new (druid->conf); */
/* 	sql_wid_db_tree_set_mode (SQL_WID_DB_TREE (wid), SQL_WID_DB_TREE_TABLES | */
/* 				  SQL_WID_DB_TREE_TABLE_FIELDS | SQL_WID_DB_TREE_TABLES_SEL | */
/* 				  SQL_WID_DB_TREE_TABLE_FIELDS_SEL); */
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 1, 2, 
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND,
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND, 0, 0);
	druid->fields_to_sel = wid;
	
	bb = gtk_vbutton_box_new ();
	gtk_button_box_set_child_size (GTK_BUTTON_BOX (bb), 15, 15);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_table_attach (GTK_TABLE (table), bb, 1, 2, 1, 2, 0, 
			  GTK_EXPAND | GTK_SHRINK | GTK_FILL, GNOME_PAD/2., GNOME_PAD/2.);

	arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_box_pack_start (GTK_BOX (bb), button, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (button, FALSE);
	druid->add_field_button = button;

	arrow = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_OUT);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_box_pack_start (GTK_BOX (bb), button, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (button, FALSE);
	druid->del_field_button = button;	

	cdlist = packed_clist_new_with_titles (1, titles1, TRUE);
	packed_clist_set_show_arrows (PACKED_CLIST (cdlist), TRUE);
	gtk_table_attach (GTK_TABLE (table), cdlist, 2, 3, 1, 2, 
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND,
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND, 0, 0);	
	druid->fields_selected = cdlist;

	gtk_widget_show_all (GTK_WIDGET (druid->fields_page));
	
	

	
	/* 
	 * create the finish page 
	 */
	druid->finish_page = GNOME_DRUID_PAGE_EDGE (gnome_druid_page_edge_new (GNOME_EDGE_FINISH));
	if (logo)
		gnome_druid_page_edge_set_logo (druid->finish_page, logo);
	gnome_druid_page_edge_set_title (druid->finish_page,
					   _("All information retrieved"));
	gnome_druid_page_edge_set_text(druid->finish_page,
					 _("All information needed to create a new query \n"
					   "has been retrieved. Now, press 'Finish' to confirm \n"
					   "the creation of the query."));
	gtk_widget_show_all (GTK_WIDGET (druid->finish_page));
	

	
	/* append all pages to the druid */
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(druid->start_page));
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(druid->info_page));
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(druid->fields_page));
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(druid->finish_page));
	gnome_druid_set_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(druid->start_page));
	
	/* 
	 * connect to signals 
	 */
	/* the whole druid */
	g_signal_connect (G_OBJECT (druid), "cancel",
			    G_CALLBACK (cancel_druid_cb), druid);
	g_signal_connect (G_OBJECT (druid->finish_page), "finish",
			    G_CALLBACK (druid_finished_cb), druid);

	
	/* the first page */
	g_signal_connect (G_OBJECT (druid->info_page), "prepare",
			    G_CALLBACK (info_page_prepare_cb), druid);
	g_signal_connect (G_OBJECT (druid->info_page), "next",
			    G_CALLBACK (info_page_next_cb), druid);
	g_signal_connect (G_OBJECT (druid->name_entry), "changed",
			    G_CALLBACK (info_page_name_changed_cb), druid);

	/* Query fields selection page */
	g_signal_connect (G_OBJECT (druid->fields_to_sel), "table_selected",
			    G_CALLBACK (fields_page_table_selected_cb), druid);
	g_signal_connect (G_OBJECT (druid->fields_to_sel), "field_selected",
			    G_CALLBACK (fields_page_field_selected_cb), druid);
	g_signal_connect (G_OBJECT (PACKED_CLIST (druid->fields_selected)->clist), 
			    "select_row",
			    G_CALLBACK (fields_page_sels_select_cb), druid);
	g_signal_connect (G_OBJECT (PACKED_CLIST (druid->fields_selected)->clist), 
			    "unselect_row",
			    G_CALLBACK (fields_page_sels_unselect_cb), druid);
	g_signal_connect (G_OBJECT (druid->add_field_button), "clicked",
			    G_CALLBACK (fields_page_add_field_cb), druid);
	g_signal_connect (G_OBJECT (druid->del_field_button), "clicked",
			    G_CALLBACK (fields_page_del_field_cb), druid);

	g_signal_connect (G_OBJECT (druid->conf->db), "table_dropped",
			  G_CALLBACK (fields_page_table_dropped_cb), druid);
	g_signal_connect (G_OBJECT (druid->conf->db), "field_dropped",
			  G_CALLBACK (fields_page_field_dropped_cb), druid);

	/* the last page */
	g_signal_connect (G_OBJECT (druid->finish_page), "prepare",
			    G_CALLBACK (finish_page_prepare_cb), druid);
	g_signal_connect (G_OBJECT (druid->finish_page), "back",
			    G_CALLBACK (finish_page_back_cb), druid);
}



/*
 * query_create_druid_new
 */
GtkWidget *
query_create_druid_new (ConfManager * conf)
{
	QueryCreateDruid *druid;

	druid = QUERY_CREATE_DRUID (g_object_new (QUERY_CREATE_DRUID_TYPE, NULL));

	druid->conf = conf;

	/* UI stuff */
	query_create_druid_post_init (druid);

	return GTK_WIDGET(druid);
}


Query *
query_create_druid_get_query(QueryCreateDruid *druid)
{
	g_return_val_if_fail (druid, NULL);
	g_return_val_if_fail(IS_QUERY_CREATE_DRUID (druid), NULL);

	return druid->q;
}












/*
 * General Druid Callbacks
 */
static void
cancel_druid_cb (GnomeDruid *gnome_druid, QueryCreateDruid *qdruid)
{
	g_return_if_fail(IS_QUERY_CREATE_DRUID (qdruid));

	if (qdruid->q) 
		g_object_unref (G_OBJECT (qdruid->q));
}

static void
druid_finished_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid)
{
	GtkWidget *menu, *menu_item;
	QueryType qtype;

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (druid->query_type));
	menu_item = gtk_menu_get_active (GTK_MENU (menu));
	qtype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "qt"));

	druid->q = QUERY (query_new (gtk_entry_get_text (GTK_ENTRY (druid->name_entry)),
				     QUERY (druid->conf->top_query),
				     druid->conf));
	druid->q->type = qtype;
	if (*gtk_entry_get_text (GTK_ENTRY (druid->descr_entry))) 
		query_set_name (druid->q, NULL, gtk_entry_get_text (GTK_ENTRY (druid->descr_entry)));

	if (qtype == QUERY_TYPE_STD) {
		gint row;
		gpointer data;
		GtkCList *clist;
		GSList *tables_list = NULL, *list;

		/* Preparing the list of QueryViews */
		clist = GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist);
		for (row=0; row < clist->rows; row ++) {
			DbTable *table = NULL; /* table to add to the list of tables, if any */

			data = gtk_clist_get_row_data (clist, row);
			if (IS_DB_FIELD (data)) {
				table = database_find_table_from_field (druid->conf->db,
									DB_FIELD (data));
				g_assert (table);
			}
			else {
				if (IS_DB_TABLE (data)) 
					table = DB_TABLE (data);
			}

			if (table && !g_slist_find (tables_list, table)) 
				tables_list = g_slist_append (tables_list, table);
		}

		/* Adding the Query Views */
		list = tables_list;
		while (list) {
			DbTable *table;

			table = DB_TABLE (list->data);
			query_add_view_with_obj (druid->q, G_OBJECT (table));

			list = g_slist_next (list);
		}
		g_slist_free (tables_list);


		/* Adding the Query Fields */
		for (row=0; row < clist->rows; row ++) {
			DbTable *table = NULL; /* table to add to the list of tables, if any */

			data = gtk_clist_get_row_data (clist, row);
			if (IS_DB_FIELD (data)) {
				QueryField *qf;
				gchar *str, car;
				
				table = database_find_table_from_field (druid->conf->db,
									DB_FIELD (data));
				g_assert (table);
				str = g_strdup (DB_FIELD (data)->name);
				g_strup (str);
				car = *str;
				g_free (str);
				str = g_strdup (DB_FIELD (data)->name);
				*str = car;

				qf = QUERY_FIELD (query_field_new (druid->q, str, QUERY_FIELD_FIELD));
				query_field_set_alias (qf, str);
				g_free (str);
				query_field_field_set_field (qf, NULL, DB_FIELD (data));
				query_field_activate (qf);
				query_add_field (druid->q, qf);
			}
			else {
				if (IS_DB_TABLE (data)) {
					QueryField *qf;
					QueryView *qv;

					qv = query_add_view_with_obj (druid->q, G_OBJECT (data));

					qf = QUERY_FIELD (query_field_new (druid->q, DB_TABLE (data)->name, 
									   QUERY_FIELD_ALLFIELDS));
					query_field_allfields_set_table (qf, qv, DB_TABLE (data));
					query_field_activate (qf);
					query_add_field (druid->q, qf);
					table = DB_TABLE (data);
				}
			}
		}
		

		/* REM: the default query joins are created automatically
		   from the previous operation.
		*/
	}

#ifdef debug_signal
	g_print (">> 'FINISH' from druid_finished_cb\n");
#endif
	g_signal_emit (G_OBJECT (druid), druid_signals[FINISH], 0);
#ifdef debug_signal
	g_print ("<< 'FINISH' from druid_finished_cb\n");
#endif
}




/*
 * Callbacks for the info page 
 */
static void 
info_page_prepare_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid)
{
	const gchar *str;

	str = gtk_entry_get_text (GTK_ENTRY (druid->name_entry));
	if (*str)
		gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, TRUE, TRUE, FALSE);
	else
		gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, FALSE, TRUE, FALSE);
}

static void 
info_page_name_changed_cb (GnomeDruidPage *druid_page, QueryCreateDruid *druid)
{
	info_page_prepare_cb (druid_page, NULL, druid);	
}

static gboolean
info_page_next_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid)
{
	GtkWidget *menu, *menu_item;
	QueryType qtype;
	gboolean cont_wiz;

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (druid->query_type));
	menu_item = gtk_menu_get_active (GTK_MENU (menu));
	qtype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "qt"));

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (druid->wizard_type));
	menu_item = gtk_menu_get_active (GTK_MENU (menu));
	cont_wiz = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "bool"));
	if (!cont_wiz) {
		/* stop the wizard here and prepare the query */
		druid->q = QUERY (query_new (gtk_entry_get_text (GTK_ENTRY (druid->name_entry)),
					     QUERY (druid->conf->top_query), 
					     druid->conf));
		druid->q->type = qtype;
#ifdef debug_signal
		g_print (">> 'FINISH' from info_page_next_cb\n");
#endif
		g_signal_emit (G_OBJECT (druid), druid_signals[FINISH], 0);
#ifdef debug_signal
		g_print ("<< 'FINISH' from info_page_next_cb\n");
#endif		
		return TRUE;
	}

	/* continue with tests on the kind of query: composed or not */
	if (qtype == QUERY_TYPE_STD) {
		/* continue the wizard */
		return FALSE;
	}
	else {
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid->finish_page));
		return TRUE;
	}
}




/*
 * Callbacks for the Query fields selection page
 */
static void 
fields_page_table_selected_cb (GtkWidget *wid, DbTable *table, QueryCreateDruid *druid)
{
	g_object_set_data (G_OBJECT (druid), "table", table);
	g_object_set_data (G_OBJECT (druid), "field", NULL);
	gtk_widget_set_sensitive (druid->add_field_button, table ? TRUE : FALSE);

}

static void 
fields_page_field_selected_cb (GtkWidget *wid, DbTable *table, DbField *field,
			       QueryCreateDruid *druid)
{
	g_object_set_data (G_OBJECT (druid), "table", table);
	g_object_set_data (G_OBJECT (druid), "field", field);
	gtk_widget_set_sensitive (druid->add_field_button, field ? TRUE : FALSE);
}

static void 
fields_page_sels_select_cb (GtkCList *clist, gint row,
			    gint column, GdkEventButton *event, 
			    QueryCreateDruid *druid)
{
	gtk_widget_set_sensitive (druid->del_field_button, TRUE);
}

static void 
fields_page_sels_unselect_cb (GtkCList *clist, gint row,
			      gint column, GdkEventButton *event, 
			      QueryCreateDruid *druid)
{
	gtk_widget_set_sensitive (druid->del_field_button, FALSE);
}

static void 
fields_page_add_field_cb (GtkWidget *button, QueryCreateDruid *druid)
{
	DbTable *table = NULL;
	DbField *field = NULL;
	gchar *text[1];
	gpointer data;
	gint row;

	data = g_object_get_data (G_OBJECT (druid), "table");
	if (data)
		table = DB_TABLE (data);
	data = g_object_get_data (G_OBJECT (druid), "field");
	if (data)
		field = DB_FIELD (data);
	data = NULL;

	if (!table) 
		return;

	if (field) {
		/* table + field to add */
		text[0] = g_strdup_printf ("%s.%s", table->name, field->name);
		data = field;
	}
	else {
		/* only table to add */
		text[0] = g_strdup_printf ("%s.*", table->name);
		data = table;
	}

	row = gtk_clist_append (GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist),
				text);
	gtk_clist_set_row_data (GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist),
				row, data);
}

static void 
fields_page_table_dropped_cb (Database *db, DbTable *table, QueryCreateDruid *druid)
{
	gint row = 0;
	GtkCList *clist;
	gpointer data;
	gboolean row_removed;

	clist = GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist);
	while (row < clist->rows) {
		row_removed = FALSE;
		data = gtk_clist_get_row_data (clist, row);
		g_assert (data);
		if (IS_DB_TABLE (data)) {
			if (data == table) {
				gtk_clist_remove (clist, row);
				row_removed = TRUE;
			}
		}
		else {
			if (IS_DB_FIELD (data)) {
				DbTable *t;

				/* the table is not in the list of tables
				   anymore when the "table_dropped" signal is emitted,
				   so if we can't find the table, we consider the field is
				   to be removed */
 				t = database_find_table_from_field (druid->conf->db,
								    DB_FIELD (data));
				if (!t) {
					gtk_clist_remove (clist, row);
					row_removed = TRUE;
				}
			}
		}

		if (row_removed)
			row = 0;
		else
			row ++;
	}
}

static void 
fields_page_field_dropped_cb (Database *db, DbTable *table, DbField *field,
			      QueryCreateDruid *druid)
{
	gint row = 0;
	GtkCList *clist;
	gpointer data;
	gboolean row_removed;

	clist = GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist);
	while (row < clist->rows) {
		row_removed = FALSE;
		data = gtk_clist_get_row_data (clist, row);
		g_assert (data);
		if (IS_DB_FIELD (data)) {
			if (data == field) {
				gtk_clist_remove (clist, row);
				row_removed = TRUE;
			}
		}

		if (row_removed)
			row = 0;
		else
			row ++;
	}
}


static void 
fields_page_del_field_cb (GtkWidget *button, QueryCreateDruid *druid)
{
	gint row;

	row = gtk_clist_find_row_from_data (GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist),
					    PACKED_CLIST (druid->fields_selected)->actual_selection);

	if (row >= 0)
		gtk_clist_remove (GTK_CLIST (PACKED_CLIST (druid->fields_selected)->clist),
				  row);
}



/*
 * Callbacks for the finish page 
 */
static void 
finish_page_prepare_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid)
{
	gchar *str, *text;
	GtkWidget *menu, *menu_item;
	QueryType qtype;

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (druid->query_type));
	menu_item = gtk_menu_get_active (GTK_MENU (menu));
	qtype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "qt"));
	
	if (qtype == QUERY_TYPE_STD) {
		text = g_strdup (_("All information needed to create a new query \n"
				   "has been retrieved. Now, press 'Finish' to confirm \n"
				   "the creation of the query."));
	}
	else {
		switch (qtype) {
		case QUERY_TYPE_INTERSECT:
			str = _("INTERSECT");
			break;
		case QUERY_TYPE_UNION:
			str = _("UNION");
			break;
		default:
			str = "UNKNOWN";
			break;
		}
		
		text = g_strdup_printf (_("All information needed to create a new query \n"
					  "has been retrieved.\n\n"
					  "The query is a composed query (%s), and\n"
					  "new sub queries will have to be created before it"
					  "can be used."), str);
	}
	gnome_druid_page_edge_set_text(druid->finish_page, text);
	g_free (text);
}

static gboolean
finish_page_back_cb (GnomeDruidPage *druid_page, gpointer arg1, QueryCreateDruid *druid)
{
	GtkWidget *menu, *menu_item;
	QueryType qtype;

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (druid->query_type));
	menu_item = gtk_menu_get_active (GTK_MENU (menu));
	qtype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "qt"));

	if (qtype != QUERY_TYPE_STD) {
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid->info_page));
		return TRUE;
	}
	
	return FALSE;
}
