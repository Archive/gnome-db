/* choicecombo.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __CHOICE_COMBO__
#define __CHOICE_COMBO__

#include <gnome.h>
#include <config.h>

G_BEGIN_DECLS

#define CHOICE_COMBO_TYPE          (choice_combo_get_type())
#define CHOICE_COMBO(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, choice_combo_get_type(), ChoiceCombo)
#define CHOICE_COMBO_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, choice_combo_get_type (), ChoiceComboClass)
#define IS_CHOICE_COMBO(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, choice_combo_get_type ())


typedef struct _ChoiceCombo ChoiceCombo;
typedef struct _ChoiceComboClass ChoiceComboClass;


/* struct for the object's data */
struct _ChoiceCombo
{
	GtkCombo object;

	GSList  *main_list;
	gint     changed_signal_handler;
};

/* struct for the object's class */
struct _ChoiceComboClass
{
	GtkComboClass parent_class;
	void        (*selection_changed) (ChoiceCombo * combo, gpointer data);
};

/* widget's methods */
guint      choice_combo_get_type          (void);
GtkWidget *choice_combo_new               (void);

void       choice_combo_set_content       (ChoiceCombo * combo, GSList * list,
					   gint offset);
gpointer   choice_combo_get_selection     (ChoiceCombo * combo);
void       choice_combo_set_selection_txt (ChoiceCombo * combo, gchar * item);
void       choice_combo_set_selection_num (ChoiceCombo * combo, gint num);
void       choice_combo_set_selection_ptr (ChoiceCombo * combo, gpointer ptr);

G_END_DECLS

#endif
