/* interface_cb.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2002 Rodrigo Moya
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "interface_cb.h"
#include "server-rs.h"
#include "mainpagequery.h"
#include "relship.h"
#include <bonobo.h>
#include <libgnomedb/libgnomedb.h>

#include "passwd.xpm"

#define XML_EGNIMA_DTD_FILE DTDINSTALLDIR"/egnima.dtd"
/* DTD validation */
extern int xmlDoValidityCheckingDefaultValue;

/* Password dialog pixmap */
static GdkPixmap *passwd_icon = NULL;
static GdkBitmap *passwd_mask = NULL;


/*
 * Display a dialog to help the contributer
 */
static void
todo (ConfManager * conf, const gchar *file, int line)
{
	gchar *str;
	str = g_strdup_printf (_("This function needs to be implemented\n"
				 "see file %s line %d if you want to contribute."),
			       file, line);
	gnome_ok_dialog_parented (str, GTK_WINDOW (conf->app));
	g_free (str);
}


/********************************************************************
 *
 * User Interface actions CBs
 *
 ********************************************************************/



/* Cb to open the connexion to the SQL server */
void
sql_conn_open_cb (GtkWidget * widget, ConfManager * conf)
{
	if (*(conf->srv->gda_datasource->str)) {
		/* we check if we have a password or not; if not, ask for one */
		if (conf->user_name && (!conf->user_passwd || (*conf->user_passwd==0))) {
			GtkWidget *passdlg, *label, *entry, *pixmap, *table;
			gint button;
			gchar *str;

			passdlg = gtk_dialog_new_with_buttons (_("Password requested"), NULL, 0,
							       GTK_STOCK_OK, GTK_RESPONSE_OK,
							       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							       NULL);
			gtk_dialog_set_default_response (GTK_DIALOG (passdlg), GTK_RESPONSE_OK);

			table = gtk_table_new (2, 2, FALSE);
			gtk_box_pack_start (GTK_BOX (GTK_DIALOG (passdlg)->vbox), 
					    table, TRUE, TRUE, GNOME_PAD);
			gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
			gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);

			if (!passwd_icon)
				passwd_icon = gdk_pixmap_create_from_xpm_d (GTK_WIDGET (conf->app)->window, 
									    &passwd_mask,
									    NULL, passwd_xpm);
			pixmap = gtk_pixmap_new (passwd_icon, passwd_mask);
			gtk_table_attach (GTK_TABLE (table), pixmap, 0, 1, 0, 1,
					  GTK_SHRINK, GTK_SHRINK, GNOME_PAD, GNOME_PAD);
			
			str = g_strdup_printf(_("Enter password for user '%s' to open connection,\n"
						"or leave it empty if no password is required."),
					      conf->user_name);
			label = gtk_label_new (str);
			g_free (str);
			gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, 0, 1);

			label = gtk_label_new (_("Password:"));
			gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

			entry = gtk_entry_new ();
			gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
			gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 1, 2);

			gtk_widget_show_all (GTK_DIALOG (passdlg)->vbox);
			button = gtk_dialog_run (GTK_DIALOG (passdlg));
			switch (button) {
			case GTK_RESPONSE_DELETE_EVENT:  
			case GTK_RESPONSE_CANCEL:
			default: 
				return;
				break;
			case GTK_RESPONSE_OK: 
				/* fetch the password */
				if (conf->user_passwd)
					g_free (conf->user_passwd);
				conf->user_passwd = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
				g_string_assign (conf->srv->password, conf->user_passwd);
				break;
			}
			gtk_widget_destroy (passdlg);

		}
		server_access_open_connect (conf->srv);
	}
	else {
		conf->conn_open_requested = TRUE;
		options_config_cb (NULL, conf);
	}
}

/* Cb to close the connexion for the SQL server */
void
sql_conn_close_cb (GtkWidget * widget, ConfManager * conf)
{
	gint answer;
	GtkWidget *dialog, *label, *hb, *pixmap;

	/* ask if the user really wants to quit */
	if (!conf->save_up_to_date) {
		dialog = gtk_dialog_new_with_buttons (_("Disconnect confirmation"), GTK_WINDOW (conf->app),
						      0,
						      _("Save and Disconnect"), 1,
						      _("Disconnect without saving"), 2,
						      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);

		label = gtk_label_new (_("Some data have not yet been saved and should be saved before\n"
					 "closing the connection (or will be lost).\n\n"
					 "What do you want to do?"));

	}
	else {
		dialog = gtk_dialog_new_with_buttons (_("Disconnect confirmation"), GTK_WINDOW (conf->app), 
						      0,
						      _("Yes, disconnect"), 2,
						      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);

		label = gtk_label_new (_("Do you really want to disconnect?"));
	}

	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hb, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (hb);

	pixmap = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_BUTTON);
	gtk_box_pack_start (GTK_BOX (hb), pixmap, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (pixmap);

	gtk_box_pack_start (GTK_BOX (hb), label, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (label);
	answer = gtk_dialog_run (GTK_DIALOG (dialog));

	switch (answer) {
	case 1: /* Save and disconnect asked */
		file_save_cb (NULL, conf);
		if (!conf->working_file)
			conf->close_after_saving = TRUE;
	case 2: /* disconnect, no saving */
		conf->save_up_to_date = TRUE;
		/* here we delay the closing of the connection because the user need to choose
		   a filename to save data to */
		if (! conf->close_after_saving)
			server_access_close_connect (conf->srv);
		break;
	default:
		break;
	}
	gtk_widget_destroy (dialog);
}


/*
 * options_config_cb
 */

static gint open_conn_idle_func (ConfManager * conf);

void
options_config_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *props;

	props = gnome_db_login_dialog_new (_("Connection's configuration"));
	if (gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (props))) {
		conf->save_up_to_date = FALSE;
		
		/* fetching DSN and provider */
		g_string_assign (conf->srv->gda_datasource, 
				 gnome_db_login_dialog_get_dsn(GNOME_DB_LOGIN_DIALOG (props)));
		g_string_assign (conf->srv->user_name, 
				 gnome_db_login_dialog_get_username(GNOME_DB_LOGIN_DIALOG (props)));
		g_string_assign (conf->srv->password, 
				 gnome_db_login_dialog_get_password(GNOME_DB_LOGIN_DIALOG (props)));
		
		if (conf->user_name) 
			g_free (conf->user_name);
		conf->user_name = g_strdup (conf->srv->gda_datasource->str);
		if (conf->user_passwd) 
			g_free (conf->user_passwd);
		conf->user_passwd = g_strdup (conf->srv->password->str);
		
		/* if the connection was to be opened, we open it now */
		if (conf->conn_open_requested)
			gtk_idle_add ((GtkFunction) open_conn_idle_func, conf);
	}
	gtk_widget_destroy (props);
}


/* used as a gtk_idle_func! */
static gint
open_conn_idle_func (ConfManager * conf)
{
	sql_conn_open_cb (NULL, conf);
	return FALSE;
}




/*
 * sql_show_relations_cb
 */

static void show_relations_dialog_response_cb   (GtkDialog *dlg, gint button_number,
						 ConfManager *conf);
static void show_relations_dialog_destroy_cb    (GtkDialog *dlg, ConfManager *conf);
static void show_relations_dialog_conn_close_cb (ServerAccess *srv, ConfManager *conf);

void
sql_show_relations_cb (GObject * obj, ConfManager * conf)
{
	/* to hide the DLG */
	if (!conf) 
		return;

	if (!conf->relations_dialog) {
		GtkWidget *dlg, *rels;
		RelShip *rs;

		dlg = gtk_dialog_new_with_buttons (_("Database Relations"), NULL, 0,
						   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);

		gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
		g_signal_connect (G_OBJECT (dlg), "response",
				  G_CALLBACK (show_relations_dialog_response_cb), conf);
		g_signal_connect (G_OBJECT (dlg), "destroy",
				  G_CALLBACK (show_relations_dialog_destroy_cb), conf);
		g_signal_connect (G_OBJECT (conf->srv), "conn_to_close",
				  G_CALLBACK (show_relations_dialog_conn_close_cb), conf);

		/* dialog contents */
		rs = RELSHIP (relship_find (QUERY (conf->top_query)));
		rels = relship_get_sw_view (rs);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), rels, TRUE, TRUE, 0);

		gtk_widget_set_usize (dlg, 825, 570);
		gtk_widget_show_all (dlg);

		conf->relations_dialog = dlg;
	}
	else
		gdk_window_raise (conf->relations_dialog->window);
}


static void 
show_relations_dialog_destroy_cb (GtkDialog *dlg, ConfManager *conf)
{
	conf->relations_dialog = NULL;
	g_signal_handlers_disconnect_by_func (G_OBJECT (conf->srv), 
					      G_CALLBACK (show_relations_dialog_conn_close_cb), conf);
}

static void
show_relations_dialog_response_cb (GtkDialog *dlg, gint button_number, ConfManager *conf)
{
	gtk_widget_destroy (GTK_WIDGET (dlg));
}


static void 
show_relations_dialog_conn_close_cb (ServerAccess *srv, ConfManager *conf)
{
	gtk_widget_destroy (GTK_WIDGET (conf->relations_dialog));
}








/*
 * sql_data_view_cb
 */

void
sql_data_view_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	/* PORTING FIXME with Rodrigo's magic widget */
	/*dlg = sql_data_wid_new_dlg (conf->srv);
	  gtk_widget_show (dlg);*/
}


/*
 * sql_mem_update_cb
 */

static gboolean clean_empty_shell_queries (Query *q);

void
sql_mem_update_cb (GtkWidget * widget, ConfManager * conf)
{
	/* update of the data types known by the provider */
	server_access_refresh_datas (conf->srv);

	/* let's refresh the memory's representation */
	database_refresh (conf->db, conf->srv);

	/* clean the Queries which are just empty shells */
	clean_empty_shell_queries (QUERY (conf->top_query));
}

/* This function destroys any Query which is only an empty shell (no QueryFields and
   no sub queries), it returns TRUE if the query q was cleaned (removed) */
static gboolean
clean_empty_shell_queries (Query *q)
{
	GSList *list;

	g_assert (q); 

	/* clean the sub queries first */
	list = q->sub_queries;
	while (list) {
		if (clean_empty_shell_queries (QUERY (list->data)))
			list = q->sub_queries;
		else
			list = g_slist_next (list);
	}

	/* do we remove that query ? */
	if (!q->fields && !q->sub_queries && q->parent) {
		query_del_sub_query (q->parent, q);
		return TRUE;
	}
	else
		return FALSE;
}

/*
 * rescan_display_plugins_cb
 */

static void refresh_plugins_table_cb (GObject * obj,
				      ConfManager * conf);
static gboolean is_plugin_used (ConfManager * conf,
				DataDisplayFns * fns);
struct foreach_hash_struct
{
	gpointer find_value;
	gboolean found;
};
static void foreach_hash_cb (gpointer key, gpointer value,
			     struct foreach_hash_struct *user_data);


void
rescan_display_plugins_cb (GObject * obj, ConfManager * conf)
{
	server_access_rescan_display_plugins (conf->srv, conf->plugins_dir);
	if (conf->config_plugins_dlg)
		refresh_plugins_table_cb (NULL, conf);
}

static void
refresh_plugins_table_cb (GObject * obj, ConfManager * conf)
{
	GSList *list;
	gchar *text[5];
	DataDisplayFns *fns;
	GtkCList *clist;
	gint row;

	clist = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				   "clist");
	list = conf->srv->data_types_display;
	gtk_clist_freeze (clist);
	while (list) {
		fns = (DataDisplayFns *) (list->data);
		if (fns->plugin_name) {	/* making sure it is a plugin */
			row = gtk_clist_find_row_from_data (clist,
							    list->data);
			text[0] = fns->plugin_name;
			text[1] = fns->version;

			if (is_plugin_used (conf, fns))
				text[2] = _("Yes");
			else
				text[2] = _("No");
			text[3] = fns->descr;
			text[4] = fns->plugin_file;
			if (row >= 0) {
				gtk_clist_remove (clist, row);
				gtk_clist_insert (clist, row, text);
			}
			else
				row = gtk_clist_append (clist, text);
			gtk_clist_set_row_data (clist, row, fns);
		}
		list = g_slist_next (list);
	}

	/* removing any row not related to a plugin anymore */
	row = 0;
	while (row < clist->rows) {
		fns = gtk_clist_get_row_data (clist, row);
		if (!g_slist_find (conf->srv->data_types_display, fns))
			gtk_clist_remove (clist, row);
		else
			row++;
	}

	gtk_clist_thaw (clist);
}

static gboolean
is_plugin_used (ConfManager * conf, DataDisplayFns * fns)
{
	struct foreach_hash_struct *fhs;
	fhs = g_new (struct foreach_hash_struct, 1);
	fhs->find_value = fns;
	fhs->found = FALSE;

	g_hash_table_foreach (conf->srv->types_objects_hash,
			      (GHFunc) (foreach_hash_cb), fhs);

	return fhs->found;
}

static void
foreach_hash_cb (gpointer key, gpointer value,
		 struct foreach_hash_struct *user_data)
{
	if (value == user_data->find_value)
		user_data->found = TRUE;
}


/*
 * config_display_plugins_cb
 */

static void change_location_cb (GObject * obj, ConfManager * conf);
static void change_location_cancel_cb (GObject * obj,
				       ConfManager * conf);
static void change_location_ok_cb (GObject * obj, ConfManager * conf);
static void data_types_clist_select_cb (GtkWidget * widget, gint row,
					gint col, GdkEventButton * event,
					ConfManager * conf);
static void plugins_ch_combo_sel_changed_cb (GObject * obj,
					     gpointer * newsel,
					     ConfManager * conf);
static void data_types_clist_unselect_cb (GtkWidget * widget, gint row,
					  gint col, GdkEventButton * event,
					  ConfManager * conf);
static void plugin_list_update_description (GtkWidget * Bdescr,
					    GtkWidget * Ddescr,
					    DataDisplayFns * fns);
static void plug_menu_table_created_dropped_cb (GObject * obj,
						DbTable * new_table,
						ConfManager * conf);
static void table_field_clist_select_cb (GtkWidget * clist, gint row,
					 gint col, GdkEventButton * event,
					 ConfManager * conf);
/* build the contents of the tables clist contents */
static void plugins_ch_combo_sel_changed_cb2 (GObject * object,
					      gpointer * newsel,
					      ConfManager * conf);
static void table_field_clist_unselect_cb (GtkWidget * clist,
					   gint row, gint col,
					   GdkEventButton * event,
					   ConfManager * conf);
static void refresh_table_fields_menu_cb (Database * db, DbTable * table,
					  DbField * field,
					  ConfManager * conf);
static void config_plugins_dlg_destroy_cb (GtkWidget * wid,
					   ConfManager * conf);
static void config_plugins_dlg_response_cb (GtkDialog *dialog,  
					    gint arg1, ConfManager *conf);

/* refreshes the clist with the data types */
static void data_types_updated_cb (GObject * obj, ConfManager * conf);
static void build_tables_contents_menu_cb (GObject * mitem,
					   ConfManager * conf);

void
config_display_plugins_cb (GObject * obj, ConfManager * conf)
{
	GtkWidget *props, *label, *page, *wid, *sw, *box, *table, *box2, *clist, *nb;
	GtkWidget *frame, *tmpwid, *menu;
	GSList *list;
	
	if (conf->config_plugins_dlg) {
		gdk_window_raise (conf->config_plugins_dlg->window);
		return;
	}

	props = gtk_dialog_new_with_buttons (_("Plugins preferences"), NULL, 0,
					     GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	conf->config_plugins_dlg = props;
	nb = gtk_notebook_new ();
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (props)->vbox), nb);

	/* 
	 * known plugins and their usage: 1st page
	 */
	label = gtk_label_new (_("Plugins list"));
	page = gtk_hbox_new (FALSE, GNOME_PAD);
	g_object_set_data (G_OBJECT (props), "pluglist", page);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);

	/* on the left is a clist in its sw and the location of plugins */
	box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);

	box2 = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), box2, FALSE, TRUE, GNOME_PAD);
	wid = gtk_label_new (_("Plugins location:"));
	gtk_box_pack_start (GTK_BOX (box2), wid, FALSE, TRUE, GNOME_PAD);
	wid = gtk_label_new (conf->plugins_dir);
	g_object_set_data (G_OBJECT (props), "loclabel", wid);
	gtk_box_pack_start (GTK_BOX (box2), wid, FALSE, TRUE, GNOME_PAD);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, GNOME_PAD);
	clist = gtk_clist_new (5);
	g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "clist",
			   clist);
	gtk_clist_set_column_title (GTK_CLIST (clist), 0, _("Plugin"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 1, _("Version"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 2, _("Used"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 3, _("Description"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 4, _("File"));
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 2, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 3, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 4, TRUE);
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_show (GTK_CLIST (clist));
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_container_add (GTK_CONTAINER (sw), clist);

	/* on the right are some buttons and the dir. location of the plugins */
	box = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (box),
				   GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start (GTK_BOX (page), box, FALSE, FALSE, GNOME_PAD);
	wid = gtk_button_new_with_label (_("Change location"));
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (change_location_cb), conf);
	gtk_container_add (GTK_CONTAINER (box), wid);
	wid = gtk_button_new_with_label (_("Rescan plugins"));
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (rescan_display_plugins_cb),
			  conf);
	gtk_container_add (GTK_CONTAINER (box), wid);
	wid = gtk_button_new_with_label (_("Refresh"));
	gtk_container_add (GTK_CONTAINER (box), wid);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (refresh_plugins_table_cb), conf);


	gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	refresh_plugins_table_cb (NULL, conf);


	if (server_access_is_open (conf->srv)) {
		/* 
		 * Data types bindings: 2nd page
		 */
		label = gtk_label_new (_("Data Type bindings"));
		page = gtk_hbox_new (FALSE, GNOME_PAD);
		g_object_set_data (G_OBJECT (props), "dtbinding", page);
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);
	       		
		/* on the left side is a clist in a sw */
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (page), sw, TRUE, TRUE, GNOME_PAD);
		wid = gtk_clist_new (1);
		gtk_clist_set_column_title (GTK_CLIST (wid), 0, _("Data types"));
		gtk_clist_set_selection_mode (GTK_CLIST (wid), GTK_SELECTION_SINGLE);
		gtk_clist_column_titles_show (GTK_CLIST (wid));
		gtk_clist_column_titles_passive (GTK_CLIST (wid));
		gtk_container_add (GTK_CONTAINER (sw), wid);
		list = conf->srv->data_types;
		while (list) {
			gint row;
			row = gtk_clist_append (GTK_CLIST (wid),
						&(SERVER_DATA_TYPE (list->data)->sqlname));
			gtk_clist_set_row_data (GTK_CLIST (wid), row, list->data);
			list = g_slist_next (list);
		}
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "tclist", wid);
		g_signal_connect (G_OBJECT (wid), "select_row",
				  G_CALLBACK (data_types_clist_select_cb), conf);
		g_signal_connect (G_OBJECT (wid), "unselect_row",
				  G_CALLBACK (data_types_clist_unselect_cb), conf);
		g_signal_connect (G_OBJECT (conf->srv), "data_types_updated",
				  G_CALLBACK (data_types_updated_cb), conf);
		g_signal_connect (G_OBJECT (conf->srv), "objects_bindings_updated",
				  G_CALLBACK (refresh_plugins_table_cb), conf);
		
		/* on the right side is the action area */
		box = gtk_vbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);
		
		table = gtk_table_new (3, 2, FALSE);
		gtk_table_set_col_spacing (GTK_TABLE (table), 0, GNOME_PAD);
		gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 0);
		wid = gtk_label_new (_("Data Type:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
		wid = gtk_label_new (_("<Select one>"));
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "seltname", wid);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
		wid = gtk_label_new (_("Description:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
		wid = gtk_label_new (_("<NONE>"));
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "seltdescr", wid);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
		wid = gtk_label_new (_("Display Plugin:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 2, 3);
		wid = choice_combo_new ();
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "selcc", wid);
		choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 2, 3);
		
		/* below is a frame and label to put detailled description of the plugin */
		frame = gtk_frame_new (_("Plugin description"));
		gtk_box_pack_start (GTK_BOX (box), frame, TRUE, TRUE, GNOME_PAD);
		wid = gtk_vbox_new (FALSE, GNOME_PAD);
		gtk_container_add (GTK_CONTAINER (frame), wid);
		
		tmpwid = gtk_label_new ("");
		gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "Bdescr", tmpwid);
		
		tmpwid = gtk_label_new ("");
		gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "Ddescr", tmpwid);
		
		gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
		
		/* 
		 * Fine tuned plugins usage: 3rd page
		 */
		label = gtk_label_new (_("Individual objects bindings"));
		page = gtk_hbox_new (FALSE, GNOME_PAD);
		g_object_set_data (G_OBJECT (props), "objbinding", page);
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);		
		
		/* on the left side is a menu and a clist in a sw */
		box = gtk_vbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);
		
		/* menu */
		tmpwid = gtk_option_menu_new ();
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg),
				   "tablesomenu", tmpwid);
		gtk_box_pack_start (GTK_BOX (box), tmpwid, FALSE, TRUE, 0);
		menu = gtk_menu_new ();
		list = conf->db->tables;
		while (list) {
			wid = gtk_menu_item_new_with_label (DB_TABLE (list->data)->name);
			g_object_set_data (G_OBJECT (wid), "titem", list->data);
			g_signal_connect (G_OBJECT (wid), "activate",
					  G_CALLBACK (build_tables_contents_menu_cb), conf);
			gtk_menu_append (GTK_MENU (menu), wid);
			list = g_slist_next (list);
		}
		gtk_option_menu_set_menu (GTK_OPTION_MENU (tmpwid), menu);
		/* signals for coherent menu contents */
		g_signal_connect (G_OBJECT (conf->db), "table_created_filled", 
				  G_CALLBACK (plug_menu_table_created_dropped_cb), conf);
		
		g_signal_connect (G_OBJECT (conf->db), "table_dropped", 
				  G_CALLBACK (plug_menu_table_created_dropped_cb), conf);
		
		/* clist in a sw */
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);
		wid = gtk_clist_new (1);
		gtk_clist_column_titles_hide (GTK_CLIST (wid));
		gtk_clist_set_selection_mode (GTK_CLIST (wid), GTK_SELECTION_SINGLE);
		gtk_container_add (GTK_CONTAINER (sw), wid);
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "tablesclist", wid);
		g_signal_connect (G_OBJECT (wid), "select_row",
				  G_CALLBACK (table_field_clist_select_cb), conf);
		g_signal_connect (G_OBJECT (wid), "unselect_row",
				  G_CALLBACK (table_field_clist_unselect_cb), conf);
		g_signal_connect (G_OBJECT (conf->db), "field_created", 
				  G_CALLBACK (refresh_table_fields_menu_cb), conf);
		g_signal_connect (G_OBJECT (conf->db), "field_dropped", 
				  G_CALLBACK (refresh_table_fields_menu_cb), conf);
		
		/* on the right side is the action area */
		box = gtk_vbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);
		
		table = gtk_table_new (3, 2, FALSE);
		gtk_table_set_col_spacing (GTK_TABLE (table), 0, GNOME_PAD);
		gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 0);
		wid = gtk_label_new (_("Object:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
		wid = gtk_label_new (_("<Select one>"));
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "seltab", wid);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
		wid = gtk_label_new (_("Data type:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
		wid = gtk_label_new ("-");
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "tabtype", wid);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
		wid = gtk_label_new (_("Display Plugin:"));
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 2, 3);
		wid = choice_combo_new ();
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "seltabplug", wid);
		choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 2, 3);
		
		/* below is a frame and label to put detailled description of the plugin */
		frame = gtk_frame_new (_("Plugin description"));
		gtk_box_pack_start (GTK_BOX (box), frame, TRUE, TRUE, GNOME_PAD);
		wid = gtk_vbox_new (FALSE, GNOME_PAD);
		gtk_container_add (GTK_CONTAINER (frame), wid);
		
		tmpwid = gtk_label_new ("");
		gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "Bdescr2", tmpwid);
		
		tmpwid = gtk_label_new ("");
		gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "Ddescr2", tmpwid);
		
		/* building the tables contents */
		if (gtk_menu_get_active (GTK_MENU (menu)))
			build_tables_contents_menu_cb (G_OBJECT (gtk_menu_get_active (GTK_MENU (menu))),
						       conf);
		gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	}

	gtk_widget_show_all (nb);
	gtk_widget_set_usize (props, 600, 300);

	g_signal_connect (G_OBJECT (props), "response",
			  G_CALLBACK (config_plugins_dlg_response_cb), conf);
	g_signal_connect (G_OBJECT (props), "destroy",
			  G_CALLBACK (config_plugins_dlg_destroy_cb), conf);

	gtk_widget_show (props);
}

static void
config_plugins_dlg_destroy_cb (GtkWidget * wid, ConfManager * conf)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (conf->srv),
					      G_CALLBACK (data_types_updated_cb), conf);
	g_signal_handlers_disconnect_by_func (G_OBJECT (conf->srv),
					      G_CALLBACK (refresh_plugins_table_cb), conf);

	if (server_access_is_open (conf->srv)) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (conf->db), 
						      G_CALLBACK (plug_menu_table_created_dropped_cb), conf);
		
		g_signal_handlers_disconnect_by_func (G_OBJECT (conf->db),
						      G_CALLBACK (refresh_table_fields_menu_cb), conf);
	}

	conf->config_plugins_dlg = NULL;
}

static void 
config_plugins_dlg_response_cb (GtkDialog *dialog, gint arg1, ConfManager *conf)
{
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
change_location_cb (GObject * obj, ConfManager * conf)
{
	GtkWidget *fw;

	fw = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"fw");
	if (!fw) {
		fw = gtk_file_selection_new ("Select plugins directory");
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg),
				   "fw", fw);
		gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION
							(fw));
		g_signal_connect (G_OBJECT
				  (GTK_FILE_SELECTION (fw)->cancel_button),
				  "clicked",
				  G_CALLBACK
				  (change_location_cancel_cb), conf);
		g_signal_connect (G_OBJECT
				  (GTK_FILE_SELECTION (fw)->ok_button),
				  "clicked",
				  G_CALLBACK (change_location_ok_cb),
				  conf);
		gtk_widget_show (fw);
	}
	else
		gdk_window_raise (fw->window);
}

static void
change_location_cancel_cb (GObject * obj, ConfManager * conf)
{
	GtkWidget *fw;
	fw = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"fw");
	g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "fw",
			   NULL);
	gtk_widget_destroy (fw);
}

static void
change_location_ok_cb (GObject * obj, ConfManager * conf)
{
	gchar *str, *ptr;
	GtkWidget *fw;
	GtkWidget *wid;

	fw = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"fw");
	str = g_strdup (gtk_file_selection_get_filename
			(GTK_FILE_SELECTION (fw)));
	/* removing trailing caracters in case we have a file instead of a dir */
	ptr = str + strlen (str) - 1;
	while (*ptr != '/') {
		*ptr = '\0';
		ptr = str + strlen (str) - 1;
	}
	*ptr = '\0';
	if (conf->plugins_dir)
		g_free (conf->plugins_dir);
	conf->plugins_dir = g_strdup (str);
	g_free (str);
	/* refreshing the display */
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "loclabel");
	gtk_label_set_text (GTK_LABEL (wid), conf->plugins_dir);
	rescan_display_plugins_cb (NULL, conf);

	g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "fw",
			   NULL);
	gtk_widget_destroy (fw);
}


static void
data_types_clist_select_cb (GtkWidget * widget, gint row, gint col,
			    GdkEventButton * event, ConfManager * conf)
{
	GtkWidget *wid, *clist;
	ServerDataType *dt;
	GSList *blist, *list;
	DataDisplayFns *fns;
	gint i;

	clist = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "tclist");
	dt = gtk_clist_get_row_data (GTK_CLIST (clist), row);
	if (dt) {
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "dt", dt);
		wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "seltname");
		gtk_label_set_text (GTK_LABEL (wid), dt->sqlname);
		wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "seltdescr");
		gtk_label_set_text (GTK_LABEL (wid), dt->descr);
		/* computing the list for choice combo from the gda types accepted
		   by each plugin */
		wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "selcc");
		blist = g_slist_append (NULL, NULL);
		list = conf->srv->data_types_display;
		while (list) {
			fns = (DataDisplayFns *) (list->data);
			if (fns->plugin_name) {
				gboolean found;
				found = FALSE;
				i = 0;
				if (fns->nb_gda_type == 0)	/* any Gda Type is OK */
					found = TRUE;

				while ((i < fns->nb_gda_type) && !found) {
					if (fns->valid_gda_types[i] == dt->gda_type)
						found = TRUE;
					i++;
				}
				if (found) 
					blist = g_slist_append (blist, fns);
			}
			list = g_slist_next (list);
		}
		choice_combo_set_content (CHOICE_COMBO (wid), blist,
					  GTK_STRUCT_OFFSET (DataDisplayFns, plugin_name));
		/* if a plugin is already bound, set it as default */
		fns = g_hash_table_lookup (conf->srv->types_objects_hash, dt);
		if (fns) {
			i = g_slist_index (blist, fns);
			if (i >= 0) {
				GtkWidget *w1, *w2;
				choice_combo_set_selection_num (CHOICE_COMBO
								(wid), i);
				/* updating the plugin description */
				w1 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "Bdescr");
				w2 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "Ddescr");
				plugin_list_update_description (w1, w2, fns);
			}
		}
		g_slist_free (blist);
		g_signal_connect (G_OBJECT (wid), "selection_changed",
				  G_CALLBACK (plugins_ch_combo_sel_changed_cb), conf);
	}
	else
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "dt", NULL);
}

static void
plugins_ch_combo_sel_changed_cb (GObject * obj, gpointer * newsel,
				 ConfManager * conf)
{
	gpointer ptr;
	ServerDataType *dt;
	GtkWidget *w1, *w2;

	dt = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"dt");
	ptr = g_hash_table_lookup (conf->srv->types_objects_hash, dt);
	if (ptr)
		server_access_unbind_object_display (conf->srv, G_OBJECT (dt));

	if (newsel) {
		server_access_bind_object_display (conf->srv, G_OBJECT (dt),
						   (DataDisplayFns *) newsel);
		/* tell that we have modified the working environment */
		conf->save_up_to_date = FALSE;
	}
	refresh_plugins_table_cb (NULL, conf);

	w1 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "Bdescr");
	w2 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg), "Ddescr");
	plugin_list_update_description (w1, w2, (DataDisplayFns *) newsel);
}

static void
data_types_clist_unselect_cb (GtkWidget * widget, gint row, gint col,
			      GdkEventButton * event,
			      ConfManager * conf)
{
	GtkWidget *wid;

	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "seltname");
	gtk_label_set_text (GTK_LABEL (wid), _("<Select one>"));
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "seltdescr");
	gtk_label_set_text (GTK_LABEL (wid), _("<NONE>"));

	/* removing any comment in the plugin detail frame */
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "Bdescr");
	gtk_label_set_text (GTK_LABEL (wid), "");
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "Ddescr");
	gtk_label_set_text (GTK_LABEL (wid), "");

	/* choice combo */
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "selcc");
	g_signal_handlers_disconnect_by_func (G_OBJECT (wid),
					      G_CALLBACK (plugins_ch_combo_sel_changed_cb), conf);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "dt", NULL);
}


static void
plugin_list_update_description (GtkWidget * Bdescr,
				GtkWidget * Ddescr, DataDisplayFns * fns)
{
	GtkWidget *wid;

	wid = Bdescr;
	if (fns)
		gtk_label_set_text (GTK_LABEL (wid), fns->descr);
	else
		gtk_label_set_text (GTK_LABEL (wid), "");
	wid = Ddescr;
	if (fns) {
		if (fns->detailled_descr)
			gtk_label_set_text (GTK_LABEL (wid),
					    fns->detailled_descr);
		else
			gtk_label_set_text (GTK_LABEL (wid), "");
	}
	else
		gtk_label_set_text (GTK_LABEL (wid), "");
}


/* having an updated version of the list of tables/vues in the menu */
static void
plug_menu_table_created_dropped_cb (GObject * obj,
				    DbTable * new_table,
				    ConfManager * conf)
{
	GtkWidget *wid, *mitem, *menu, *optionmenu;
	gpointer titem;
	GSList *list;

	optionmenu =
		g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				   "tablesomenu");
	/* what is the selection now? */
	mitem = gtk_menu_get_active
		(GTK_MENU (gtk_option_menu_get_menu
			   (GTK_OPTION_MENU (optionmenu))));
	if (mitem)
		titem = g_object_get_data (G_OBJECT (mitem), "titem");
	else
		titem = NULL;

	/* clear the displayed menu */
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (optionmenu));

	/* new menu now */
	menu = gtk_menu_new ();
	mitem = NULL;
	list = conf->db->tables;
	while (list) {
		wid = gtk_menu_item_new_with_label (DB_TABLE
						    (list->data)->name);
		g_object_set_data (G_OBJECT (wid), "titem", list->data);
		g_signal_connect (G_OBJECT (wid), "activate",
				  G_CALLBACK
				  (build_tables_contents_menu_cb), conf);
		gtk_menu_append (GTK_MENU (menu), wid);
		gtk_widget_show (wid);
		if (titem == list->data)
			mitem = wid;
		list = g_slist_next (list);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu);
	gtk_widget_show (menu);

	/* set the selection as it was */
	if (mitem)
		gtk_menu_item_activate (GTK_MENU_ITEM (mitem));
	else {
		gpointer argobj;
		argobj = gtk_menu_get_active (GTK_MENU (menu));
		if (argobj)
			build_tables_contents_menu_cb (argobj, conf);
		else
			build_tables_contents_menu_cb (NULL, conf);
	}
}

static void
table_field_clist_select_cb (GtkWidget * clist, gint row, gint col,
			     GdkEventButton * event, ConfManager * conf)
{
	GtkWidget *wid;
	ServerDataType *dt;
	GSList *blist, *list;
	DataDisplayFns *fns;
	gint i;
	DbField *field;
	DbTable *table;
	gchar *str;

	/* set the new selection */
	g_object_set_data (G_OBJECT (clist), "selrow",
			   GINT_TO_POINTER (row + 1));

	field = gtk_clist_get_row_data (GTK_CLIST (clist), row);
	table = database_find_table_from_field (conf->db, field);
	dt = field->type;
	if (dt) {
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg),
				   "obj", field);
		wid = g_object_get_data (G_OBJECT
					 (conf->config_plugins_dlg),
					 "seltab");
		str = g_strdup_printf ("%s.%s", table->name, field->name);
		gtk_label_set_text (GTK_LABEL (wid), str);
		g_free (str);
		wid = g_object_get_data (G_OBJECT
					 (conf->config_plugins_dlg),
					 "tabtype");
		gtk_label_set_text (GTK_LABEL (wid), dt->sqlname);
		/* computing the list for choice combo from tge gda types accepted
		   by each plugin */
		wid = g_object_get_data (G_OBJECT
					 (conf->config_plugins_dlg),
					 "seltabplug");
		blist = g_slist_append (NULL, NULL);
		list = conf->srv->data_types_display;
		while (list) {
			fns = (DataDisplayFns *) (list->data);
			if (fns->plugin_name) {
				gboolean found;
				found = FALSE;
				i = 0;
				if (fns->nb_gda_type == 0)	/* any Gda Type is OK */
					found = TRUE;
				while ((i < fns->nb_gda_type) && !found) {
					if (fns->valid_gda_types[i] ==
					    dt->gda_type)
						found = TRUE;
					i++;
				}
				if (found)
					blist = g_slist_append (blist, fns);
			}
			list = g_slist_next (list);
		}
		choice_combo_set_content (CHOICE_COMBO (wid), blist,
					  GTK_STRUCT_OFFSET
					  (DataDisplayFns, plugin_name));
		/* if a plugin is already bound, set it as default */
		fns = g_hash_table_lookup (conf->srv->types_objects_hash,
					   field);
		if (fns) {
			i = g_slist_index (blist, fns);
			if (i >= 0) {
				GtkWidget *w1, *w2;
				choice_combo_set_selection_num (CHOICE_COMBO
								(wid), i);
				/* updating the plugin description */
				w1 = g_object_get_data (G_OBJECT
							(conf->
							 config_plugins_dlg),
							"Bdescr2");
				w2 = g_object_get_data (G_OBJECT
							(conf->
							 config_plugins_dlg),
							"Ddescr2");
				plugin_list_update_description (w1, w2, fns);
			}
		}
		g_slist_free (blist);
		g_signal_connect (G_OBJECT (wid), "selection_changed",
				  G_CALLBACK
				  (plugins_ch_combo_sel_changed_cb2), conf);
	}
	else
		g_object_set_data (G_OBJECT (conf->config_plugins_dlg),
				   "obj", NULL);
}

/* build the contents of the tables clist contents */
static void
plugins_ch_combo_sel_changed_cb2 (GObject * object,
				  gpointer * newsel, ConfManager * conf)
{
	gpointer ptr;
	gpointer obj;
	GtkWidget *w1, *w2;

	obj = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "obj");
	ptr = g_hash_table_lookup (conf->srv->types_objects_hash, obj);
	if (ptr) {
		server_access_unbind_object_display (conf->srv, G_OBJECT (obj));
		g_print ("Unbinded object %p\n", obj);
	}

	if (newsel) {
		server_access_bind_object_display (conf->srv, G_OBJECT (obj),
						   (DataDisplayFns *) newsel);
		g_print ("Binded object %p to %s\n", obj,
			 ((DataDisplayFns *) newsel)->descr);
		/* tell that we have modified the working environment */
		conf->save_up_to_date = FALSE;
	}
	refresh_plugins_table_cb (NULL, conf);

	w1 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"Bdescr2");
	w2 = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				"Ddescr2");
	plugin_list_update_description (w1, w2, (DataDisplayFns *) newsel);
}


static void
table_field_clist_unselect_cb (GtkWidget * clist, gint row, gint col,
			       GdkEventButton * event,
			       ConfManager * conf)
{
	GtkWidget *wid;

	/* set the new selection */
	g_object_set_data (G_OBJECT (clist), "selrow",
			   GINT_TO_POINTER (0));

	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "seltab");
	gtk_label_set_text (GTK_LABEL (wid), _("<Select one>"));
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "tabtype");
	gtk_label_set_text (GTK_LABEL (wid), "-");

	/* removing any comment in the plugin detail frame */
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "Bdescr2");
	gtk_label_set_text (GTK_LABEL (wid), "");
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "Ddescr2");
	gtk_label_set_text (GTK_LABEL (wid), "");

	/* choice combo */
	wid = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				 "seltabplug");
	g_signal_handlers_disconnect_by_func (G_OBJECT (wid),
					      G_CALLBACK (plugins_ch_combo_sel_changed_cb2), conf);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	g_object_set_data (G_OBJECT (conf->config_plugins_dlg), "obj", NULL);
}

/* called to refresh the contents of the clist when a field is added or 
   removed in a table */
static void
refresh_table_fields_menu_cb (Database * db, DbTable * table,
			      DbField * field, ConfManager * conf)
{
	GtkWidget *clist;
	gint selrow, row;
	DbField *sel_field = NULL;

	clist = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				   "tablesclist");
	/* undo the selection */
	selrow = GPOINTER_TO_INT (g_object_get_data
				  (G_OBJECT (clist), "selrow"));
	if (selrow) {
		sel_field =
			gtk_clist_get_row_data (GTK_CLIST (clist),
						selrow - 1);
		gtk_clist_unselect_row (GTK_CLIST (clist), selrow - 1, 0);
	}

	/* remove or add the field */
	row = gtk_clist_find_row_from_data (GTK_CLIST (clist), field);
	if (row >= 0)		/* field to be removed */
		gtk_clist_remove (GTK_CLIST (clist), row);
	else {			/* field to be added */

		gchar *txt[1];
		txt[0] = field->name;
		gtk_clist_insert (GTK_CLIST (clist),
				  g_slist_index (table->fields, field), txt);
	}

	if (sel_field && (sel_field != field)) {
		selrow = gtk_clist_find_row_from_data (GTK_CLIST (clist),
						       sel_field);
		gtk_clist_select_row (GTK_CLIST (clist), selrow, 0);
	}
}

static void
data_types_updated_cb (GObject * obj, ConfManager * conf)
{
	gint row;
	GtkCList *clist;
	gpointer data;
	GSList *list;

	clist = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				   "tclist");

	/* add the ones not yet there */
	list = conf->srv->data_types;
	while (list) {
		row = gtk_clist_find_row_from_data (clist, list->data);
		if (row < 0) {	/* need to be added */
			row = gtk_clist_append (clist,
						&(SERVER_DATA_TYPE (list->data)->
						  sqlname));
			gtk_clist_set_row_data (clist, row, list->data);
		}
		list = g_slist_next (list);
	}

	/* remove the ones not present anymore */
	row = 0;
	while (row < clist->rows) {
		data = gtk_clist_get_row_data (clist, row);
		if (!g_slist_find (conf->srv->data_types, data))
			gtk_clist_remove (clist, row);
		else
			row++;
	}
}

static void
build_tables_contents_menu_cb (GObject * mitem, ConfManager * conf)
{
	DbTable *titem;
	GtkWidget *clist;
	gint selrow;

	clist = g_object_get_data (G_OBJECT (conf->config_plugins_dlg),
				   "tablesclist");
	/* if there was a selection, unselect it. */
	selrow = GPOINTER_TO_INT (g_object_get_data
				  (G_OBJECT (clist), "selrow"));
	if (selrow)
		gtk_clist_unselect_row (GTK_CLIST (clist), selrow - 1, 0);

	gtk_clist_freeze (GTK_CLIST (clist));
	gtk_clist_clear (GTK_CLIST (clist));
	if (mitem) {
		GSList *list;
		gchar *content[1];
		gint i;

		titem = g_object_get_data (mitem, "titem");
		/* filling the clist with the fields' names */
		list = titem->fields;
		while (list) {
			content[0] = DB_FIELD (list->data)->name;
			i = gtk_clist_append (GTK_CLIST (clist), content);
			gtk_clist_set_row_data (GTK_CLIST (clist), i,
						list->data);
			list = g_slist_next (list);
		}
		/* no row selected */
		g_object_set_data (G_OBJECT (clist), "selrow",
				   GINT_TO_POINTER (0));
	}
	gtk_clist_thaw (GTK_CLIST (clist));
}


/*
 * users_settings_cb
 */
void
users_settings_cb (GObject * obj, ConfManager * conf)
{
	/* TODO: use conf->users_list_dlg to store a dialog with the users settings */
	todo (conf, __FILE__, __LINE__);
}


/*
 * users_access_cb
 */

void
users_access_cb (GObject * obj, ConfManager * conf)
{
	/* TODO: use conf->users_acl_dlg to store a dialog with the users rights */
	todo (conf, __FILE__, __LINE__);
}


/*
 * users_groups_cb
 */

void
users_groups_cb (GObject * obj, ConfManager * conf)
{
	/* TODO: use conf->users_groups_dlg to store a dialog with the users groups */
	todo (conf, __FILE__, __LINE__);
}


/*
 * printer_setup_cb
 */

void
printer_setup_cb (GObject * obj, ConfManager * conf)
{
	todo (conf, __FILE__, __LINE__);
}


/*
 * CBs to set the tab of the notebook to display the right page
 */

void
show_tables_page_cb (GtkWidget * shortcut, const gchar *label, const gchar *tooltip, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 0);
}

void
show_seqs_page_cb (GtkWidget * wid, const gchar *label, const gchar *tooltip, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 1);
}

void
show_queries_page_cb (GtkWidget * shortcut, const gchar *label, const gchar *tooltip, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 2);
}

void
show_forms_page_cb (GtkWidget * shortcut, const gchar *label, const gchar *tooltip, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 3);
}

void
show_sql_page_cb (GtkWidget * shortcut, const gchar *label, const gchar *tooltip, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 4);
}


/*
 * run_gnomedb_manager_cb
 */
void
run_gnomedb_manager_cb (GtkWidget * w, ConfManager * conf)
{
	if (gnome_execute_shell (NULL, "gnome-database-properties") == -1)
		gnome_db_show_error (_("Could not execute database properties applet"));
}


/* 
 * set_opened_file
 */

gint
set_opened_file (ConfManager * conf, gchar * filetxt)
{
	gchar *txt;
	gint retval = 0;
	xmlDocPtr doc;
	xmlNodePtr tree;

	if (!g_file_test (filetxt, G_FILE_TEST_EXISTS)) {
		txt = g_strdup_printf (_("Cannot open file '%s'\n(file does not exist "
					 "or is not readable)."), filetxt);
		gnome_error_dialog (txt);
		g_free (txt);
		return -1;
	}
	else {
		doc = xmlParseFile (filetxt);
		if (doc) {
			txt = xmlGetProp (xmlDocGetRootElement(doc), "id_serial");
			if (txt) {
				conf->id_serial = atoi (txt);
			}
			tree = xmlDocGetRootElement(doc)->xmlChildrenNode;
			while (tree) {
				txt = egnima_xml_clean_string_ends
					(xmlNodeGetContent (tree));
				if (!strcmp (tree->name, "gda_datasource")) {
					if (txt)
						g_string_assign (conf->srv->gda_datasource, txt);
					else
						g_string_assign (conf->srv->gda_datasource, "");
				}

				if (!strcmp (tree->name, "username")) {
					if (txt)
						g_string_assign (conf->srv->user_name, txt);
					else
						g_string_assign (conf->srv->user_name, "");
					if (conf->user_name)
						g_free (conf->user_name);
					conf->user_name = g_strdup (conf->srv->user_name->str);
				}

				tree = tree->next;
				if (txt)
					g_free (txt);
			}
			xmlFreeDoc (doc);
			g_string_assign (conf->srv->password, "");

			if (conf->working_file)
				g_free (conf->working_file);
			conf->working_file = filetxt;
			txt = conf_manager_get_title (conf);
			gtk_window_set_title (GTK_WINDOW (conf->app), txt);
			g_free (txt);
		}
		else {
			gnome_warning_dialog (_("An error has occured while loading the "
						"connexion parameters.\n"
						"Set them manually."));
			retval = -1;	/* ERROR backpropagation */
		}
	}

	return retval;
}


/*
 * file_new_cb
 */

static void file_new_dlg_cancel_cb (GtkWidget * widget, GtkWidget *data);
static void file_new_dlg_ok_cb (GtkWidget * widget, GtkWidget *data);

void
file_new_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	dlg = gtk_file_selection_new (_("New file"));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	g_object_set_data (G_OBJECT (dlg), "conf", conf);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button), 
			  "clicked", G_CALLBACK (file_new_dlg_cancel_cb), dlg);	/* ! */
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			  "clicked",
			  G_CALLBACK (file_new_dlg_ok_cb), dlg);
	gtk_widget_show (dlg);
}

static void
file_new_dlg_cancel_cb (GtkWidget * widget, GtkWidget *data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static void
file_new_dlg_ok_cb (GtkWidget * widget, GtkWidget *data)
{
	ConfManager *conf;
	gchar *filetxt;

	conf = (ConfManager *) g_object_get_data (G_OBJECT (data), "conf");
	filetxt = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (data)));
	gtk_widget_destroy (GTK_WIDGET (data));

	if (conf->working_file && !conf->save_up_to_date) {
		GtkWidget *wid;
		guint answer;
		
		wid = gtk_message_dialog_new (GTK_WINDOW (conf->app),
					      GTK_DIALOG_MODAL,
					      GTK_MESSAGE_QUESTION,
					      GTK_BUTTONS_YES_NO,
					      _("Some work has not been "
						"saved.\nDo you want to save it now?"));
		answer = gtk_dialog_run (GTK_DIALOG (wid));
		if (answer == GTK_RESPONSE_YES)
			file_save_cb (NULL, conf);
		gtk_widget_destroy (wid);
	}

	if (conf->working_file)
		g_free (conf->working_file);
	conf->working_file = filetxt;
	filetxt = conf_manager_get_title (conf);
	gtk_window_set_title (GTK_WINDOW (conf->app), filetxt);
	g_free (filetxt);
}


/*
 * file_open_cb
 */

static void file_open_dlg_ok_cb (GtkWidget * widget, GtkWidget *data);
static void file_open_dlg_cancel_cb (GtkWidget * widget, GtkWidget *data);

void
file_open_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	/* first test if the connection is established, ... */
	if (server_access_is_open (conf->srv)) {
		gnome_warning_dialog (_
				      ("The connection to an SQL server is already "
				       "established.\nDisconnect before trying to open "
				       "another file."));
		return;
	}

	/* let's now open a dialog... */
	dlg = gtk_file_selection_new (_("File to open"));
	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (dlg));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	g_object_set_data (G_OBJECT (dlg), "conf", conf);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button),
			  "clicked", G_CALLBACK (file_open_dlg_cancel_cb), dlg);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			  "clicked", G_CALLBACK (file_open_dlg_ok_cb), dlg);
	gtk_widget_show (dlg);
}

static void
file_open_dlg_ok_cb (GtkWidget * widget, GtkWidget *data)
{
	ConfManager *conf;
	gchar *filetxt;

	conf = (ConfManager *) g_object_get_data (G_OBJECT (data),
						  "conf");
	filetxt = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (data)));
	gtk_widget_destroy (GTK_WIDGET (data));

	if (set_opened_file (conf, filetxt) == -1)
		g_free (filetxt);
}

static void
file_open_dlg_cancel_cb (GtkWidget * widget, GtkWidget *data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}


/*
 * file_close_cb
 */

void
file_close_cb (GtkWidget * widget, ConfManager * conf)
{
	gchar *str;

	if (conf->working_file) {
		g_free (conf->working_file);
		conf->working_file = NULL;
		str = conf_manager_get_title (conf);
		gtk_window_set_title (GTK_WINDOW (conf->app), str);
		g_free (str);
	}
}


/*
 * file_save_cb
 */
static void quit_real (ConfManager * conf);
void
file_save_cb (GtkWidget * widget, ConfManager * conf)
{
	if (conf->working_file) {
		xmlDocPtr doc;
		xmlNodePtr root, tree;
		gchar *str;

		doc = xmlNewDoc ("1.0");
		/* DTD insertion */
		xmlCreateIntSubset(doc, "CONNECTION", NULL, XML_EGNIMA_DTD_FILE);

		root = xmlNewDocNode (doc, NULL, "CONNECTION", NULL);
		xmlDocSetRootElement (doc, root);


		/* Building the tree */
		str = g_strdup_printf ("%d", conf->id_serial);
		xmlSetProp (root, "id_serial", str);
		g_free (str);
		tree = xmlNewChild (root, NULL, "gda_datasource", conf->srv->gda_datasource->str);
		tree = xmlNewChild (root, NULL, "username", conf->srv->user_name->str);

		/* the ServerAccess object */
		server_access_build_xml_tree (conf->srv, doc);

		/* the Database object */
		database_build_xml_tree (conf->db, doc);

		/* all the Query objects, from the top level one */
		g_assert (conf->top_query);
		query_build_xml_tree (QUERY (conf->top_query), root, conf);
		
		/* ALL the GUI goes here */
		tree = xmlNewChild (root, NULL, "GUI", NULL);
		relship_build_xml_tree (NULL, tree, conf);


		xmlSaveFormatFile (conf->working_file, doc, TRUE);
		xmlFreeDoc (doc);
		conf->save_up_to_date = TRUE;

		/* check if we were waiting after saving to close the connection */
		if (conf->close_after_saving) {
			conf->close_after_saving = FALSE;
			server_access_close_connect (conf->srv);
		}
		/* check if we were waiting after saving to quit the application */
		if (conf->quit_after_saving) {
			conf->quit_after_saving = FALSE;
			quit_real (conf);
		}
	}
	else if (!conf->save_up_to_date)
		file_save_as_cb (NULL, conf);
}

/*
 * file_save_as_cb
 */

static void file_saveas_dlg_ok_cb (GtkWidget * widget, GtkWidget *data);

void
file_save_as_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	/* let's now open a dialog... */
	dlg = gtk_file_selection_new (_("File to save to"));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	g_object_set_data (G_OBJECT (dlg), "conf", conf);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button), "clicked", 
			  G_CALLBACK (file_open_dlg_cancel_cb), dlg);	/* ! */
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button), "clicked",
			  G_CALLBACK (file_saveas_dlg_ok_cb), dlg);
	gtk_widget_show (dlg);
}


static void
file_saveas_dlg_ok_cb (GtkWidget * widget, GtkWidget *data)
{
	ConfManager *conf;
	gchar *txt, *filetxt;

	conf = (ConfManager *) g_object_get_data (G_OBJECT (data),
						  "conf");
	filetxt =
		g_strdup (gtk_file_selection_get_filename
			  (GTK_FILE_SELECTION (data)));
	gtk_widget_destroy (GTK_WIDGET (data));

	if (g_file_test (filetxt, G_FILE_TEST_IS_DIR)) {
		txt = g_strdup_printf (_("File '%s' is a directory!\n"),
				       filetxt);
		gnome_error_dialog (txt);
		g_free (txt);
		return;
	}
	else {
		if (conf->working_file)
			g_free (conf->working_file);
		conf->working_file = filetxt;
		txt = conf_manager_get_title (conf);
		gtk_window_set_title (GTK_WINDOW (conf->app), txt);
		g_free (txt);
		file_save_cb (NULL, conf);
	}
	
	/* we set the following flag back to FALSE */
	conf->close_after_saving = FALSE;
}

void
quit_cb (GtkWidget * widget, ConfManager * conf)
{
	gint answer;
	GtkWidget *dialog, *label, *hb, *pixmap;

	/* ask if the user really wants to quit */
	if (server_access_is_open (conf->srv) && !conf->save_up_to_date) {
		dialog = gtk_dialog_new_with_buttons (_("Quit confirmation"), GTK_WINDOW (conf->app), 0,
						      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						      _("Quit without saving"), 2,
						      _("Save and Quit"), 1, NULL);

		label = gtk_label_new (_("Some data have not yet been saved and should be saved before\n"
					 "quitting (or will be lost).\n\n"
					 "What do you want to do?"));

	}
	else {
		dialog = gtk_dialog_new_with_buttons (_("Quit confirmation"), GTK_WINDOW (conf->app), 0,
						      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						      _("Yes, quit"), 2, NULL);

		label = gtk_label_new (_("Do you really want to quit?"));
	}

	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hb, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (hb);

	pixmap = gtk_image_new_from_stock (GTK_STOCK_QUIT, GTK_ICON_SIZE_BUTTON);
	gtk_box_pack_start (GTK_BOX (hb), pixmap, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (pixmap);

	gtk_box_pack_start (GTK_BOX (hb), label, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (label);

	answer = gtk_dialog_run (GTK_DIALOG (dialog));

	switch (answer) {
	case 1: /* Save and quit */
		file_save_cb (NULL, conf);
		if (!conf->working_file)
			conf->quit_after_saving = TRUE;
	case 2: /* just quit */
		conf->save_up_to_date = TRUE;
		if (!conf->quit_after_saving)
			quit_real (conf);
		break;
	default:
		break;
	}
	gtk_widget_destroy (dialog);
}

static void 
quit_real (ConfManager * conf)
{
	/* saving state of the configuration */
	gnome_db_config_set_string ("/apps/Egnima/SqlServer/datasource",
				    conf->srv->gda_datasource->str);
	gnome_db_config_set_string ("/apps/Egnima/SqlServer/plugins_dir", conf->plugins_dir);

	if (!conf->save_up_to_date) /* ALL data will be lost */
		conf->save_up_to_date = TRUE;
	
	/* clears everything */
	g_object_unref (G_OBJECT (conf));

	/* and then quit */
	gtk_main_quit ();
}


/********************************************************************
 *
 * CBs to signals emitted by Database objects
 *
 ********************************************************************/


/*
 * sql_server_conn_open_cb
 */

static void ask_to_refresh_after_connect_cb (gint reply,
					     ConfManager * conf);
static void xml_parser_error_func (void *ctx, const char *msg, ...);
static void
xml_parser_error_func (void *ctx, const char *msg, ...)
{
	static gboolean has_error = FALSE;
	xmlValidCtxtPtr ctxt = (xmlValidCtxtPtr) ctx;
	ConfManager *conf;
	va_list args;
	gchar *str;

	if (!has_error) {
		GtkWidget *dlg;
		conf = (ConfManager *) (ctxt->userData);
		va_start (args, msg);
		str = g_strdup_vprintf (msg, args);
		va_end (args);

		dlg = gtk_message_dialog_new (GTK_WINDOW (conf->app),
					      GTK_DIALOG_MODAL,
					      GTK_MESSAGE_ERROR,
					      GTK_BUTTONS_CLOSE,
					      _("The XML file has the following error:\n%s"),
					      str);
		g_free (str);

		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
		has_error = TRUE;
	}
}

/* from the server to tell the connection is UP */
void
sql_server_conn_open_cb (GObject * wid, ConfManager * conf)
{
	xmlDocPtr doc;
	xmlNodePtr tree;
	gboolean ok, csud, res;
	gchar *txt;

	conf->loading_in_process = TRUE;
	gnome_app_flash (GNOME_APP (conf->app),
			 _("Connection to the SQL server opened"));
	csud = conf->save_up_to_date;

	/* try to load the structure from an XML file if it exists */
	if (conf->working_file && g_file_test (conf->working_file, G_FILE_TEST_EXISTS)) {
		/* 
		 * load the XML file 
		 */

		ok = TRUE;
		xmlDoValidityCheckingDefaultValue = 0;
		doc = xmlParseFile (conf->working_file);
		if (doc) {
			xmlValidCtxtPtr validc;
			gint i;
			validc = g_new0 (xmlValidCtxt, 1);
			validc->userData = conf;
			validc->error = xml_parser_error_func;
			validc->warning = xml_parser_error_func;
			xmlDoValidityCheckingDefaultValue = 1;
			i = xmlValidateDocument (validc, doc);
			if (!i) {
				xmlFreeDoc (doc);
				doc = NULL;
			}
		}
		
		if (doc) {
			tree = xmlDocGetRootElement(doc)->xmlChildrenNode;
			while (tree) {
				txt = egnima_xml_clean_string (xmlNodeGetContent (tree));
				/*
				 * ServerAccess object
				 */
				if (!strcmp (tree->name, "SERVER")) {
					res = server_access_build_from_xml_tree (conf->srv, tree);
					ok = res ? ok : FALSE;
				}

				/*
				 * Database object
				 */
				if (!strcmp (tree->name, "DATABASE")) {
					res = database_build_db_from_xml_tree (conf->db, tree);
					ok = res ? ok : FALSE;
				}


				/*
				 * Query Objects
				 */
				if (!strcmp (tree->name, "Query")) {
					/* conf->top_query already exists because the
					   connection is opened */
					Query *q;
					q = QUERY (query_build_from_xml_tree (conf, tree, NULL));
					ok = q ? ok : FALSE;
				}

				/*
				 * QueryEnv Objects
				 */
				if (!strcmp (tree->name, "FORMS")) {
					xmlNodePtr node;

					node = tree->xmlChildrenNode;
					while (node) {
						if (!strcmp (node->name, "QueryEnv"))
							query_env_build_from_xml_tree (conf, node);
						node = node->next;
					}
				}

				/*
				 *  GUI parts
				 */
				if (!strcmp (tree->name, "GUI")) {
					xmlNodePtr node;

					node = tree->xmlChildrenNode;
					while (node) {
						if (!strcmp (node->name, "RelShip"))
							relship_build_from_xml_tree (conf, node);
						node = node->next;
					}
				}


				tree = tree->next;
				if (txt)
					g_free (txt);
			}
			xmlFreeDoc (doc);
			conf->save_up_to_date = csud;
		}
		else
			ok = FALSE;

		if (!ok)
			gnome_warning_dialog (_
					      ("An error has occured while loading the "
					       "selected document. The file (XML format)\n"
					       "is probably corrupted. Try to repair it."));
		else {
			/* ask to refresh the structure */
			txt = g_strdup_printf (_("DataBase '%s':\n"
						 "do you want to update the list of\n data types,"
						 " tables, functions, "
						 "sequences, ... from the server\n(recommanded "
						 "if the structure of the DB has been "
						 "modified).\n"
						 "Say Yes if unsure."),
					       conf->srv->gda_datasource->
					       str);
			gnome_question_dialog (txt,
					       (GnomeReplyCallback)
					       ask_to_refresh_after_connect_cb,
					       conf);
			g_free (txt);
		}
	}
	else {
		server_access_refresh_datas (conf->srv);
		database_refresh (conf->db, conf->srv);
	}

	/* sets the notebook to be displayed */
	if (conf->srv->features.sequences)
		gtk_widget_show (conf->sequences_page);
	else
		gtk_widget_hide (conf->sequences_page);

	gtk_widget_hide (conf->welcome);
	gtk_widget_show (conf->working_box);

	conf->loading_in_process = FALSE;
}

static void
ask_to_refresh_after_connect_cb (gint reply, ConfManager * conf)
{
	if (reply == 0)
		sql_mem_update_cb (NULL, conf);
}


/*
 * sql_server_conn_to_close_cb
 */

/* from the server to tell the connection is going to go DOWN */
void
sql_server_conn_to_close_cb (GObject * wid, ConfManager * conf)
{
	if (conf->check_dlg) {
		conf->check_perform = FALSE;
		gtk_widget_destroy (GTK_WIDGET (conf->check_dlg));
		conf->check_dlg = NULL;
		conf->check_pbar = NULL;
		conf->check_link_name = NULL;
		conf->check_errors = NULL;
	}
}


/*
 * sql_server_conn_close_cb
 */

/* from the server to tell the connection is DOWN */
void
sql_server_conn_close_cb (GObject * wid, ConfManager * conf)
{
	gnome_app_flash (GNOME_APP (conf->app),
			 _("Connection to the SQL server closed"));

	/* sets the welcome message to be displayed */
	gtk_widget_hide (conf->working_box);
	gtk_widget_show (conf->welcome);

	/* Tell that the work has not been modified (like when starting Egnima) */
	conf->save_up_to_date = TRUE;
}




/*
 * sql_server_catch_errors_cb
 */
static gint err_dlg_closed (GnomeDialog * dialog, ConfManager * conf);

void sql_server_catch_errors_cb (ServerAccess *srv, GdaConnection *cnc,
				 GList * error_list, ConfManager * conf)
{
	if (!conf->error_dlg) {
		conf->error_dlg = gnome_db_error_dialog_new (_("Error Viewer"));
		gnome_db_error_dialog_show_errors (GNOME_DB_ERROR_DIALOG (conf->error_dlg),
						   error_list);
		g_signal_connect (G_OBJECT (conf->error_dlg), "close",
				  G_CALLBACK (err_dlg_closed), conf);
	}
}

static gint
err_dlg_closed (GnomeDialog * dialog, ConfManager * conf)
{
	conf->error_dlg = NULL;
	return 0;
}
