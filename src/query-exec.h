/* query-exec.h
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_EXEC__
#define __QUERY_EXEC__

#include <gnome.h>
#include "query-env.h"
#include "server-rs.h"

G_BEGIN_DECLS

#define QUERY_EXEC_TYPE          (query_exec_get_type())
#define QUERY_EXEC(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_exec_get_type(), QueryExec)
#define QUERY_EXEC_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_exec_get_type (), QueryExecClass)
#define IS_QUERY_EXEC(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_exec_get_type ())


typedef struct _QueryExec      QueryExec;
typedef struct _QueryExecClass QueryExecClass;

/* possible return values for the execution of a query */
typedef enum {
	QUERY_EXEC_OK,
	QUERY_EXEC_CANCELLED,
	QUERY_EXEC_ERROR
} QueryExecStatus;

/* struct for the object's data */
struct _QueryExec
{
	GObject          object;

	Query           *q;	/* q is a copy of env->q, so here is a ref to the q to be used */
	QueryEnv        *env;
	ServerResultset *sres;

	/* private data */
	GSList          *missing_data;
	gchar           *executed_query; /* SQL (or XML) query which is executed or going to be executed */
	GSList          *exec_uis; /* list of user interfaces opened for the data */
};

/* struct for the object's class */
struct _QueryExecClass
{
	GObjectClass       parent_class;

	void (*not_valid) (QueryExec * qe);
};

/* generic widget's functions */
guint             query_exec_get_type           (void);
GObject          *query_exec_new                (QueryEnv * env);

/* Runs the query and before ask for confirmation with the query which will be run if show_query is TRUE */
QueryExecStatus   query_exec_run                (QueryExec * exec, gboolean show_query);
GtkWidget        *query_exec_get_edit_form      (QueryExec * exec, gulong numrow);
GtkWidget        *query_exec_get_insert_form    (QueryExec * exec);
GtkWidget        *query_exec_get_consult_grid   (QueryExec * exec, gulong numrow);

/* helper function which is used equally by DataGrid and DataForm, so it is put here */
GtkWidget        *query_exec_get_action_buttons (QueryExec * qx);

G_END_DECLS

#endif
