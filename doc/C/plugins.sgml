<chapter id="plugins">
  <title>Plugins usage</title>
  <Sect1>
    <title>Why plugins?</title>
    <para>
      As gASQL can be connected to numerous DBMS, and as each DBMS has its own data types, 
      it is very difficult (if not impossible) to write proper interface elements to enable
      the user to interact with data of any type. Some DBMS also allow the users to define
      their own data types such as complex numbers, ...
    </para>
    <para>
      Moreover, data types may be used for special things which may require a special way
      of displaying (for example a field in a table may be of a 'string' type which usually
      is represented with a simple on line text entry, but in fact may represent a file name
      for a picture, in which case it would be better to display the picture rather than its
      name).
    </para>
    <para>
      gASQL solves this problem by having a plugin system for data types: it is possible
      to design plugins for any data type (even user defined ones). Plugins are simple object
      files which are loaded separately from gASQL.
    </para>
    <para>
      Plugins are loaded when gASQL starts and can be used at any time.
    </para>
  </Sect1>

  <Sect1>
    <title>Defining plugins usage</title>
    <para>
      Plugins usage is done using a single dialog window with three tabs: a summary tab, a data types
      tab, and a tab for individual objects.
    </para>
    <para>
      Plugins bindings are kept in the file and are reloaded with the other information when
      a session is resumed.
    </para>
    <Sect2>
      <title>Usage summary tab</title>
      <para>
        This tab basically lists all the plugins, and tells if a plugin is used or not. From this tab
        it is also possbile to change the location of the plugins and refresh the display (though it
        should always be automatically kept up to date).
        <mediaobject>
          <imageobject>
            <imagedata fileref="images/plugins_sum.eps" format="eps">
          </imageobject>
          <imageobject>
            <imagedata fileref="images/plugins_sum.jpg" format="jpg">
          </imageobject>
          <textobject>
            <phrase>Plugins usage summary tab</phrase>
          </textobject>
        </mediaobject>
      </para>
    </Sect2>
    <Sect2>
      <title>Data types tab</title>
      <para>
        This tab allows for the 'binding' of a plugin to a data type. In that situation, every
        time a data of this data type needs to be displayed, modified, edited this plugin will be
        used.
        <mediaobject>
          <imageobject>
            <imagedata fileref="images/plugins_dt.eps" format="eps">
          </imageobject>
          <imageobject>
            <imagedata fileref="images/plugins_dt.jpg" format="jpg">
          </imageobject>
          <textobject>
            <phrase>Plugins data types tab</phrase>
          </textobject>
        </mediaobject>
      </para>
    </Sect2>
    <Sect2>
      <title>Individual objects tab</title>
      <para>
        This tab was made to provide a better control to how plugins are used, by allowing to
        bind a plugin to a single table's field (if there is already a different pluging binded
        to the data type of the selected field, this second binding overrides the first one).
        to the data type 
        <mediaobject>
          <imageobject>
            <imagedata fileref="images/plugins_objects.eps" format="eps">
          </imageobject>
          <imageobject>
            <imagedata fileref="images/plugins_objects.jpg" format="jpg">
          </imageobject>
          <textobject>
            <phrase>Plugins individual objects tab</phrase>
          </textobject>
        </mediaobject>
      </para>
    </Sect2>
  </Sect1>
</chapter>
