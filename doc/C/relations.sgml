<chapter id="relations">
  <title>Tables relations</title>
  <Sect1>
    <title>Generalities on relations</title>
    <para>
      Relations are the way the tables are linked to each other. They can be considered
      as the result, in the database structure, of an Entity-Relationship design. 
    </para>
    <para>gASQL considers two kinds of relations (sometimes refered to as links): relations
      between two tables' fields, and relations between a sequence and a table's field. The two
      can be understood as 'an object A has an influence on an object B' where there is a
      relation from A to B.
    </para>
    <para>
      Relations between two fields of two different tables will be
      used to specify joins in a selection, and can correspond (but
      don't have to) to foreign keys. For example in the 'Sales' database as example, some examples
      or relations include:
      <itemizedlist>
        <listitem>
          <para>the relation from the salesman id to the customer's 'served_by'
            field: the customer is 'served by' the sales man it references.
	  </para>
        </listitem>
        <listitem>
          <para>the relation from the orders id and the order_contents: each entry in the
            order_contents table can only exist if there is a corresponding order which
            it references.
	  </para>
        </listitem>
      </itemizedlist>
    <para>
    <para>
      If sequences are supported by the database, relations are also
      possible from sequences to fields, which means that the field
      will get its values from the sequence (and thus guarantees a
      unique value for each field, to be used with primary keys).
    </para>
    <para>
      It is important to take the time to declare all the possible
      relations in the database as gASQL will use this information
      when creating queries to automatically take care of the joins
      between tables.
    </para>
    <para>
      Relation cardinality are not yet supported: there is no information
      about the kind of link which is represented: 1:1, 1:N, etc. This
      feature may be inserted in future releases if needed.
    </para>
    <para>
      gASQL uses one kind of window to create and delete relations, and one
      window to display a summary of all the relations for some (or
      all) of the tables and views in the database.
    </para>
  </Sect1>

  <Sect1>
    <title>Creation and deletion of relations</title>
    <para>
      For every table and view in the database, it is possible to open
      a properties dialog which contains a subpart (the lower part of
      the dialog) to manage relations between table's fields.
    </para>
    <para>
      When a table (or view) has its properties box displayed, all the
      information is centered on that table (or view): the presented
      relations are the ones which are about a field of the table in
      them. The relations edited in one properties box, if they also
      appear in another properties box, will be automatically updated,
      so all the information in all the properties are always up to date.
    </para>
    <para>
      To manage all the relations related to a field, simply select a
      field from the ones at the top of the dialog. The lower part of
      the dialog then shows the selected field and the relations
      related to that field: the ones shown on the left are the links
      from something (either a table's field or a sequence) and the
      ones on the right represent links from the selected field to the
      table's field shown.
    </para>
    <para>Below is a screen shot of the properties dialog of the 'order_contents' table. As the 
      order_id field is selected in the upper part of the dialog, the lower part shows the 
      relation in which this field is engaged (a relation from the 'orders' table and 'id' field
      to this selected field).
      <mediaobject>
        <imageobject>
          <imagedata fileref="images/properties.eps" format="eps">
        </imageobject>
        <imageobject>
          <imagedata fileref="images/properties.jpg" format="jpg">
        </imageobject>
        <textobject>
          <phrase>Tables properties' dialog showing a relation</phrase>
        </textobject>
      </mediaobject>
    </para>
  </Sect1>

  <Sect1>
    <title>Global view of the relations</title>
    <para>
      This dialog allows relations to be displayed all at once (and is
      in sync with the properties dialog mentionned in the previous
      section); the relations cannot be edited (either created or
      removed) from that dialog (this is an improvment that may be
      done in a future version).
    </para>
    <para>
      Using it is simple: the tables, views and sequences are all
      listed in the left part of the dialog, and when selected will be
      displayed (or hidden) on the right part of the dialog, along
      with the relations. Sequences appear as elliptical objects and
      tables or views as rectangular ones. Only the relations between
      two objects visible will be displayed using arrows.
    </para>
    <para>
      It is possible to move the objects to better organize the
      display. The positions will be saved when the session is closed,
      and a hidden object will reappear at the same place it was
      before being hidden.
    </para>
    <para>Here is a screen shot of a global dialog for relations:
      <mediaobject>
        <imageobject>
          <imagedata fileref="images/rels.eps" format="eps">
        </imageobject>
        <imageobject>
          <imagedata fileref="images/rels.jpg" format="jpg">
        </imageobject>
        <textobject>
          <phrase>Global dialog for relations</phrase>
        </textobject>
      </mediaobject>
    </para>
  </Sect1>
</chapter>